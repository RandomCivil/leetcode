# Given an integer array nums, return the length of the longest strictly increasing
# subsequence.

# Example 1:

# Input: nums = [10,9,2,5,3,7,101,18]
# Output: 4
# Explanation: The longest increasing subsequence is [2,3,7,101], therefore the length is 4.
# Example 2:

# Input: nums = [0,1,0,3,2,3]
# Output: 4
# Example 3:

# Input: nums = [7,7,7,7,7,7,7]
# Output: 1


# Constraints:

# 1 <= nums.length <= 2500
# -104 <= nums[i] <= 104


# Follow up: Can you come up with an algorithm that runs in O(n log(n)) time complexity?


class Solution:
    def lengthOfLIS(self, nums) -> int:
        result = 0
        size = len(nums)
        dp = [0] * size
        dp[0] = 1
        for i in range(0, size):
            mx = 1
            for j in range(i):
                if nums[i] > nums[j]:
                    mx = max(mx, dp[j] + 1)
            dp[i] = mx
            result = max(result, dp[i])
        print(dp)
        return result


if __name__ == "__main__":
    # 4
    nums = [10, 9, 2, 5, 3, 7, 101, 18]
    print(Solution().lengthOfLIS(nums))
