inst_694 = __import__("694")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "grid,expected",
        [
            ([[1, 1, 0, 0, 0], [1, 1, 0, 0, 0], [0, 0, 0, 1, 1], [0, 0, 0, 1, 1]], 1),
            ([[1, 1, 0, 1, 1], [1, 0, 0, 0, 0], [0, 0, 0, 0, 1], [1, 1, 0, 1, 1]], 3),
        ],
    )
    def test_numDistinctIslands(self, grid, expected):
        assert inst_694.Solution().numDistinctIslands(grid) == expected
