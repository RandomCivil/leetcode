# You are given two lists of closed intervals, firstList and secondList, where firstList[i] = [starti, endi] and secondList[j] = [startj, endj]. Each list of intervals is pairwise disjoint and in sorted order.

# Return the intersection of these two interval lists.

# A closed interval [a, b] (with a < b) denotes the set of real numbers x with a <= x <= b.

# The intersection of two closed intervals is a set of real numbers that are either empty or represented as a closed interval. For example, the intersection of [1, 3] and [2, 4] is [2, 3].

# Example 1:

# Input: firstList = [[0,2],[5,10],[13,23],[24,25]], secondList = [[1,5],[8,12],[15,24],[25,26]]
# Output: [[1,2],[5,5],[8,10],[15,23],[24,24],[25,25]]

# Example 2:

# Input: firstList = [[1,3],[5,9]], secondList = []
# Output: []

# Example 3:

# Input: firstList = [], secondList = [[4,8],[10,12]]
# Output: []

# Example 4:


# Input: firstList = [[1,7]], secondList = [[3,10]]
# Output: [[3,7]]
class Solution:
    def intervalIntersection(
        self, firstList: list[list[int]], secondList: list[list[int]]
    ) -> list[list[int]]:
        size1, size2 = len(firstList), len(secondList)
        if size1 == 0 or size2 == 0:
            return []

        i, j = 0, 0
        result = []
        while i < size1 and j < size2:
            fs, fe = firstList[i]
            ss, se = secondList[j]
            if fe < se:
                if fe >= ss:
                    result.append([max(fs, ss), min(fe, se)])
                i += 1
            elif fe > se:
                if se >= fs:
                    result.append([max(fs, ss), min(fe, se)])
                j += 1
            else:
                result.append([max(fs, ss), firstList[i][1]])
                i += 1
                j += 1
        return result