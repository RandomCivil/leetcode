package leetcode

import (
	"fmt"

	"gitlab.com/RandomCivil/leetcode/common"
)

//You are a professional robber planning to rob houses along a street. Each house has a certain amount of money stashed. All houses at this place are arranged in a circle. That means the first house is the neighbor of the last one. Meanwhile, adjacent houses have security system connected and it will automatically contact the police if two adjacent houses were broken into on the same night.
//Given a list of non-negative integers representing the amount of money of each house, determine the maximum amount of money you can rob tonight without alerting the police.

/*
Input: [2,3,2]
Output: 3
Explanation: You cannot rob house 1 (money = 2) and then rob house 3 (money = 2),
             because they are adjacent houses.

Input: [1,2,3,1]
Output: 4
Explanation: Rob house 1 (money = 1) and then rob house 3 (money = 3).
             Total amount you can rob = 1 + 3 = 4.
*/
//nums := []int{1, 2, 3, 1}
//nums :=[]int{2,7,9,3,1}

//func rob(nums []int) int {
func Rob(nums []int) int {
	fmt.Println(nums)
	size := len(nums)
	if size == 1 {
		return nums[0]
	}

	//consider first
	dp := make([]int, size)
	//no consider first
	dp1 := make([]int, size)
	for i := 0; i < size-1; i++ {
		dp[i] = nums[i]
	}
	for i := 1; i < size; i++ {
		dp1[i] = nums[i]
	}

	for i := 0; i < size; i++ {
		if i <= 1 {
			continue
		}
		arr := make([]int, size)
		arr1 := make([]int, size)
		for j := 0; j < i-1; j++ {
			arr = append(arr, dp[j])
			arr1 = append(arr1, dp1[j])
		}

		if i < size-1 {
			max := common.Max(arr...)
			dp[i] = max + nums[i]
		}

		max1 := common.Max(arr1...)
		dp1[i] = max1 + nums[i]
	}
	fmt.Println(dp, dp1)
	dp = append(dp, dp1...)
	return common.Max(dp...)
}
