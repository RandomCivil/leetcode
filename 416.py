# Given a non-empty array nums containing only positive integers, find if the array can be partitioned into two subsets such that the sum of elements in both subsets is equal.

# Example 1:

# Input: nums = [1,5,11,5]
# Output: true
# Explanation: The array can be partitioned as [1, 5, 5] and [11].

# Example 2:

# Input: nums = [1,2,3,5]
# Output: false
# Explanation: The array cannot be partitioned into equal sum subsets.

# Constraints:


# 1 <= nums.length <= 200
# 1 <= nums[i] <= 100
class Solution:
    def canPartition(self, nums) -> bool:
        target, mod = divmod(sum(nums), 2)
        if mod != 0:
            return False

        size = len(nums)
        dp = [[False] * (target + 1) for _ in range(size)]
        for i in range(size):
            dp[i][0] = True

        for i in range(1, size):
            for j in range(1, target + 1):
                if j - nums[i] >= 0:
                    dp[i][j] = dp[i - 1][j] or dp[i - 1][j - nums[i]]
                else:
                    dp[i][j] = dp[i - 1][j]

        print(dp)
        for i in range(size):
            if dp[i][-1]:
                return True
        return False


if __name__ == "__main__":
    # true
    nums = [1, 5, 11, 5]
    # false
    # nums = [1, 2, 5]
    print(Solution().canPartition(nums))
