# An integer has sequential digits if and only if each digit in the number is one more than the previous digit.

# Return a sorted list of all the integers in the range [low, high] inclusive that have sequential digits.

# Example 1:

# Input: low = 100, high = 300
# Output: [123,234]

# Example 2:


# Input: low = 1000, high = 13000
# Output: [1234,2345,3456,4567,5678,6789,12345]
class Solution:
    def sequentialDigits(self, low: int, high: int):
        r = []
        i, j = len(str(low)), len(str(high))
        print(i, j)
        for size in range(i, j + 1):
            for start in range(1, 10 - size + 1):
                print(start)
                digitals = [start]
                for _ in range(size - 1):
                    last = digitals[-1] + 1
                    digitals.append(last)

                v = int(''.join(map(str, digitals)))
                if v < low:
                    continue
                if v > high:
                    break

                r.append(v)

        return r


if __name__ == '__main__':
    # [123,234]
    # low, high = 100, 300
    # [1234,2345,3456,4567,5678,6789,12345]
    low, high = 100, 13000
    # [67,78,89,123]
    # low,high=58,123
    print(Solution().sequentialDigits(low, high))
