package leetcode

import (
	"fmt"

	"gitlab.com/RandomCivil/leetcode/common"
)

// You have a graph of n nodes. You are given an integer n and an array edges where edges[i] = [ai, bi] indicates that there is an edge between ai and bi in the graph.

// Return the number of connected components in the graph.

// Example 1:

// Input: n = 5, edges = [[0,1],[1,2],[3,4]]
// Output: 2
// Example 2:

// Input: n = 5, edges = [[0,1],[1,2],[2,3],[3,4]]
// Output: 1

// Constraints:

// 1 <= n <= 2000
// 1 <= edges.length <= 5000
// edges[i].length == 2
// 0 <= ai <= bi < n
// ai != bi
// There are no repeated edges.
func countComponents(n int, edges [][]int) int {
	if len(edges) == 0 {
		return n
	}
	uf := common.NewUnionFind(n, -1)
	for _, e := range edges {
		uf.Union(e[0], e[1])
	}
	result := make(map[int]struct{})
	for i := 0; i < n; i++ {
		result[uf.Find(i)] = struct{}{}
	}
	fmt.Println(result)
	return len(result)
}
