package leetcode

import (
	"fmt"

	"gitlab.com/RandomCivil/leetcode/common"
)

//Given an integer array arr, partition the array into (contiguous) subarrays of length at most k. After partitioning, each subarray has their values changed to become the maximum value of that subarray.

//Return the largest sum of the given array after partitioning. Test cases are generated so that the answer fits in a 32-bit integer.

//Example 1:

//Input: arr = [1,15,7,9,2,5,10], k = 3
//Output: 84
//Explanation: arr becomes [15,15,15,9,10,10,10]

//Example 2:

//Input: arr = [1,4,1,5,7,3,6,1,9,9,3], k = 4
//Output: 83

//Example 3:

//Input: arr = [1], k = 1
//Output: 1
func MaxSumAfterPartitioning(arr []int, k int) int {
	fmt.Println(arr, k)
	var (
		size = len(arr)
		dp   = make([]int, size+1)
	)
	for i := 1; i <= size; i++ {
		var nums []int
		for j := 1; j <= k && i-j >= 0; j++ {
			nums = append(nums, arr[i-j])
			fmt.Println(i, j, nums)
			dp[i] = common.Max(dp[i], dp[i-j]+common.Max(nums...)*(j))
		}
	}
	fmt.Println(dp)
	return dp[size]
}
