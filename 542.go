package leetcode

import (
	"fmt"
	"math"

	"gitlab.com/RandomCivil/leetcode/common"
)

//Given an m x n binary matrix mat, return the distance of the nearest 0 for each cell.

//The distance between two adjacent cells is 1.

//Example 1:

//Input: mat = [[0,0,0],[0,1,0],[0,0,0]]
//Output: [[0,0,0],[0,1,0],[0,0,0]]

//Example 2:

//Input: mat = [[0,0,0],[0,1,0],[1,1,1]]
//Output: [[0,0,0],[0,1,0],[1,2,1]]
func UpdateMatrix(mat [][]int) [][]int {
	var (
		m, n = len(mat), len(mat[0])
		dp   = make([][]int, m)
	)
	for i := 0; i < m; i++ {
		dp[i] = make([]int, n)
		for j := 0; j < n; j++ {
			dp[i][j] = math.MaxInt32
		}
	}

	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if mat[i][j] == 0 {
				dp[i][j] = 0
				continue
			}
			if j > 0 {
				dp[i][j] = common.Min(dp[i][j], dp[i][j-1]+1)
			}
			if i > 0 {
				dp[i][j] = common.Min(dp[i][j], dp[i-1][j]+1)
			}
		}
	}
	fmt.Println(dp)

	for i := m - 1; i >= 0; i-- {
		for j := n - 1; j >= 0; j-- {
			if mat[i][j] == 0 {
				dp[i][j] = 0
				continue
			}
			if j < n-1 {
				dp[i][j] = common.Min(dp[i][j], dp[i][j+1]+1)
			}
			if i < m-1 {
				dp[i][j] = common.Min(dp[i][j], dp[i+1][j]+1)
			}
		}
	}

	return dp
}
