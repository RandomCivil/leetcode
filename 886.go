package leetcode

import "fmt"

func possibleBipartition(n int, dislikes [][]int) bool {
	adj := make(map[int][]int)
	for _, d := range dislikes {
		s, e := d[0], d[1]
		if _, ok := adj[s]; !ok {
			adj[s] = make([]int, 0)
		}
		if _, ok := adj[e]; !ok {
			adj[e] = make([]int, 0)
		}
		adj[s] = append(adj[s], e)
		adj[e] = append(adj[e], s)
	}

	l := make([]int, n+1)
	var dfs func(cur, parent, val int) bool
	dfs = func(cur, parent, val int) bool {
		if l[cur] != 0 { // visited
			if l[cur] != val {
				return false
			}
			return true
		}
		l[cur] = val
		for _, next := range adj[cur] {
			if next == parent {
				continue
			}
			if !dfs(next, cur, -val) {
				return false
			}
		}
		return true
	}

	for i := 1; i <= n; i++ {
		if l[i] == 0 {
			if !dfs(i, 0, 1) {
				fmt.Println(i, dislikes, l)
				return false
			}
		}
	}
	return true
}
