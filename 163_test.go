package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestFindMissingRanges(t *testing.T) {
	tests := []struct {
		nums         []int
		lower, upper int
		expect       [][]int
	}{
		{
			nums:  []int{0, 1, 3, 50, 75},
			lower: 0,
			upper: 99,
			expect: [][]int{
				{2, 2}, {4, 49}, {51, 74}, {76, 99},
			},
		},
		{
			nums:   []int{-1},
			lower:  -1,
			upper:  -1,
			expect: [][]int{},
		},
	}
	Convey("TestFindMissingRanges", t, func() {
		for _, tt := range tests {
			r := findMissingRanges(tt.nums, tt.lower, tt.upper)
			if len(r) > 0 {
				So(findMissingRanges(tt.nums, tt.lower, tt.upper), ShouldResemble, tt.expect)
			} else {
				So(findMissingRanges(tt.nums, tt.lower, tt.upper), ShouldBeEmpty)
			}
		}
	})
}
