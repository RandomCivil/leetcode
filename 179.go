package leetcode

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
)

// Given a list of non-negative integers nums, arrange them such that they form the largest number and return it.

// Since the result may be very large, so you need to return a string instead of an integer.

// Example 1:

// Input: nums = [10,2]
// Output: "210"
// Example 2:

// Input: nums = [3,30,34,5,9]
// Output: "9534330"

// Constraints:

// 1 <= nums.length <= 100
// 0 <= nums[i] <= 109

func largestNumber(nums []int) string {
	var strList customStr
	for _, n := range nums {
		strList = append(strList, strconv.Itoa(n))
	}
	fmt.Println(strList)
	sort.Sort(strList)
	fmt.Println(strList)
	return strings.Join(strList, "")
}

type customStr []string

func (s customStr) Less(i, j int) bool {
	return strings.Compare(s[i]+s[j], s[j]+s[i]) == 1
}

func (s customStr) Len() int      { return len(s) }
func (s customStr) Swap(i, j int) { s[i], s[j] = s[j], s[i] }
