package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestMatrixScore(t *testing.T) {
	tests := []struct {
		grid   [][]int
		expect int
	}{
		{
			grid: [][]int{
				{0, 0, 1, 1},
				{1, 0, 1, 0},
				{1, 1, 0, 0},
			},
			expect: 39,
		},
	}
	Convey("TestMatrixScore", t, func() {
		for _, tt := range tests {
			So(matrixScore(tt.grid), ShouldEqual, tt.expect)
			So(matrixScore1(tt.grid), ShouldEqual, tt.expect)
		}
	})
}
