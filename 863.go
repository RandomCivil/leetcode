package leetcode

import (
	"gitlab.com/RandomCivil/leetcode/common"
)

//We are given a binary tree (with root node root), a target node, and an integer value K.
//Return a list of the values of all nodes that have a distance K from the target node.  The answer can be returned in any order.

//Input: root = [3,5,1,6,2,0,8,null,null,7,4], target = 5, K = 2
//Output: [7,4,1]

//Explanation:
//The nodes that are a distance 2 from the target node (with value 5)
//have values 7, 4, and 1.

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

func distanceK(root *common.TreeNode, target *common.TreeNode, K int) []int {
	parentMap := make(map[*common.TreeNode]*common.TreeNode)
	var result []int
	var dfs func(cur *common.TreeNode, parent *common.TreeNode)
	dfs = func(cur *common.TreeNode, parent *common.TreeNode) {
		if cur == nil {
			return
		}
		parentMap[cur] = parent
		dfs(cur.Left, cur)
		dfs(cur.Right, cur)
	}
	dfs(root, nil)

	queue := []*common.TreeNode{target}
	var nextQueue []*common.TreeNode
	depth := 0
	visited := make(map[*common.TreeNode]struct{})
	for len(queue) > 0 {
		pop := queue[0]
		queue = queue[1:]

		visited[pop] = struct{}{}
		// fmt.Printf("pop %+v depth:%d queue:%d\n", pop,depth,len(queue))
		if depth == K {
			// fmt.Printf("find %+v\n", pop)
			result = append(result, pop.Val)
			continue
		}

		if _, ok := visited[pop.Left]; !ok && pop.Left != nil {
			nextQueue = append(nextQueue, pop.Left)
		}
		if _, ok := visited[pop.Right]; !ok && pop.Right != nil {
			nextQueue = append(nextQueue, pop.Right)
		}
		if v, ok := parentMap[pop]; ok && v != nil {
			if _, ok1 := visited[v]; !ok1 {
				nextQueue = append(nextQueue, v)
			}
		}

		if len(queue) == 0 {
			depth++
			queue = nextQueue
			nextQueue = []*common.TreeNode{}
		}
	}
	return result
}
