package leetcode

import (
	"fmt"
	"strings"
)

//One way to serialize a binary tree is to use pre-order traversal. When we encounter a non-null node, we record the node's value. If it is a null node, we record using a sentinel value such as #.

//For example, the above binary tree can be serialized to the string "9,3,4,#,#,1,#,#,2,#,6,#,#", where # represents a null node.
//Given a string of comma separated values, verify whether it is a correct preorder traversal serialization of a binary tree. Find an algorithm without reconstructing the tree.

//You may assume that the input format is always valid, for example it could never contain two consecutive commas such as "1,,3".

/*
Input: "9,3,4,#,#,1,#,#,2,#,6,#,#"
Output: true

Input: "1,#"
Output: false

Input: "9,#,#,1"
Output: false
*/

//s := "9,3,4,#,#,1,#,#,2,#,6,#,#"
//s := "9,#,#,1"
//s :="1,#,#,#,#"
//false
//s :="#"
//false
//s :="9,3,4,#,#,1,#,#,#,2,#,6,#,#"
//false
/*
     _9_
    /   \
   3     2
  / \   / \
 4   1  #  6
/ \ / \   / \
# # # #   # #
*/

//func isValidSerialization(preorder string) bool {
func IsValidSerialization(preorder string) bool {
	arr := strings.Split(preorder, ",")
	if len(arr) == 1 && arr[0] == "#" {
		return true
	}
	stack := [][]string{}
	for i, s := range arr {
		if s != "#" {
			if i > 0 && len(stack) == 0 {
				return false
			}
			stack = append(stack, []string{s, "0"})
		} else {
			if len(stack) == 0 {
				return false
			}
			entry := stack[len(stack)-1]
			if entry[1] == "0" {
				entry[1] = "1"
			} else {
				fmt.Println("len", stack)
				remove := true
				for remove {
					stack = stack[:len(stack)-1]
					if len(stack) == 0 {
						break
					}
					tail := stack[len(stack)-1]
					if tail[1] == "1" {
						remove = true
					} else {
						tail[1] = "1"
						remove = false
					}
				}
				fmt.Println("len1", stack)
			}
		}
	}
	fmt.Println("hehe", stack)
	if len(stack) == 0 {
		return true
	}
	return false
}
