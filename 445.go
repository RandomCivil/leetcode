package leetcode

import "gitlab.com/RandomCivil/leetcode/common"

//You are given two non-empty linked lists representing two non-negative integers. The most significant digit comes first and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.
//You may assume the two numbers do not contain any leading zero, except the number 0 itself.

//Follow up:
//What if you cannot modify the input lists? In other words, reversing the lists is not allowed.

//Input: (7 -> 2 -> 4 -> 3) + (5 -> 6 -> 4)
//Output: 7 -> 8 -> 0 -> 7

func AddTwoNumbers(l1 *common.ListNode, l2 *common.ListNode) *common.ListNode {
	a1 := []int{}
	a2 := []int{}
	head := l1
	for head != nil {
		a1 = append(a1, head.Val)
		head = head.Next
	}

	head = l2
	for head != nil {
		a2 = append(a2, head.Val)
		head = head.Next
	}
	//fmt.Println(a1, a2)

	var sum int
	var next *common.ListNode = &common.ListNode{}
	var target *common.ListNode
	for len(a1) > 0 || len(a2) > 0 {
		sum = 0
		if len(a1) > 0 {
			sum += a1[len(a1)-1]
		}
		if len(a2) > 0 {
			sum += a2[len(a2)-1]
		}
		if next != nil {
			sum += next.Val
		}

		next.Val = sum % 10
		target = next
		d := sum / 10
		next = &common.ListNode{Val: d, Next: next}
		if d > 0 {
			target = next
		}

		if len(a1) > 0 {
			a1 = a1[:len(a1)-1]
		}
		if len(a2) > 0 {
			a2 = a2[:len(a2)-1]
		}
	}
	//fmt.Println(target, target.Next)
	return target
}
