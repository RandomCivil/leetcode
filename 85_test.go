package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestMaximalRectangle(t *testing.T) {
	tests := []struct {
		matrix [][]byte
		expect int
	}{
		{
			matrix: [][]byte{
				{'1', '0', '1', '0', '0'},
				{'1', '0', '1', '1', '1'},
				{'1', '1', '1', '1', '1'},
				{'1', '0', '0', '1', '0'},
			},
			expect: 6,
		},
		{
			matrix: [][]byte{
				{'0', '1'}, {'1', '0'},
			},
			expect: 1,
		},
		{
			matrix: [][]byte{
				{'0'},
			},
			expect: 0,
		},
		{
			matrix: [][]byte{
				{'1'},
			},
			expect: 1,
		},
	}
	Convey("TestMaximalRectangle", t, func() {
		for _, tt := range tests {
			So(maximalRectangle(tt.matrix), ShouldEqual, tt.expect)
		}
	})
}
