# Given an array of unique integers preorder, return true if it is the correct preorder traversal sequence of a binary search tree.

# Example 1:


# Input: preorder = [5,2,1,3,6]
# Output: true
# Example 2:

# Input: preorder = [5,2,6,1,3]
# Output: false


# Constraints:

# 1 <= preorder.length <= 104
# 1 <= preorder[i] <= 104
# All the elements of preorder are unique.


# Follow up: Could you do it using only constant space complexity?


class Solution:
    def verifyPreorder(self, preorder) -> bool:
        self.index = 0
        d = {}
        inorder = sorted(preorder)
        for i, n in enumerate(inorder):
            d[n] = i
        print(d)

        size = len(preorder)

        def build(start, end):
            if start > end:
                return True
            if self.index == size:
                return False
            cur = preorder[self.index]
            if start == end and inorder[start] != cur:
                return False
            pos = d[cur]
            self.index += 1
            l = build(start, pos - 1)
            r = build(pos + 1, end)
            return l and r

        return build(0, size - 1)
