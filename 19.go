package leetcode

import "gitlab.com/RandomCivil/leetcode/common"

// Given the head of a linked list, remove the nth node from the end of the list and return its head.

// Example 1:

// Input: head = [1,2,3,4,5], n = 2
// Output: [1,2,3,5]
// Example 2:

// Input: head = [1], n = 1
// Output: []
// Example 3:

// Input: head = [1,2], n = 1
// Output: [1]

// Constraints:

// The number of nodes in the list is sz.
// 1 <= sz <= 30
// 0 <= Node.val <= 100
// 1 <= n <= sz

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func removeNthFromEnd(head *common.ListNode, n int) *common.ListNode {
	result, cur, tail := head, head, head
	for n > 0 && tail != nil {
		tail = tail.Next
		n--
	}

	var prev *common.ListNode
	for tail != nil {
		prev = cur
		cur = cur.Next
		tail = tail.Next
	}
	// fmt.Println(prev,cur,tail)

	if cur == result {
		return cur.Next
	}

	if prev == nil {
		return nil
	}
	prev.Next = cur.Next
	return result
}
