# You are given an m x n grid where each cell can have one of three values:

# 0 representing an empty cell,
# 1 representing a fresh orange, or
# 2 representing a rotten orange.
# Every minute, any fresh orange that is 4-directionally adjacent to a rotten orange becomes rotten.

# Return the minimum number of minutes that must elapse until no cell has a fresh orange. If this is impossible, return -1.

# Example 1:

# Input: grid = [[2,1,1],[1,1,0],[0,1,1]]
# Output: 4

# Example 2:

# Input: grid = [[2,1,1],[0,1,1],[1,0,1]]
# Output: -1
# Explanation: The orange in the bottom left corner (row 2, column 0) is never rotten, because rotting only happens 4-directionally.

# Example 3:

# Input: grid = [[0,2]]
# Output: 0
# Explanation: Since there are already no fresh oranges at minute 0, the answer is just 0.

# Constraints:

# m == grid.length
# n == grid[i].length
# 1 <= m, n <= 10
# grid[i][j] is 0, 1, or 2.
from collections import deque


class Solution:
    def orangesRotting(self, grid) -> int:
        m, n = len(grid), len(grid[0])
        rotten = set()
        fresh = set()
        q = deque()

        for i in range(m):
            for j in range(n):
                if grid[i][j] == 1:
                    fresh.add((i, j))
                elif grid[i][j] == 2:
                    q.append((i, j, 0))

        mx = 0
        while len(q) > 0:
            px, py, step = q.popleft()
            if px < 0 or px == m or py < 0 or py == n or \
                    grid[px][py] == 0 or \
                    (px, py) in rotten:
                continue
            print(px, py, grid[px][py], step)
            if (px, py) in fresh:
                fresh.remove((px, py))
            rotten.add((px, py))
            mx = max(mx, step)
            q.extend([(px + 1, py, step + 1), (px - 1, py, step + 1),
                      (px, py + 1, step + 1), (px, py - 1, step + 1)])

        print(rotten, fresh, mx)
        return mx if len(fresh) == 0 else -1


if __name__ == '__main__':
    # 4
    grid = [[2, 1, 1], [1, 1, 0], [0, 1, 1]]

    # -1
    grid = [[2, 1, 1], [0, 1, 1], [1, 0, 1]]

    # 0
    grid = [[0, 2]]

    # 2
    grid = [[2, 1, 1], [1, 1, 1], [0, 1, 2]]
    print(Solution().orangesRotting(grid))
