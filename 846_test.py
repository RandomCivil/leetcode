inst_846_heap = __import__("846_heap")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "hand, groupSize, expected",
        [
            ([1, 2, 3, 6, 2, 3, 4, 7, 8], 3, True),
            ([1, 2, 3, 4, 5, 6], 3, True),
            ([1, 1, 2, 2, 3, 3], 2, False),
            ([2, 1], 2, True),
            ([8, 10, 12], 3, False),
        ],
    )
    def test_isNStraightHand(self, hand, groupSize, expected):
        assert inst_846_heap.Solution().isNStraightHand(hand, groupSize) == expected
