# Given a string s, check if the letters can be rearranged so that two characters that are adjacent to each other are not the same.

# If possible, output any possible result.  If not possible, return the empty string.

# Example 1:

# Input: s = "aab"
# Output: "aba"

# Example 2:

# Input: s = "aaab"
# Output: ""

# Note:

# s will consist of lowercase letters and have length in range [1, 500].
from collections import Counter
import heapq


class Solution:
    def reorganizeString(self, s: str) -> str:
        c = Counter(s)
        h = [(-v, k) for k, v in c.items()]
        heapq.heapify(h)
        s2 = ""
        n2 = 0
        result = ""
        while len(h) > 0:
            n1, s1 = heapq.heappop(h)
            result += s1
            if len(s2) > 0:
                if n2 + 1 < 0:
                    heapq.heappush(h, (n2 + 1, s2))

            if len(h) == 0:
                continue

            n2, s2 = heapq.heappop(h)
            result += s2
            if n1 + 1 < 0:
                heapq.heappush(h, (n1 + 1, s1))
        if len(result) != len(s):
            return ""
        return result


if __name__ == "__main__":
    s = "aaabbc"
    # s = 'aab'
    # s = 'aaab'
    print(Solution().reorganizeString(s))
