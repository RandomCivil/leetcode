package leetcode

import "gitlab.com/RandomCivil/leetcode/common"

// Given the root of a binary tree, return the level order traversal of its nodes' values. (i.e., from left to right, level by level).

// Example 1:

// Input: root = [3,9,20,null,null,15,7]
// Output: [[3],[9,20],[15,7]]
// Example 2:

// Input: root = [1]
// Output: [[1]]
// Example 3:

// Input: root = []
// Output: []

// Constraints:

// The number of nodes in the tree is in the range [0, 2000].
// -1000 <= Node.val <= 1000
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func levelOrder(root *common.TreeNode) [][]int {
	var result [][]int
	if root == nil {
		return result
	}
	q1 := []*common.TreeNode{root}
	var q2 []*common.TreeNode
	var level []int
	for len(q1) > 0 {
		pop := q1[0]
		level = append(level, pop.Val)
		q1 = q1[1:]
		if pop.Left != nil {
			q2 = append(q2, pop.Left)
		}
		if pop.Right != nil {
			q2 = append(q2, pop.Right)
		}
		if len(q1) == 0 {
			result = append(result, level)
			level = make([]int, 0)
			q1 = q2
			q2 = make([]*common.TreeNode, 0)
		}
	}
	return result
}
