package leetcode

import (
	"fmt"

	"gitlab.com/RandomCivil/leetcode/common"
)

//Given a linked list, return the node where the cycle begins. If there is no cycle, return null.

//There is a cycle in a linked list if there is some node in the list that can be reached again by continuously following the next pointer. Internally, pos is used to denote the index of the node that tail's next pointer is connected to. Note that pos is not passed as a parameter.

//Notice that you should not modify the linked list.

//Example 1:

//Input: head = [3,2,0,-4], pos = 1
//Output: tail connects to node index 1
//Explanation: There is a cycle in the linked list, where tail connects to the second node.

//Example 2:

//Input: head = [1,2], pos = 0
//Output: tail connects to node index 0
//Explanation: There is a cycle in the linked list, where tail connects to the first node.

//Example 3:

//Input: head = [1], pos = -1
//Output: no cycle
//Explanation: There is no cycle in the linked list.
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func DetectCycle(head *common.ListNode) *common.ListNode {
	if head == nil {
		return nil
	}
	var meet *common.ListNode
	slow, fast := head, head
	for slow.Next != nil && fast.Next != nil && fast.Next.Next != nil {
		slow = slow.Next
		fast = fast.Next.Next
		if slow == fast {
			meet = slow
			break
		}
	}
	fmt.Println(meet)
	if meet == nil {
		return nil
	}

	for head != meet {
		head = head.Next
		meet = meet.Next
	}
	return head
}
