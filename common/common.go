package common

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

type LinkList struct {
	Nums []int
}

func (l *LinkList) Build() *ListNode {
	size := len(l.Nums)
	if size == 0 {
		return nil
	}

	preRoot := new(ListNode)
	head := preRoot
	for i := 0; i < size; i++ {
		head.Next = &ListNode{Val: l.Nums[i]}
		head = head.Next
	}
	return preRoot.Next
}

func (l *LinkList) Traversal(root *ListNode) (vals []int) {
	head := root
	for head != nil {
		fmt.Println("Traversal", head.Val)
		vals = append(vals, head.Val)
		head = head.Next
	}
	return vals
}

func BinarySearch(nums []int, target int) int {
	size := len(nums)
	lo, hi := 0, size-1
	for lo <= hi {
		mid := (lo + hi) / 2
		if nums[mid] == target {
			return mid
		}
		if nums[mid] > target {
			hi = mid - 1
		} else {
			lo = mid + 1
		}
	}
	return -1
}

// 类似于bisect()
func BinarySearchUpperBound(nums []int, target int) int {
	size := len(nums)
	lo, hi := 0, size-1
	for lo <= hi {
		mid := (lo + hi) / 2
		if nums[mid] > target {
			hi = mid - 1
		} else {
			lo = mid + 1
		}
	}
	return lo
}

// 类似于bisect_left()
func BinarySearchLowerBound(nums []int, target int) int {
	size := len(nums)
	lo, hi := 0, size-1
	for lo <= hi {
		mid := (lo + hi) / 2
		if nums[mid] < target {
			lo = mid + 1
		} else {
			hi = mid - 1
		}
	}
	return lo
}

type UnionFind struct {
	l []int
	d int
}

func NewUnionFind(size, d int) *UnionFind {
	uf := &UnionFind{
		l: make([]int, size),
		d: d,
	}
	if d == 0 {
		return uf
	}
	for i := 0; i < size; i++ {
		uf.l[i] = d
	}
	return uf
}

func (u *UnionFind) Find(x int) int {
	if u.l[x] == u.d {
		return x
	}
	return u.Find(u.l[x])
}

func (u *UnionFind) Union(x, y int) {
	px, py := u.Find(x), u.Find(y)
	if px != py {
		u.l[py] = px
	}
}
