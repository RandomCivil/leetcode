package common

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestBfsInsert(t *testing.T) {
	tests := []struct {
		arr, preOrder, inOrder []int
	}{
		// {
		// 	arr:      []int{1},
		// 	preOrder: []int{1},
		// 	inOrder:  []int{1},
		// },
		// {
		// 	arr:      []int{1, 2},
		// 	preOrder: []int{1, 2},
		// 	inOrder:  []int{2, 1},
		// },
		// {
		// 	arr:      []int{1, 0, 2},
		// 	preOrder: []int{1, 2},
		// 	inOrder:  []int{1, 2},
		// },
		{
			arr:      []int{1, 2, 3, 6, 0, 4, 5},
			preOrder: []int{1, 2, 6, 3, 4, 5},
			inOrder:  []int{6, 2, 1, 4, 3, 5},
		},
		{
			arr:      []int{1, 2, 3, 0, 6, 4, 5},
			preOrder: []int{1, 2, 6, 3, 4, 5},
			inOrder:  []int{2, 6, 1, 4, 3, 5},
		},
		{
			arr:      []int{1, 2, 3, 0, 0, 4, 5},
			preOrder: []int{1, 2, 3, 4, 5},
			inOrder:  []int{2, 1, 4, 3, 5},
		},
		{
			arr:      []int{1, 2, 3, 4},
			preOrder: []int{1, 2, 4, 3},
			inOrder:  []int{4, 2, 1, 3},
		},
		{
			arr:      []int{1, 2, 3, 4, 0, 5},
			preOrder: []int{1, 2, 4, 3, 5},
			inOrder:  []int{4, 2, 1, 5, 3},
		},
		{
			arr:      []int{1, 2, 3, 4, 0, 0, 5},
			preOrder: []int{1, 2, 4, 3, 5},
			inOrder:  []int{4, 2, 1, 3, 5},
		},
		{
			arr:      []int{1, 0, 2, 0, 3, 0, 4},
			preOrder: []int{1, 2, 3, 4},
			inOrder:  []int{1, 2, 3, 4},
		},
		{
			arr:      []int{1, 2, 0, 3, 0, 4},
			preOrder: []int{1, 2, 3, 4},
			inOrder:  []int{4, 3, 2, 1},
		},
	}
	Convey("TestBfsInsert", t, func() {
		for _, tt := range tests {
			root := BuildTree(tt.arr)
			t.Log(root)
			var preVals, inVals []int
			TraversalPreOrder(root, &preVals)
			TraversalInOrder(root, &inVals)
			t.Log("preorder", preVals)
			t.Log("inorder", inVals)
			So(preVals, ShouldResemble, tt.preOrder)
			So(inVals, ShouldResemble, tt.inOrder)
		}
	})
}
