package common

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestBuildLinkList(t *testing.T) {
	tests := [][]int{
		[]int{1},
		[]int{1, 2},
		[]int{1, 2, 3, 4, 5},
	}
	for _, tt := range tests {
		Convey("TestBuildLinkList", t, func() {
			l := &LinkList{Nums: tt}
			root := l.Build()
			t.Log(root)
			So(l.Traversal(root), ShouldResemble, tt)
		})
	}
}

func TestBinarySearch(t *testing.T) {
	tests := []struct {
		nums           []int
		target, expect int
	}{
		{
			nums:   []int{1, 2, 3, 4, 5},
			target: 3,
			expect: 2,
		},
		{
			nums:   []int{1, 2, 3, 4, 5},
			target: 2,
			expect: 1,
		},
		{
			nums:   []int{1, 2, 3, 4, 5},
			target: 1,
			expect: 0,
		},
		{
			nums:   []int{1, 2, 3, 4, 5},
			target: 4,
			expect: 3,
		},
		{
			nums:   []int{1, 2, 3, 4, 5},
			target: 5,
			expect: 4,
		},
		{
			nums:   []int{1, 2, 3, 4, 5},
			target: 6,
			expect: -1,
		},
	}
	Convey("TestBinarySearch", t, func() {
		for _, tt := range tests {
			So(BinarySearch(tt.nums, tt.target), ShouldEqual, tt.expect)
		}
	})
}

// 类似于bisect()
func TestBinarySearchUpperBound(t *testing.T) {
	tests := []struct {
		nums           []int
		target, expect int
	}{
		{
			nums:   []int{1, 2, 3, 4},
			target: 3,
			expect: 3,
		},
		{
			nums:   []int{1, 2, 4, 5},
			target: 3,
			expect: 2,
		},
		{
			nums:   []int{1, 2, 4, 5},
			target: 6,
			expect: 4,
		},
		{
			nums:   []int{1, 2, 4, 5},
			target: 1,
			expect: 1,
		},
		{
			nums:   []int{1, 2, 4, 5},
			target: 0,
			expect: 0,
		},
	}
	Convey("TestBinarySearchUpperBound", t, func() {
		for _, tt := range tests {
			So(BinarySearchUpperBound(tt.nums, tt.target), ShouldEqual, tt.expect)
		}
	})
}

// 类似于bisect_left()
func TestBinarySearchLowerBound(t *testing.T) {
	tests := []struct {
		nums           []int
		target, expect int
	}{
		{
			nums:   []int{1, 2, 3, 4},
			target: 3,
			expect: 2,
		},
		{
			nums:   []int{1, 2, 4, 5},
			target: 3,
			expect: 2,
		},
		{
			nums:   []int{1, 2, 4, 5},
			target: 6,
			expect: 4,
		},
		{
			nums:   []int{1, 2, 4, 5},
			target: 1,
			expect: 0,
		},
		{
			nums:   []int{1, 2, 4, 5},
			target: 0,
			expect: 0,
		},
	}
	Convey("TestBinarySearchLowerBound", t, func() {
		for _, tt := range tests {
			So(BinarySearchLowerBound(tt.nums, tt.target), ShouldEqual, tt.expect)
		}
	})
}
