package common

import "math"

func Min(nums ...int) int {
	if len(nums) == 0 {
		return 0
	}
	r := math.MaxInt64
	for _, n := range nums {
		if n < r {
			r = n
		}
	}
	return r
}

func Max(nums ...int) int {
	if len(nums) == 0 {
		return 0
	}
	r := math.MinInt64
	for _, n := range nums {
		if n > r {
			r = n
		}
	}
	return r
}
