package common

type TreeNode struct {
	Val   int
	Val1  int
	Left  *TreeNode
	Right *TreeNode
}

func BuildTree(arr []int) *TreeNode {
	if len(arr) == 0 {
		return nil
	}
	root := &TreeNode{Val: arr[0]}
	q := []*TreeNode{root}

	i := 0
	size := len(arr)
	if size%2 == 0 {
		arr = append(arr, 0)
		size++
	}
	for 2*i+2 < size {
		head := q[0]
		q = q[1:]
		left := arr[2*i+1]
		right := arr[2*i+2]

		if left != 0 {
			head.Left = &TreeNode{Val: left}
			q = append(q, head.Left)
		}
		if right != 0 {
			head.Right = &TreeNode{Val: right}
			q = append(q, head.Right)
		}
		i++
	}
	return root
}

func TraversalPreOrder(head *TreeNode, vals *[]int) {
	if head != nil {
		*vals = append(*vals, head.Val)
		TraversalPreOrder(head.Left, vals)
		TraversalPreOrder(head.Right, vals)
	}
}

func TraversalInOrder(head *TreeNode, vals *[]int) {
	if head != nil {
		TraversalInOrder(head.Left, vals)
		*vals = append(*vals, head.Val)
		TraversalInOrder(head.Right, vals)
	}
}
