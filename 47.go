package leetcode

import (
	"fmt"
	"sort"
)

//Given a collection of numbers, nums, that might contain duplicates, return all possible unique permutations in any order.

//Example 1:

//Input: nums = [1,1,2]
//Output:
//[[1,1,2],
//[1,2,1],
//[2,1,1]]

//Example 2:

//Input: nums = [1,2,3]
//Output: [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]

//Constraints:

// 1 <= nums.length <= 8
// -10 <= nums[i] <= 10
func permuteUnique(nums []int) [][]int {
	var r [][]int
	var cp []int
	size := len(nums)

	counter := make(map[int]int)
	for _, n := range nums {
		if _, ok := counter[n]; !ok {
			counter[n] = 1
		} else {
			counter[n]++
		}
	}
	fmt.Println(counter)

	sort.Ints(nums)

	var dfs func(i int, path []int)
	dfs = func(i int, path []int) {
		if i > size {
			return
		}
		if i == size {
			cp = make([]int, size)
			copy(cp, path)
			r = append(r, cp)
			return
		}
		for j, n := range nums {
			if counter[n] <= 0 {
				continue
			}
			if j > 0 && nums[j-1] == nums[j] {
				continue
			}
			counter[n]--
			path = append(path, n)
			dfs(i+1, path)
			path = path[:len(path)-1]
			counter[n]++
		}
	}
	dfs(0, []int{})
	return r
}
