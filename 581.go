package leetcode

import (
	"fmt"

	"gitlab.com/RandomCivil/leetcode/common"
)

//Given an integer array nums, you need to find one continuous subarray that if you only sort this subarray in ascending order, then the whole array will be sorted in ascending order.

//Return the shortest such subarray and output its length.

//Example 1:

//Input: nums = [2,6,4,8,10,9,15]
//Output: 5
//Explanation: You need to sort [6, 4, 8, 10, 9] in ascending order to make the whole array sorted in ascending order.

//Example 2:

//Input: nums = [1,2,3,4]
//Output: 0

//Example 3:

// Input: nums = [1]
// Output: 0
func FindUnsortedSubarray(nums []int) int {
	fmt.Println(nums)
	var (
		size  = len(nums)
		stack []int
	)
	l, r := size, 0
	for i := 0; i < size; i++ {
		for len(stack) > 0 && nums[i] < nums[stack[len(stack)-1]] {
			l = common.Min(l, stack[len(stack)-1])
			stack = stack[:len(stack)-1]
		}
		stack = append(stack, i)
	}

	stack = make([]int, 0)
	for i := size - 1; i >= 0; i-- {
		for len(stack) > 0 && nums[i] > nums[stack[len(stack)-1]] {
			r = common.Max(r, stack[len(stack)-1])
			stack = stack[:len(stack)-1]
		}
		stack = append(stack, i)
	}
	fmt.Println(l, r)
	if l > r {
		return 0
	}
	return r - l + 1
}
