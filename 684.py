# In this problem, a tree is an undirected graph that is connected and has no cycles.

# You are given a graph that started as a tree with n nodes labeled from 1 to n, with one additional edge added. The added edge has two different vertices chosen from 1 to n, and was not an edge that already existed. The graph is represented as an array edges of length n where edges[i] = [ai, bi] indicates that there is an edge between nodes ai and bi in the graph.

# Return an edge that can be removed so that the resulting graph is a tree of n nodes. If there are multiple answers, return the answer that occurs last in the input.

# Example 1:


# Input: edges = [[1,2],[1,3],[2,3]]
# Output: [2,3]
# Example 2:


# Input: edges = [[1,2],[2,3],[3,4],[1,4],[1,5]]
# Output: [1,4]


# Constraints:

# n == edges.length
# 3 <= n <= 1000
# edges[i].length == 2
# 1 <= ai < bi <= edges.length
# ai != bi
# There are no repeated edges.
# The given graph is connected.
from collections import defaultdict


class Solution:
    def findRedundantConnection(self, edges: list[list[int]]) -> list[int]:
        d = defaultdict(list)
        edgesToIndex = {}
        for i, (s, e) in enumerate(edges):
            d[s].append(e)
            d[e].append(s)
            edgesToIndex[(s, e)] = i
            edgesToIndex[(e, s)] = i

        self.visited = set()
        self.path = []
        self.max_index = -1
        self.result = None

        def dfs(cur, parent):
            if cur in self.visited:
                return True
            self.visited.add(cur)
            for nxt in d[cur]:
                if nxt == parent:
                    continue
                self.path.append((cur, nxt))
                if dfs(nxt, cur):
                    return True
                self.path.pop()
            return False

        dfs(1, None)
        print("path", self.path)
        print("edgesToIndex", edgesToIndex)

        s, e = self.path.pop()
        self.max_index = edgesToIndex[(s, e)]
        i = len(self.path) - 1
        while i >= 0:
            cs, ce = self.path[i]
            self.max_index = max(self.max_index, edgesToIndex[self.path[i]])
            if cs == e:
                break
            i -= 1
        return edges[self.max_index]
