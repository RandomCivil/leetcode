package leetcode

import (
	"fmt"
	"math"
)

//We are given an array asteroids of integers representing asteroids in a row.
//For each asteroid, the absolute value represents its size, and the sign represents its direction (positive meaning right, negative meaning left). Each asteroid moves at the same speed.
//Find out the state of the asteroids after all collisions. If two asteroids meet, the smaller one will explode. If both are the same size, both will explode. Two asteroids moving in the same direction will never meet.

//Example 1:

//Input: asteroids = [5,10,-5]
//Output: [5,10]
//Explanation: The 10 and -5 collide resulting in 10.  The 5 and 10 never collide.

//Example 2:

//Input: asteroids = [8,-8]
//Output: []
//Explanation: The 8 and -8 collide exploding each other.

//Example 3:

//Input: asteroids = [10,2,-5]
//Output: [10]
//Explanation: The 2 and -5 collide resulting in -5. The 10 and -5 collide resulting in 10.

//Example 4:

//Input: asteroids = [-2,-1,1,2]
//Output: [-2,-1,1,2]
//Explanation: The -2 and -1 are moving left, while the 1 and 2 are moving right. Asteroids moving the same direction never meet, so no asteroids will meet each other.

func AsteroidCollision(asteroids []int) []int {
	fmt.Println(asteroids)
	size := len(asteroids)
	var stack []int
	for i := 0; i < size; i++ {
		flag := true
		for len(stack) > 0 {
			flag = false
			pop := stack[len(stack)-1]
			if pop < 0 || asteroids[i] > 0 {
				flag = true
				break
			}
			stack = stack[:len(stack)-1]
			if float64(pop) > math.Abs(float64(asteroids[i])) {
				stack = append(stack, pop)
				break
			} else if float64(pop) == math.Abs(float64(asteroids[i])) {
				break
			} else {
				flag = true
				continue
			}
		}
		if flag {
			stack = append(stack, asteroids[i])
		}

	}
	return stack
}
