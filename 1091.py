# Given an n x n binary matrix grid, return the length of the shortest clear path in the matrix. If there is no clear path, return -1.

# A clear path in a binary matrix is a path from the top-left cell (i.e., (0, 0)) to the bottom-right cell (i.e., (n - 1, n - 1)) such that:

# All the visited cells of the path are 0.
# All the adjacent cells of the path are 8-directionally connected (i.e., they are different and they share an edge or a corner).
# The length of a clear path is the number of visited cells of this path.

# Example 1:

# Input: grid = [[0,1],[1,0]]
# Output: 2

# Example 2:

# Input: grid = [[0,0,0],[1,1,0],[1,1,0]]
# Output: 4

# Example 3:

# Input: grid = [[1,0,0],[1,1,0],[1,1,0]]
# Output: -1

# Constraints:

# n == grid.length
# n == grid[i].length
# 1 <= n <= 100
# grid[i][j] is 0 or 1
from collections import deque


class Solution:
    def shortestPathBinaryMatrix(self, grid) -> int:
        if grid[0][0] == 1:
            return -1

        m = len(grid)
        n = len(grid[0])
        nxt = [(1, 0), (-1, 0), (0, 1), (0, -1), (1, 1), (1, -1), (-1, 1),
               (-1, -1)]
        queue = deque([(0, 0, 1)])
        r = float('inf')
        step = 1
        while len(queue) > 0:
            x, y, step = queue.popleft()
            print(x, y, step, queue)
            if x == m - 1 and y == n - 1:
                print('find', step)
                r = min(r, step)
            for nx, ny in nxt:
                if x + nx < 0 or x + nx == m or y + ny < 0 or y + ny == n:
                    continue
                if grid[x + nx][y + ny] == 1:
                    continue
                grid[x + nx][y + ny] = 1
                queue.append((x + nx, y + ny, step + 1))

        return r if r != float('inf') else -1


if __name__ == '__main__':
    grid = [[0, 0, 0], [1, 1, 0], [1, 1, 0]]
    grid = [[0, 1], [1, 0]]
    grid = [[1, 0, 0], [1, 1, 0], [1, 1, 0]]
    print(Solution().shortestPathBinaryMatrix(grid))
