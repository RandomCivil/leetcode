package leetcode

import (
	"fmt"
)

//For an undirected graph with tree characteristics, we can choose any node as the root. The result graph is then a rooted tree. Among all possible rooted trees, those with minimum height are called minimum height trees (MHTs). Given such a graph, write a function to find all the MHTs and return a list of their root labels.
//The graph contains n nodes which are labeled from 0 to n - 1. You will be given the number n and a list of undirected edges (each edge is a pair of labels).
//You can assume that no duplicate edges will appear in edges. Since all edges are undirected, [0, 1] is the same as [1, 0] and thus will not appear together in edges.
/*
   Input: n = 6, edges = [[0, 3], [1, 3], [2, 3], [4, 3], [5, 4]]

        0  1  2
         \ | /
           3
           |
           4
           |
           5

   Output: [3, 4]

           0
           |
           1
          / \
         2   3

   Output: [1]

*/
//map[0:[0] 1:[0 1 2] 2:[1] 3:[2]]
//Input: n = 4, edges = [[1, 0], [1, 2], [1, 3]]

//The height of a rooted tree is the number of edges on the longest downward path between the root and a leaf.

/*
	n := 6
	edges := [][]int{
		{0, 3},
		{1, 3},
		{2, 3},
		{4, 3},
		{5, 4},
	}

	n := 1
	[[0,0]]
	[0]

	n := 1
	[]
	[0]
*/

//func findMinHeightTrees(n int, edges [][]int) []int {
func FindMinHeightTrees(n int, edges [][]int) []int {
	if n == 0 {
		return []int{}
	}
	m := make(map[int]map[int]bool)
	for _, e := range edges {
		start, end := e[0], e[1]
		if _, ok := m[start]; !ok {
			set := make(map[int]bool)
			set[end] = true
			m[start] = set
		} else {
			m[start][end] = true
		}

		if _, ok := m[end]; !ok {
			set := make(map[int]bool)
			set[start] = true
			m[end] = set
		} else {
			m[end][start] = true
		}
	}
	fmt.Println(m, n)

	leaves := []int{}
	for node, nexts := range m {
		if len(nexts) == 1 {
			leaves = append(leaves, node)
		}
	}

	for n > 2 {
		n -= len(leaves)

		leaves1 := []int{}
		for _, leaf := range leaves {
			nexts := m[leaf]
			var next int
			for next, _ = range nexts {
				delete(m[next], leaf)
			}
			delete(m, leaf)

			if len(m[next]) == 1 {
				leaves1 = append(leaves1, next)
			}
		}
		leaves = leaves1
	}

	return leaves
}
