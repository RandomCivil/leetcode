# You may recall that an array arr is a mountain array if and only if:

# arr.length >= 3
# There exists some index i (0-indexed) with 0 < i < arr.length - 1 such that:

# arr[0] < arr[1] < ... < arr[i - 1] < arr[i]
# arr[i] > arr[i + 1] > ... > arr[arr.length - 1]
# Given an integer array arr, return the length of the longest subarray, which is a mountain. Return 0 if there is no mountain subarray.

# Example 1:

# Input: arr = [2,1,4,7,3,2,5]
# Output: 5
# Explanation: The largest mountain is [1,4,7,3,2] which has length 5.

# Example 2:


# Input: arr = [2,2,2]
# Output: 0
# Explanation: There is no mountain.
class Solution:
    def longestMountain(self, arr) -> int:
        size = len(arr)
        i = 0
        result = 0
        while i < size - 1:
            while i + 1 < size and arr[i] >= arr[i + 1]:
                i += 1
            start = i
            while i + 1 < size and arr[i] < arr[i + 1]:
                i += 1

            top = False
            if i + 1 < size and arr[i] > arr[i + 1]:
                top = True
            else:
                continue

            while top and i + 1 < size and arr[i] > arr[i + 1]:
                i += 1
            result = max(result, i - start + 1)
        return result


if __name__ == "__main__":
    # 5
    arr = [2, 1, 4, 7, 3, 2, 5]

    # 2
    arr = [2, 2, 2]
    print(Solution().longestMountain(arr))
