inst_1010 = __import__("1010")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "time, expected",
        [
            ([30, 20, 150, 100, 40], 3),
            ([60, 60, 60], 3),
        ],
    )
    def test_numPairsDivisibleBy60(self, time, expected):
        assert inst_1010.Solution().numPairsDivisibleBy60(time) == expected
