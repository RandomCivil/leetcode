inst_532_me = __import__("532_me")
inst_532 = __import__("532")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "nums,k,expected",
        [
            ([3, 1, 4, 1, 5], 2, 2),
            ([1, 2, 3, 4, 5], 1, 4),
            ([1, 3, 1, 5, 4], 0, 1),
            ([1, 1, 3, 3, 4, 5], 0, 2),
        ],
    )
    def test_findPairs(self, nums, k, expected):
        assert inst_532.Solution().findPairs(nums, k) == expected
        assert inst_532_me.Solution().findPairs(nums, k) == expected
