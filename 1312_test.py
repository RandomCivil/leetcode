inst_1312 = __import__("1312")
inst_1312_recurse = __import__("1312_recurse")


import pytest


class TestClass:
    @pytest.mark.parametrize(
        "s,expected",
        [
            ("mbadm", 2),
            # ("leetcode", 5),
            ("mbcadem", 4),
            ("mbcadcm", 2),
            ("mbcacdm", 2),
            ("acba", 1),
            ("zjveiiwvc", 5),
        ],
    )
    def test_minInsertions(self, s, expected):
        assert inst_1312.Solution().minInsertions(s) == expected
        assert inst_1312_recurse.Solution().minInsertions(s) == expected
