# There are N students in a class. Some of them are friends, while some are not. Their friendship is transitive in nature. For example, if A is a direct friend of B, and B is a direct friend of C, then A is an indirect friend of C. And we defined a friend circle is a group of students who are direct or indirect friends.
# Given a N*N matrix M representing the friend relationship between students in the class. If M[i][j] = 1, then the ith and jth students are direct friends with each other, otherwise not. And you have to output the total number of friend circles among all the students.
"""
Input:
[[1,1,0],
 [1,1,0],
 [0,0,1]]
Output: 2
Explanation:The 0th and 1st students are direct friends, so they are in a friend circle.
The 2nd student himself is in a friend circle. So return 2.

Input:
[[1,1,0],
 [1,1,1],
 [0,1,1]]
Output: 1
Explanation:The 0th and 1st students are direct friends, the 1st and 2nd students are direct friends,
so the 0th and 2nd students are indirect friends. All of them are in the same friend circle, so return 1.
"""


class Solution:
    def findCircleNum(self, M) -> int:
        print(M)
        size = len(M)
        # (i,parent)
        l = [[i, None] for i in range(size)]

        def find_root(cur):
            if l[cur][1] is None:
                return cur
            else:
                return find_root(l[cur][1])

        def union(x, y):
            xr = find_root(x)
            yr = find_root(y)
            if xr != yr:
                l[yr][1] = xr

        s = set()
        for i in range(size):
            for j in range(size):
                if i > j:
                    continue
                if M[i][j] == 1:
                    union(i, j)

        for i in range(size):
            s.add(find_root(i))

        print(l, s)
        return len(s)


if __name__ == '__main__':
    M = [[1, 1, 0], [1, 1, 0], [0, 0, 1]]
    # M = [[1, 1, 0],
    # [1, 1, 1],
    # [0, 1, 1]]

    print(Solution().findCircleNum(M))
