package leetcode

import (
	"fmt"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestReverseBits(t *testing.T) {
	tests := []struct {
		num    uint32
		expect string
	}{
		{
			num:    0b00000010100101000001111010011100,
			expect: "111001011110000010100101000000",
		},
		{
			num:    0b11111111111111111111111111111101,
			expect: "10111111111111111111111111111111",
		},
	}
	Convey("TestReverseBits", t, func() {
		for _, tt := range tests {
			So(fmt.Sprintf("%b", reverseBits(tt.num)), ShouldEqual, tt.expect)
		}
	})
}
