inst_24 = __import__("24")

import pytest, common


class TestClass:
    @pytest.mark.parametrize(
        "nums,expected",
        [
            ([1, 2, 3, 4], [2, 1, 4, 3]),
            ([1, 2], [2, 1]),
            ([1, 2, 3, 4, 5], [2, 1, 4, 3, 5]),
            ([1], [1]),
            ([], []),
        ],
    )
    def test_swapPairs(self, nums, expected):
        l = common.LinkList(nums)
        assert l.traversal(inst_24.Solution().swapPairs(l.build())) == expected
