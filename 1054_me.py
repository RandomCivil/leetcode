# In a warehouse, there is a row of barcodes, where the ith barcode is barcodes[i].
# Rearrange the barcodes so that no two adjacent barcodes are equal. You may return any answer, and it is guaranteed an answer exists.

# Example 1:

# Input: barcodes = [1,1,1,2,2,2]
# Output: [2,1,2,1,2,1]

# Example 2:

# Input: barcodes = [1,1,1,1,2,2,3,3]
# Output: [1,3,1,3,1,2,1,2]

# Constraints:

# 1 <= barcodes.length <= 10000
# 1 <= barcodes[i] <= 10000
from collections import Counter


class Solution:
    def rearrangeBarcodes(self, barcodes):
        r = []
        c = Counter(barcodes)
        print(c)
        while len(c) > 0:
            for k, v in c.most_common(2):
                c[k] -= 1
                r.append(k)
                if c[k] == 0:
                    del c[k]
        return r


if __name__ == '__main__':
    # barcodes = [1, 1, 1, 2, 2, 2]
    barcodes = [1, 1, 1, 1, 2, 2, 3, 3]
    # barcodes = [1, 1, 1, 2, 2, 2]
    print(Solution().rearrangeBarcodes(barcodes))
