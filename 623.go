package leetcode

import "gitlab.com/RandomCivil/leetcode/common"

//Given the root of a binary tree, then value v and depth d, you need to add a row of nodes with value v at the given depth d. The root node is at depth 1.
//The adding rule is: given a positive integer depth d, for each NOT null tree nodes N in depth d-1, create two tree nodes with value v as N's left subtree root and right subtree root. And N's original left subtree should be the left subtree of the new left subtree root, its original right subtree should be the right subtree of the new right subtree root. If depth d is 1 that means there is no depth d-1 at all, then create a tree node with value v as the new root of the whole original tree, and the original tree is the new root's left subtree.
/*
Input:
A binary tree as following:
       4
     /   \
    2     6
   / \   /
  3   1 5

v = 1

d = 2

Output:
       4
      / \
     1   1
    /     \
   2       6
  / \     /
 3   1   5

Input:
A binary tree as following:
      4
     /
    2
   / \
  3   1

v = 1

d = 3

Output:
      4
     /
    2
   / \
  1   1
 /     \
3       1
*/
/**
 * Definition for a binary tree node.
 * type common.TreeNode struct {
 *     Val int
 *     Left *common.TreeNode
 *     Right *common.TreeNode
 * }
 */
func AddOneRow(root *common.TreeNode, v int, d int) *common.TreeNode {
	if d == 1 {
		node := &common.TreeNode{Val: v, Left: root}
		return node
	}
	q := []*common.TreeNode{root}
	q1 := []*common.TreeNode{}
	depth := 1
	for len(q) > 0 {
		p := q[0]
		q = q[1:]

		var flag bool
		if d-1 == depth {
			flag = true
		}
		if flag {
			node := &common.TreeNode{Val: v, Left: p.Left}
			p.Left = node

			node = &common.TreeNode{Val: v, Right: p.Right}
			p.Right = node
		} else {
			if p.Left != nil {
				q1 = append(q1, p.Left)
			}
			if p.Right != nil {
				q1 = append(q1, p.Right)
			}
		}

		if len(q) == 0 {
			if flag {
				break
			}
			depth++
			q = q1
			q1 = []*common.TreeNode{}
		}
	}
	return root
}
