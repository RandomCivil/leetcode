# We define the string s to be the infinite wraparound string of "abcdefghijklmnopqrstuvwxyz", so s will look like this:

# "...zabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcd....".

# Given a string p, return the number of unique non-empty substrings of p are present in s.

# Example 1:

# Input: p = "a"
# Output: 1
# Explanation: Only the substring "a" of p is in s.

# Example 2:

# Input: p = "cac"
# Output: 2
# Explanation: There are two substrings ("a", "c") of p in s.

# Example 3:

# Input: p = "zab"
# Output: 6
# Explanation: There are six substrings ("z", "a", "b", "za", "ab", and "zab") of p in s.
from collections import Counter


class Solution:
    def findSubstringInWraproundString(self, p: str) -> int:
        dp = Counter()
        size = len(p)
        v = 0
        for i in range(0, size):
            if i > 0 and (ord(p[i]) - ord(p[i - 1]) == 1 or
                          (p[i] == 'a' and p[i - 1] == 'z')):
                # v = dp[p[i - 1]] + 1
                v += 1
            else:
                v = 1
            dp[p[i]] = max(dp[p[i]], v)
        print(dp)
        return sum(dp.values())


if __name__ == '__main__':
    # 2
    p = 'cac'

    # 6
    # p = 'zab'

    # 6
    # p = 'zaba'

    # 339
    p = "cdefghefghijklmnopqrstuvwxmnijklmnopqrstuvbcdefghijklmnopqrstuvwabcddefghijklfghijklmabcdefghijklmnopqrstuvwxymnopqrstuvwxyz"
    print(Solution().findSubstringInWraproundString(p))
