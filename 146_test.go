package leetcode

import (
	"testing"
)

func TestLRUCache(t *testing.T) {
	cache := Constructor(2)
	t.Log("get(2)", cache.Get(2))

	cache.Put(2, 6)
	for e := cache.l.Front(); e != nil; e = e.Next() {
		t.Log("after put(2,6)", e.Value)
	}

	t.Log("get(1)", cache.Get(1))

	cache.Put(1, 5)
	for e := cache.l.Front(); e != nil; e = e.Next() {
		t.Log("after put(1,5)", e.Value)
	}

	cache.Put(1, 2)
	for e := cache.l.Front(); e != nil; e = e.Next() {
		t.Log("after put(1,2)", e.Value)
	}

	t.Log("get(1)", cache.Get(1))
	t.Log("get(2)", cache.Get(2))
}
