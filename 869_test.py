inst_869 = __import__("869")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "n ,expected",
        [
            (46, True),
            (10, False),
            (1, True),
        ],
    )
    def test_reorderedPowerOf2(self, n, expected):
        assert inst_869.Solution().reorderedPowerOf2(n) == expected
