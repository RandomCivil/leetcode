# Given an integer n, return all the numbers in the range [1, n] sorted in lexicographical order.

# You must write an algorithm that runs in O(n) time and uses O(1) extra space.

# Example 1:

# Input: n = 13
# Output: [1,10,11,12,13,2,3,4,5,6,7,8,9]

# Example 2:


# Input: n = 2
# Output: [1,2]
class Solution:
    def lexicalOrder(self, n: int):
        self.result = []

        def dfs(arr):
            num = int(''.join(map(str, arr)))
            if num > n:
                return
            self.result.append(num)
            for nn in range(10):
                arr.append(nn)
                dfs(arr)
                arr.pop()

        for i in range(1, 10):
            dfs([i])
        return self.result


if __name__ == '__main__':
    n = 13
    print(Solution().lexicalOrder(n))
