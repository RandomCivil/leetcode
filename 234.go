package leetcode

import "gitlab.com/RandomCivil/leetcode/common"

//Given a singly linked list, determine if it is a palindrome.

//Example 1:

//Input: 1->2
//Output: false

//Example 2:

//Input: 1->2->2->1
//Output: true
//Follow up:
//Could you do it in O(n) time and O(1) space?

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func isPalindrome(head *common.ListNode) bool {
	fast := head
	slow := head
	for fast != nil && fast.Next != nil {
		slow = slow.Next
		fast = fast.Next.Next
	}

	var t, node *common.ListNode
	for slow != nil {
		t = slow.Next
		slow.Next = node
		node = slow
		slow = t
	}
	for node != nil {
		if node.Val != head.Val {
			return false
		}
		node = node.Next
		head = head.Next
	}
	return true
}
