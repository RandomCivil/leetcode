# Given two integers n and k, return the kth lexicographically smallest integer in the range [1, n].

# Example 1:

# Input: n = 13, k = 2
# Output: 10
# Explanation: The lexicographical order is [1, 10, 11, 12, 13, 2, 3, 4, 5, 6, 7, 8, 9], so the second smallest number is 10.
# Example 2:

# Input: n = 1, k = 1
# Output: 1


# Constraints:


# 1 <= k <= n <= 109
class Solution:
    def findKthNumber(self, n: int, k: int) -> int:
        size = len(str(n))
        self.l = []

        def dfs(step, num):
            print(step, num)
            if step == size + 1:
                return
            if int(num) > n:
                return
            self.l.append(int(num))
            for i in range(10):
                if step == 0:
                    if i == 0:
                        continue
                    dfs(step + 1, str(i))
                else:
                    dfs(step + 1, num + str(i))

        dfs(0, "0")
        return self.l[k]
