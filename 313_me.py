# A super ugly number is a positive integer whose prime factors are in the array primes.

# Given an integer n and an array of integers primes, return the nth super ugly number.

# The nth super ugly number is guaranteed to fit in a 32-bit signed integer.

# Example 1:

# Input: n = 12, primes = [2,7,13,19]
# Output: 32
# Explanation: [1,2,4,7,8,13,14,16,19,26,28,32] is the sequence of the first 12 super ugly numbers given primes = [2,7,13,19].

# Example 2:


# Input: n = 1, primes = [2,3,5]
# Output: 1
# Explanation: 1 has no prime factors, therefore all of its prime factors are in the array primes = [2,3,5].
class Solution:
    def nthSuperUglyNumber(self, n: int, primes) -> int:
        s = {1}
        i = 0
        mn = 2
        while i < n:
            mn = min(s)
            # print(mn, s)
            for p in primes:
                s.add(mn * p)
            s.remove(mn)
            i += 1

        return mn


if __name__ == '__main__':
    n = 12
    primes = [2, 7, 13, 19]

    # n = 1
    # primes = [2,3,5]
    n=100000
    primes=[7,19,29,37,41,47,53,59,61,79,83,89,101,103,109,127,131,137,139,157,167,179,181,199,211,229,233,239,241,251]
    print(Solution().nthSuperUglyNumber(n, primes))
