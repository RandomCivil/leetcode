package leetcode

import "gitlab.com/RandomCivil/leetcode/common"

//Given a binary tree, find the leftmost value in the last row of the tree.
/*
Input:

    2
   / \
  1   3

Output:
1

Input:

        1
       / \
      2   3
     /   / \
    4   5   6
       /
      7

Output:
7
*/
func FindBottomLeftValue(root *common.TreeNode) int {
	if root == nil {
		return 0
	}
	result := [][]int{}
	result = append(result, []int{})
	q := []*common.TreeNode{root}
	//next level queue
	q1 := []*common.TreeNode{}
	i := 0
	for len(q) > 0 {
		head := q[0]
		result[i] = append(result[i], head.Val)
		q = q[1:]

		if head.Left != nil {
			q1 = append(q1, head.Left)
		}
		if head.Right != nil {
			q1 = append(q1, head.Right)
		}

		if len(q) == 0 {
			q = q1
			if len(q1) > 0 {
				result = append(result, []int{})
				i++
			}
			q1 = []*common.TreeNode{}
		}
	}
	size := len(result)
	return result[size-1][0]
}
