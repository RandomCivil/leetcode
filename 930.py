# In an array nums of 0s and 1s, how many non-empty subarrays have sum goal?

# Example 1:

# Input: nums = [1,0,1,0,1], goal = 2
# Output: 4
# Explanation:
# The 4 subarrays are bolded below:
# [1,0,1,0,1]
# [1,0,1,0,1]
# [1,0,1,0,1]
# [1,0,1,0,1]

# Note:

# nums.length <= 30000
# 0 <= goal <= nums.length
# nums[i] is either 0 or 1.
from collections import Counter


class Solution:
    def numSubarraysWithSum(self, nums, goal: int) -> int:
        r = 0
        s = 0
        c = Counter({0: 1})
        for n in nums:
            s += n
            r += c[s - goal]
            c[s] += 1
        print(c)
        return r


if __name__ == '__main__':
    # 4
    nums = [1, 0, 1, 0, 1]
    goal = 2

    # 15
    nums = [0, 0, 0, 0, 0]
    goal = 0
    print(Solution().numSubarraysWithSum(nums, goal))
