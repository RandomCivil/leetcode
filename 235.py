# # Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None


class Solution:
    def lowestCommonAncestor(
        self, root: "TreeNode", p: "TreeNode", q: "TreeNode"
    ) -> "TreeNode":
        self.d = {}

        def find(cur, target):
            if not cur:
                return []
            self.d[cur.val] = cur
            if cur.val == target.val:
                return [cur.val]
            elif cur.val < target.val:
                return [cur.val] + find(cur.right, target)
            else:
                return [cur.val] + find(cur.left, target)

        path1, path2 = find(root, p), find(root, q)
        print(path1, path2)

        s = set(path2)
        for i in range(len(path1) - 1, -1, -1):
            if path1[i] in s:
                return self.d[path1[i]]
        return None
