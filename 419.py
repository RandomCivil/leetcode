# Given an m x n matrix board where each cell is a battleship 'X' or empty '.', return the number of the battleships on board.

# Battleships can only be placed horizontally or vertically on board. In other words, they can only be made of the shape 1 x k (1 row, k columns) or k x 1 (k rows, 1 column), where k can be of any size. At least one horizontal or vertical cell separates between two battleships (i.e., there are no adjacent battleships).

# Example 1:

# Input: board = [["X",".",".","X"],[".",".",".","X"],[".",".",".","X"]]
# Output: 2

# Example 2:

# Input: board = [["."]]
# Output: 0

# Constraints:


# m == board.length
# n == board[i].length
# 1 <= m, n <= 200
# board[i][j] is either '.' or 'X'.
class Solution:
    def countBattleships(self, board) -> int:
        m, n = len(board), len(board[0])

        def dfs(x, y):
            if x == -1 or x == m or y == -1 or y == n:
                return
            if board[x][y] == '.':
                return
            board[x][y] = '.'
            dfs(x + 1, y)
            dfs(x - 1, y)
            dfs(x, y + 1)
            dfs(x, y - 1)

        r = 0
        for x in range(m):
            for y in range(n):
                if board[x][y] == 'X':
                    dfs(x, y)
                    r += 1
        return r


if __name__ == '__main__':
    board = [["X", ".", ".", "X"], [".", ".", ".", "X"], [".", ".", ".", "X"]]
    print(Solution().countBattleships(board))
    print(board)
