# Given an array of strings wordsDict and two strings that already exist in the array word1 and word2, return the shortest distance between the occurrence of these two words in the list.

# Note that word1 and word2 may be the same. It is guaranteed that they represent two individual words in the list.

# Example 1:

# Input: wordsDict = ["practice", "makes", "perfect", "coding", "makes"], word1 = "makes", word2 = "coding"
# Output: 1
# Example 2:

# Input: wordsDict = ["practice", "makes", "perfect", "coding", "makes"], word1 = "makes", word2 = "makes"
# Output: 3


# Constraints:


# 1 <= wordsDict.length <= 105
# 1 <= wordsDict[i].length <= 10
# wordsDict[i] consists of lowercase English letters.
# word1 and word2 are in wordsDict.
class Solution:
    def shortestWordDistance(self, wordsDict, word1: str, word2: str) -> int:
        result = float("inf")
        last1, last2 = None, None
        for i, word in enumerate(wordsDict):
            if word == word1:
                if last2 is not None:
                    result = min(result, i - last2)
                if word1 == word2:
                    last2 = i
                    continue
                last1 = i
            if word == word2:
                if last1 is not None:
                    result = min(result, i - last1)
                last2 = i
        return result


if __name__ == "__main__":
    # 1
    wordsDict = ["practice", "makes", "perfect", "coding", "makes"]
    word1 = "makes"
    word2 = "coding"

    # 3
    wordsDict = ["practice", "makes", "perfect", "coding", "makes"]
    word1 = "makes"
    word2 = "makes"

    # 1
    wordsDict = [
        "mo",
        "mo",
        "mo",
        "mo",
        "mo",
        "mo",
        "mo",
        "mo",
        "salah",
        "salah",
        "salah",
        "salah",
        "salah",
        "salah",
        "salah",
        "salah",
        "salah",
    ]
    word1 = "mo"
    word2 = "salah"
    print(Solution().shortestWordDistance(wordsDict, word1, word2))
