# Given the root of a binary tree, return the length of the longest consecutive path in the tree.

# A consecutive path is a path where the values of the consecutive nodes in the path differ by one. This path can be either increasing or decreasing.

# For example, [1,2,3,4] and [4,3,2,1] are both considered valid, but the path [1,2,4,3] is not valid.
# On the other hand, the path can be in the child-Parent-child order, where not necessarily be parent-child order.

# Example 1:


# Input: root = [1,2,3]
# Output: 2
# Explanation: The longest consecutive path is [1, 2] or [2, 1].
# Example 2:


# Input: root = [2,1,3]
# Output: 3
# Explanation: The longest consecutive path is [1, 2, 3] or [3, 2, 1].


# Constraints:


# The number of nodes in the tree is in the range [1, 3 * 104].
# -3 * 104 <= Node.val <= 3 * 104
# Definition for a binary tree node.
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def longestConsecutive(self, root: Optional[TreeNode]) -> int:
        self.result = 0

        def dfs(cur):
            if not cur:
                return 1, 1
            linc, ldec = dfs(cur.left)
            rinc, rdec = dfs(cur.right)

            if cur.left:
                if cur.left.val - cur.val == 1:
                    ldec += 1
                    linc = 1
                elif cur.val - cur.left.val == 1:
                    linc += 1
                    ldec = 1
                else:
                    linc, ldec = 1, 1
            if cur.right:
                if cur.right.val - cur.val == 1:
                    rdec += 1
                    rinc = 1
                elif cur.val - cur.right.val == 1:
                    rinc += 1
                    rdec = 1
                else:
                    rinc, rdec = 1, 1

            self.result = max(self.result, linc, ldec, rinc, rdec)
            if cur.left and cur.right:
                if (cur.left.val - cur.val == cur.val - cur.right.val == 1) or (
                    cur.right.val - cur.val == cur.val - cur.left.val == 1
                ):
                    self.result = max(self.result, linc + rdec - 1, rinc + ldec - 1)
            return max(linc, rinc), max(ldec, rdec)

        dfs(root)
        return self.result
