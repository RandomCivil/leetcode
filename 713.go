package leetcode

import "fmt"

//Your are given an array of positive integers nums.

//Count and print the number of (contiguous) subarrays where the product of all the elements in the subarray is less than k.

//Example 1:
//Input: nums = [10, 5, 2, 6], k = 100
//Output: 8
//Explanation: The 8 subarrays that have product less than 100 are: [10], [5], [2], [6], [10, 5], [5, 2], [2, 6], [5, 2, 6].
//Note that [10, 5, 2] is not included as the product of 100 is not strictly less than k.

//0 < nums.length <= 50000.
//0 < nums[i] < 1000.
//0 <= k < 10^6.

func NumSubarrayProductLessThanK(nums []int, k int) int {
	fmt.Println(nums, k)
	size := len(nums)
	r := 0

	dividend := 1
	i := 0
	for j := 0; j < size; j++ {
		dividend = dividend * nums[j]
		for i < size {
			fmt.Println(i, j, dividend)
			if dividend < k {
				r += j - i + 1
				break
			}
			dividend = dividend / nums[i]
			i++
		}
	}
	return r
}
