# Given an integer array nums of length n and an integer target, find three integers in nums such that the sum is closest to target.

# Return the sum of the three integers.

# You may assume that each input would have exactly one solution.

# Example 1:

# Input: nums = [-1,2,1,-4], target = 1
# Output: 2
# Explanation: The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
# Example 2:

# Input: nums = [0,0,0], target = 1
# Output: 0
# Explanation: The sum that is closest to the target is 0. (0 + 0 + 0 = 0).

# Constraints:

# 3 <= nums.length <= 500
# -1000 <= nums[i] <= 1000
# -104 <= target <= 104


class Solution:
    def threeSumClosest(self, nums, target):
        mn = 3000 + 10**4
        result = sum(nums[0:3])
        size = len(nums)
        nums.sort()
        for i in range(size - 2):
            if i > 0 and nums[i] == nums[i - 1]:
                continue
            l, r = i + 1, size - 1
            while l < r:
                s = nums[i] + nums[l] + nums[r]
                a = abs(s - target)
                if a < mn:
                    mn = a
                    result = s

                if s > target:
                    r -= 1
                elif s < target:
                    l += 1
                else:
                    return target
        return result


if __name__ == "__main__":
    # 2
    nums = [-1, 2, 1, -4]
    target = 1
    # -3000
    nums = [-1000, -1000, -1000]
    target = 10000
    print(Solution().threeSumClosest(nums, target))
