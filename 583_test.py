inst_583 = __import__("583")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        " word1,word2,expected",
        [
            ("sea", "eat", 2),
            ("leetcode", "etco", 4),
        ],
    )
    def test_minDistance(self, word1, word2, expected):
        assert inst_583.Solution().minDistance(word1, word2) == expected
