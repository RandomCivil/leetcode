package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestNumMatchingSubseq(t *testing.T) {
	tests := []struct {
		s      string
		words  []string
		expect int
	}{
		{
			s:      "abcde",
			words:  []string{"a", "bb", "acd", "ace"},
			expect: 3,
		},
		{
			s:      "dsahjpjauf",
			words:  []string{"ahjpjau", "ja", "ahbwzgqnuk", "tnmlanowax"},
			expect: 2,
		},
	}
	Convey("TestNumMatchingSubseq", t, func() {
		for _, tt := range tests {
			So(numMatchingSubseq(tt.s, tt.words), ShouldEqual, tt.expect)
		}
	})
}
