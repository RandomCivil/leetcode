package leetcode

import "fmt"

//You may recall that an array arr is a mountain array if and only if:

//arr.length >= 3
//There exists some index i (0-indexed) with 0 < i < arr.length - 1 such that:
//arr[0] < arr[1] < ... < arr[i - 1] < arr[i]
//arr[i] > arr[i + 1] > ... > arr[arr.length - 1]
//Given an integer array arr, return the length of the longest subarray, which is a mountain. Return 0 if there is no mountain subarray.

//Example 1:

//Input: arr = [2,1,4,7,3,2,5]
//Output: 5
//Explanation: The largest mountain is [1,4,7,3,2] which has length 5.

//Example 2:

//Input: arr = [2,2,2]
//Output: 0
//Explanation: There is no mountain.

//Constraints:

//1 <= arr.length <= 104
//0 <= arr[i] <= 104

func LongestMountain(arr []int) int {
	fmt.Println(arr)
	r := 0
	size := len(arr)
	i := 0
	for i < size-1 {
		start := i
		fmt.Println(start, i)
		flag := false
		for i < size-1 && arr[i] < arr[i+1] {
			i++
			flag = true
		}
		if i < size-1 && arr[i] > arr[i+1] && flag {
			for i < size-1 && arr[i] > arr[i+1] {
				i++
			}
			gap := i - start + 1
			fmt.Println(start, i, gap)
			if gap > r {
				r = gap
			}
		}
		if start == i {
			i++
		}
	}
	return r
}
