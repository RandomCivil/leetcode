inst_3 = __import__("3")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "s,expected",
        [
            ("abcabcbb", 3),
            ("bbbbb", 1),
            ("pwwkew", 3),
            ("abba", 2),
            ("aabaab!bb", 3),
        ],
    )
    def test_lengthOfLongestSubstring(self, s, expected):
        assert inst_3.Solution().lengthOfLongestSubstring(s) == expected
