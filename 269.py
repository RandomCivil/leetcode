# There is a new alien language that uses the English alphabet. However, the order among the letters is unknown to you.

# You are given a list of strings words from the alien language's dictionary, where the strings in words are
# sorted lexicographically
#  by the rules of this new language.

# Return a string of the unique letters in the new alien language sorted in lexicographically increasing order by the new language's rules. If there is no solution, return "". If there are multiple solutions, return any of them.


# Example 1:

# Input: words = ["wrt","wrf","er","ett","rftt"]
# Output: "wertf"
# Example 2:

# Input: words = ["z","x"]
# Output: "zx"
# Example 3:

# Input: words = ["z","x","z"]
# Output: ""
# Explanation: The order is invalid, so return "".


# Constraints:

# 1 <= words.length <= 100
# 1 <= words[i].length <= 100
# words[i] consists of only lowercase English letters.
from collections import defaultdict, deque
from itertools import zip_longest


class Solution:
    def alienOrder(self, words: list[str]) -> str:
        size = len(words)
        if size == 1:
            return "".join(set(words[0]))

        d = defaultdict(set)
        rd = defaultdict(set)

        # s较小，t较大
        def compare(s, t):
            i, j = 0, 0
            size1, size2 = len(s), len(t)
            while i < size1 and j < size2:
                if s[i] != t[j]:
                    break
                i += 1
                j += 1
            else:
                if i == size1 and j == size2:
                    return True
                if i < size1 and j == size2:
                    return False
                if j < size2 and i == size1:
                    return True

            d[s[i]].add(t[j])
            rd[t[j]].add(s[i])
            return True

        chs = set(words[0])
        for i in range(1, size):
            chs = chs | set(words[i])
            if not compare(words[i - 1], words[i]):
                return ""

        q = deque()
        for ch in chs:
            if len(rd[ch]) == 0:
                q.append(ch)

        n = len(chs)
        count = 0
        result = []
        while len(q) > 0:
            cur = q.popleft()
            chs.remove(cur)
            result.append(cur)
            count += 1

            for nxt in d[cur]:
                rd[nxt].remove(cur)
                if len(rd[nxt]) == 0:
                    q.append(nxt)
        if count < n:
            return ""
        return "".join(result) + "".join(chs)
