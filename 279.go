package leetcode

import (
	"fmt"
	"math"

	"gitlab.com/RandomCivil/leetcode/common"
)

//Given an integer n, return the least number of perfect square numbers that sum to n.

//A perfect square is an integer that is the square of an integer; in other words, it is the product of some integer with itself. For example, 1, 4, 9, and 16 are perfect squares while 3 and 11 are not.

//Example 1:

//Input: n = 12
//Output: 3
//Explanation: 12 = 4 + 4 + 4.

//Example 2:

//Input: n = 13
//Output: 2
//Explanation: 13 = 4 + 9.
func NumSquares(n int) int {
	fmt.Println(n)
	var (
		dp = make([]int, (n + 1))
	)
	for i := 1; i <= n; i++ {
		dp[i] = math.MaxInt32
	}
	for nn := 1; nn <= n; nn++ {
		for square := 1; square*square <= nn; square++ {
			dp[nn] = common.Min(dp[nn], dp[nn-square*square]+1)
		}
	}
	fmt.Println(dp)
	return dp[n]
}
