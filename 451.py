#! /usr/bin/env python3

# Given a string, sort it in decreasing order based on the frequency of characters.
"""
Input:
"tree"

Output:
"eert"

Explanation:
'e' appears twice while 'r' and 't' both appear once.
So 'e' must appear before both 'r' and 't'. Therefore "eetr" is also a valid answer.

Input:
"Aabb"

Output:
"bbAa"

Explanation:
"bbaA" is also a valid answer, but "Aabb" is incorrect.
Note that 'A' and 'a' are treated as two different characters.
"""

from collections import Counter


class Solution:
    def frequencySort(self, s: str) -> str:
        result = ''
        c = Counter(s)
        freq = c.most_common()
        for ch, n in freq:
            result += ch*n
        return result


if __name__ == '__main__':
    s = 'eert'
    print(Solution().frequencySort(s))
