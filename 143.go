package leetcode

import (
	"fmt"

	"gitlab.com/RandomCivil/leetcode/common"
)

// You are given the head of a singly linked-list. The list can be represented as:

// L0 → L1 → … → Ln - 1 → Ln
// Reorder the list to be on the following form:

// L0 → Ln → L1 → Ln - 1 → L2 → Ln - 2 → …
// You may not modify the values in the list's nodes. Only nodes themselves may be changed.

// Example 1:

// Input: head = [1,2,3,4]
// Output: [1,4,2,3]
// Example 2:

// Input: head = [1,2,3,4,5]
// Output: [1,5,2,4,3]

// Constraints:

// The number of nodes in the list is in the range [1, 5 * 104].
// 1 <= Node.val <= 1000
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func reorderList(head *common.ListNode) {
	slow, fast := head, head
	for slow != nil && fast != nil && fast.Next != nil {
		slow = slow.Next
		fast = fast.Next.Next
	}
	cur2 := reverseList(slow.Next)
	slow.Next = nil
	fmt.Println(slow)

	cur1 := head
	fmt.Println(cur2)
	for cur1 != nil && cur2 != nil {
		t1, t2 := cur1.Next, cur2.Next
		cur1.Next = cur2
		cur2.Next = t1

		cur1 = t1
		cur2 = t2
	}
}
