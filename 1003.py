# Given a string s, determine if it is valid.

# A string s is valid if, starting with an empty string t = "", you can transform t into s after performing the following operation any number of times:

# Insert string "abc" into any position in t. More formally, t becomes tleft + "abc" + tright, where t == tleft + tright. Note that tleft and tright may be empty.
# Return true if s is a valid string, otherwise, return false.

# Example 1:

# Input: s = "aabcbc"
# Output: true
# Explanation:
# "" -> "abc" -> "aabcbc"
# Thus, "aabcbc" is valid.
# Example 2:

# Input: s = "abcabcababcc"
# Output: true
# Explanation:
# "" -> "abc" -> "abcabc" -> "abcabcabc" -> "abcabcababcc"
# Thus, "abcabcababcc" is valid.
# Example 3:

# Input: s = "abccba"
# Output: false
# Explanation: It is impossible to get "abccba" using the operation.


# Constraints:


# 1 <= s.length <= 2 * 104
# s consists of letters 'a', 'b', and 'c'
class Solution:
    def isValid(self, s: str) -> bool:
        stack = []
        for ss in s:
            while len(stack) > 2 and stack[-3] == "a" and stack[-2] == "b" and stack[-1]=='c':
                stack.pop()
                stack.pop()
                stack.pop()
            stack.append(ss)
        print(stack)
        return len(stack) == 0 or ''.join(stack)=='abc'
