# Given a positive integer n, find the smallest integer which has exactly the same digits existing in the integer n and is greater in value than n. If no such positive integer exists, return -1.

# Note that the returned integer should fit in 32-bit integer, if there is a valid answer but it does not fit in 32-bit integer, return -1.

# Example 1:

# Input: n = 12
# Output: 21

# Example 2:

# Input: n = 21
# Output: -1


class Solution:
    def nextGreaterElement(self, n: int) -> int:
        print(n)
        arr = list(map(int, str(n)))
        size = len(arr)
        flag = True
        i = 0
        for i in range(size - 2, -1, -1):
            if arr[i] < arr[i + 1]:
                break
        else:
            flag = False
        print(i)

        if not flag:
            return -1

        sorted_arr = sorted(arr[i + 1:])
        arr[i + 1:] = sorted_arr
        print(arr)

        j = i + 1
        while arr[j] <= arr[i]:
            j += 1
        arr[i], arr[j] = arr[j], arr[i]
        print(arr, sorted_arr, j)

        result = int(''.join(map(str, arr)))
        return result if result <= 2**31 - 1 else -1


if __name__ == '__main__':
    # 21
    n = 12

    # 13222344
    n = 12443322

    # 9527846
    n = 9527684
    print(Solution().nextGreaterElement(n))
