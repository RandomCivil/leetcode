package leetcode

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
)

//Given a list of 24-hour clock time points in "Hour:Minutes" format, find the minimum minutes difference between any two time points in the list.
/*
Input: ["23:59","00:00"]
Output: 1
*/

func FindMinDifference(timePoints []string) int {
	fmt.Println(timePoints)
	sort.Strings(timePoints)
	fmt.Println(timePoints)
	size := len(timePoints)

	firstSplit := strings.Split(timePoints[0], ":")
	fsh, _ := strconv.Atoi(firstSplit[0])
	fsh += 24
	nextFirst := strconv.Itoa(fsh) + ":" + firstSplit[1]
	min := diff(timePoints[size-1], nextFirst)

	for i := 1; i < size; i++ {
		d := diff(timePoints[i-1], timePoints[i])
		if d < min {
			min = d
		}
	}
	return min
}

func diff(a, b string) int {
	as := strings.Split(a, ":")
	bs := strings.Split(b, ":")
	ash, _ := strconv.Atoi(as[0])
	bsh, _ := strconv.Atoi(bs[0])
	asm, _ := strconv.Atoi(as[1])
	bsm, _ := strconv.Atoi(bs[1])
	r := (bsh-ash)*60 + bsm - asm
	return r
}
