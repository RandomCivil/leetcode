package leetcode

import "fmt"

//A zero-indexed array A of length N contains all integers from 0 to N-1. Find and return the longest length of set S, where S[i] = {A[i], A[A[i]], A[A[A[i]]], ... } subjected to the rule below.
//Suppose the first element in S starts with the selection of element A[i] of index = i, the next element in S should be A[A[i]], and then A[A[A[i]]]… By that analogy, we stop adding right before a duplicate element occurs in S.
/*
Input: A = [5,4,0,3,1,6,2]
Output: 4
Explanation:
A[0] = 5, A[1] = 4, A[2] = 0, A[3] = 3, A[4] = 1, A[5] = 6, A[6] = 2.

One of the longest S[K]:
S[0] = {A[0], A[5], A[6], A[2]} = {5, 6, 2, 0}
*/
func ArrayNesting1(nums []int) int {
	fmt.Println(nums)
	r := 0
	visited := make(map[int]struct{})
	for i, n := range nums {
		if _, ok := visited[n]; ok {
			continue
		}
		count := 1
		start := i
		fmt.Println(i, n)
		for start != nums[n] {
			fmt.Println("hehe", start, n, nums[n])
			visited[nums[n]] = struct{}{}
			count++
			n = nums[n]
		}
		if n != i {
			count++
		}
		fmt.Println("count", count)
		if count > r {
			r = count
		}
	}
	return r
}
