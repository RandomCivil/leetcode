# Given an array consists of non-negative integers, your task is to count the number of triplets chosen from the array that can make triangles if we take them as side lengths of a triangle.
"""
Input: [2,2,3,4]
Output: 3
Explanation:
Valid combinations are:
2,3,4 (using the first 2)
2,3,4 (using the second 2)
2,2,3
"""
# The length of the given array won't exceed 1000.
# The integers in the given array are in the range of [0, 1000].
from collections import defaultdict


class Solution:
    def triangleNumber(self, nums) -> int:
        print(nums)
        nums = sorted(nums)
        print(nums)
        size = len(nums)
        d = defaultdict(int)
        for i in range(size-1):
            for j in range(i+1, size):
                d[(nums[i]+nums[j]), j] += 1

        count = 0
        for (s, end), n in d.items():
            for i in range(end+1, size):
                if nums[i] < s:
                    count += n

        print(d,count)
        return count


if __name__ == '__main__':
    nums = [2, 2, 3, 4]
    # nums = [2, 5, 3, 4]
    print(Solution().triangleNumber(nums))
