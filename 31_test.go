package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestNextPermutation(t *testing.T) {
	tests := []struct {
		nums, expect []int
	}{
		{
			nums:   []int{1, 2, 3},
			expect: []int{1, 3, 2},
		},
		{
			nums:   []int{1, 3, 2},
			expect: []int{2, 1, 3},
		},
		{
			nums:   []int{2, 3, 1},
			expect: []int{3, 1, 2},
		},
		{
			nums:   []int{3},
			expect: []int{3},
		},
		{
			nums:   []int{3, 2, 1},
			expect: []int{1, 2, 3},
		},
		{
			nums:   []int{1, 2, 3, 4},
			expect: []int{1, 2, 4, 3},
		},
		{
			nums:   []int{1, 2, 4, 3},
			expect: []int{1, 3, 2, 4},
		},
		{
			nums:   []int{1, 1, 5},
			expect: []int{1, 5, 1},
		},
		{
			nums:   []int{1, 5, 1},
			expect: []int{5, 1, 1},
		},
	}
	Convey("TestGenerateParenthesis", t, func() {
		for _, tt := range tests {
			nextPermutation(tt.nums)
			So(tt.nums, ShouldResemble, tt.expect)
		}
	})
}
