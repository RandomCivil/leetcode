package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestFindPermutation(t *testing.T) {
	tests := []struct {
		s      string
		expect []int
	}{
		{
			s:      "D",
			expect: []int{2, 1},
		},
		{
			s:      "I",
			expect: []int{1, 2},
		},
		{
			s:      "DI",
			expect: []int{2, 1, 3},
		},
		{
			s:      "DID",
			expect: []int{2, 1, 4, 3},
		},
		{
			s:      "DII",
			expect: []int{2, 1, 3, 4},
		},
		{
			s:      "DDI",
			expect: []int{3, 2, 1, 4},
		},
		{
			s:      "IDDID",
			expect: []int{1, 4, 3, 2, 6, 5},
		},
	}
	Convey("TestFindPermutation", t, func() {
		for _, tt := range tests {
			So(findPermutation(tt.s), ShouldResemble, tt.expect)
		}
	})
}
