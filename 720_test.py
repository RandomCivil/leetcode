inst_720 = __import__("720")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "words,expected",
        [
            (["w", "wo", "wor", "worl", "world"], "world"),
            (["a", "banana", "app", "appl", "ap", "apply", "apple"], "apple"),
        ],
    )
    def test_longestWord(self, words, expected):
        assert inst_720.Solution().longestWord(words) == expected
