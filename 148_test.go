package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/RandomCivil/leetcode/common"
)

func TestSortList(t *testing.T) {
	tests := []struct {
		nums, expect []int
	}{
		{
			nums:   []int{4, 2, 1, 3},
			expect: []int{1, 2, 3, 4},
		},
		{
			nums:   []int{-1, 5, 3, 4, 0},
			expect: []int{-1, 0, 3, 4, 5},
		},
	}
	Convey("TestSortList", t, func() {
		for _, tt := range tests {
			l := &common.LinkList{Nums: tt.nums}
			head := sortList(l.Build())
			var result []int
			for head != nil {
				result = append(result, head.Val)
				head = head.Next
			}
			So(result, ShouldResemble, tt.expect)
		}
	})
}
