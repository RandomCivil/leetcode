package leetcode

import (
	"bytes"
	"fmt"
)

// Given a character array s, reverse the order of the words.

// A word is defined as a sequence of non-space characters. The words in s will be separated by a single space.

// Your code must solve the problem in-place, i.e. without allocating extra space.

// Example 1:

// Input: s = ["t","h","e"," ","s","k","y"," ","i","s"," ","b","l","u","e"]
// Output: ["b","l","u","e"," ","i","s"," ","s","k","y"," ","t","h","e"]
// Example 2:

// Input: s = ["a"]
// Output: ["a"]

// Constraints:

// 1 <= s.length <= 105
// s[i] is an English letter (uppercase or lowercase), digit, or space ' '.
// There is at least one word in s.
// s does not contain leading or trailing spaces.
// All the words in s are guaranteed to be separated by a single space.
func reverseWords(s []byte) {
	reverse := func(start, end int) {
		for start < end {
			s[start], s[end] = s[end], s[start]
			start++
			end--
		}
	}
	size := len(s)
	reverse(0, size-1)
	fmt.Println(string(s))

	start, end := 0, 0
	for end < size {
		if bytes.Compare([]byte{s[end]}, []byte{' '}) != 0 {
			end++
			continue
		}
		reverse(start, end-1)
		start = end + 1
		end = start
	}
	reverse(start, size-1)
	fmt.Println(string(s))
}
