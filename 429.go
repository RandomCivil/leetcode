package leetcode

//Given an n-ary tree, return the level order traversal of its nodes' values.

//Nary-Tree input serialization is represented in their level order traversal, each group of children is separated by the null value (See examples).

/*
Input: root = [1,null,3,2,4,null,5,6]
Output: [[1],[3,2,4],[5,6]]

Input: root = [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]
Output: [[1],[2,3,4,5],[6,7,8,9,10],[11,12,13],[14]]
*/

type Node struct {
	Val      int
	Children []*Node
}

//func levelOrder(root *Node) [][]int {
func LevelOrder(root *Node) [][]int {
	if root == nil {
		return [][]int{}
	}
	result := [][]int{}
	result = append(result, []int{})
	q := []*Node{root}
	//next level queue
	q1 := []*Node{}
	i := 0
	for len(q) > 0 {
		head := q[0]
		result[i] = append(result[i], head.Val)
		q = q[1:]

		for _, child := range head.Children {
			q1 = append(q1, child)
		}

		if len(q) == 0 {
			q = q1
			if len(q1) > 0 {
				result = append(result, []int{})
				i++
			}
			q1 = []*Node{}
		}
	}
	return result
}
