package leetcode

import (
	"fmt"
	"math"

	"gitlab.com/RandomCivil/leetcode/common"
)

//Say you have an array for which the ith element is the price of a given stock on day i.

//Design an algorithm to find the maximum profit. You may complete as many transactions as you like (ie, buy one and sell one share of the stock multiple times) with the following restrictions:

//You may not engage in multiple transactions at the same time (ie, you must sell the stock before you buy again).
//After you sell your stock, you cannot buy stock on next day. (ie, cooldown 1 day)

//Input: [1,2,3,0,2]
//Output: 3
//Explanation: transactions = [buy, sell, cooldown, buy, sell]

//prices := []int{1, 2, 3, 0, 2}
func MaxProfit(prices []int) int {
	size := len(prices)
	if size < 2 {
		return 0
	}
	b, s, c := make([]int, size+1), make([]int, size+1), make([]int, size+1)
	c[0], s[0] = 0, 0
	b[0] = math.MinInt32
	for i := 1; i <= size; i++ {
		c[i] = common.Max(c[i-1], s[i-1])
		b[i] = common.Max(b[i-1], c[i-1]-prices[i-1])
		s[i] = common.Max(s[i-1], b[i-1]+prices[i-1])
	}
	fmt.Println(b, s, c)
	return common.Max(c[size], s[size], b[size])
}
