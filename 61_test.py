inst_61 = __import__("61")

import pytest, common


class TestClass:
    @pytest.mark.parametrize(
        "nums,k,expected",
        [
            ([1, 2, 3, 4, 5], 2, [4, 5, 1, 2, 3]),
            ([0,1,2],4,[2,0,1]),
            ([1],1,[1]),
        ],
    )
    def test_rotateRight(self, nums, k, expected):
        l = common.LinkList(nums)
        assert l.traversal(inst_61.Solution().rotateRight(l.build(), k)) == expected
