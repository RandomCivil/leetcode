# Given an array of meeting time intervals where intervals[i] = [starti, endi], determine if a person could attend all meetings.

# Example 1:

# Input: intervals = [[0,30],[5,10],[15,20]]
# Output: false
# Example 2:

# Input: intervals = [[7,10],[2,4]]
# Output: true


# Constraints:


# 0 <= intervals.length <= 104
# intervals[i].length == 2
# 0 <= starti < endi <= 106
class Solution:
    def canAttendMeetings(self, intervals) -> bool:
        l = sorted(intervals, key=lambda item: item[0])
        size = len(l)
        for i in range(1, size):
            _, end1 = l[i - 1]
            start2, _ = l[i]
            if end1 > start2:
                return False
        return True


if __name__ == "__main__":
    intervals = [[1, 2], [2, 3]]
    print(Solution().canAttendMeetings(intervals))
