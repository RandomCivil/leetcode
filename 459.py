# Given a string s, check if it can be constructed by taking a substring of it and appending multiple copies of the substring together.

# Example 1:

# Input: s = "abab"
# Output: true
# Explanation: It is the substring "ab" twice.

# Example 2:

# Input: s = "aba"
# Output: false

# Example 3:

# Input: s = "abcabcabcabc"
# Output: true
# Explanation: It is the substring "abc" four times or the substring "abcabc" twice.

# Constraints:

# 1 <= s.length <= 104
# s consists of lowercase English letters.


class Solution:
    def repeatedSubstringPattern(self, s: str) -> bool:
        size = len(s)
        for i in range(size):
            n = size % (i + 1)
            if n == 0:
                times = int(size / (i + 1))
                print(times, s[:i + 1], s[:i + 1] * times)
                if i < size - 1 and s[:i + 1] * times == s:
                    return True

        return False


if __name__ == '__main__':
    s = 'abab'
    s = 'aba'
    s = "abcabcabcabc"
    print(Solution().repeatedSubstringPattern(s))
