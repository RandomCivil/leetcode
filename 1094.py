# You are driving a vehicle that has capacity empty seats initially available for passengers.  The vehicle only drives east (ie. it cannot turn around and drive west.)

# Given a list of trips, trip[i] = [num_passengers, start_location, end_location] contains information about the i-th trip: the number of passengers that must be picked up, and the locations to pick them up and drop them off.  The locations are given as the number of kilometers due east from your vehicle's initial location.

# Return true if and only if it is possible to pick up and drop off all passengers for all the given trips.

# Example 1:

# Input: trips = [[2,1,5],[3,3,7]], capacity = 4
# Output: false

# Example 2:

# Input: trips = [[2,1,5],[3,3,7]], capacity = 5
# Output: true

# Example 3:

# Input: trips = [[2,1,5],[3,5,7]], capacity = 3
# Output: true

# Example 4:

# Input: trips = [[3,2,7],[3,7,9],[8,3,9]], capacity = 11
# Output: true

# Constraints:

# trips.length <= 1000
# trips[i].length == 3
# 1 <= trips[i][0] <= 100
# 0 <= trips[i][1] < trips[i][2] <= 1000
# 1 <= capacity <= 100000
from collections import defaultdict


class Solution:
    def carPooling(self, trips, capacity: int) -> bool:
        pick_up = defaultdict(int)
        drop_off = defaultdict(int)
        for x in sorted(trips, key=lambda x: x[1]):
            pick_up[x[1]] += x[0]
        for x in sorted(trips, key=lambda x: x[2]):
            drop_off[x[1]] += x[0]
        event = sorted(list(pick_up | drop_off))
        print(pick_up, drop_off, event)

        seated = 0
        for e in event:
            if e in pick_up:
                seated += pick_up[e]
            if e in drop_off:
                seated -= drop_off[e]
            print(e, seated)
            if seated > capacity:
                return False
        return True


if __name__ == '__main__':
    trips = [[2, 1, 5], [3, 3, 7]]
    capacity = 4

    trips = [[2, 1, 5], [3, 3, 7]]
    capacity = 5

    trips = [[2, 1, 5], [3, 5, 7]]
    capacity = 3

    trips = [[3, 2, 7], [3, 7, 9], [8, 3, 9]]
    capacity = 10

    trips = [[3, 2, 8], [4, 4, 6], [10, 8, 9]]
    capacity = 11

    trips = [[9, 3, 4], [9, 1, 7], [4, 2, 4], [7, 4, 5]]
    capacity = 23

    print(Solution().carPooling(trips, capacity))
