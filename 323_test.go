package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestCountComponents(t *testing.T) {
	tests := []struct {
		n, expect int
		edges     [][]int
	}{
		{
			n: 5,
			edges: [][]int{
				{0, 1},
				{1, 2},
				{3, 4},
			},
			expect: 2,
		},
		{
			n: 5,
			edges: [][]int{
				{0, 1},
				{1, 2},
				{2, 3},
				{3, 4},
			},
			expect: 1,
		},
		{
			n:      2,
			edges:  [][]int{},
			expect: 2,
		},
		{
			n: 5,
			edges: [][]int{
				{0, 1},
				{0, 2},
				{2, 3},
				{2, 4},
			},
			expect: 1,
		},
	}
	Convey("TestCountComponents", t, func() {
		for _, tt := range tests {
			So(countComponents(tt.n, tt.edges), ShouldEqual, tt.expect)
		}
	})
}
