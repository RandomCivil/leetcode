package leetcode

import "fmt"

//Given the root of a binary tree, the depth of each node is the shortest distance to the root.
//Return the smallest subtree such that it contains all the deepest nodes in the original tree.
//A node is called the deepest if it has the largest depth possible among any node in the entire tree.
//The subtree of a node is tree consisting of that node, plus the set of all descendants of that node.

//Input: root = [3,5,1,6,2,0,8,null,null,7,4]
//Output: [2,7,4]
//Explanation: We return the node with value 2, colored in yellow in the diagram.
//The nodes coloured in blue are the deepest nodes of the tree.
//Notice that nodes 5, 3 and 2 contain the deepest nodes in the tree but node 2 is the smallest subtree among them, so we return it.

//Constraints:

//The number of nodes in the tree will be in the range [1, 500].
//0 <= Node.val <= 500
//The values of the nodes in the tree are unique.

//type TreeNode struct {
//Val   int
//Left  *TreeNode
//Right *TreeNode
//}

func subtreeWithAllDeepest(root *TreeNode) *TreeNode {
	m := make(map[*TreeNode]int)
	maxDepth := 0
	var dfs func(cur *TreeNode, depth int)
	dfs = func(cur *TreeNode, depth int) {
		if cur == nil {
			return
		}
		m[cur] = depth
		if depth > maxDepth {
			maxDepth = depth
		}
		dfs(cur.Left, depth+1)
		dfs(cur.Right, depth+1)
	}

	dfs(root, 0)
	fmt.Println(maxDepth)

	var recurse func(cur *TreeNode) *TreeNode
	recurse = func(cur *TreeNode) *TreeNode {
		if cur == nil {
			return nil
		}
		if mv, ok := m[cur]; ok && mv == maxDepth {
			return cur
		}
		left, right := recurse(cur.Left), recurse(cur.Right)
		if left != nil && right != nil {
			return cur
		} else if left != nil {
			return left
		} else if right != nil {
			return right
		}
		return nil
	}
	return recurse(root)
}
