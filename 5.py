# Given a string s, return the longest palindromic substring in s.

# Example 1:

# Input: s = "babad"
# Output: "bab"
# Note: "aba" is also a valid answer.

# Example 2:

# Input: s = "cbbd"
# Output: "bb"

# Example 3:

# Input: s = "a"
# Output: "a"

# Example 4:

# Input: s = "ac"
# Output: "a"

# Constraints:

# 1 <= s.length <= 1000
# s consist of only digits and English letters (lower-case and/or upper-case),


class Solution:
    def longestPalindrome(self, s: str) -> str:
        size = len(s)
        mx = 1
        result = s[0]

        def expand(lo, hi):
            while lo >= 0 and hi < size and s[lo] == s[hi]:
                lo -= 1
                hi += 1
            return s[lo + 1:hi], hi - lo - 1

        for i in range(size):
            s1, l1 = expand(i, i)
            s2, l2 = expand(i, i + 1)
            mx, result = max((mx, result), (l1, s1), (l2, s2))

        return result


if __name__ == '__main__':
    # 3
    s = "babad"

    # 2
    # s = "cbbd"

    # 1
    # s = "ac"

    # s = "aaaabaaa"
    print(Solution().longestPalindrome(s))
