# Given a binary array, find the maximum length of a contiguous subarray with equal number of 0 and 1.

# Example 1:
# Input: [0,1]
# Output: 2
# Explanation: [0, 1] is the longest contiguous subarray with equal number of 0 and 1.

# Example 2:
# Input: [0,1,0]
# Output: 2
# Explanation: [0, 1] (or [1, 0]) is a longest contiguous subarray with equal number of 0 and 1.

# Note: The length of the given binary array will not exceed 50,000.
from collections import defaultdict, Counter


class Solution:
    def findMaxLength(self, nums) -> int:
        result = 0
        d = defaultdict(list)
        c = Counter()
        d[0].append(-1)
        for i, n in enumerate(nums):
            c[n] += 1
            gap = c[0] - c[1]
            if gap in d:
                result = max(result, i - d[gap][0])
            d[gap].append(i)
        return result


if __name__ == "__main__":
    nums = [0, 0, 1, 0, 0, 0, 1, 1]
    # nums=[0,1]
    # nums=[0,1,0]
    print(Solution().findMaxLength(nums))
