# Given a string s, find the longest palindromic subsequence's length in s.

# A subsequence is a sequence that can be derived from another sequence by deleting some or no elements without changing the order of the remaining elements.

# Example 1:

# Input: s = "bbbab"
# Output: 4
# Explanation: One possible longest palindromic subsequence is "bbbb".
# Example 2:

# Input: s = "cbbd"
# Output: 2
# Explanation: One possible longest palindromic subsequence is "bb".

# Constraints:


# 1 <= s.length <= 1000
# s consists only of lowercase English letters.
class Solution:
    def longestPalindromeSubseq(self, s: str) -> int:
        result = 1
        size = len(s)
        dp = [[0] * size for _ in range(size)]
        for i in range(size):
            dp[i][i] = 1

        for i in range(size - 1):
            if s[i] == s[i + 1]:
                dp[i][i + 1] = 2
                result = max(result, 2)
            else:
                dp[i][i + 1] = 1

        for l in range(3, size + 1):
            for i in range(size - l + 1):
                j = i + l - 1
                if s[i] == s[j]:
                    dp[i][j] = max(dp[i][j], dp[i + 1][j - 1] + 2)
                else:
                    dp[i][j] = max(dp[i][j], dp[i][j - 1], dp[i + 1][j])
                result = max(result, dp[i][j])
        print(dp, result)
        return result


if __name__ == '__main__':
    # 4
    s = "bbbab"

    # 2
    # s = "cbbd"
    print(Solution().longestPalindromeSubseq(s))
