# Given the root of a binary tree, return the zigzag level order traversal of its nodes' values. (i.e., from left to right, then right to left for the next level and alternate between).

# Example 1:

# Input: root = [3,9,20,null,null,15,7]
# Output: [[3],[20,9],[15,7]]

# Example 2:

# Input: root = [1]
# Output: [[1]]

# Example 3:

# Input: root = []
# Output: []

# Constraints:

# The number of nodes in the tree is in the range [0, 2000].
# -100 <= Node.val <= 100
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
from collections import deque


class Solution:
    def zigzagLevelOrder(self, root):
        if not root:
            return []
        r = [[]]
        queue = deque([root])
        nxt = deque()
        level = 0
        while len(queue) > 0:
            pop = queue.popleft()
            r[level].append(pop.val)
            if pop.left:
                nxt.append(pop.left)
            if pop.right:
                nxt.append(pop.right)

            if len(queue) == 0 and len(nxt) > 0:
                r.append([])
                level += 1
                queue = nxt
                nxt = deque()

        for i, l in enumerate(r):
            if i % 2 == 1:
                r[i] = list(reversed(l))
        return r
