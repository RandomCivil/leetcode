package leetcode

import (
	"fmt"
	"math"

	"gitlab.com/RandomCivil/leetcode/common"
)

// There is a row of n houses, where each house can be painted one of three colors: red, blue, or green. The cost of painting each house with a certain color is different. You have to paint all the houses such that no two adjacent houses have the same color.

// The cost of painting each house with a certain color is represented by an n x 3 cost matrix costs.

// For example, costs[0][0] is the cost of painting house 0 with the color red; costs[1][2] is the cost of painting house 1 with color green, and so on...
// Return the minimum cost to paint all houses.

// Example 1:

// Input: costs = [[17,2,17],[16,16,5],[14,3,19]]
// Output: 10
// Explanation: Paint house 0 into blue, paint house 1 into green, paint house 2 into blue.
// Minimum cost: 2 + 5 + 3 = 10.
// Example 2:

// Input: costs = [[7,6,2]]
// Output: 2

// Constraints:

// costs.length == n
// costs[i].length == 3
// 1 <= n <= 100
// 1 <= costs[i][j] <= 20
func minCost(costs [][]int) int {
	m, n := len(costs), len(costs[0])
	dp := make([][]int, m+1)
	for i := 0; i < m+1; i++ {
		dp[i] = make([]int, n)
	}
	for i := 1; i < m+1; i++ {
		for j := 0; j < n; j++ {
			if j-1 >= 0 && j+1 < n {
				dp[i][j] = common.Min(dp[i-1][j-1], dp[i-1][j+1]) + costs[i-1][j]
			} else if j-1 < 0 {
				dp[i][j] = common.Min(dp[i-1][j+1], dp[i-1][n-1]) + costs[i-1][j]
			} else if j+1 == n {
				dp[i][j] = common.Min(dp[i-1][j-1], dp[i-1][0]) + costs[i-1][j]
			}
		}
	}
	fmt.Println(dp)

	result := math.MaxInt
	for i := 0; i < n; i++ {
		result = common.Min(result, dp[m][i])
	}
	return result
}
