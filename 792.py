# Given a string s and an array of strings words, return the number of words[i] that is a subsequence of s.

# A subsequence of a string is a new string generated from the original string with some characters (can be none) deleted without changing the relative order of the remaining characters.

# For example, "ace" is a subsequence of "abcde".

# Example 1:

# Input: s = "abcde", words = ["a","bb","acd","ace"]
# Output: 3
# Explanation: There are three strings in words that are a subsequence of s: "a", "acd", "ace".

# Example 2:


# Input: s = "dsahjpjauf", words = ["ahjpjau","ja","ahbwzgqnuk","tnmlanowax"]
# Output: 2
class Solution:
    def numMatchingSubseq(self, s: str, words) -> int:
        print(s, words)
        r = 0
        for w in words:
            i = -1
            flag = True
            for ch in w:
                i = s.find(ch, i + 1)
                if i == -1:
                    flag = False
                    break
            if flag:
                r += 1
        return r


if __name__ == '__main__':
    s = "abcde"
    words = ["a", "bb", "acd", "ace"]
    print(Solution().numMatchingSubseq(s, words))
