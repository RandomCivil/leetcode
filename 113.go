package leetcode

import (
	"fmt"

	"gitlab.com/RandomCivil/leetcode/common"
)

//Given the root of a binary tree and an integer targetSum, return all root-to-leaf paths where each path's sum equals targetSum.

//A leaf is a node with no children.

//Example 1:

//Input: root = [5,4,8,11,null,13,4,7,2,null,null,5,1], targetSum = 22
//Output: [[5,4,11,2],[5,8,4,5]]

//Example 2:

//Input: root = [1,2,3], targetSum = 5
//Output: []

//Example 3:

//Input: root = [1,2], targetSum = 0
//Output: []

//Constraints:

//The number of nodes in the tree is in the range [0, 5000].
//-1000 <= Node.val <= 1000
//-1000 <= targetSum <= 1000
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func PathSum(root *common.TreeNode, targetSum int) [][]int {
	var result [][]int
	var dfs func(cur *common.TreeNode, path []int, sum int)
	dfs = func(cur *common.TreeNode, path []int, sum int) {
		if cur == nil {
			return
		}

		path = append(path, cur.Val)
		dfs(cur.Left, path, sum+cur.Val)
		dfs(cur.Right, path, sum+cur.Val)
		fmt.Println(cur, sum, path)
		if sum+cur.Val > targetSum {
			path = path[:len(path)-1]
			return
		}

		if cur.Left == nil && cur.Right == nil && sum+cur.Val == targetSum {
			item := make([]int, len(path))
			copy(item, path)
			result = append(result, item)
		}
		path = path[:len(path)-1]
	}
	dfs(root, make([]int, 0), 0)
	return result
}
