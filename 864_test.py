inst_864 = __import__("864")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "grid,expected",
        [
            (["@.a..", "###.#", "b.A.B"], 8),
            (["@..aA", "..B#.", "....b"], 6),
            (["@Aa"], -1),
        ],
    )
    def test_shortestPathAllKeys(self, grid, expected):
        assert inst_864.Solution().shortestPathAllKeys(grid) == expected
