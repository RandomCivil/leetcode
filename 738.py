# Given a non-negative integer n, find the largest number that is less than or equal to n with monotone increasing digits.

# (Recall that an integer has monotone increasing digits if and only if each pair of adjacent digits x and y satisfy x <= y.)

# Example 1:

# Input: n = 10
# Output: 9

# Example 2:

# Input: n = 1234
# Output: 1234

# Example 3:


# Input: n = 332
# Output: 299
class Solution:
    def monotoneIncreasingDigits(self, n: int) -> int:
        arr = list(map(int, str(n)))
        print(arr)
        size = len(arr)
        pos = size
        for i in range(size - 1, 0, -1):
            if arr[i] < arr[i - 1]:
                pos = i
                arr[i - 1] -= 1

        for i in range(pos, size):
            arr[i] = 9

        print(arr)
        return int(''.join(map(str, arr)))


if __name__ == '__main__':
    # 299
    n = 332

    # 9
    n = 10
    print(Solution().monotoneIncreasingDigits(n))
