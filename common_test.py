import pytest
import common


class TestClass:
    @pytest.mark.parametrize(
        "s,e,expected",
        [
            (1, 6, True),
            (1, 5, True),
            (2, 6, True),
            (1, 8, False),
            (3, 9, True),
        ],
    )
    def test_UnionFind(self, s, e, expected):
        l = [[1, 2], [2, 5], [5, 6], [6, 7], [3, 8], [8, 9]]
        uf = common.UnionFind(10)
        for ss, ee in l:
            uf.union(ss, ee)
        print(uf.l)
        print(uf.rank)
        assert uf.isconnected(s, e) == expected
        print(uf.l)

    @pytest.mark.parametrize(
        "nums",
        [
            ([1]),
            ([1, 2]),
            ([1, 2, 3, 4, 5]),
        ],
    )
    def test_LinkList(self, nums):
        l = common.LinkList(nums)
        root = l.build()
        assert l.traversal(root) == nums
