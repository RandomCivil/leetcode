package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestLengthOfLongestSubstringTwoDistinct(t *testing.T) {
	tests := []struct {
		s      string
		expect int
	}{
		{
			s:      "eceba",
			expect: 3,
		},
		{
			s:      "ccaabbb",
			expect: 5,
		},
		{
			s:      "ababffzzeee",
			expect: 5,
		},
	}
	Convey("TestLengthOfLongestSubstringTwoDistinct", t, func() {
		for _, tt := range tests {
			So(lengthOfLongestSubstringTwoDistinct(tt.s), ShouldEqual, tt.expect)
		}
	})
}
