inst_773 = __import__("773")
inst_773_me = __import__("773_me")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "board,expected",
        [
            ([[1, 2, 3], [4, 0, 5]], 1),
            ([[1, 2, 3], [5, 4, 0]], -1),
            ([[4, 1, 2], [5, 0, 3]], 5),
        ],
    )
    def test_slidingPuzzle(self, board, expected):
        assert inst_773_me.Solution().slidingPuzzle(board) == expected
        assert inst_773.Solution().slidingPuzzle(board) == expected
