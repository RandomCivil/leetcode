package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestCanFinish(t *testing.T) {
	tests := []struct {
		numCourses    int
		prerequisites [][]int
		expect        bool
	}{
		{
			numCourses: 5,
			prerequisites: [][]int{
				[]int{4, 3},
				[]int{0, 4},
				[]int{2, 0},
				[]int{1, 2},
				[]int{0, 1},
			},
			expect: false,
		},
		{
			numCourses: 2,
			prerequisites: [][]int{
				[]int{1, 0},
			},
			expect: true,
		},
		{
			numCourses: 2,
			prerequisites: [][]int{
				[]int{1, 0},
				[]int{0, 1},
			},
			expect: false,
		},
		{
			numCourses: 3,
			prerequisites: [][]int{
				[]int{0, 2},
				[]int{1, 2},
				[]int{2, 0},
			},
			expect: false,
		},
	}
	Convey("TestNumIslands", t, func() {
		for _, tt := range tests {
			So(canFinish(tt.numCourses, tt.prerequisites), ShouldEqual, tt.expect)
		}
	})
}
