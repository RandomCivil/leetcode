# Given an undirected graph, return true if and only if it is bipartite.

# Recall that a graph is bipartite if we can split its set of nodes into two independent subsets A and B, such that every edge in the graph has one node in A and another node in B.

# The graph is given in the following form: graph[i] is a list of indexes j for which the edge between nodes i and j exists.  Each node is an integer between 0 and graph.length - 1.  There are no self edges or parallel edges: graph[i] does not contain i, and it doesn't contain any element twice.

# Input: graph = [[1,3],[0,2],[1,3],[0,2]]
# Output: true
# Explanation: We can divide the vertices into two groups: {0, 2} and {1, 3}.

# Input: graph = [[1,2,3],[0,2],[0,1,3],[0,2]]
# Output: false
# Explanation: We cannot find a way to divide the set of nodes into two independent subsets.

# 1 <= graph.length <= 100
# 0 <= graph[i].length < 100
# 0 <= graph[i][j] <= graph.length - 1
# graph[i][j] != i
# All the values of graph[i] are unique.
# The graph is guaranteed to be undirected.

from collections import deque


class Solution:
    def isBipartite(self, graph: list[list[int]]) -> bool:
        size = len(graph)
        l = [None] * size

        def check(start):
            q = deque()
            q.append((start, 1))
            while len(q) > 0:
                cur, v = q.popleft()
                if l[cur] is not None:
                    if l[cur] != v:
                        return False
                    continue
                l[cur] = v
                for nxt in graph[cur]:
                    q.append((nxt, -v))
            print(l)
            return True

        for i in range(size):
            if l[i] is not None:
                continue
            if not check(i):
                return False
        return True

