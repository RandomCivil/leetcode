# Given a string S, we can transform every letter individually to be lowercase or uppercase to create another string.
# Return a list of all possible strings we could create. You can return the output in any order.

# Example 1:

# Input: S = "a1b2"
# Output: ["a1b2","a1B2","A1b2","A1B2"]
# Example 2:

# Input: S = "3z4"
# Output: ["3z4","3Z4"]
# Example 3:

# Input: S = "12345"
# Output: ["12345"]
# Example 4:

# Input: S = "0"
# Output: ["0"]


class Solution:
    def letterCasePermutation(self, S: str):
        print(S)
        result = []
        def dfs(cur, step):
            if step == len(S):
                result.append(cur)
                return
            print(cur, step, S[step])
            if S[step].isdigit():
                # cur += S[step]
                dfs(cur + S[step], step + 1)
            else:
                dfs(cur + S[step].lower(), step + 1)
                dfs(cur + S[step].upper(), step + 1)

        dfs('', 0)
        return result


if __name__ == '__main__':
    # S = 'a1b2'
    # S = '3z4'
    S = '0'
    print(Solution().letterCasePermutation(S))
