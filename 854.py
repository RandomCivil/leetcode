# Strings s1 and s2 are k-similar (for some non-negative integer k) if we can swap the positions of two letters in s1 exactly k times so that the resulting string equals s2.

# Given two anagrams s1 and s2, return the smallest k for which s1 and s2 are k-similar.


# Example 1:

# Input: s1 = "ab", s2 = "ba"
# Output: 1
# Explanation: The two string are 1-similar because we can use one swap to change s1 to s2: "ab" --> "ba".
# Example 2:

# Input: s1 = "abc", s2 = "bca"
# Output: 2
# Explanation: The two strings are 2-similar because we can use two swaps to change s1 to s2: "abc" --> "bac" --> "bca".


# Constraints:

# 1 <= s1.length <= 20
# s2.length == s1.length
# s1 and s2 contain only lowercase letters from the set {'a', 'b', 'c', 'd', 'e', 'f'}.
# s2 is an anagram of s1.
from collections import deque


class Solution:
    def kSimilarity(self, s1: str, s2: str) -> int:
        print(s1, s2)
        q = deque([(s1, 0)])
        size = len(s1)
        visited = set()
        while len(q) > 0:
            cur, n = q.popleft()
            if cur in visited:
                continue
            visited.add(cur)
            if cur == s2:
                return n

            # 从不匹配的地方开始交换
            i = 0
            while cur[i] == s2[i]:
                i += 1

            for j in range(i, size):
                # 直接找到cur里面下一个和s2匹配的地方，不用两个for循环
                if cur[j] == s2[i]:
                    nxt = cur[:i] + cur[j] + cur[i + 1 : j] + cur[i] + cur[j + 1 :]
                    q.append((nxt, n + 1))
        return -1
