package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestPossibleBipartition(t *testing.T) {
	tests := []struct {
		n        int
		dislikes [][]int
		expect   bool
	}{
		{
			n: 4,
			dislikes: [][]int{
				{1, 2}, {1, 3}, {2, 4},
			},
			expect: true,
		},
		{
			n: 3,
			dislikes: [][]int{
				{1, 2}, {1, 3}, {2, 3},
			},
			expect: false,
		},
		{
			n: 10,
			dislikes: [][]int{
				{1, 2}, {3, 4}, {5, 6}, {6, 7}, {8, 9}, {7, 8},
			},
			expect: true,
		},
		{
			n: 4,
			dislikes: [][]int{
				{1, 2}, {3, 4}, {2, 4}, {1, 3}, {1, 4},
			},
			expect: false,
		},
	}
	Convey("TestPossibleBipartition", t, func() {
		for _, tt := range tests {
			So(possibleBipartition(tt.n, tt.dislikes), ShouldEqual, tt.expect)
		}
	})
}
