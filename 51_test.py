inst_51 = __import__("51")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "n,expected",
        [
            (4, [[".Q..", "...Q", "Q...", "..Q."], ["..Q.", "Q...", "...Q", ".Q.."]]),
            (1, [["Q"]]),
        ],
    )
    def test_solveNQueens(self, n, expected):
        assert inst_51.Solution().solveNQueens(n) == expected
