# Given two strings s1 and s2, return true if s2 contains the permutation of s1.

# In other words, one of s1's permutations is the substring of s2.

# Example 1:

# Input: s1 = "ab", s2 = "eidbaooo"
# Output: true
# Explanation: s2 contains one permutation of s1 ("ba").

# Example 2:

# Input: s1 = "ab", s2 = "eidboaoo"
# Output: false
from collections import Counter


class Solution:
    def checkInclusion(self, s1: str, s2: str) -> bool:
        size1, size2 = len(s1), len(s2)
        c1 = Counter(s1)
        c2 = Counter(s2[0 : size1 - 1])
        for i in range(size1 - 1, size2):
            c2[s2[i]] += 1
            if c1 == c2:
                return True
            c2[s2[i - size1 + 1]] -= 1
        return False


if __name__ == "__main__":
    s1 = "ab"
    s2 = "eidbaooo"

    s1 = "ab"
    s2 = "eidboaoo"
    print(Solution().checkInclusion(s1, s2))
