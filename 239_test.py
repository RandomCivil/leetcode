inst_239 = __import__("239")


import pytest


class TestClass:
    @pytest.mark.parametrize(
        "nums,k,expected",
        [
            ([1, 3, -1, -3, 5, 3, 6, 7], 3, [3, 3, 5, 5, 6, 7]),
            ([3, 1, -1, -3, 5, 3, 6, 7], 3, [3, 1, 5, 5, 6, 7]),
        ],
    )
    def test_maxSlidingWindow(self, nums, k, expected):
        assert inst_239.Solution().maxSlidingWindow(nums, k) == expected
