# The thief has found himself a new place for his thievery again. There is only one entrance to this area, called root.

# Besides the root, each house has one and only one parent house. After a tour, the smart thief realized that all houses in this place form a binary tree. It will automatically contact the police if two directly-linked houses were broken into on the same night.

# Given the root of the binary tree, return the maximum amount of money the thief can rob without alerting the police.

# Example 1:

# Input: root = [3,2,3,null,3,null,1]
# Output: 7
# Explanation: Maximum amount of money the thief can rob = 3 + 3 + 1 = 7.

# Example 2:


# Input: root = [3,4,5,1,3,null,1]
# Output: 9
# Explanation: Maximum amount of money the thief can rob = 4 + 5 = 9.
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def rob(self, root) -> int:
        # (rob,not rob)
        def dfs(cur):
            if not cur:
                return 0, 0
            left_rob, left_not_rob = dfs(cur.left)
            right_rob, right_not_rob = dfs(cur.right)
            return cur.val + left_not_rob + right_not_rob, max(
                left_rob + right_rob,
                left_rob + right_not_rob,
                left_not_rob + right_rob,
                left_not_rob + right_not_rob,
            )

        a, b = dfs(root)
        print(a, b)
        return max(a, b)
