# Given an integer array nums of length n where all the integers of nums are in the range [1, n] and each integer appears once or twice, return an array of all the integers that appears twice.

# Example 1:

# Input: nums = [4,3,2,7,8,2,3,1]
# Output: [2,3]

# Example 2:

# Input: nums = [1,1,2]
# Output: [1]

# Example 3:


# Input: nums = [1]
# Output: []
class Solution:
    def findDuplicates(self, nums):
        r = []
        d = set()
        for n in nums:
            if n not in d:
                d.add(n)
            else:
                r.append(n)
        return r


if __name__ == '__main__':
    nums = [4, 3, 2, 7, 8, 2, 3, 1]
    print(Solution().findDuplicates(nums))
