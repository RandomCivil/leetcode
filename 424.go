package leetcode

import "fmt"

//Given a string s that consists of only uppercase English letters, you can perform at most k operations on that string.

//In one operation, you can choose any character of the string and change it to any other uppercase English character.

//Find the length of the longest sub-string containing all repeating letters you can get after performing the above operations.

//Note:
//Both the string's length and k will not exceed 104.

/*
Input:
s = "ABAB", k = 2

Output:
4

Explanation:
Replace the two 'A's with two 'B's or vice versa.
*/

/*
Input:
s = "AABABBA", k = 1

Output:
4

Explanation:
Replace the one 'A' in the middle with 'B' and form "AABBBBA".
The substring "BBBB" has the longest repeating letters, which is 4.
*/

//s, k := "AABABBA", 1
//s, k := "ABAB", 2
//s, k := "ABAA", 0
//s, k := "ABBB", 2
//s, k := "AAAA", 2

//func characterReplacement(s string, k int) int {
func CharacterReplacement(s string, k int) int {
	fmt.Println(s, k)
	if len(s) == 1 {
		return 1
	}

	diffPos := []int{0}
	for i := 0; i < len(s)-1; i++ {
		if s[i+1] != s[i] {
			diffPos = append(diffPos, i+1)
		}
	}
	fmt.Println(diffPos)

	if len(diffPos) == 0 {
		return len(s)
	}

	kn := k
	max := 0
	start, end := 0, 0
	for _, p := range diffPos {
		start, end = p, p
		kn = k
		fmt.Println(start, end)
		for ; end < len(s) && kn > 0; end++ {
			if s[end] != s[start] {
				kn--
			}
		}
		for end < len(s) && s[end] == s[start] {
			end++
		}
		if start > 0 && kn > 0 {
			start--
		}
		fmt.Println("max", start, end, end-start, kn)
		if max < end-start {
			max = end - start
		}
	}

	return max
}
