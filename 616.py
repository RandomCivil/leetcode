# You are given a string s and an array of strings words.

# You should add a closed pair of bold tag <b> and </b> to wrap the substrings in s that exist in words.

# If two such substrings overlap, you should wrap them together with only one pair of closed bold-tag.
# If two substrings wrapped by bold tags are consecutive, you should combine them.
# Return s after adding the bold tags.


# Example 1:

# Input: s = "abcxyz123", words = ["abc","123"]
# Output: "<b>abc</b>xyz<b>123</b>"
# Explanation: The two strings of words are substrings of s as following: "abcxyz123".
# We add <b> before each substring and </b> after each substring.
# Example 2:

# Input: s = "aaabbb", words = ["aa","b"]
# Output: "<b>aaabbb</b>"
# Explanation:
# "aa" appears as a substring two times: "aaabbb" and "aaabbb".
# "b" appears as a substring three times: "aaabbb", "aaabbb", and "aaabbb".
# We add <b> before each substring and </b> after each substring: "<b>a<b>a</b>a</b><b>b</b><b>b</b><b>b</b>".
# Since the first two <b>'s overlap, we merge them: "<b>aaa</b><b>b</b><b>b</b><b>b</b>".
# Since now the four <b>'s are consecuutive, we merge them: "<b>aaabbb</b>".


# Constraints:


# 1 <= s.length <= 1000
# 0 <= words.length <= 100
# 1 <= words[i].length <= 1000
# s and words[i] consist of English letters and digits.
# All the values of words are unique.
class Solution:
    def addBoldTag(self, s: str, words: list[str]) -> str:
        intervals = []
        size = len(s)
        for i in range(size):
            for w in words:
                if s[i : i + len(w)] == w:
                    intervals.append([i, i + len(w)])
        print(intervals)

        if len(intervals)==0:
            return s

        merged = [intervals[0]]
        for cs, ce in intervals[1:]:
            ls, le = merged[-1]
            if cs <= le:
                merged[-1][1] = max(le, ce)
            else:
                merged.append([cs, ce])
        print("merged", merged)

        result = ""
        last = 0
        for start, end in merged:
            result += s[last:start]
            result += "<b>" + s[start:end] + "</b>"
            last = end
        result+=s[end:]
        return result
