inst_853 = __import__("853")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "target, position, speed, expected",
        [
            (12, [10, 8, 0, 5, 3], [2, 4, 1, 1, 3], 3),
            (100, [0, 2, 4], [4, 2, 1], 1),
            (10, [2, 4], [3, 2], 1),
            (10, [3], [3], 1),
            (12, [10, 6, 3, 5, 0], [1, 2, 3, 1, 1], 4),
        ],
    )
    def test_carFleet(self, target, position, speed, expected):
        assert inst_853.Solution().carFleet(target, position, speed) == expected
