# Given a string S, return the "reversed" string where all characters that are not a letter stay in the same place, and all letters reverse their positions.

# Example 1:

# Input: "ab-cd"
# Output: "dc-ba"

# Example 2:

# Input: "a-bC-dEf-ghIj"
# Output: "j-Ih-gfE-dCba"

# Example 3:

# Input: "Test1ng-Leet=code-Q!"
# Output: "Qedo1ct-eeLg=ntse-T!"

# Note:

# S.length <= 100
# 33 <= S[i].ASCIIcode <= 122
# S doesn't contain \ or "


class Solution:
    def reverseOnlyLetters(self, S: str) -> str:
        non_letter = [ch for ch in S if ch.isalpha()]
        arr = []
        for i, ch in enumerate(S):
            if ch.isalpha():
                arr.append(non_letter.pop())
            else:
                arr.append(ch)

        return ''.join(arr)


if __name__ == '__main__':
    S = "a-bC-dEf-ghIj"
    print(Solution().reverseOnlyLetters(S))
