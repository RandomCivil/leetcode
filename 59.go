package leetcode

// Given a positive integer n, generate an n x n matrix filled with elements from 1 to n2 in spiral order.

// Example 1:

// Input: n = 3
// Output: [[1,2,3],[8,9,4],[7,6,5]]
// Example 2:

// Input: n = 1
// Output: [[1]]

// Constraints:

// 1 <= n <= 20
func generateMatrix(n int) [][]int {
	result := make([][]int, n)
	left, right := 0, n-1
	up, down := 0, n-1
	for i := 0; i < n; i++ {
		result[i] = make([]int, n)
	}
	var v int
	for v < n*n {
		for i := left; i <= right; i++ {
			result[up][i] = v + 1
			v++
		}
		for i := up + 1; i <= down; i++ {
			result[i][right] = v + 1
			v++
		}
		if up != down {
			for i := right - 1; i >= left; i-- {
				result[down][i] = v + 1
				v++
			}
		}
		if left != right {
			for i := down - 1; i > up; i-- {
				result[i][left] = v + 1
				v++
			}
		}
		left++
		right--
		up++
		down--
	}
	return result
}
