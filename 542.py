# Given a matrix consists of 0 and 1, find the distance of the nearest 0 for each cell.
# The distance between two adjacent cells is 1.
"""
Input:
[[0,0,0],
 [0,1,0],
 [0,0,0]]

Output:
[[0,0,0],
 [0,1,0],
 [0,0,0]]

Input:
[[0,0,0],
 [0,1,0],
 [1,1,1]]

Output:
[[0,0,0],
 [0,1,0],
 [1,2,1]]
"""


class Solution:
    def updateMatrix(self, matrix):
        print(matrix)
        l = []
        directions = [(1, 0), (0, -1), (-1, 0), (0, 1)]
        m = len(matrix)
        n = 0
        if m > 0:
            n = len(matrix[0])

        result = [[0]*n for _ in range(m)]
        for i in range(m):
            for j in range(n):
                if matrix[i][j] == 1:
                    l.append((i, j))

        for i, j in l:
            q = [(i, j)]
            nq = []
            has = set()
            count = 1
            while len(q) > 0:
                x, y = q[0]
                q = q[1:]
                stop = False
                for dx, dy in directions:
                    if 0 <= x+dx < m and 0 <= y+dy < n and (x+dx, y+dy) not in has:
                        if matrix[x+dx][y+dy] == 0:
                            stop = True
                            break
                        has.add((x+dx, y+dy))
                        nq.append((x+dx, y+dy))
                if stop:
                    break
                if len(q) == 0:
                    count += 1
                    q = nq
                    nq = []
            result[i][j] = count
        return result


if __name__ == '__main__':
    matrix = [[0, 0, 0],
              [0, 1, 0],
              [1, 1, 1]]
    # matrix = [[0, 0, 0],
              # [0, 1, 0],
              # [0, 0, 0]]
    # matrix=[[0,1],[1,0]]

    print(Solution().updateMatrix(matrix))
