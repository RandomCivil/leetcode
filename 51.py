# The n-queens puzzle is the problem of placing n queens on an n x n chessboard such that no two queens attack each other.

# Given an integer n, return all distinct solutions to the n-queens puzzle. You may return the answer in any order.

# Each solution contains a distinct board configuration of the n-queens' placement, where 'Q' and '.' both indicate a queen and an empty space, respectively.

# Example 1:


# Input: n = 4
# Output: [[".Q..","...Q","Q...","..Q."],["..Q.","Q...","...Q",".Q.."]]
# Explanation: There exist two distinct solutions to the 4-queens puzzle as shown above
# Example 2:

# Input: n = 1
# Output: [["Q"]]


# Constraints:


# 1 <= n <= 9
class Solution:
    def solveNQueens(self, n: int) -> list[list[str]]:
        self.result = []

        def dfs(step, path, col, diagonal1, diagonal2):
            if step == n:
                self.result.append(path.copy())
            for i in range(n):
                if col[i] or step + i in diagonal1 or step - i in diagonal2:
                    continue
                col[i] = True
                diagonal1.add(step + i)
                diagonal2.add(step - i)
                path.append("." * i + "Q" + "." * (n - i - 1))
                dfs(step + 1, path, col, diagonal1, diagonal2)
                path.pop()
                col[i] = False
                diagonal1.remove(step + i)
                diagonal2.remove(step - i)

        dfs(0, [], [False] * n, set(), set())
        return self.result
