inst_67 = __import__("67")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "a,b,expected",
        [
            ("11", "1", "100"),
            ("1010", "1011", "10101"),
        ],
    )
    def test_getFactors(self, a, b, expected):
        assert inst_67.Solution().addBinary(a, b) == expected
