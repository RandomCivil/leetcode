# Numbers can be regarded as the product of their factors.

# For example, 8 = 2 x 2 x 2 = 2 x 4.
# Given an integer n, return all possible combinations of its factors. You may return the answer in any order.

# Note that the factors should be in the range [2, n - 1].

# Example 1:

# Input: n = 1
# Output: []
# Example 2:

# Input: n = 12
# Output: [[2,6],[3,4],[2,2,3]]
# Example 3:

# Input: n = 37
# Output: []


# Constraints:


# 1 <= n <= 107

import math


class Solution:
    def getFactors(self, n: int) -> list[list[int]]:
        self.result = []

        def dfs(path):
            if len(path) == 0:
                return
            pop = path.pop()
            start = 2
            if len(path) > 0:
                start = path[-1]
            for i in range(start, math.floor(math.sqrt(pop)) + 1):
                if pop % i == 0:
                    path.extend([i, int(pop / i)])
                    self.result.append(path.copy())
                    dfs(path)
                    path.pop()
                    path.pop()
            path.append(pop)

        dfs([n])
        return self.result
