package leetcode

import "gitlab.com/RandomCivil/leetcode/common"

//Given the head of a singly linked list, reverse the list, and return the reversed list.

//Example 1:

//Input: head = [1,2,3,4,5]
//Output: [5,4,3,2,1]

//Example 2:

//Input: head = [1,2]
//Output: [2,1]

//Example 3:

//Input: head = []
//Output: []

//Constraints:

//The number of nodes in the list is the range [0, 5000].
//-5000 <= Node.val <= 5000
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func reverseList(head *common.ListNode) *common.ListNode {
	var prev *common.ListNode
	cur := head
	for cur != nil {
		t := cur.Next
		cur.Next = prev

		prev = cur
		cur = t
	}
	return prev
}
