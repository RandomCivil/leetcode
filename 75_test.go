package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestSortColors(t *testing.T) {
	tests := []struct {
		nums, expect []int
	}{
		{
			nums:   []int{2, 0, 2, 1, 1, 0},
			expect: []int{0, 0, 1, 1, 2, 2},
		},
		{
			nums:   []int{2, 0, 1},
			expect: []int{0, 1, 2},
		},
	}
	Convey("TestSortColors", t, func() {
		for _, tt := range tests {
			sortColors(tt.nums)
			So(tt.nums, ShouldResemble, tt.expect)
		}
	})
}
