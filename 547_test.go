package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestFindCircleNum(t *testing.T) {
	tests := []struct {
		isConnected [][]int
		expect      int
	}{
		{
			isConnected: [][]int{
				{1, 1, 0},
				{1, 1, 0},
				{0, 0, 1},
			},
			expect: 2,
		},
		{
			isConnected: [][]int{
				{1, 0, 0},
				{0, 1, 0},
				{0, 0, 1},
			},
			expect: 3,
		},
	}
	Convey("TestFindCircleNum", t, func() {
		for _, tt := range tests {
			So(findCircleNum(tt.isConnected), ShouldEqual, tt.expect)
		}
	})
}
