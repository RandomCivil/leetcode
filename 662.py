class Solution:
    def widthOfBinaryTree(self, root) -> int:
        q = [(root, 1)]
        q1 = []
        result = 0
        mn, mx = float("inf"), float("-inf")
        while len(q) > 0:
            cur, count = q[0]
            mx = max(mx, count)
            mn = min(mn, count)
            q = q[1:]
            if cur.left:
                q1.append((cur.left, 2 * count))
            if cur.right:
                q1.append((cur.right, 2 * count + 1))
            if len(q) == 0:
                result = max(result, mx - mn + 1)
                q = q1
                q1 = []
                mn, mx = float("inf"), float("-inf")
        return result
