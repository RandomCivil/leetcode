# Given a string s, remove duplicate letters so that every letter appears once and only once. You must make sure your result is the smallest in lexicographical order among all possible results.

# Example 1:

# Input: s = "bcabc"
# Output: "abc"

# Example 2:


# Input: s = "cbacdcbc"
# Output: "acdb"
class Solution:
    def removeDuplicateLetters(self, s: str) -> str:
        stack = []
        visited = set()
        right_index = {ch: i for i, ch in enumerate(s)}
        for i, ch in enumerate(s):
            if ch in visited:
                continue
            while len(stack) > 0:
                if ch < stack[-1] and i < right_index[stack[-1]]:
                    visited.remove(stack[-1])
                    stack.pop()
                else:
                    break

            visited.add(ch)
            stack.append(ch)

        return ''.join(stack)


if __name__ == '__main__':
    s = "bcabc"
    s = "cbacdcbc"
    print(Solution().removeDuplicateLetters(s))
