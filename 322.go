package leetcode

import (
	"fmt"
	"math"

	"gitlab.com/RandomCivil/leetcode/common"
)

//You are given an integer array coins representing coins of different denominations and an integer amount representing a total amount of money.

//Return the fewest number of coins that you need to make up that amount. If that amount of money cannot be made up by any combination of the coins, return -1.

//You may assume that you have an infinite number of each kind of coin.

//Example 1:

//Input: coins = [1,2,5], amount = 11
//Output: 3
//Explanation: 11 = 5 + 5 + 1

//Example 2:

//Input: coins = [2], amount = 3
//Output: -1

//Example 3:

//Input: coins = [1], amount = 0
//Output: 0

//Example 4:

//Input: coins = [1], amount = 1
//Output: 1

//Example 5:

//Input: coins = [1], amount = 2
//Output: 2
func CoinChange(coins []int, amount int) int {
	fmt.Println(coins)
	var (
		mn int
	)
	dp := make([]int, amount+1)
	for n := 1; n <= amount; n++ {
		mn = math.MaxInt32 - 1
		for _, c := range coins {
			if n-c >= 0 {
				mn = common.Min(mn, dp[n-c])
			}
		}
		dp[n] = mn + 1
	}
	if dp[amount] == math.MaxInt32 {
		return -1
	}
	return dp[amount]
}
