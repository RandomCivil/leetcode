package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestCanPartitionKSubsets(t *testing.T) {
	tests := []struct {
		nums   []int
		k      int
		expect bool
	}{
		{
			nums:   []int{4, 3, 2, 3, 5, 2, 1},
			k:      4,
			expect: true,
		},
		{
			nums:   []int{1, 2, 3, 4},
			k:      3,
			expect: false,
		},
		{
			nums:   []int{9, 6, 1, 8, 4, 3, 4, 1, 7, 3, 7, 4, 5, 3, 2, 3},
			k:      10,
			expect: false,
		},
		{
			nums:   []int{10, 1, 10, 9, 6, 1, 9, 5, 9, 10, 7, 8, 5, 2, 10, 8},
			k:      11,
			expect: false,
		},
		{
			nums:   []int{3, 2, 1, 3, 6, 1, 4, 8, 10, 8, 9, 1, 7, 9, 8, 1},
			k:      9,
			expect: false,
		},
	}
	Convey("TestCanPartitionKSubsets", t, func() {
		for _, tt := range tests {
			So(canPartitionKSubsets(tt.nums, tt.k), ShouldEqual, tt.expect)
		}
	})
}
