inst_881 = __import__("881")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "people, limit, expected",
        [
            ([3, 2, 2, 1], 3, 3),
            ([3, 5, 3, 4], 5, 4),
            ([2, 4], 5, 2),
            ([5, 1, 4, 2], 6, 2),
            ([3, 2, 3, 2, 2], 6, 3),
        ],
    )
    def test_numRescueBoats(self, people, limit, expected):
        assert inst_881.Solution().numRescueBoats(people, limit) == expected
