package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestNumIslands(t *testing.T) {
	tests := []struct {
		grid   [][]byte
		expect int
	}{
		{
			grid: [][]byte{
				[]byte{'1', '1', '1', '1', '0'},
				[]byte{'1', '1', '0', '1', '0'},
				[]byte{'1', '1', '0', '0', '0'},
				[]byte{'0', '0', '0', '0', '0'},
			},
			expect: 1,
		},
		{
			grid: [][]byte{
				[]byte{'1', '1', '0', '0', '0'},
				[]byte{'1', '1', '0', '0', '0'},
				[]byte{'0', '0', '1', '0', '0'},
				[]byte{'0', '0', '0', '1', '1'},
			},
			expect: 3,
		},
		{
			grid: [][]byte{
				[]byte{'1', '1', '1'},
				[]byte{'0', '1', '0'},
				[]byte{'1', '1', '1'},
			},
			expect: 1,
		},
	}
	Convey("TestNumIslands", t, func() {
		for _, tt := range tests {
			So(numIslands(tt.grid), ShouldEqual, tt.expect)
		}
	})
}
