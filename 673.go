package leetcode

import "fmt"

func FindNumberOfLIS(nums []int) int {
	fmt.Println(nums)
	size := len(nums)
	if size == 0 {
		return 0
	}
	dp := make([]int, size)
	counter := make([]int, size)
	for i := 0; i < size; i++ {
		dp[i] = 1
		counter[i] = 1
	}

	max := 0
	for i := 0; i < size; i++ {
		for j := 0; j < i; j++ {
			if nums[i] > nums[j] {
				if dp[j]+1 > dp[i] {
					dp[i] = dp[j] + 1
					counter[i] = counter[j]
				} else if dp[i] == dp[j]+1 {
					counter[i] += counter[j]
				}
			}
		}
		if dp[i] > max {
			max = dp[i]
		}
	}

	var num int
	for i, n := range counter {
		if dp[i] == max {
			num += n
		}
	}
	fmt.Println(dp, max)
	fmt.Println(counter)
	return num
}
