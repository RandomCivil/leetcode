package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestFindPivot(t *testing.T) {
	tests := []struct {
		nums   []int
		expect int
	}{
		{
			nums:   []int{4, 5, 1, 2, 3},
			expect: 2,
		},
		{
			nums:   []int{4, 5, 6, 7, 0, 1, 2},
			expect: 4,
		},
		{
			nums:   []int{5, 6, 7, 0, 1, 2, 4},
			expect: 3,
		},
		{
			nums:   []int{6, 7, 0, 1, 2, 4, 5},
			expect: 2,
		},
	}
	Convey("TestFindPivot", t, func() {
		for _, tt := range tests {
			So(findPivot(tt.nums), ShouldResemble, tt.expect)
		}
	})
}

func TestPivotSearch(t *testing.T) {
	tests := []struct {
		nums           []int
		expect, target int
	}{
		{
			nums:   []int{4, 5, 6, 7, 0, 1, 2},
			target: 0,
			expect: 4,
		},
		{
			nums:   []int{4, 5, 6, 7, 0, 1, 2},
			target: 1,
			expect: 5,
		},
		{
			nums:   []int{4, 5, 6, 7, 0, 1, 2},
			target: 2,
			expect: 6,
		},
		{
			nums:   []int{0, 1, 2, 4, 5, 6, 7},
			target: 2,
			expect: 2,
		},
		{
			nums:   []int{0, 1, 2, 4, 5, 6, 7},
			target: 6,
			expect: 5,
		},
		{
			nums:   []int{4, 5, 6, 7, 0, 1, 2},
			target: 3,
			expect: -1,
		},
		{
			nums:   []int{5, 1, 3},
			target: 2,
			expect: -1,
		},
		{
			nums:   []int{3, 1},
			target: 0,
			expect: -1,
		},
		{
			nums:   []int{5, 1, 3},
			target: 5,
			expect: 0,
		},
		{
			nums:   []int{4, 5, 1, 2, 3},
			target: 1,
			expect: 2,
		},
		{
			nums:   []int{1},
			target: 1,
			expect: 0,
		},
		{
			nums:   []int{1},
			target: 2,
			expect: -1,
		},
	}
	Convey("TestPivotSearch", t, func() {
		for _, tt := range tests {
			So(search(tt.nums, tt.target), ShouldResemble, tt.expect)
		}
	})
}
