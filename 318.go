package leetcode

import (
	"gitlab.com/RandomCivil/leetcode/common"
)

// Given a string array words, return the maximum value of length(word[i]) * length(word[j]) where the two words do not share common letters. If no such two words exist, return 0.

// Example 1:

// Input: words = ["abcw","baz","foo","bar","xtfn","abcdef"]
// Output: 16
// Explanation: The two words can be "abcw", "xtfn".
// Example 2:

// Input: words = ["a","ab","abc","d","cd","bcd","abcd"]
// Output: 4
// Explanation: The two words can be "ab", "cd".
// Example 3:

// Input: words = ["a","aa","aaa","aaaa"]
// Output: 0
// Explanation: No such pair of words.

// Constraints:

// 2 <= words.length <= 1000
// 1 <= words[i].length <= 1000
// words[i] consists only of lowercase English letters.
func maxProduct(words []string) int {
	bitMask := func(s string) int {
		var result int
		for _, ss := range s {
			result |= 1 << (ss - 97)
		}
		return result
	}
	var result int
	size := len(words)
	l := make([]int, size)
	for i, w := range words {
		l[i] = bitMask(w)
	}
	for i := 0; i < size; i++ {
		for j := i + 1; j < size; j++ {
			if l[i]&l[j] == 0 {
				result = common.Max(result, len(words[i])*len(words[j]))
			}
		}
	}
	return result
}
