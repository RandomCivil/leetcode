package leetcode

import (
	"sort"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/RandomCivil/leetcode/common"
)

func TestFlipMatchVoyage(t *testing.T) {
	tests := []struct {
		nums, voyage, expect []int
	}{
		{
			nums:   []int{1, 2, 3},
			voyage: []int{1, 3, 2},
			expect: []int{1},
		},
		{
			nums:   []int{1, 2},
			voyage: []int{2, 1},
			expect: []int{-1},
		},
		{
			nums:   []int{1, 2, 3, 4, 5, 6, 0, 0, 0, 7, 8},
			voyage: []int{1, 3, 6, 2, 4, 5, 8, 7},
			expect: []int{1, 5},
		},
	}
	Convey("TestFlipMatchVoyage", t, func() {
		for _, tt := range tests {
			root := common.BuildTree(tt.nums)
			result := flipMatchVoyage(root, tt.voyage)
			sort.Ints(result)
			So(result, ShouldResemble, tt.expect)
		}
	})
}
