inst_84 = __import__("84")
inst_84_me = __import__("84_me")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "heights,expected",
        [
            ([2, 1, 5, 6, 2, 3], 10),
            ([2, 4], 4),
            ([2], 2),
            ([1, 3], 3),
            ([3, 2], 4),
            ([2, 1, 2], 3),
        ],
    )
    def test_largestRectangleArea(self, heights, expected):
        assert inst_84_me.Solution().largestRectangleArea(heights) == expected
        assert inst_84.Solution().largestRectangleArea(heights) == expected
