import pytest


inst_1101 = __import__("1101")
inst_1101_me = __import__("1101_me")


class TestClass:
    @pytest.mark.parametrize(
        "logs,n,expected",
        [
            (
                [
                    [20190101, 0, 1],
                    [20190104, 3, 4],
                    [20190107, 2, 3],
                    [20190211, 1, 5],
                    [20190224, 2, 4],
                    [20190301, 0, 3],
                    [20190312, 1, 2],
                    [20190322, 4, 5],
                ],
                6,
                20190301,
            ),
            ([[0, 2, 0], [1, 0, 1], [3, 0, 3], [4, 1, 2], [7, 3, 1]], 4, 3),
            ([[0, 0, 1], [1, 0, 2], [2, 1, 4], [3, 5, 6]], 7, -1),
        ],
    )
    def test_earliestAcq(self, logs, n, expected):
        assert inst_1101.Solution().earliestAcq(logs, n) == expected
        assert inst_1101_me.Solution().earliestAcq(logs, n) == expected
