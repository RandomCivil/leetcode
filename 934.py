# In a given 2D binary array grid, there are two islands.  (An island is a 4-directionally connected group of 1s not connected to any other 1s.)

# Now, we may change 0s to 1s so as to connect the two islands together to form 1 island.

# Return the smallest number of 0s that must be flipped.  (It is guaranteed that the answer is at least 1.)

# Example 1:

# Input: grid = [[0,1],[1,0]]
# Output: 1

# Example 2:

# Input: grid = [[0,1,0],[0,0,0],[0,0,1]]
# Output: 2

# Example 3:

# Input: grid = [[1,1,1,1,1],[1,0,0,0,1],[1,0,1,0,1],[1,0,0,0,1],[1,1,1,1,1]]
# Output: 1

# Constraints:

# 2 <= grid.length == grid[0].length <= 100
# grid[i][j] == 0 or grid[i][j] == 1
from collections import deque


class Solution:
    def shortestBridge(self, grid: list[list[int]]) -> int:
        m, n = len(grid), len(grid[0])
        self.s = set()
        directions = [(1, 0), (-1, 0), (0, 1), (0, -1)]

        def dfs(x, y, px, py):
            if x == -1 or x == m or y == -1 or y == n:
                return
            if grid[x][y] == 0:
                self.s.add((px, py, 0))
                return
            if grid[x][y] == -1:
                return
            grid[x][y] = -1
            for xx, yy in directions:
                dfs(x + xx, y + yy, x, y)

        def first():
            for i in range(m):
                for j in range(n):
                    if grid[i][j] == 1:
                        dfs(i, j, None, None)
                        return

        first()
        print(self.s)

        visited = set()
        q = deque(self.s)
        while len(q) > 0:
            x, y, c = q.popleft()
            if grid[x][y] == 1:
                return c - 1
            for xx, yy in directions:
                if (
                    0 <= x + xx < m
                    and 0 <= y + yy < n
                    and grid[x + xx][y + yy] != -1
                    and (x + xx, y + yy) not in visited
                ):
                    visited.add((x + xx, y + yy))
                    q.append((x + xx, y + yy, c + 1))
        return 0


if __name__ == "__main__":
    # 1
    grid = [[0, 1], [1, 0]]

    # 2
    grid = [[0, 1, 0], [0, 0, 0], [0, 0, 1]]

    # 1
    grid = [
        [1, 1, 1, 1, 1],
        [1, 0, 0, 0, 1],
        [1, 0, 1, 0, 1],
        [1, 0, 0, 0, 1],
        [1, 1, 1, 1, 1],
    ]
    print(Solution().shortestBridge(grid))
