# Given string num representing a non-negative integer num, and an integer k, return the smallest possible integer after removing k digits from num.

# Example 1:

# Input: num = "1432219", k = 3
# Output: "1219"
# Explanation: Remove the three digits 4, 3, and 2 to form the new number 1219 which is the smallest.

# Example 2:

# Input: num = "10200", k = 1
# Output: "200"
# Explanation: Remove the leading 1 and the number is 200. Note that the output must not contain leading zeroes.

# Example 3:


# Input: num = "10", k = 2
# Output: "0"
# Explanation: Remove all the digits from the number and it is left with nothing which is 0.
import heapq


class Solution:
    def removeKdigits(self, num: str, k: int) -> str:
        stack = []
        for n in num:
            while len(stack) > 0 and k > 0 and stack[-1] > n:
                stack.pop()
                k -= 1
            stack.append(n)

        print(stack, k)

        remove_indexes = []
        if k > 0:
            l = [(-int(n), i) for i, n in enumerate(stack)]
            heapq.heapify(l)
            while len(l) > 0 and k > 0:
                n, i = heapq.heappop(l)
                if n == 0:
                    continue
                remove_indexes.append(i)
                k -= 1

        for i in remove_indexes:
            stack[i] = ""

        i = 0
        while i < len(stack) and stack[i] == "0":
            stack[i] = ""
            i += 1

        result = "".join(stack)
        return "0" if len(result) == 0 else result


if __name__ == "__main__":
    # 1219
    num = "1432219"
    k = 3

    # 200
    # num = "10200"
    # k = 1

    # 0
    # num = "10"
    # k = 2

    # 0
    # num = "10001"
    # k = 4

    # 0
    # num = '9'
    # k = 1

    # 11
    # num="1173"
    # k=2

    # 2
    # num = "1022"
    # k = 2
    print(Solution().removeKdigits(num, k))
