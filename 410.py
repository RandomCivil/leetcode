# Given an integer array nums and an integer k, split nums into k non-empty subarrays such that the largest sum of any subarray is minimized.

# Return the minimized largest sum of the split.

# A subarray is a contiguous part of the array.


# Example 1:

# Input: nums = [7,2,5,10,8], k = 2
# Output: 18
# Explanation: There are four ways to split nums into two subarrays.
# The best way is to split it into [7,2,5] and [10,8], where the largest sum among the two subarrays is only 18.
# Example 2:

# Input: nums = [1,2,3,4,5], k = 2
# Output: 9
# Explanation: There are four ways to split nums into two subarrays.
# The best way is to split it into [1,2,3] and [4,5], where the largest sum among the two subarrays is only 9.


# Constraints:


# 1 <= nums.length <= 1000
# 0 <= nums[i] <= 106
# 1 <= k <= min(50, nums.length)
class Solution:
    def splitArray(self, nums: list[int], k: int) -> int:
        print(nums)
        hi, lo = 0, 0
        for n in nums:
            hi += n
            lo = max(lo, n)

        def check(target):
            c = 0
            s = 0
            for n in nums:
                s += n
                if s > target:
                    c += 1
                    s = n
            return c + 1 <= k

        result = -1
        while lo <= hi:
            mid = (lo + hi) // 2
            if check(mid):
                result = mid
                hi = mid - 1
            else:
                lo = mid + 1
        return result
