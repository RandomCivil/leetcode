# Given an integer array nums, return the number of longest increasing subsequences.

# Notice that the sequence has to be strictly increasing.

# Example 1:

# Input: nums = [1,3,5,4,7]
# Output: 2
# Explanation: The two longest increasing subsequences are [1, 3, 4, 7] and [1, 3, 5, 7].
# Example 2:

# Input: nums = [2,2,2,2,2]
# Output: 5
# Explanation: The length of the longest increasing subsequence is 1, and there are 5 increasing subsequences of length 1, so output 5.


# Constraints:


# 1 <= nums.length <= 2000
# -106 <= nums[i] <= 106
class Solution:
    def findNumberOfLIS(self, nums: list[int]) -> int:
        size = len(nums)
        dp = [[0, 0]] * size
        dp[0] = [1, 1]
        all_max_len = 1
        for i in range(1, size):
            max_len = 1
            max_times = 1
            for j in range(i):
                prev_times, prev_len = dp[j]
                if nums[i] > nums[j]:
                    if prev_len + 1 > max_len:
                        max_len = prev_len + 1
                        max_times = prev_times
                    elif prev_len + 1 == max_len:
                        max_len = prev_len + 1
                        max_times += prev_times
            dp[i] = [max_times, max_len]
            all_max_len = max(all_max_len, max_len)
        print(dp, all_max_len)
        return sum([n for n, l in dp if l == all_max_len])
