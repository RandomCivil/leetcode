# Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.
# An input string is valid if:
# Open brackets must be closed by the same type of brackets.
# Open brackets must be closed in the correct order.

# Example 1:

# Input: s = "()"
# Output: true

# Example 2:

# Input: s = "()[]{}"
# Output: true

# Example 3:

# Input: s = "(]"
# Output: false

# Example 4:

# Input: s = "([)]"
# Output: false

# Example 5:

# Input: s = "{[]}"
# Output: true

# Constraints:

# 1 <= s.length <= 104
# s consists of parentheses only '()[]{}'.


class Solution:
    def isValid(self, s: str) -> bool:
        stack = []
        check = {')': '(', '}': '{', ']': '['}
        for ch in s:
            if len(stack) > 0 and ch in check:
                if check[ch] == stack[-1]:
                    stack.pop()
                else:
                    break
            else:
                stack.append(ch)
        return True if len(stack) == 0 else False


if __name__ == '__main__':
    s = '(()){[]}'
    s = '(())[}]'
    print(Solution().isValid(s))
