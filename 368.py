# Given a set of distinct positive integers nums, return the largest subset answer such that every pair (answer[i], answer[j]) of elements in this subset satisfies:

# answer[i] % answer[j] == 0, or
# answer[j] % answer[i] == 0
# If there are multiple solutions, return any of them.

# Example 1:

# Input: nums = [1,2,3]
# Output: [1,2]
# Explanation: [1,3] is also accepted.

# Example 2:


# Input: nums = [1,2,4,8]
# Output: [1,2,4,8]
class Solution:
    def largestDivisibleSubset(self, nums):
        size = len(nums)
        nums.sort()
        dp = [[nums[0]]]
        for i in range(1, size):
            mx = []
            for j in range(i):
                if nums[i] % nums[j] == 0 or nums[j] % nums[i] == 0:
                    if len(dp[j]) > len(mx):
                        mx = dp[j]
            dp.append(mx + [nums[i]])
        print(dp)
        return max(dp, key=len)


if __name__ == "__main__":
    nums = [1, 2, 3]
    # nums = [1,2,4,8]

    # [1,2,4,8]
    nums = [1, 2, 4, 8, 3, 6, 9, 10]

    # [3]
    # nums = [3, 17]
    print(Solution().largestDivisibleSubset(nums))
