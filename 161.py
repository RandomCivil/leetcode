# Given two strings s and t, return true if they are both one edit distance apart, otherwise return false.

# A string s is said to be one distance apart from a string t if you can:

# Insert exactly one character into s to get t.
# Delete exactly one character from s to get t.
# Replace exactly one character of s with a different character to get t.


# Example 1:

# Input: s = "ab", t = "acb"
# Output: true
# Explanation: We can insert 'c' into s to get t.
# Example 2:

# Input: s = "", t = ""
# Output: false
# Explanation: We cannot get t from s by only one step.


# Constraints:

# 0 <= s.length, t.length <= 104
# s and t consist of lowercase letters, uppercase letters, and digits.
class Solution:
    def isOneEditDistance(self, s: str, t: str) -> bool:
        size1, size2 = len(s), len(t)
        if size1 > size2:
            return self.isOneEditDistance(t, s)
        # len(s)<len(t)
        if size2 - size1 > 1:
            return False
        for i in range(size1):
            if s[i] != t[i]:
                if size1 == size2:  # replace index i
                    return s[i + 1 :] == t[i + 1 :]
                else:  # s is one char less than t.s add t[i]
                    return s[i:] == t[i + 1 :]
        # s==t[:-1]
        return size1 + 1 == size2


if __name__ == "__main__":
    s, t = "ab", "acb"
    print(Solution().isOneEditDistance(s, t))
