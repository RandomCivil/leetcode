# A bus has n stops numbered from 0 to n - 1 that form a circle. We know the distance between all pairs of neighboring stops where distance[i] is the distance between the stops number i and (i + 1) % n.
# The bus goes along both directions i.e. clockwise and counterclockwise.
# Return the shortest distance between the given start and destination stops.

# Example 1:

# Input: distance = [1,2,3,4], start = 0, destination = 1
# Output: 1
# Explanation: Distance between 0 and 1 is 1 or 9, minimum is 1.

# Example 2:

# Input: distance = [1,2,3,4], start = 0, destination = 2
# Output: 3
# Explanation: Distance between 0 and 2 is 3 or 7, minimum is 3.

# Example 3:


# Input: distance = [1,2,3,4], start = 0, destination = 3
# Output: 4
# Explanation: Distance between 0 and 3 is 6 or 4, minimum is 4.
class Solution:
    def distanceBetweenBusStops(self, distance, start: int,
                                destination: int) -> int:
        forward = backward = 0
        size = len(distance)
        i = start
        while i != destination:
            # print('forward', i)
            forward += distance[i]
            i += 1
            if i == size:
                i = 0

        i = size - 1 if start == 0 else start - 1
        destination = size-1 if destination == 0 else destination - 1
        while i != destination:
            # print('backward', i)
            backward += distance[i]
            i -= 1
            if i == -1:
                i = size - 1

        print(forward, backward)
        return min(forward, backward)


if __name__ == '__main__':
    distance = [1, 2, 3, 4]
    start = 0
    destination = 2

    start = 2
    destination = 0

    print(Solution().distanceBetweenBusStops(distance, start, destination))
