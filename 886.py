# We want to split a group of n people (labeled from 1 to n) into two groups of any size. Each person may dislike some other people, and they should not go into the same group.

# Given the integer n and the array dislikes where dislikes[i] = [ai, bi] indicates that the person labeled ai does not like the person labeled bi, return true if it is possible to split everyone into two groups in this way.

# Example 1:

# Input: n = 4, dislikes = [[1,2],[1,3],[2,4]]
# Output: true
# Explanation: The first group has [1,4], and the second group has [2,3].
# Example 2:

# Input: n = 3, dislikes = [[1,2],[1,3],[2,3]]
# Output: false
# Explanation: We need at least 3 groups to divide them. We cannot put them in two groups.


# Constraints:

# 1 <= n <= 2000
# 0 <= dislikes.length <= 104
# dislikes[i].length == 2
# 1 <= ai < bi <= n
# All the pairs of dislikes are unique.
from collections import defaultdict, deque


class Solution:
    def possibleBipartition(self, n: int, dislikes) -> bool:
        l = [None] * (n + 1)
        d = defaultdict(list)
        for s, e in dislikes:
            d[s].append(e)
            d[e].append(s)

        def bfs(start):
            q = deque([(start, None, 1)])
            while len(q) > 0:
                cur, parent, v = q.popleft()
                if l[cur] is not None:
                    if l[cur] != v:
                        return False
                    continue
                l[cur] = v
                for nxt in d[cur]:
                    if nxt == parent:
                        continue
                    q.append((nxt, cur, -v))
            return True

        for i in range(1, n + 1):
            if l[i] is not None:
                continue
            if not bfs(i):
                return False
        return True
