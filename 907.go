package leetcode

import "fmt"

// Given an array of integers arr, find the sum of min(b), where b ranges over every (contiguous) subarray of arr. Since the answer may be large, return the answer modulo 109 + 7.

// Example 1:

// Input: arr = [3,1,2,4]
// Output: 17
// Explanation:
// Subarrays are [3], [1], [2], [4], [3,1], [1,2], [2,4], [3,1,2], [1,2,4], [3,1,2,4].
// Minimums are 3, 1, 2, 4, 1, 1, 2, 1, 1, 1.
// Sum is 17.
// Example 2:

// Input: arr = [11,81,94,43,3]
// Output: 444

// Constraints:

// 1 <= arr.length <= 3 * 104
// 1 <= arr[i] <= 3 * 104
func sumSubarrayMins(arr []int) int {
	fmt.Println(arr)
	size := len(arr)
	dp := make([]int, size)
	mod := 1000000000 + 7
	var stack []int
	var result int
	for i, n := range arr {
		// stack asc
		for len(stack) > 0 && arr[stack[len(stack)-1]] >= n {
			stack = stack[:len(stack)-1]
		}
		fmt.Println(stack)

		if len(stack) > 0 {
			prev := stack[len(stack)-1]
			dp[i] = dp[prev] + (i-prev)*n
		} else {
			dp[i] = (i + 1) * n
		}
		result += dp[i]
		stack = append(stack, i)
	}
	return result % mod
}
