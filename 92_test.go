package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/RandomCivil/leetcode/common"
)

func TestReverseBetween(t *testing.T) {
	tests := []struct {
		nums        []int
		left, right int
		expect      []int
	}{
		{
			nums:   []int{1, 2, 3, 4, 5},
			left:   2,
			right:  4,
			expect: []int{1, 4, 3, 2, 5},
		},
		{
			nums:   []int{3, 4, 5},
			left:   1,
			right:  2,
			expect: []int{4, 3, 5},
		},
		{
			nums:   []int{3, 4, 5},
			left:   2,
			right:  3,
			expect: []int{3, 5, 4},
		},
		{
			nums:   []int{3, 4, 5},
			left:   1,
			right:  3,
			expect: []int{5, 4, 3},
		},
	}
	Convey("TestReverseBetween", t, func() {
		for _, tt := range tests {
			l := common.LinkList{Nums: tt.nums}
			So(l.Traversal(reverseBetween1(l.Build(), tt.left, tt.right)), ShouldResemble, tt.expect)
			So(l.Traversal(reverseBetween(l.Build(), tt.left, tt.right)), ShouldResemble, tt.expect)
		}
	})
}
