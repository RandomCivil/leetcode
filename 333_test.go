package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/RandomCivil/leetcode/common"
)

func TestLargestBSTSubtree(t *testing.T) {
	tests := []struct {
		nums   []int
		expect int
	}{
		{
			nums:   []int{10, 5, 15, 1, 8, 0, 7},
			expect: 3,
		},
		{
			nums:   []int{4, 2, 7, 2, 3, 5, 0, 2, 0, 0, 0, 0, 0, 1},
			expect: 2,
		},
		{
			nums:   []int{},
			expect: 0,
		},
	}
	Convey("TestLargestBSTSubtree1", t, func() {
		for _, tt := range tests {
			root := common.BuildTree(tt.nums)
			So(largestBSTSubtree(root), ShouldEqual, tt.expect)
			So(largestBSTSubtree1(root), ShouldEqual, tt.expect)
		}
	})
}
