package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestPermuteUnique(t *testing.T) {
	tests := []struct {
		nums   []int
		expect [][]int
	}{
		{
			nums: []int{1, 1, 2},
			expect: [][]int{
				[]int{1, 1, 2},
				[]int{1, 2, 1},
				[]int{2, 1, 1},
			},
		},
		{
			nums: []int{1, 3, 2},
			expect: [][]int{
				[]int{1, 2, 3},
				[]int{1, 3, 2},
				[]int{2, 1, 3},
				[]int{2, 3, 1},
				[]int{3, 1, 2},
				[]int{3, 2, 1},
			},
		},
		{
			nums: []int{1},
			expect: [][]int{
				[]int{1},
			},
		},
	}
	Convey("TestPermuteUnique", t, func() {
		for _, tt := range tests {
			So(permuteUnique(tt.nums), ShouldResemble, tt.expect)
		}
	})
}
