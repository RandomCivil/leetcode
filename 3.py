# Given a string s, find the length of the longest substring without repeating characters.

# Example 1:

# Input: s = "abcabcbb"
# Output: 3
# Explanation: The answer is "abc", with the length of 3.
# Example 2:

# Input: s = "bbbbb"
# Output: 1
# Explanation: The answer is "b", with the length of 1.
# Example 3:

# Input: s = "pwwkew"
# Output: 3
# Explanation: The answer is "wke", with the length of 3.
# Notice that the answer must be a substring, "pwke" is a subsequence and not a substring.
# Example 4:


# Input: s = ""
# Output: 0
from collections import Counter


class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        size = len(s)
        c = Counter()
        res = 0
        i = 0
        for j in range(size):
            c[s[j]] += 1
            if len(c) < j - i + 1:  # 有重复
                c[s[i]] -= 1
                if c[s[i]] == 0:
                    del c[s[i]]
                i += 1
            else:
                res = max(res, j - i + 1)
        return res
