# You are given a list of non-negative integers, a1, a2, ..., an, and a target, S. Now you have 2 symbols + and -. For each integer, you should choose one from + and - as its new symbol.
# Find out how many ways to assign symbols to make sum of integers equal to target S.
"""
Input: nums is [1, 1, 1, 1, 1], S is 3.
Output: 5
Explanation:

-1+1+1+1+1 = 3
+1-1+1+1+1 = 3
+1+1-1+1+1 = 3
+1+1+1-1+1 = 3
+1+1+1+1-1 = 3

There are 5 ways to assign symbols to make the sum of nums be target 3.
"""


class Solution:
    def findTargetSumWays(self, nums, target: int) -> int:
        prefix = 1000
        size = len(nums)
        dp = [[0] * (2 * prefix + 2000) for _ in range(size)]
        dp[0][prefix + nums[0]] += 1
        dp[0][prefix - nums[0]] += 1

        for i in range(1, size):
            for j in range(2 * prefix):
                dp[i][j] += dp[i - 1][j - nums[i]] + dp[i - 1][j + nums[i]]
        return dp[-1][prefix + target]


if __name__ == "__main__":
    nums = [1, 1, 1, 1, 1]
    S = 3
    # 6638
    nums = [34, 16, 5, 38, 20, 20, 8, 43, 3, 46, 24, 12, 28, 19, 22, 28, 9, 46, 25, 36]
    S = 0
    print(Solution().findTargetSumWays(nums, S))
