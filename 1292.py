# Given a m x n matrix mat and an integer threshold. Return the maximum side-length of a square with a sum less than or equal to threshold or return 0 if there is no such square.

# Example 1:

# Input: mat = [[1,1,3,2,4,3,2],[1,1,3,2,4,3,2],[1,1,3,2,4,3,2]], threshold = 4
# Output: 2
# Explanation: The maximum side length of square with sum less than 4 is 2 as shown.

# Example 2:

# Input: mat = [[2,2,2,2,2],[2,2,2,2,2],[2,2,2,2,2],[2,2,2,2,2],[2,2,2,2,2]], threshold = 1
# Output: 0

# Example 3:

# Input: mat = [[1,1,1,1],[1,0,0,0],[1,0,0,0],[1,0,0,0]], threshold = 6
# Output: 3

# Example 4:

# Input: mat = [[18,70],[61,1],[25,85],[14,40],[11,96],[97,96],[63,45]], threshold = 40184
# Output: 2


class Solution:
    def maxSideLength(self, mat, threshold: int) -> int:
        r = 0
        m, n = len(mat), len(mat[0])
        prefix = [[0] * (n + 1) for _ in range(m + 1)]
        for i in range(1, m + 1):
            for j in range(1, n + 1):
                prefix[i][j] = prefix[i-1][j]+prefix[i][j-1] - \
                    prefix[i-1][j-1]+mat[i-1][j-1]

                lo = 1
                hi = min(i, j)
                while lo <= hi:
                    mid = (lo + hi) // 2
                    cur = prefix[i][j]-prefix[i-mid][j] - \
                        prefix[i][j-mid]+prefix[i-mid][j-mid]
                    if cur <= threshold:
                        r = max(r, mid)
                        lo = mid + 1
                    else:
                        hi = mid - 1

        return r


if __name__ == '__main__':
    # 2
    mat = [[1, 1, 3, 2, 4, 3, 2], [1, 1, 3, 2, 4, 3, 2], [1, 1, 3, 2, 4, 3, 2]]
    threshold = 4

    # 2
    mat = [[18, 70], [61, 1], [25, 85], [14, 40], [11, 96], [97, 96], [63, 45]]
    threshold = 40184
    print(Solution().maxSideLength(mat, threshold))
