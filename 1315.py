# Given a binary tree, return the sum of values of nodes with even-valued grandparent.  (A grandparent of a node is the parent of its parent, if it exists.)

# If there are no nodes with an even-valued grandparent, return 0.

# Example 1:


# Input: root = [6,7,8,2,7,1,3,9,null,1,4,null,null,null,5]
# Output: 18
# Explanation: The red nodes are the nodes with even-value grandparent while the blue nodes are the even-value grandparents.
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def sumEvenGrandparent(self, root) -> int:
        self.r = 0

        def dfs(cur, parent):
            if not cur:
                return

            if parent and parent.val % 2 == 0:
                if cur.left:
                    self.r += cur.left.val
                if cur.right:
                    self.r += cur.right.val

            dfs(cur.left, cur)
            dfs(cur.right, cur)

        dfs(root, None)

        return self.r
