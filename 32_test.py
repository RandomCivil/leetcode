inst_32 = __import__("32")
inst_32_me = __import__("32_me")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "s,expected",
        [
            (")()())", 4),
            ("(()", 2),
            ("(())(()(())", 6),
            (")()())()()(", 4),
            ("(())(()", 4),
        ],
    )
    def test_longestValidParentheses(self, s, expected):
        assert inst_32_me.Solution().longestValidParentheses(s) == expected
        assert inst_32.Solution().longestValidParentheses(s) == expected
