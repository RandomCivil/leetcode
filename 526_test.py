inst_526 = __import__("526")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "n,expected",
        [
            (2, 2),
            (1, 1),
            (3, 3),
            (4, 8),
            (5, 10),
        ],
    )
    def test_countArrangement(self, n, expected):
        assert inst_526.Solution().countArrangement(n) == expected
