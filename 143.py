# You are given the head of a singly linked-list. The list can be represented as:

# L0 → L1 → … → Ln - 1 → Ln
# Reorder the list to be on the following form:

# L0 → Ln → L1 → Ln - 1 → L2 → Ln - 2 → …
# You may not modify the values in the list's nodes. Only nodes themselves may be changed.


# Example 1:


# Input: head = [1,2,3,4]
# Output: [1,4,2,3]
# Example 2:


# Input: head = [1,2,3,4,5]
# Output: [1,5,2,4,3]


# Constraints:


# The number of nodes in the list is in the range [1, 5 * 104].
# 1 <= Node.val <= 1000
# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def reorderList(self, head: Optional[ListNode]) -> None:
        """
        Do not return anything, modify head in-place instead.
        """

        def middle(head):
            prev = None
            slow, fast = head, head
            while fast and fast.next:
                prev = slow
                slow = slow.next
                fast = fast.next.next
            return prev

        def reverse(head):
            prev = None
            while head:
                t = head.next
                head.next = prev
                prev = head
                head = t
            return prev

        mid = middle(head)
        rhead = None
        if mid:
            rhead = reverse(mid.next)
            mid.next = None

        root = head
        prev = None
        while head and rhead:
            t1 = head.next
            t2 = rhead.next
            head.next = rhead
            rhead.next = t1
            prev = rhead
            head = t1
            rhead = t2

        if rhead:
            prev.next = rhead
        return root
