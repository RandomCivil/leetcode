#! /usr/bin/env python3

# Given a non-empty integer array, find the minimum number of moves required to make all array elements equal, where a move is incrementing a selected element by 1 or decrementing a selected element by 1.
"""
Input:
[1,2,3]

Output:
2

Explanation:
Only two moves are needed (remember each move increments or decrements one element):

[1,2,3]  =>  [2,2,3]  =>  [2,2,2]
"""

import math


class Solution:
    def minMoves2(self, nums) -> int:
        result = 0
        nums = sorted(nums)
        size = len(nums)
        if size % 2 != 0:
            target = nums[size // 2]
        else:
            target = (nums[int(size / 2)] + nums[int(size / 2) - 1]) / 2
        for n in nums:
            result += abs(target - n)
        return int(result)


if __name__ == "__main__":
    nums = [1, 3, 2]
    print(Solution().minMoves2(nums))
