# Given an array of meeting time intervals intervals where intervals[i] = [starti, endi], return the minimum number of conference rooms required.

# Example 1:

# Input: intervals = [[0,30],[5,10],[15,20]]
# Output: 2
# Example 2:

# Input: intervals = [[7,10],[2,4]]
# Output: 1


# Constraints:


# 1 <= intervals.length <= 104
# 0 <= starti < endi <= 106
from collections import defaultdict


class Solution:
    def minMeetingRooms(self, intervals) -> int:
        d = defaultdict(int)
        for start, end in intervals:
            for i in range(start, end):
                d[i] += 1
        print(d)
        return max(d.values())


if __name__ == "__main__":
    intervals = [[0, 12], [5, 13], [11, 20]]
    print(Solution().minMeetingRooms(intervals))
