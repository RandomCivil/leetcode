# Given a binary tree, return all duplicate subtrees. For each kind of duplicate subtrees, you only need to return the root node of any one of them.
# Two trees are duplicate if they have the same structure with same node values.
from collections import defaultdict


class Solution:
    def findDuplicateSubtrees(self, root):
        self.result = []
        self.d = defaultdict(int)

        def dfs(cur):
            v = str(cur.val)
            if cur.left:
                v += dfs(cur.left)

            if cur.right:
                if not cur.left:
                    v += "()"
                v += dfs(cur.right)

            if self.d[v] == 1:
                self.result.append(cur)

            self.d[v] += 1
            return "(" + v + ")"

        dfs(root)
        return list(self.result)
