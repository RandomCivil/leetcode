inst_333 = __import__("333")


import pytest, common


class TestClass:
    @pytest.mark.parametrize(
        "nums, expected",
        [
            ([10, 5, 15, 1, 8, 0, 7], 3),
            ([4, 2, 7, 2, 3, 5, 0, 2, 0, 0, 0, 0, 0, 1], 2),
            ([], 0),
            ([1, 2], 1),
        ],
    )
    def test_largestBSTSubtree(self, nums, expected):
        assert (
            inst_333.Solution().largestBSTSubtree(common.build_tree(nums)) == expected
        )
