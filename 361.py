# Given an m x n matrix grid where each cell is either a wall 'W', an enemy 'E' or empty '0', return the maximum enemies you can kill using one bomb. You can only place the bomb in an empty cell.

# The bomb kills all the enemies in the same row and column from the planted point until it hits the wall since it is too strong to be destroyed.

# Example 1:


# Input: grid = [["0","E","0","0"],["E","0","W","E"],["0","E","0","0"]]
# Output: 3
# Example 2:


# Input: grid = [["W","W","W"],["0","0","0"],["E","E","E"]]
# Output: 1


# Constraints:


# m == grid.length
# n == grid[i].length
# 1 <= m, n <= 500
# grid[i][j] is either 'W', 'E', or '0'.
class Solution:
    def maxKilledEnemies(self, grid) -> int:
        m, n = len(grid), len(grid[0])
        dp = [[0] * n for _ in range(m)]
        enemies = []
        for i in range(m):
            for j in range(n):
                if grid[i][j] == "E":
                    enemies.append((i, j))
        print(enemies)

        def cal(x, y):
            result = 0
            for i in range(y - 1, -1, -1):
                if grid[x][i] == "E":
                    continue
                if grid[x][i] == "W":
                    break
                dp[x][i] += 1
                result = max(result, dp[x][i])
            for i in range(y + 1, n):
                if grid[x][i] == "E":
                    continue
                if grid[x][i] == "W":
                    break
                dp[x][i] += 1
                result = max(result, dp[x][i])

            for i in range(x - 1, -1, -1):
                if grid[i][y] == "E":
                    continue
                if grid[i][y] == "W":
                    break
                dp[i][y] += 1
                result = max(result, dp[i][y])
            for i in range(x + 1, m):
                if grid[i][y] == "E":
                    continue
                if grid[i][y] == "W":
                    break
                dp[i][y] += 1
                result = max(result, dp[i][y])
            return result

        result = 0
        for x, y in enemies:
            result = max(result, cal(x, y))
        print(dp)
        return result


if __name__ == "__main__":
    # 3
    grid = [["0", "E", "0", "0"], ["E", "0", "W", "E"], ["0", "E", "0", "0"]]
    print(Solution().maxKilledEnemies(grid))
