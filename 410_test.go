package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestSplitArray(t *testing.T) {
	tests := []struct {
		nums   []int
		k      int
		expect int
	}{
		{
			nums:   []int{7, 2, 5, 10, 8},
			k:      2,
			expect: 18,
		},
		{
			nums:   []int{1, 2, 3, 4, 5},
			k:      2,
			expect: 9,
		},
	}
	Convey("TestSplitArray", t, func() {
		for _, tt := range tests {
			So(splitArray(tt.nums, tt.k), ShouldEqual, tt.expect)
		}
	})
}
