inst_990 = __import__("990")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "equations, expected",
        [
            (["a==b", "b!=a"], False),
            (["b==a", "a==b"], True),
            (["b==a", "c==b", "c!=a"], False),
            (["b==a", "c==b", "c==a"], True),
        ],
    )
    def test_equationsPossible(self, equations, expected):
        assert inst_990.Solution().equationsPossible(equations) == expected
