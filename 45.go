package leetcode

import "fmt"

//Given an array of non-negative integers nums, you are initially positioned at the first index of the array.
//Each element in the array represents your maximum jump length at that position.
//Your goal is to reach the last index in the minimum number of jumps.
//You can assume that you can always reach the last index.

//Example 1:

//Input: nums = [2,3,1,1,4]
//Output: 2
//Explanation: The minimum number of jumps to reach the last index is 2. Jump 1 step from index 0 to 1, then 3 steps to the last index.

//Example 2:

//Input: nums = [2,3,0,1,4]
//Output: 2
func Jump(nums []int) int {
	fmt.Println(nums)
	r := 0
	size := len(nums)
	var mx, i int
	for i < size-1 {
		if i+nums[i] >= size-1 {
			r++
			break
		}
		mx = i + 1
		for j := i + 1; j <= i+nums[i]; j++ {
			if j < size && mx < size && j+nums[j] >= mx+nums[mx] {
				mx = j
			}
		}
		fmt.Println(i, mx, nums[mx])
		i = mx
		r++
	}
	return r
}
