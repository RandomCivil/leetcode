package leetcode

import "fmt"

// There are a total of numCourses courses you have to take, labeled from 0 to numCourses - 1. You are given an array prerequisites where prerequisites[i] = [ai, bi] indicates that you must take course bi first if you want to take course ai.

// For example, the pair [0, 1], indicates that to take course 0 you have to first take course 1.
// Return true if you can finish all courses. Otherwise, return false.

// Example 1:

// Input: numCourses = 2, prerequisites = [[1,0]]
// Output: true
// Explanation: There are a total of 2 courses to take.
// To take course 1 you should have finished course 0. So it is possible.
// Example 2:

// Input: numCourses = 2, prerequisites = [[1,0],[0,1]]
// Output: false
// Explanation: There are a total of 2 courses to take.
// To take course 1 you should have finished course 0, and to take course 0 you should also have finished course 1. So it is impossible.

// Constraints:

// 1 <= numCourses <= 2000
// 0 <= prerequisites.length <= 5000
// prerequisites[i].length == 2
// 0 <= ai, bi < numCourses
// All the pairs prerequisites[i] are unique.
func canFinish(numCourses int, prerequisites [][]int) bool {
	d := make(map[int][]int)
	for _, p := range prerequisites {
		next, cur := p[0], p[1]
		if _, ok := d[cur]; !ok {
			d[cur] = []int{next}
		} else {
			d[cur] = append(d[cur], next)
		}
	}
	fmt.Println(d)

	cache := make(map[int]bool)
	var checkCircle func(cur int, visited map[int]struct{}) bool
	checkCircle = func(cur int, visited map[int]struct{}) bool {
		if _, ok := cache[cur]; ok {
			return false
		}
		if _, ok := visited[cur]; ok {
			return true
		}
		visited[cur] = struct{}{}

		for _, next := range d[cur] {
			if checkCircle(next, visited) {
				return true
			}
		}
		cache[cur] = true
		return false
	}

	for n := 0; n < numCourses; n++ {
		r := checkCircle(n, make(map[int]struct{}))
		if r {
			return false
		}
	}
	return true
}
