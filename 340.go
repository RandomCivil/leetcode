package leetcode

import "gitlab.com/RandomCivil/leetcode/common"

// Given a string s and an integer k, return the length of the longest substring of s that contains at most k distinct characters.

// Example 1:

// Input: s = "eceba", k = 2
// Output: 3
// Explanation: The substring is "ece" with length 3.
// Example 2:

// Input: s = "aa", k = 1
// Output: 2
// Explanation: The substring is "aa" with length 2.

// Constraints:

// 1 <= s.length <= 5 * 104
// 0 <= k <= 50
func lengthOfLongestSubstringKDistinct(s string, k int) int {
	size := len(s)
	counter := make(map[string]int)
	var result int
	left := 0
	for right := 0; right < size; right++ {
		counter[string(s[right])]++
		if len(counter) > k {
			counter[string(s[left])]--
			if counter[string(s[left])] == 0 {
				delete(counter, string(s[left]))
			}
			left++
		} else {
			result = common.Max(result, right-left+1)
		}
	}
	return result
}
