# Given an integer array arr, and an integer target, return the number of tuples i, j, k such that i < j < k and arr[i] + arr[j] + arr[k] == target.

# As the answer can be very large, return it modulo 109 + 7.

# Example 1:

# Input: arr = [1,1,2,2,3,3,4,4,5,5], target = 8
# Output: 20
# Explanation:
# Enumerating by the values (arr[i], arr[j], arr[k]):
# (1, 2, 5) occurs 8 times;
# (1, 3, 4) occurs 8 times;
# (2, 2, 4) occurs 2 times;
# (2, 3, 3) occurs 2 times.

# Example 2:

# Input: arr = [1,1,2,2,2,2], target = 5
# Output: 12
# Explanation:
# arr[i] = 1, arr[j] = arr[k] = 2 occurs 12 times:
# We choose one 1 from [1,1] in 2 ways,
# and two 2s from [2,2,2,2] in 6 ways.

# Constraints:

# 3 <= arr.length <= 3000
# 0 <= arr[i] <= 100
# 0 <= target <= 300
from collections import Counter


class Solution:
    def threeSumMulti(self, arr: list[int], target: int) -> int:
        mod = 10**9 + 7
        result = 0
        c = Counter(arr)

        def cal(a, b):
            result = 1
            for i in range(b - a + 1, b + 1):
                result *= i
            for i in range(1, a + 1):
                result /= i
            return int(result)

        size = len(arr)
        arr.sort()
        for i in range(size - 2):
            if i > 0 and arr[i] == arr[i - 1]:
                continue
            l, r = i + 1, size - 1
            while l < r:
                s = arr[i] + arr[l] + arr[r]
                if s > target:
                    r -= 1
                elif s < target:
                    l += 1
                else:
                    if arr[i] != arr[l] != arr[r]:
                        result += (
                            cal(1, c[arr[i]]) * cal(1, c[arr[l]]) * cal(1, c[arr[r]])
                        )
                    if arr[i] == arr[l] != arr[r]:
                        result += cal(2, c[arr[i]]) * cal(1, c[arr[r]])
                    if arr[i] != arr[l] == arr[r]:
                        result += cal(1, c[arr[i]]) * cal(2, c[arr[r]])
                    if arr[i] == arr[l] == arr[r]:
                        result += cal(3, c[arr[i]])

                    while l + 1 < size and arr[l] == arr[l + 1]:
                        l += 1
                    while r - 1 >= 0 and arr[r] == arr[r - 1]:
                        r -= 1
                    l += 1
                    r -= 1
        return result % mod


if __name__ == "__main__":
    # 20
    arr = [1, 1, 2, 2, 3, 3, 4, 4, 5, 5]
    target = 8
    print(Solution().threeSumMulti(arr, target))
