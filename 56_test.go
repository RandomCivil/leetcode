package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestMergeIntervals(t *testing.T) {
	tests := []struct {
		intervals [][]int
		expect    [][]int
	}{
		{
			intervals: [][]int{
				[]int{1, 3},
				[]int{2, 6},
				[]int{8, 10},
				[]int{15, 18},
			},
			expect: [][]int{
				[]int{1, 6},
				[]int{8, 10},
				[]int{15, 18},
			},
		},
		{
			intervals: [][]int{
				[]int{1, 4},
				[]int{4, 5},
			},
			expect: [][]int{
				[]int{1, 5},
			},
		},
		{
			intervals: [][]int{
				[]int{1, 4},
				[]int{0, 4},
			},
			expect: [][]int{
				[]int{0, 4},
			},
		},
		{
			intervals: [][]int{
				[]int{1, 3},
				[]int{2, 6},
				[]int{4, 7},
				[]int{8, 10},
				[]int{15, 18},
			},
			expect: [][]int{
				[]int{1, 7},
				[]int{8, 10},
				[]int{15, 18},
			},
		},
	}
	Convey("TestMergeIntervals", t, func() {
		for _, tt := range tests {
			So(merge(tt.intervals), ShouldResemble, tt.expect)
		}
	})
}
