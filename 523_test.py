inst_523 = __import__("523")


import pytest


class TestClass:
    @pytest.mark.parametrize(
        "nums, k, expected",
        [
            ([23, 2, 4, 6, 7], 6, True),
            ([23, 2, 6, 4, 7], 6, True),
            ([23, 2, 6, 4, 7], 13, False),
            ([23, 2, 4, 6, 6], 7, True),
            ([5, 0, 0, 0], 3, True),
            ([2, 4, 3], 6, True),
            ([1, 2, 3], 5, True),
            ([0], 1, False),
        ],
    )
    def test_checkSubarraySum(self, nums, k, expected):
        assert inst_523.Solution().checkSubarraySum(nums, k) == expected
