inst_698 = __import__("698")


import pytest


class TestClass:
    @pytest.mark.parametrize(
        "nums, k,expected",
        [
            ([4, 3, 2, 3, 5, 2, 1], 4, True),
            ([3, 2, 1, 3, 6, 1, 4, 8, 10, 8, 9, 1, 7, 9, 8, 1], 9, False),
        ],
    )
    def test_canPartitionKSubsets(self, nums, k, expected):
        assert inst_698.Solution().canPartitionKSubsets(nums, k) == expected
