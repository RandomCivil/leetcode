package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestMaximalSquare(t *testing.T) {
	tests := []struct {
		matrix [][]byte
		expect int
	}{
		{
			matrix: [][]byte{
				[]byte{'1', '0', '1', '0', '0'},
				[]byte{'1', '0', '1', '1', '1'},
				[]byte{'1', '1', '1', '1', '1'},
				[]byte{'1', '0', '0', '1', '0'},
			},
			expect: 4,
		},
		{
			matrix: [][]byte{
				[]byte{'0', '1'},
				[]byte{'1', '1'},
			},
			expect: 1,
		},
		{
			matrix: [][]byte{
				[]byte{'0', '1', '1', '0', '1'},
				[]byte{'1', '1', '0', '1', '0'},
				[]byte{'0', '1', '1', '1', '0'},
				[]byte{'1', '1', '1', '1', '0'},
				[]byte{'1', '1', '1', '1', '1'},
				[]byte{'0', '0', '0', '0', '0'},
			},
			expect: 9,
		},
	}
	Convey("TestNumIslands", t, func() {
		for _, tt := range tests {
			So(maximalSquare(tt.matrix), ShouldEqual, tt.expect)
		}
	})
}
