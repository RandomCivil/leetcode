# Given a list of folders, remove all sub-folders in those folders and return in any order the folders after removing.

# If a folder[i] is located within another folder[j], it is called a sub-folder of it.

# The format of a path is one or more concatenated strings of the form: / followed by one or more lowercase English letters. For example, /leetcode and /leetcode/problems are valid paths while an empty string and / are not.

# Example 1:

# Input: folder = ["/a","/a/b","/c/d","/c/d/e","/c/f"]
# Output: ["/a","/c/d","/c/f"]
# Explanation: Folders "/a/b/" is a subfolder of "/a" and "/c/d/e" is inside of folder "/c/d" in our filesystem.

# Example 2:

# Input: folder = ["/a","/a/b/c","/a/b/d"]
# Output: ["/a"]
# Explanation: Folders "/a/b/c" and "/a/b/d/" will be removed because they are subfolders of "/a".

# Example 3:

# Input: folder = ["/a/b/c","/a/b/ca","/a/b/d"]
# Output: ["/a/b/c","/a/b/ca","/a/b/d"]


class Solution:
    def removeSubfolders(self, folder):
        folder = sorted(folder)
        folder += ' '
        print(folder)
        r = []
        i = 0
        j = 1
        size = len(folder)
        while j < size:
            print(folder[i], folder[j][:len(folder[i])])
            prev_len = len(folder[i])
            if folder[i] == folder[j][:prev_len] and \
                    (len(folder[j]) > prev_len and folder[j][prev_len] == '/'):
                j += 1
            else:
                r.append(folder[i])
                i = j
                j += 1
        return r


if __name__ == '__main__':
    # ["/a","/c/d","/c/f"]
    folder = ["/a", "/a/b", "/c/d", "/c/d/e", "/c/f"]
    # ["/a"]
    folder = ["/a", "/a/b/c", "/a/b/d"]
    # ["/a/b/c","/a/b/ca","/a/b/d"]
    folder = ["/a/b/c", "/a/b/ca", "/a/b/d"]
    print(Solution().removeSubfolders(folder))
