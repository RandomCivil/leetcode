inst_227 = __import__("227")


import pytest


class TestClass:
    @pytest.mark.parametrize(
        "s,expected",
        [
            ("3+2*2", 7),
            (" 3/2 ", 1),
            (" 3+5 / 2 ", 5),
            ("03+3", 6),
            ("14/3*2", 8),
        ],
    )
    def test_calculate(self, s, expected):
        assert inst_227.Solution().calculate(s) == expected
