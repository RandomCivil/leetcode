package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestCanCross(t *testing.T) {
	tests := []struct {
		stones []int
		expect bool
	}{
		{
			stones: []int{0, 1, 3, 5, 6, 8, 12, 17},
			expect: true,
		},
		{
			stones: []int{0, 1, 2, 3, 4, 8, 9, 11},
			expect: false,
		},
		{
			stones: []int{0, 1},
			expect: true,
		},
		{
			stones: []int{0, 2},
			expect: false,
		},
		{
			stones: []int{0, 1, 2, 3, 4, 8, 9, 11},
			expect: false,
		},
	}
	Convey("TestCanCross", t, func() {
		for _, tt := range tests {
			So(canCross1(tt.stones), ShouldEqual, tt.expect)
			So(canCross(tt.stones), ShouldEqual, tt.expect)
		}
	})
}
