package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestUniquePaths(t *testing.T) {
	tests := []struct {
		m, n, expect int
	}{
		{
			m:      3,
			n:      7,
			expect: 28,
		},
		{
			m:      3,
			n:      2,
			expect: 3,
		},
		{
			m:      2,
			n:      2,
			expect: 2,
		},
		{
			m:      10,
			n:      10,
			expect: 48620,
		},
	}
	Convey("TestUniquePaths", t, func() {
		for _, tt := range tests {
			So(uniquePaths(tt.m, tt.n), ShouldEqual, tt.expect)
		}
	})
}

func TestUniquePathsDp(t *testing.T) {
	tests := []struct {
		m, n, expect int
	}{
		{
			m:      3,
			n:      7,
			expect: 28,
		},
		{
			m:      3,
			n:      2,
			expect: 3,
		},
		{
			m:      2,
			n:      2,
			expect: 2,
		},
		{
			m:      10,
			n:      10,
			expect: 48620,
		},
	}
	Convey("TestUniquePathsDp", t, func() {
		for _, tt := range tests {
			So(uniquePathsDp(tt.m, tt.n), ShouldEqual, tt.expect)
		}
	})
}
