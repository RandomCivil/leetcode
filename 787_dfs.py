# There are n cities connected by some number of flights. You are given an array flights where flights[i] = [fromi, toi, pricei] indicates that there is a flight from city fromi to city toi with cost pricei.

# You are also given three integers src, dst, and k, return the cheapest price from src to dst with at most k stops. If there is no such route, return -1.

# Example 1:

# Input: n = 3, flights = [[0,1,100],[1,2,100],[0,2,500]], src = 0, dst = 2, k = 1
# Output: 200
# Explanation: The graph is shown.
# The cheapest price from city 0 to city 2 with at most 1 stop costs 200, as marked red in the picture.

# Example 2:

# Input: n = 3, flights = [[0,1,100],[1,2,100],[0,2,500]], src = 0, dst = 2, k = 0
# Output: 500
# Explanation: The graph is shown.
# The cheapest price from city 0 to city 2 with at most 0 stop costs 500, as marked blue in the picture.
from collections import defaultdict


class Solution:
    def findCheapestPrice(self, n: int, flights, src: int, dst: int,
                          k: int) -> int:
        d = defaultdict(list)
        for _src, _dst, price in flights:
            d[_src].append((price, _dst))
        print(d)

        self.r = float('inf')

        def dfs(cur, price, stop):
            if stop > k + 1:
                return
            if cur == dst:
                self.r = price
                return
            for p, n in d[cur]:
                if price + p <= self.r:
                    dfs(n, price + p, stop + 1)

        dfs(src, 0, 0)
        return self.r if self.r != float('inf') else -1


if __name__ == '__main__':
    # 200
    flights = [[0, 1, 100], [1, 2, 100], [0, 2, 500]]
    n, src, dst, k = 3, 0, 2, 1

    # 500
    flights = [[0, 1, 100], [1, 2, 100], [0, 2, 500]]
    n, src, dst, k = 3, 0, 2, 0

    flights = [[0, 1, 2], [1, 2, 1], [2, 0, 10]]
    n, src, dst, k = 3, 1, 2, 1

    # 6
    flights = [[0, 1, 1], [0, 2, 5], [1, 2, 1], [2, 3, 1]]
    n, src, dst, k = 4, 0, 3, 1
    print(Solution().findCheapestPrice(n, flights, src, dst, k))
