package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestPartitionLabels(t *testing.T) {
	tests := []struct {
		s      string
		expect []int
	}{
		{
			s:      "ababcbacadefegdehijhklij",
			expect: []int{9, 7, 8},
		},
		{
			s:      "eccbbbbdec",
			expect: []int{10},
		},
	}
	Convey("TestPartitionLabels", t, func() {
		for _, tt := range tests {
			So(partitionLabels(tt.s), ShouldResemble, tt.expect)
		}
	})
}
