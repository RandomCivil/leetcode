# Given the root of a binary tree, return an array of the largest value in each row of the tree (0-indexed).

# Example 1:

# Input: root = [1,3,2,5,3,null,9]
# Output: [1,3,9]

# Example 2:

# Input: root = [1,2,3]
# Output: [1,3]

# Example 3:

# Input: root = [1]
# Output: [1]

# Example 4:

# Input: root = [1,null,2]
# Output: [1,2]

# Example 5:

# Input: root = []
# Output: []
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
from collections import deque


class Solution:
    def largestValues(self, root):
        if not root:
            return []
        q = deque([root])
        q1 = deque()
        result = [float('-inf')]
        while len(q) > 0:
            pop = q.popleft()
            result[-1] = max(result[-1], pop.val)
            if pop.left:
                q1.append(pop.left)
            if pop.right:
                q1.append(pop.right)
            if len(q) == 0:
                if len(q1) > 0:
                    result.append(float('-inf'))
                q = q1
                q1 = deque()
        return result
