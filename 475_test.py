inst_475 = __import__("475")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "houses,heaters,expected",
        [
            ([1, 2, 3], [2], 1),
            ([1, 2, 3, 4], [1, 4], 1),
            ([1, 5], [2], 3),
        ],
    )
    def test_find132pattern(self, houses, heaters, expected):
        assert inst_475.Solution().findRadius(houses, heaters) == expected
