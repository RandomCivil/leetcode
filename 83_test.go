package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/RandomCivil/leetcode/common"
)

func TestDeleteDuplicates(t *testing.T) {
	tests := []struct {
		num, expect []int
	}{
		{
			num:    []int{1, 1, 2},
			expect: []int{1, 2},
		},
		{
			num:    []int{1, 1, 2, 3, 3},
			expect: []int{1, 2, 3},
		},
		{
			num:    []int{1},
			expect: []int{1},
		},
		{
			num:    []int{1, 2},
			expect: []int{1, 2},
		},
		{
			num:    []int{1, 2, 2},
			expect: []int{1, 2},
		},
		{
			num:    []int{1, 1},
			expect: []int{1},
		},
	}
	Convey("TestDeleteDuplicates", t, func() {
		for _, tt := range tests {
			l := &common.LinkList{Nums: tt.num}
			head := l.Build()
			var result []int
			head = deleteDuplicates(head)
			for head != nil {
				result = append(result, head.Val)
				head = head.Next
			}
			if len(result) > 0 {
				So(result, ShouldResemble, tt.expect)
			} else {
				So(result, ShouldBeEmpty)
			}
		}
	})
}
