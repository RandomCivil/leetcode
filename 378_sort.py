# Given an n x n matrix where each of the rows and columns are sorted in ascending order, return the kth smallest element in the matrix.

# Note that it is the kth smallest element in the sorted order, not the kth distinct element.

# Example 1:

# Input: matrix = [[1,5,9],[10,11,13],[12,13,15]], k = 8
# Output: 13
# Explanation: The elements in the matrix are [1,5,9,10,11,12,13,13,15], and the 8th smallest number is 13

# Example 2:

# Input: matrix = [[-5]], k = 1
# Output: -5
import itertools


class Solution:
    def kthSmallest(self, matrix, k: int) -> int:
        l = list(itertools.chain.from_iterable(matrix))
        s = sorted(l)
        return s[k - 1]


if __name__ == '__main__':
    matrix = [[1, 5, 9], [10, 11, 13], [12, 13, 15]]
    k = 9

    # matrix = [[1, 2], [1, 3]]
    # k = 2
    print(Solution().kthSmallest(matrix, k))
