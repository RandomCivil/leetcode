package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestPlusOne1(t *testing.T) {
	tests := []struct {
		digits, expect []int
	}{
		{
			digits: []int{1, 2, 3},
			expect: []int{1, 2, 4},
		},
		{
			digits: []int{9},
			expect: []int{1, 0},
		},
		{
			digits: []int{8, 9},
			expect: []int{9, 0},
		},
		{
			digits: []int{9, 9},
			expect: []int{1, 0, 0},
		},
		{
			digits: []int{1, 9, 9},
			expect: []int{2, 0, 0},
		},
		{
			digits: []int{1, 2, 9, 9},
			expect: []int{1, 3, 0, 0},
		},
	}
	Convey("TestPlusOne1", t, func() {
		for _, tt := range tests {
			So(plusOne1(tt.digits), ShouldResemble, tt.expect)
		}
	})
}
