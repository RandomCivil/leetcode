# Given a string formula representing a chemical formula, return the count of each atom.

# The atomic element always starts with an uppercase character, then zero or more lowercase letters, representing the name.

# One or more digits representing that element's count may follow if the count is greater than 1. If the count is 1, no digits will follow.

# For example, "H2O" and "H2O2" are possible, but "H1O2" is impossible.
# Two formulas are concatenated together to produce another formula.

# For example, "H2O2He3Mg4" is also a formula.
# A formula placed in parentheses, and a count (optionally added) is also a formula.

# For example, "(H2O2)" and "(H2O2)3" are formulas.
# Return the count of all elements as a string in the following form: the first name (in sorted order), followed by its count (if that count is more than 1), followed by the second name (in sorted order), followed by its count (if that count is more than 1), and so on.

# The test cases are generated so that all the values in the output fit in a 32-bit integer.


# Example 1:

# Input: formula = "H2O"
# Output: "H2O"
# Explanation: The count of elements are {'H': 2, 'O': 1}.
# Example 2:

# Input: formula = "Mg(OH)2"
# Output: "H2MgO2"
# Explanation: The count of elements are {'H': 2, 'Mg': 1, 'O': 2}.
# Example 3:

# Input: formula = "K4(ON(SO3)2)2"
# Output: "K4N2O14S4"
# Explanation: The count of elements are {'K': 4, 'N': 2, 'O': 14, 'S': 4}.


# Constraints:

# 1 <= formula.length <= 1000
# formula consists of English letters, digits, '(', and ')'.
# formula is always valid.
from collections import Counter


class Solution:
    def countOfAtoms(self, formula: str) -> str:
        size = len(formula)

        def get_atomic(i):
            while i < size and formula[i].islower():
                i += 1
            return i

        def get_num(i):
            while i < size and formula[i].isdigit():
                i += 1
            return i

        i = 0
        stack = [Counter()]
        while i < size:
            if formula[i] == "(":
                stack.append(Counter())
                i += 1
            elif formula[i].isupper():
                j = get_atomic(i + 1)
                atomic = formula[i:j]
                k = get_num(j)
                if j == k:
                    num = 1
                else:
                    num = int(formula[j:k])

                stack[-1] += Counter({atomic: num})
                i = k
            else:
                j = get_num(i + 1)
                if i + 1 == j:
                    num = 1
                else:
                    num = int(formula[i + 1 : j])
                c = stack.pop()
                nc = Counter()
                for k in range(num):
                    nc += c
                if len(stack) > 0:
                    stack[-1] += nc
                i = j
        print(stack)

        if len(stack) == 0:
            return ""

        result = ""
        for k in sorted(stack[-1].keys()):
            if stack[-1][k] == 1:
                result += k
            else:
                result += k + str(stack[-1][k])
        return result
