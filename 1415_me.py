# A happy string is a string that:

# consists only of letters of the set ['a', 'b', 'c'].
# s[i] != s[i + 1] for all values of i from 1 to s.length - 1 (string is 1-indexed).
# For example, strings "abc", "ac", "b" and "abcbabcbcb" are all happy strings and strings "aa", "baa" and "ababbc" are not happy strings.

# Given two integers n and k, consider a list of all happy strings of length n sorted in lexicographical order.

# Return the kth string of this list or return an empty string if there are less than k happy strings of length n.

# Example 1:

# Input: n = 1, k = 3
# Output: "c"
# Explanation: The list ["a", "b", "c"] contains all happy strings of length 1. The third string is "c".

# Example 2:

# Input: n = 1, k = 4
# Output: ""
# Explanation: There are only 3 happy strings of length 1.

# Example 3:

# Input: n = 3, k = 9
# Output: "cab"
# Explanation: There are 12 different happy string of length 3 ["aba", "abc", "aca", "acb", "bab", "bac", "bca", "bcb", "cab", "cac", "cba", "cbc"]. You will find the 9th string = "cab"

# Example 4:

# Input: n = 2, k = 7
# Output: ""

# Example 5:

# Input: n = 10, k = 100
# Output: "abacbabacb"

# Constraints:


# 1 <= n <= 10
# 1 <= k <= 100
class Solution:
    def getHappyString(self, n: int, k: int) -> str:
        self.n = 0
        self.r = ''
        self.stop = False

        def dfs(s):
            if len(s) == n:
                print('find', s)
                self.n += 1
                if self.n == k:
                    self.r = s
                    self.stop = True
                return
            for ch in ['a', 'b', 'c']:
                if len(s) > 0 and s[-1] == ch:
                    continue
                if not self.stop:
                    dfs(s + ch)

        dfs('')
        return self.r


if __name__ == '__main__':
    # c
    n, k = 1, 3
    n, k = 1, 4
    # cab
    n, k = 3, 9
    n, k = 2, 7
    # abacbabacb
    n, k = 10, 100
    print(Solution().getHappyString(n, k))
