package leetcode

import (
	"fmt"
)

//Given an integer array nums and two integers left and right, return the number of contiguous non-empty subarrays such that the value of the maximum array element in that subarray is in the range [left, right].

//The test cases are generated so that the answer will fit in a 32-bit integer.

//Example 1:

//Input: nums = [2,1,4,3], left = 2, right = 3
//Output: 3
//Explanation: There are three subarrays that meet the requirements: [2], [2, 1], [3].

//Example 2:

//Input: nums = [2,9,2,5,6], left = 2, right = 8
//Output: 7
func NumSubarrayBoundedMax(nums []int, left int, right int) int {
	var (
		result int
		size   = len(nums)
		dp     = make([]int, size+1)
		start  int
	)
	for i := 1; i <= size; i++ {
		if nums[i-1] < left {
			dp[i] = dp[i-1]
		} else if left <= nums[i-1] && nums[i-1] <= right {
			dp[i] = i - start
		} else {
			start = i
		}
		result += dp[i]
	}
	fmt.Println(dp)
	return result
}
