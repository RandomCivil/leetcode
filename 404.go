package leetcode

//Find the sum of all left leaves in a given binary tree.

//Example:
/*
    3
   / \
  9  20
    /  \
   15   7
*/

//There are two left leaves in the binary tree, with values 9 and 15 respectively. Return 24.
func sumOfLeftLeaves(root *TreeNode) int {
	r := 0
	var dfs func(head *TreeNode, parent *TreeNode)
	dfs = func(head *TreeNode, parent *TreeNode) {
		if head == nil {
			return
		}
		if head.Left == nil && head.Right == nil && parent.Left == head {
			r += head.Val
		}
		dfs(head.Left, head)
		dfs(head.Right, head)
		return
	}
	dfs(root, root)
	return r
}
