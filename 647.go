package leetcode

//Given a string, your task is to count how many palindromic substrings in this string.
//The substrings with different start indexes or end indexes are counted as different substrings even they consist of same characters.
/*
Input: "abc"
Output: 3
Explanation: Three palindromic strings: "a", "b", "c".

Input: "aaa"
Output: 6
Explanation: Six palindromic strings: "a", "a", "a", "aa", "aa", "aaa".
*/
func CountSubstrings(s string) int {
	size := len(s)
	if size == 1 {
		return 1
	}
	dp := make([][]int, size)

	for i := 0; i < size; i++ {
		dp[i] = make([]int, size)
		dp[i][i] = 1
	}

	result := 0
	for i := size - 2; i >= 0; i-- {
		for j := i + 1; j < size; j++ {
			if s[i:i+1] == s[j:j+1] {
				if j-i+1 == 2 || dp[i+1][j-1] > 0 {
					result++
					dp[i][j] = dp[i+1][j-1] + 2
				}
			} else {
				dp[i][j] = 0
			}
		}
	}
	return result + size
}
