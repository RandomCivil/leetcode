import pytest


inst_886 = __import__("886")
inst_886_uf = __import__("886_uf")


class TestClass:
    @pytest.mark.parametrize(
        "n, dislikes, expected",
        [
            (4, [[1, 2], [1, 3], [2, 4]], True),
            (3, [[1, 2], [1, 3], [2, 3]], False),
        ],
    )
    def test_possibleBipartition(self, n, dislikes, expected):
        assert inst_886.Solution().possibleBipartition(n, dislikes) == expected
        assert inst_886_uf.Solution().possibleBipartition(n, dislikes) == expected
