# Given an m x n grid of characters board and a string word, return true if word exists in the grid.

# The word can be constructed from letters of sequentially adjacent cells, where adjacent cells are horizontally or vertically neighboring. The same letter cell may not be used more than once.

# Example 1:

# Input: board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "ABCCED"
# Output: true

# Example 2:

# Input: board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "SEE"
# Output: true

# Example 3:

# Input: board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "ABCB"
# Output: false

# Constraints:


# m == board.length
# n = board[i].length
# 1 <= m, n <= 6
# 1 <= word.length <= 15
# board and word consists of only lowercase and uppercase English letters.
class Solution:
    def exist(self, board, word: str) -> bool:
        print(board, word)
        m, n = len(board), len(board[0])
        directions = [(1, 0), (-1, 0), (0, 1), (0, -1)]

        def dfs(x, y, step):
            if step == len(word):
                return False
            # print(x, y, "".join(path), visited, step)
            # 每一步都比较单个字符,而不是长度等于word后再比较
            if word[step] != board[x][y]:
                return False
            if step + 1 == len(word) and word[step] == board[x][y]:
                # print("find", path)
                return True
            for xx, yy in directions:
                if x + xx < 0 or x + xx > m - 1 or y + yy < 0 or y + yy > n - 1:
                    continue
                if (x + xx, y + yy) in visited:
                    continue
                visited.add((x + xx, y + yy))
                find = dfs(x + xx, y + yy, step + 1)

                if find:
                    return True
                visited.remove((x + xx, y + yy))
            return False

        for i in range(m):
            for j in range(n):
                visited = {(i, j)}
                if dfs(i, j, 0):
                    return True
        return False


if __name__ == "__main__":
    board = [["A", "B", "C", "E"], ["S", "F", "C", "S"], ["A", "D", "E", "E"]]
    word = "ABCCF"

    # board = [["A", "B", "C", "E"], ["S", "F", "C", "S"], ["A", "D", "E", "E"]]
    # word = "SEE"

    # board = [["A", "B", "C", "E"], ["S", "F", "C", "S"], ["A", "D", "E", "E"]]
    # word = "ABCB"

    # board = [["a", "b", "c"], ["a", "e", "d"], ["a", "f", "g"]]
    # word = "abcdefg"

    # board = [["a", "a"]]
    # word = "aa"

    board=[["A","A","A","A","A","A"],["A","A","A","A","A","A"],["A","A","A","A","A","A"],["A","A","A","A","A","A"],["A","A","A","A","A","A"],["A","A","A","A","A","A"]]
    word='AAAAAAAAAAAAAAa'
    print(Solution().exist(board, word))
