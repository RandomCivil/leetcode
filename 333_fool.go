package leetcode

import (
	"math"

	"gitlab.com/RandomCivil/leetcode/common"
)

// Given the root of a binary tree, find the largest
// subtree
// , which is also a Binary Search Tree (BST), where the largest means subtree has the largest number of nodes.

// A Binary Search Tree (BST) is a tree in which all the nodes follow the below-mentioned properties:

// The left subtree values are less than the value of their parent (root) node's value.
// The right subtree values are greater than the value of their parent (root) node's value.
// Note: A subtree must include all of its descendants.

// Example 1:

// Input: root = [10,5,15,1,8,null,7]
// Output: 3
// Explanation: The Largest BST Subtree in this case is the highlighted one. The return value is the subtree's size, which is 3.
// Example 2:

// Input: root = [4,2,7,2,3,5,null,2,null,null,null,null,null,1]
// Output: 2

// Constraints:

// The number of nodes in the tree is in the range [0, 104].
// -104 <= Node.val <= 104
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func largestBSTSubtree1(root *common.TreeNode) int {
	var result int
	var validBst func(cur *common.TreeNode, min, max int) (bool, int)
	validBst = func(cur *common.TreeNode, min, max int) (bool, int) {
		if cur == nil {
			return true, 0
		}
		if !(min < cur.Val && cur.Val < max) {
			return false, 0
		}
		lvalid, lcount := validBst(cur.Left, min, cur.Val)
		rvalid, rcount := validBst(cur.Right, cur.Val, max)
		return lvalid && rvalid, lcount + rcount + 1
	}

	var tranversal func(cur *common.TreeNode)
	tranversal = func(cur *common.TreeNode) {
		if cur == nil {
			return
		}
		valid, count := validBst(cur, math.MinInt, math.MaxInt)
		if valid {
			result = common.Max(result, count)
		}
		tranversal(cur.Left)
		tranversal(cur.Right)
	}
	tranversal(root)
	return result
}
