package leetcode

import "fmt"

//A sequence of number is called arithmetic if it consists of at least three elements and if the difference between any two consecutive elements is the same.

//For example, these are arithmetic sequence:
/*
1, 3, 5, 7, 9
7, 7, 7, 7
3, -1, -5, -9
*/

//The function should return the number of arithmetic slices in the array A.

/*
A = [1, 2, 3, 4]
return: 3, for 3 arithmetic slices in A: [1, 2, 3], [2, 3, 4] and [1, 2, 3, 4] itself.

1-5 6
1-6 10
*/

//func numberOfArithmeticSlices(A []int) int {
func NumberOfArithmeticSlices(A []int) int {
	fmt.Println(A)
	size := len(A)
	dp := make([]int, size)

	sum := 0
	for j := 2; j < size; j++ {
		if A[j]-A[j-1] == A[j-1]-A[j-2] {
			dp[j] = dp[j-1] + 1
			sum += dp[j]
		}
	}
	fmt.Println(dp,sum)
	return sum
}
