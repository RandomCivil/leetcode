package leetcode

import (
	"math"

	"gitlab.com/RandomCivil/leetcode/common"
)

// You are given an array of k linked-lists lists, each linked-list is sorted in ascending order.

// Merge all the linked-lists into one sorted linked-list and return it.

// Example 1:

// Input: lists = [[1,4,5],[1,3,4],[2,6]]
// Output: [1,1,2,3,4,4,5,6]
// Explanation: The linked-lists are:
// [
//   1->4->5,
//   1->3->4,
//   2->6
// ]
// merging them into one sorted list:
// 1->1->2->3->4->4->5->6
// Example 2:

// Input: lists = []
// Output: []
// Example 3:

// Input: lists = [[]]
// Output: []

// Constraints:

// k == lists.length
// 0 <= k <= 104
// 0 <= lists[i].length <= 500
// -104 <= lists[i][j] <= 104
// lists[i] is sorted in ascending order.
// The sum of lists[i].length will not exceed 104.
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func mergeKLists(lists []*common.ListNode) *common.ListNode {
	if len(lists) == 0 {
		return nil
	}

	preRoot := &common.ListNode{}
	isEndAndFindMinIndex := func(lists []*common.ListNode) (int, bool) {
		mn := math.MaxInt
		var minIndex int
		allEmpty := true
		for i, l := range lists {
			if l != nil {
				allEmpty = false
				if l.Val < mn {
					mn = l.Val
					minIndex = i
				}
			}
		}
		return minIndex, allEmpty
	}

	cur := preRoot
	for {
		i, allEmpty := isEndAndFindMinIndex(lists)
		if allEmpty {
			break
		}
		cur.Next = &common.ListNode{
			Val: lists[i].Val,
		}
		cur = cur.Next
		lists[i] = lists[i].Next
	}
	return preRoot.Next
}
