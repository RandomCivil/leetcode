# You are a hiker preparing for an upcoming hike. You are given heights, a 2D array of size rows x columns, where heights[row][col] represents the height of cell (row, col). You are situated in the top-left cell, (0, 0), and you hope to travel to the bottom-right cell, (rows-1, columns-1) (i.e., 0-indexed). You can move up, down, left, or right, and you wish to find a route that requires the minimum effort.

# A route's effort is the maximum absolute difference in heights between two consecutive cells of the route.

# Return the minimum effort required to travel from the top-left cell to the bottom-right cell.

# Example 1:

# Input: heights = [[1,2,2],[3,8,2],[5,3,5]]
# Output: 2
# Explanation: The route of [1,3,5,3,5] has a maximum absolute difference of 2 in consecutive cells.
# This is better than the route of [1,2,2,2,5], where the maximum absolute difference is 3.
# Example 2:

# Input: heights = [[1,2,3],[3,8,4],[5,3,5]]
# Output: 1
# Explanation: The route of [1,2,3,4,5] has a maximum absolute difference of 1 in consecutive cells, which is better than route [1,3,5,3,5].
# Example 3:


# Input: heights = [[1,2,1,1,1],[1,2,1,2,1],[1,2,1,2,1],[1,2,1,2,1],[1,1,1,2,1]]
# Output: 0
# Explanation: This route does not require any effort.


# Constraints:


# rows == heights.length
# columns == heights[i].length
# 1 <= rows, columns <= 100
# 1 <= heights[i][j] <= 106
from collections import defaultdict


class Solution:
    def minimumEffortPath(self, heights: list[list[int]]) -> int:
        m, n = len(heights), len(heights[0])
        print(m, n)
        size = m * n
        directions = [(0, 1), (0, -1), (1, 0), (-1, 0)]
        dp = [defaultdict(lambda: float("inf")) for _ in range(size)]
        for i in range(size):
            dp[i][(0, 0)] = 0

        l = []
        for i in range(m):
            for j in range(n):
                l.append((i, j, heights[i][j]))

        for i in range(1, size):
            for x, y, dis in l:
                for xx, yy in directions:
                    if 0 <= x + xx < m and 0 <= y + yy < n:
                        dp[i][(x, y)] = min(
                            dp[i][(x, y)],
                            max(
                                abs(heights[x + xx][y + yy] - dis),
                                dp[i - 1][(x + xx, y + yy)],
                            ),
                        )
        print(dp)
        return dp[-1][(m - 1, n - 1)]
