inst_955 = __import__("955")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "strs, expected",
        [
            (["xga", "xfb", "yfa"], 1),
            (["zyx", "wvu", "tsr"], 3),
            (["xc", "yb", "za"], 0),
            (["ca", "bb", "ac"], 1),
        ],
    )
    def test_minDeletionSize(self, strs, expected):
        assert inst_955.Solution().minDeletionSize(strs) == expected
