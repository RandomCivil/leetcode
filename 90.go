package leetcode

import (
	"fmt"
	"sort"
)

// Given an integer array nums that may contain duplicates, return all possible subsets (the power set).

// The solution set must not contain duplicate subsets. Return the solution in any order.

// Example 1:

// Input: nums = [1,2,2]
// Output: [[],[1],[1,2],[1,2,2],[2],[2,2]]
// Example 2:

// Input: nums = [0]
// Output: [[],[0]]

// Constraints:

// 1 <= nums.length <= 10
// -10 <= nums[i] <= 10
func subsetsWithDup(nums []int) [][]int {
	var result [][]int
	var candidates []int
	counter := make(map[int]int)
	for _, n := range nums {
		if _, ok := counter[n]; ok {
			counter[n]++
		} else {
			counter[n] = 1
			candidates = append(candidates, n)
		}
	}
	sort.Ints(candidates)
	fmt.Println(candidates, counter)

	var dfs func(max, start int, path []int)
	dfs = func(max, start int, path []int) {
		if len(path) > max {
			return
		}
		if len(path) == max {
			item := make([]int, len(path))
			copy(item, path)
			result = append(result, item)
			return
		}
		for i, n := range candidates {
			if i < start {
				continue
			}
			if counter[n] <= 0 {
				continue
			}
			counter[n]--
			path = append(path, n)
			dfs(max, i, path)
			path = path[:len(path)-1]
			counter[n]++
		}
	}
	for i := 0; i <= len(nums); i++ {
		dfs(i, 0, make([]int, 0))
	}
	return result
}
