package leetcode

import (
	"fmt"
	"sort"

	"gitlab.com/RandomCivil/leetcode/common"
)

// Given an array of intervals where intervals[i] = [starti, endi], merge all overlapping intervals, and return an array of the non-overlapping intervals that cover all the intervals in the input.

// Example 1:

// Input: intervals = [[1,3],[2,6],[8,10],[15,18]]
// Output: [[1,6],[8,10],[15,18]]
// Explanation: Since intervals [1,3] and [2,6] overlap, merge them into [1,6].
// Example 2:

// Input: intervals = [[1,4],[4,5]]
// Output: [[1,5]]
// Explanation: Intervals [1,4] and [4,5] are considered overlapping.

// Constraints:

// 1 <= intervals.length <= 104
// intervals[i].length == 2
func merge(intervals [][]int) [][]int {
	sort.Slice(intervals, func(i, j int) bool {
		return intervals[i][0] < intervals[j][0]
	})
	fmt.Println(intervals)

	result := [][]int{intervals[0]}
	size := len(intervals)
	for i := 1; i < size; i++ {
		start1, end1 := result[len(result)-1][0], result[len(result)-1][1]
		start2, end2 := intervals[i][0], intervals[i][1]
		if end1 >= start2 {
			result := result[:len(result)-1]
			result = append(result, []int{start1, common.Max(end1, end2)})
		} else {
			result = append(result, intervals[i])
		}
	}
	return result
}
