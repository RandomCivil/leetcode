# You are given an m x n integer matrix heights representing the height of each unit cell in a continent. The Pacific ocean touches the continent's left and top edges, and the Atlantic ocean touches the continent's right and bottom edges.

# Water can only flow in four directions: up, down, left, and right. Water flows from a cell to an adjacent one with an equal or lower height.

# Return a list of grid coordinates where water can flow to both the Pacific and Atlantic oceans.

# Example 1:

# Input: heights = [[1,2,2,3,5],[3,2,3,4,4],[2,4,5,3,1],[6,7,1,4,5],[5,1,1,2,4]]
# Output: [[0,4],[1,3],[1,4],[2,2],[3,0],[3,1],[4,0]]

# Example 2:


# Input: heights = [[2,1],[1,2]]
# Output: [[0,0],[0,1],[1,0],[1,1]]
class Solution:
    def pacificAtlantic(self, heights):
        r = []
        m, n = len(heights), len(heights[0])
        d = [(1, 0), (-1, 0), (0, 1), (0, -1)]

        def dfs(x, y, ph, visited):
            if x == -1 or y == -1 or x == m or y == n:
                return

            if (x, y) in visited:
                return

            if ph > heights[x][y]:
                return

            visited.add((x, y))
            for xx, yy in d:
                dfs(x + xx, y + yy, heights[x][y], visited)

        from_pacific = set()
        from_atlantic = set()
        for x in range(m):
            dfs(x, 0, 0, from_pacific)
            dfs(x, n - 1, 0, from_atlantic)
        for y in range(n):
            dfs(0, y, 0, from_pacific)
            dfs(m - 1, y, 0, from_atlantic)

        for x in range(m):
            for y in range(n):
                if (x, y) in from_pacific and (x, y) in from_atlantic:
                    r.append((x, y))

        return r


if __name__ == '__main__':
    heights = [[1, 2, 2, 3, 5], [3, 2, 3, 4, 4], [2, 4, 5, 3, 1],
               [6, 7, 1, 4, 5], [5, 1, 1, 2, 4]]
    print(Solution().pacificAtlantic(heights))
