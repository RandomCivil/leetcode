# Given an array of integers and an integer k, you need to find the total number of continuous subarrays whose sum equals to k.
"""
Input:nums = [1,1,1], k = 2
Output: 2
"""
from collections import defaultdict


class Solution:
    def subarraySum(self, nums, k: int) -> int:
        result = 0
        d = defaultdict(int)
        d[0] = 1
        s = 0
        for n in nums:
            s += n
            if s - k in d:
                result += d[s - k]
            d[s] += 1
        return result


if __name__ == "__main__":
    nums = [1, 0, 1, 3, 4, 5]
    k = 9
    # nums = [1, 1, 1]
    # k = 2
    # nums = [1]
    # k = 0
    nums = [0] * 10
    nums.append(1)
    k = 1
    print(Solution().subarraySum(nums, k))
