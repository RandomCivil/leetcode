package leetcode

import (
	"fmt"

	"gitlab.com/RandomCivil/leetcode/common"
)

// Given a linked list, swap every two adjacent nodes and return its head. You must solve the problem without modifying the values in the list's nodes (i.e., only nodes themselves may be changed.)

// Example 1:

// Input: head = [1,2,3,4]
// Output: [2,1,4,3]
// Example 2:

// Input: head = []
// Output: []
// Example 3:

// Input: head = [1]
// Output: [1]

// Constraints:

// The number of nodes in the list is in the range [0, 100].
// 0 <= Node.val <= 100

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func swapPairs(head *common.ListNode) *common.ListNode {
	if head == nil || head.Next == nil {
		return head
	}
	result := head.Next
	// 至少2个节点
	var prev *common.ListNode
	cur, pair := head, head.Next
	swap := true
	for pair != nil {
		fmt.Println(swap, prev, cur, pair)
		if swap {
			if prev != nil {
				prev.Next = pair
			}
			cur.Next = pair.Next
			pair.Next = cur

			prev = pair
			pair = cur.Next
		} else {
			prev = cur
			cur = pair
			pair = pair.Next
		}
		swap = !swap
	}
	return result
}
