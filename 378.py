# Given an n x n matrix where each of the rows and columns are sorted in ascending order, return the kth smallest element in the matrix.

# Note that it is the kth smallest element in the sorted order, not the kth distinct element.

# Example 1:

# Input: matrix = [[1,5,9],[10,11,13],[12,13,15]], k = 8
# Output: 13
# Explanation: The elements in the matrix are [1,5,9,10,11,12,13,13,15], and the 8th smallest number is 13

# Example 2:

# Input: matrix = [[-5]], k = 1
# Output: -5


class Solution:
    def kthSmallest(self, matrix, k: int) -> int:
        m, n = len(matrix), len(matrix[0])
        lo, hi = matrix[0][0], matrix[m - 1][n - 1] + 1
        while lo <= hi:
            mid = (lo + hi) // 2
            count = 0
            for row in matrix:
                col = n - 1
                while col >= 0 and mid < row[col]:
                    col -= 1
                count += (col + 1)
                if col < 0:
                    break

            if count < k:
                lo = mid + 1
            else:
                hi = mid - 1

        return lo
