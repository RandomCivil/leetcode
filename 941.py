# Given an array of integers arr, return true if and only if it is a valid mountain array.
# Recall that arr is a mountain array if and only if:

# arr.length >= 3
# There exists some i with 0 < i < arr.length - 1 such that:
# arr[0] < arr[1] < ... < arr[i - 1] < arr[i]
# arr[i] > arr[i + 1] > ... > arr[arr.length - 1]

# Example 1:

# Input: arr = [2,1]
# Output: false

# Example 2:

# Input: arr = [3,5,5]
# Output: false

# Example 3:

# Input: arr = [0,3,2,1]
# Output: true


class Solution:
    def validMountainArray(self, arr) -> bool:
        size = len(arr)
        if size < 3:
            return False
        i, j = 1, size - 2
        while i < size - 1 and arr[i - 1] < arr[i]:
            i += 1

        while j > 0 and arr[j] > arr[j + 1]:
            j -= 1

        return i - 1 >= j + 1


if __name__ == '__main__':
    arr = [0, 3, 2, 1]
    print(Solution().validMountainArray(arr))
