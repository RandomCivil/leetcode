package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestMaxSubArray(t *testing.T) {
	tests := []struct {
		nums   []int
		expect int
	}{
		{
			nums:   []int{-2, 1, -3, 4, -1, 2, 1, -5, 4},
			expect: 6,
		},
		{
			nums:   []int{5, 4, -1, 7, 8},
			expect: 23,
		},
		{
			nums:   []int{-1, -2},
			expect: -1,
		},
		{
			nums:   []int{1, 2},
			expect: 3,
		},
	}
	Convey("TestMaxSubArray", t, func() {
		for _, tt := range tests {
			So(maxSubArray(tt.nums), ShouldEqual, tt.expect)
		}
	})
}
