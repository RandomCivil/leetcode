package leetcode

import (
	"math"

	"gitlab.com/RandomCivil/leetcode/common"
)

//You need to find the largest value in each row of a binary tree.

/*
Input:

          1
         / \
        3   2
       / \   \
      5   3   9

Output: [1, 3, 9]
*/

func LargestValues(root *common.TreeNode) []int {
	if root == nil {
		return []int{}
	}
	result := []int{}
	q := []*common.TreeNode{root}
	//next level queue
	q1 := []*common.TreeNode{}
	i := 0
	for len(q) > 0 {
		head := q[0]
		if head.Val > result[i] {
			result[i] = head.Val
		}
		q = q[1:]

		if head.Left != nil {
			q1 = append(q1, head.Left)
		}
		if head.Right != nil {
			q1 = append(q1, head.Right)
		}

		if len(q) == 0 {
			q = q1
			if len(q1) > 0 {
				result = append(result, math.MinInt32)
				i++
			}
			q1 = []*common.TreeNode{}
		}
	}
	return result
}
