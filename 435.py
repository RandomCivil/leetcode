class Solution:
    def eraseOverlapIntervals(self, intervals) -> int:
        size = len(intervals)
        dp = [0] * size
        intervals = sorted(intervals, key=lambda x: x[0])
        dp[0] = 1
        for i in range(1, size):
            start2, _ = intervals[i]
            for j in range(i):
                _, end1 = intervals[j]
                if end1 <= start2:
                    dp[i] = max(dp[i], dp[j] + 1)
                else:
                    dp[i] = max(dp[i], dp[j])
        print(dp)
        return size - dp[-1]


if __name__ == "__main__":
    # 1
    intervals = [[1, 2], [2, 3], [3, 4], [1, 3]]

    # 2
    intervals = [[1, 2], [1, 2], [1, 2]]

    # 0
    intervals = [[1, 2], [2, 3]]

    # 2
    intervals = [[1, 100], [11, 22], [1, 11], [2, 12]]

    # 2
    intervals = [[0, 2], [1, 3], [2, 4], [3, 5], [4, 6]]
    print(Solution().eraseOverlapIntervals(intervals))
