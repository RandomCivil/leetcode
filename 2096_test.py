inst_2096 = __import__("2096")
inst_2096_me = __import__("2096_me")


import pytest
import common


class TestClass:
    @pytest.mark.parametrize(
        "nums, startValue, destValue,expected",
        [
            ([5, 1, 2, 3, 0, 6, 4], 3, 6, "UURL"),
            ([2, 1], 2, 1, "L"),
            ([1,0,2,0,3,0,4],4,1,'UUU'),
        ],
    )
    def test_getDirections(self, nums, startValue: int, destValue: int, expected):
        root = common.build_tree(nums)
        # assert inst_2096_me.Solution().getDirections(root, startValue, destValue) == expected
        assert inst_2096.Solution().getDirections(root, startValue, destValue) == expected
        
