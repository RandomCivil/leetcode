inst_410 = __import__("410")
inst_410_me = __import__("410_me")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "nums,k,expected",
        [
            ([7, 2, 5, 10, 8], 2, 18),
            ([7, 2, 5, 10, 8], 3, 14),
            ([1, 2, 3, 4, 5], 2, 9),
            (
                [
                    100,
                    150,
                    200,
                    250,
                    300,
                    350,
                    400,
                    450,
                    500,
                    550,
                    600,
                    650,
                    700,
                    750,
                    800,
                    850,
                    900,
                    950,
                ],
                15,
                950,
            ),
        ],
    )
    def test_splitArray(self, nums, k, expected):
        assert inst_410_me.Solution().splitArray(nums, k) == expected
        assert inst_410.Solution().splitArray(nums, k) == expected
