# Given an m x n integers matrix, return the length of the longest increasing path in matrix.

# From each cell, you can either move in four directions: left, right, up, or down. You may not move diagonally or move outside the boundary (i.e., wrap-around is not allowed).


# Example 1:


# Input: matrix = [[9,9,4],[6,6,8],[2,1,1]]
# Output: 4
# Explanation: The longest increasing path is [1, 2, 6, 9].
# Example 2:


# Input: matrix = [[3,4,5],[3,2,6],[2,2,1]]
# Output: 4
# Explanation: The longest increasing path is [3, 4, 5, 6]. Moving diagonally is not allowed.
# Example 3:

# Input: matrix = [[1]]
# Output: 1


# Constraints:


# m == matrix.length
# n == matrix[i].length
# 1 <= m, n <= 200
# 0 <= matrix[i][j] <= 231 - 1
from collections import defaultdict, deque


class Solution:
    def longestIncreasingPath(self, matrix: list[list[int]]) -> int:
        m, n = len(matrix), len(matrix[0])
        result = 0
        q = deque()
        d = defaultdict(lambda: 0)
        directions = [(1, 0), (-1, 0), (0, 1), (0, -1)]
        for x in range(m):
            for y in range(n):
                for xx, yy in directions:
                    if x + xx < 0 or x + xx == m or y + yy < 0 or y + yy == n:
                        continue
                    if matrix[x][y] <= matrix[x + xx][y + yy]:
                        continue
                    d[(x, y)] += 1
                if d[(x, y)] == 0:
                    q.append((x, y, 1))
        print(d)
        print(q)

        while len(q) > 0:
            x, y, c = q.popleft()
            result = max(result, c)
            for xx, yy in directions:
                if x + xx < 0 or x + xx == m or y + yy < 0 or y + yy == n:
                    continue
                if matrix[x][y] >= matrix[x + xx][y + yy]:
                    continue
                d[(x + xx, y + yy)] -= 1
                if d[(x + xx, y + yy)] == 0:
                    q.append((x + xx, y + yy, c + 1))
        return result
