package leetcode

import "fmt"

//Given an integer array nums, return an array answer such that answer[i] is equal to the product of all the elements of nums except nums[i].

//The product of any prefix or suffix of nums is guaranteed to fit in a 32-bit integer.

//You must write an algorithm that runs in O(n) time and without using the division operation.

//Example 1:

//Input: nums = [1,2,3,4]
//Output: [24,12,8,6]

//Example 2:

//Input: nums = [-1,1,0,-3,3]
//Output: [0,0,9,0,0]
func ProductExceptSelf(nums []int) []int {
	var (
		size   = len(nums)
		arr    = make([]int, size+1)
		rarr   = make([]int, size+1)
		result = make([]int, size)
	)
	arr[0] = 1
	rarr[size] = 1
	for i := 1; i < size+1; i++ {
		arr[i] = arr[i-1] * nums[i-1]
	}
	for i := size; i > 0; i-- {
		rarr[i-1] = rarr[i] * nums[i-1]
	}
	fmt.Println(arr, rarr)

	for i := 0; i < size; i++ {
		result[i] = arr[i] * rarr[i+1]
	}
	return result
}
