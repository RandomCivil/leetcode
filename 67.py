# Given two binary strings a and b, return their sum as a binary string.

# Example 1:

# Input: a = "11", b = "1"
# Output: "100"
# Example 2:

# Input: a = "1010", b = "1011"
# Output: "10101"


# Constraints:

# 1 <= a.length, b.length <= 104
# a and b consist only of '0' or '1' characters.
# Each string does not contain leading zeros except for the zero itself.
from itertools import zip_longest


class Solution:
    def addBinary(self, a: str, b: str) -> str:
        print(a, b)
        size1, size2 = len(a), len(b)
        size = max(size1, size2)
        result = [""] * size
        if size1 > size2:
            b = "0" * (size1 - size2) + b
        else:
            a = "0" * (size2 - size1) + a
        print(a, b)

        plus = 0
        i = 0
        print(list(zip(a, b))[::-1])
        for s1, s2 in list(zip(a, b))[::-1]:
            print(s1, s2)
            v = int(s1) + int(s2) + plus
            result[size - 1 - i] = str(v % 2)
            plus = int(v / 2)
            i += 1

        s = "".join(result)
        if plus:
            s = "1" + s
        return s
