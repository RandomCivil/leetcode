package leetcode

import (
	"fmt"
	"sort"
)

//Given an integer array with all positive numbers and no duplicates, find the number of possible combinations that add up to a positive integer target.

/*
nums = [1, 2, 3]
target = 4

The possible combination ways are:
(1, 1, 1, 1)
(1, 1, 2)
(1, 2, 1)
(1, 3)
(2, 1, 1)
(2, 2)
(3, 1)

Note that different sequences are counted as different combinations.

Therefore the output is 7.
*/

//nums,target := []int{1, 2, 3},4
//nums, target := []int{8, 7, 4, 3}, 11

//func combinationSum4(nums []int, target int) int {
func CombinationSum4(nums []int, target int) int {
	sort.Ints(nums)
	size := len(nums)
	dp := make([]int, target+1)
	dp[0] = 1
	for i := 1; i <= target; i++ {
		for j := 0; j < size; j++ {
			if i < nums[j] {
				break
			}
			dp[i] += dp[i-nums[j]]
		}
	}
	fmt.Println(dp)
	return dp[len(dp)-1]
}
