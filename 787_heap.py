# There are n cities connected by m flights. Each flight starts from city u and arrives at v with a price w.

# Now given all the cities and flights, together with starting city src and the destination dst, your task is to find the cheapest price from src to dst with up to k stops. If there is no such route, output -1.

# Example 1:
# Input:
# n = 3, edges = [[0,1,100],[1,2,100],[0,2,500]]
# src = 0, dst = 2, k = 1
# Output: 200

# Example 2:
# Input:
# n = 3, edges = [[0,1,100],[1,2,100],[0,2,500]]
# src = 0, dst = 2, k = 0
# Output: 500

from collections import defaultdict, deque
import heapq


class Solution:
    def findCheapestPrice(self, n: int, flights, src: int, dst: int, k: int) -> int:
        d = defaultdict(list)
        for s, e, price in flights:
            d[s].append((price, e))

        result = [float("inf")] * n
        h = [(0, src, -1)]
        heapq.heapify(h)
        while len(h) > 0:
            price, cur, stop = heapq.heappop(h)
            if stop > k:
                continue
            if price < result[cur]:
                result[cur] = price
                for nxt, p in d[cur]:
                    heapq.heappush(h, (price + p, nxt, stop + 1))
        print(result)
        return result[dst] if result[dst] < float("inf") else -1
