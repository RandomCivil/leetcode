package leetcode

import (
	"fmt"
	"strconv"
	"strings"
)

//Given an encoded string, return its decoded string.

//The encoding rule is: k[encoded_string], where the encoded_string inside the square brackets is being repeated exactly k times. Note that k is guaranteed to be a positive integer.
//You may assume that the input string is always valid; No extra white spaces, square brackets are well-formed, etc.
//Furthermore, you may assume that the original data does not contain any digits and that digits are only for those repeat numbers, k. For example, there won't be input like 3a or 2[4].

/*
s = "3[a]2[bc]", return "aaabcbc".
s = "3[a2[c]]", return "accaccacc".
s = "2[abc]3[cd]ef", return "abcabccdcdcdef".
s := "2[abc]3[cd]ef" abcabccdcdcdef
*/

//func decodeString(s string) string {
func DecodeString(s string) string {
	fmt.Println(s)
	size := len(s)
	var result string
	var numStr string
	var arr [][]string
	for i := 0; i < size; i++ {
		fmt.Println(string(s[i]), arr)
		if s[i] == '[' {
			arr = append(arr, []string{"", numStr})
			numStr = ""
		} else if s[i] == ']' {
			item := arr[len(arr)-1]
			arr = arr[:len(arr)-1]
			repeat, _ := strconv.Atoi(item[1])
			if len(arr) > 0 {
				prev := arr[len(arr)-1]
				prev[0] = prev[0] + strings.Repeat(item[0], repeat)
			} else {
				fmt.Println("hehe")
				result = result + strings.Repeat(item[0], repeat)
			}
		} else {
			_, err := strconv.Atoi(string(s[i]))
			if err == nil { //int
				//fmt.Println("1", string(s[i]))
				numStr = numStr + string(s[i])
			} else { //str
				//fmt.Println("2", string(s[i]))
				if len(arr) > 0 {
					lastItem := arr[len(arr)-1]
					lastItem[0] = lastItem[0] + string(s[i])
				} else {
					result = result + string(s[i])
				}
			}
		}
	}
	return result
}
