package leetcode

import "fmt"

//Given an unsorted array of integers, find the length of longest increasing subsequence.
/*
Input: [10,9,2,5,3,7,101,18]
Output: 4
Explanation: The longest increasing subsequence is [2,3,7,101], therefore the length is 4.
*/
func LengthOfLIS(nums []int) int {
	fmt.Println(nums)
	size := len(nums)
	if size == 0 {
		return 0
	}
	var result int
	dp := make([]int, size)
	for i := 0; i < size; i++ {
		dp[i] = 1
	}
	for i := 0; i < size; i++ {
		max := 1
		for j := 0; j < i; j++ {
			if nums[i] > nums[j] {
				if dp[j]+1 > max {
					max = dp[j] + 1
				}
			}
		}
		dp[i] = max
		if max > result {
			result = max
		}
	}
	fmt.Println(dp)
	return result
}
