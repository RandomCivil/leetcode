# Given a string s, partition s such that every substring of the partition is a palindrome
# Return all possible palindrome partitioning of s.

# Example 1:

# Input: s = "aab"
# Output: [["a","a","b"],["aa","b"]]
# Example 2:

# Input: s = "a"
# Output: [["a"]]


# Constraints:

# 1 <= s.length <= 16
# s contains only lowercase English letters.
class Solution:
    def partition(self, s):
        result = []

        def dfs(remain, path):
            if len(remain) == 0:
                result.append(path.copy())
                return
            for i in range(len(remain)):
                if not check(remain[: i + 1]):
                    continue
                path.append(remain[: i + 1])
                dfs(remain[i + 1 :], path)
                path.pop()

        def check(s):
            size = len(s)
            for i in range(size // 2):
                if s[i] != s[size - i - 1]:
                    return False
            return True

        dfs(s, [])
        return result


if __name__ == "__main__":
    s = "aab"
    # s = "efe"
    print(Solution().partition(s))
