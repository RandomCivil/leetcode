# A super ugly number is a positive integer whose prime factors are in the array primes.

# Given an integer n and an array of integers primes, return the nth super ugly number.

# The nth super ugly number is guaranteed to fit in a 32-bit signed integer.

# Example 1:

# Input: n = 12, primes = [2,7,13,19]
# Output: 32
# Explanation: [1,2,4,7,8,13,14,16,19,26,28,32] is the sequence of the first 12 super ugly numbers given primes = [2,7,13,19].

# Example 2:

# Input: n = 1, primes = [2,3,5]
# Output: 1
# Explanation: 1 has no prime factors, therefore all of its prime factors are in the array primes = [2,3,5].
import heapq


class Solution:
    def nthSuperUglyNumber(self, n: int, primes) -> int:
        h = []
        heapq.heappush(h, (1, 0))
        c = 0
        while len(h) > 0:
            mn, start = heapq.heappop(h)
            print(mn,start)
            c += 1
            if c == n:
                return mn
            for i, p in enumerate(primes):
                if i < start:
                    continue
                heapq.heappush(h, (p * mn, i))
        return None


if __name__ == "__main__":
    n = 12
    primes = [2, 7, 13, 19]

    # n = 1
    # primes = [2, 3, 5]
    print(Solution().nthSuperUglyNumber(n, primes))
