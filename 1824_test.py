inst_1824 = __import__("1824")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "obstacles,expected",
        [([0, 1, 2, 3, 0], 2), ([0, 2, 1, 0, 3, 0], 2), ([0, 1, 1, 3, 3, 0], 0)],
    )
    def test_minSideJumps(self, obstacles, expected):
        assert inst_1824.Solution().minSideJumps(obstacles) == expected
