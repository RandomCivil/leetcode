package leetcode

import "fmt"

//Given a circular array (the next element of the last element is the first element of the array), print the Next Greater Number for every element. The Next Greater Number of a number x is the first greater number to its traversing-order next in the array, which means you could search circularly to find its next greater number. If it doesn't exist, output -1 for this number.
/*
Input: [1,2,1]
Output: [2,-1,2]
Explanation: The first 1's next greater number is 2;
The number 2 can't find next greater number;
The second 1's next greater number needs to search circularly, which is also 2.
*/

func NextGreaterElements(nums []int) []int {
	fmt.Println(nums)
	size := len(nums)
	result := make([]int, size)
	for i := 0; i < size; i++ {
		result[i] = -1
	}
	for i := 0; i < size; i++ {
		start := i + 1
		if start == size {
			start = 0
		}
		for j := start; j != i; {
			// fmt.Println(i, j)
			if nums[i] < nums[j] {
				result[i] = nums[j]
				break
			}
			j++
			if j == size {
				j = 0
			}
		}
	}
	return result
}
