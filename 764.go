package leetcode

import (
	"fmt"

	"gitlab.com/RandomCivil/leetcode/common"
)

// You are given an integer n. You have an n x n binary grid grid with all values initially 1's except for some indices given in the array mines. The ith element of the array mines is defined as mines[i] = [xi, yi] where grid[xi][yi] == 0.

// Return the order of the largest axis-aligned plus sign of 1's contained in grid. If there is none, return 0.

// An axis-aligned plus sign of 1's of order k has some center grid[r][c] == 1 along with four arms of length k - 1 going up, down, left, and right, and made of 1's. Note that there could be 0's or 1's beyond the arms of the plus sign, only the relevant area of the plus sign is checked for 1's.

// Example 1:

// Input: n = 5, mines = [[4,2]]
// Output: 2
// Explanation: In the above grid, the largest plus sign can only be of order 2. One of them is shown.
// Example 2:

// Input: n = 1, mines = [[0,0]]
// Output: 0
// Explanation: There is no plus sign, so return 0.

// Constraints:

// 1 <= n <= 500
// 1 <= mines.length <= 5000
// 0 <= xi, yi < n
// All the pairs (xi, yi) are unique.
func orderOfLargestPlusSign(n int, mines [][]int) int {
	set := make(map[[2]int]struct{})
	for _, mine := range mines {
		set[[2]int{mine[0], mine[1]}] = struct{}{}
	}
	dp := make([][]int, n)
	for i := 0; i < n; i++ {
		dp[i] = make([]int, n)
	}

	cumx := make([]int, n)
	for i := 0; i < n; i++ {
		var cumy int
		for j := 0; j < n; j++ {
			if _, ok := set[[2]int{i, j}]; ok {
				cumx[j] = 0
				cumy = 0
			} else {
				cumx[j]++
				cumy++
			}
			dp[i][j] = common.Min(cumx[j], cumy)
		}
	}
	fmt.Println(dp)

	var result int
	cumx = make([]int, n)
	for i := n - 1; i >= 0; i-- {
		var cumy int
		for j := n - 1; j >= 0; j-- {
			if _, ok := set[[2]int{i, j}]; ok {
				cumx[j] = 0
				cumy = 0
			} else {
				cumx[j]++
				cumy++
			}
			dp[i][j] = common.Min(dp[i][j], cumx[j], cumy)
			result = common.Max(result, dp[i][j])
		}
	}
	fmt.Println(dp)
	return result
}
