inst_986_me = __import__("986_me")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "firstList, secondList, expected",
        [
            (
                [[0, 2], [5, 10], [13, 23], [24, 25]],
                [[1, 5], [8, 12], [15, 24], [25, 26]],
                [[1, 2], [5, 5], [8, 10], [15, 23], [24, 24], [25, 25]],
            ),
            (
                [[3, 5], [9, 20]],
                [[4, 5], [7, 10], [11, 12], [14, 15], [16, 20]],
                [[4, 5], [9, 10], [11, 12], [14, 15], [16, 20]],
            ),
            ([[5, 10]], [[5, 10]], [[5, 10]]),
            ([[1, 4], [5, 7]], [[2, 3], [10, 12], [13, 15]], [[2, 3]]),
        ],
    )
    def test_intervalIntersection(self, firstList, secondList, expected):
        assert (
            inst_986_me.Solution().intervalIntersection(firstList, secondList)
            == expected
        )
