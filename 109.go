package leetcode

import "gitlab.com/RandomCivil/leetcode/common"

//Given the head of a singly linked list where elements are sorted in ascending order, convert it to a height balanced BST.

//For this problem, a height-balanced binary tree is defined as a binary tree in which the depth of the two subtrees of every node never differ by more than 1.

//Example 1:

//Input: head = [-10,-3,0,5,9]
//Output: [0,-3,9,-10,null,5]
//Explanation: One possible answer is [0,-3,9,-10,null,5], which represents the shown height balanced BST.

//Example 2:

//Input: head = []
//Output: []

//Example 3:

//Input: head = [0]
//Output: [0]

//Example 4:

// Input: head = [1,3]
// Output: [3,1]
func SortedListToBST(head *common.ListNode) *TreeNode {
	var (
		mid  *common.ListNode
		node *TreeNode
	)
	if head == nil {
		return nil
	}

	mid = findMiddle(head)
	node = &TreeNode{Val: mid.Val}
	if head == mid {
		return node
	}

	node.Left = SortedListToBST(head)
	node.Right = SortedListToBST(mid.Next)
	return node
}

func findMiddle(head *common.ListNode) *common.ListNode {
	var prev, slow, fast *common.ListNode = nil, head, head
	for fast != nil && fast.Next != nil {
		prev = slow
		slow = slow.Next
		fast = fast.Next.Next
	}

	//slow is middle
	if prev != nil {
		prev.Next = nil
	}
	return slow
}
