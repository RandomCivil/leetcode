package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestIsInterleave(t *testing.T) {
	tests := []struct {
		s1, s2, s3 string
		expect     bool
	}{
		{
			s1:     "aabcc",
			s2:     "dbbca",
			s3:     "aadbbcbcac",
			expect: true,
		},
		{
			s1:     "aabcc",
			s2:     "dbbca",
			s3:     "aadbbbaccc",
			expect: false,
		},
		{
			s1:     "",
			s2:     "",
			s3:     "",
			expect: true,
		},
		{
			s1:     "ab",
			s2:     "bc",
			s3:     "babc",
			expect: true,
		},
	}
	Convey("TestReverseBetween", t, func() {
		for _, tt := range tests {
			So(isInterleave(tt.s1, tt.s2, tt.s3), ShouldEqual, tt.expect)
		}
	})
}
