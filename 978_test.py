inst_978_me = __import__("978_me")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "arr, expected",
        [
            ([9, 4, 2, 10, 7, 8, 8, 1, 9], 5),
            ([4, 8, 12, 16], 2),
            ([100], 1),
            ([1, 2], 2),
            ([2, 1, 3], 3),
            ([3, 2, 1], 2),
            ([9, 9], 1),
            ([100, 100, 100], 1),
        ],
    )
    def test_maxTurbulenceSize(self, arr, expected):
        assert inst_978_me.Solution().maxTurbulenceSize(arr) == expected
