package leetcode

import (
	"fmt"

	"gitlab.com/RandomCivil/leetcode/common"
)

// Given a rows x cols binary matrix filled with 0's and 1's, find the largest rectangle containing only 1's and return its area.

// Example 1:

// Input: matrix = [["1","0","1","0","0"],["1","0","1","1","1"],["1","1","1","1","1"],["1","0","0","1","0"]]
// Output: 6
// Explanation: The maximal rectangle is shown in the above picture.
// Example 2:

// Input: matrix = [["0"]]
// Output: 0
// Example 3:

// Input: matrix = [["1"]]
// Output: 1

// Constraints:

// rows == matrix.length
// cols == matrix[i].length
// 1 <= row, cols <= 200
// matrix[i][j] is '0' or '1'.
func maximalRectangle(matrix [][]byte) int {
	fmt.Println(matrix)
	m, n := len(matrix), len(matrix[0])
	dp := make([]int, n+1)
	var result int
	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if matrix[i][j] == '1' {
				dp[j+1]++
			} else {
				dp[j+1] = 0
			}
		}
		fmt.Println(dp)

		var stack [][2]int
		for k, h := range append(dp, 0) {
			for len(stack) > 0 && h < stack[len(stack)-1][0] {
				pop := stack[len(stack)-1]
				stack = stack[:len(stack)-1]
				right := k - 1
				left := stack[len(stack)-1][1] + 1
				result = common.Max(result, pop[0]*(right-left+1))
			}
			stack = append(stack, [2]int{h, k})
		}
	}
	return result
}
