package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/RandomCivil/leetcode/common"
)

func TestPlusOne(t *testing.T) {
	tests := []struct {
		nums, expect []int
	}{
		{
			nums:   []int{1, 2, 3},
			expect: []int{1, 2, 4},
		},
		{
			nums:   []int{1, 9, 9},
			expect: []int{2, 0, 0},
		},
		{
			nums:   []int{9, 9},
			expect: []int{1, 0, 0},
		},
	}
	Convey("TestPlusOne", t, func() {
		for _, tt := range tests {
			l := common.LinkList{Nums: tt.nums}
			So(l.Traversal(plusOne(l.Build())), ShouldResemble, tt.expect)
		}
	})
}
