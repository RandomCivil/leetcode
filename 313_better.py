# A super ugly number is a positive integer whose prime factors are in the array primes.

# Given an integer n and an array of integers primes, return the nth super ugly number.

# The nth super ugly number is guaranteed to fit in a 32-bit signed integer.

# Example 1:

# Input: n = 12, primes = [2,7,13,19]
# Output: 32
# Explanation: [1,2,4,7,8,13,14,16,19,26,28,32] is the sequence of the first 12 super ugly numbers given primes = [2,7,13,19].

# Example 2:

# Input: n = 1, primes = [2,3,5]
# Output: 1
# Explanation: 1 has no prime factors, therefore all of its prime factors are in the array primes = [2,3,5].
from collections import deque


class Solution:
    def nthSuperUglyNumber(self, n: int, primes) -> int:
        size = len(primes)
        dp = []
        for _ in range(size):
            dp.append(deque([1]))

        ii = 0
        while ii < n:
            mn = float('inf')
            for i in range(size):
                mn = min(mn, dp[i][0])
            for i, row in enumerate(dp):
                while len(row) > 0 and row[0] == mn:
                    row.popleft()
                dp[i].append(primes[i] * mn)
            ii += 1

        return mn


if __name__ == '__main__':
    n = 12
    primes = [2, 7, 13, 19]

    n = 1
    primes = [2, 3, 5]
    print(Solution().nthSuperUglyNumber(n, primes))
