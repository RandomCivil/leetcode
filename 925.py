# Your friend is typing his name into a keyboard. Sometimes, when typing a character c, the key might get long pressed, and the character will be typed 1 or more times.
# You examine the typed characters of the keyboard. Return True if it is possible that it was your friends name, with some characters (possibly none) being long pressed.

# Example 1:

# Input: name = "alex", typed = "aaleex"
# Output: true
# Explanation: 'a' and 'e' in 'alex' were long pressed.

# Example 2:

# Input: name = "saeed", typed = "ssaaedd"
# Output: false
# Explanation: 'e' must have been pressed twice, but it wasn't in the typed output.

# Example 3:

# Input: name = "leelee", typed = "lleeelee"
# Output: true

# Example 4:

# Input: name = "laiden", typed = "laiden"
# Output: true
# Explanation: It's not necessary to long press any character.

# Constraints:

# 1 <= name.length <= 1000
# 1 <= typed.length <= 1000
# name and typed contain only lowercase English letters.
import itertools


class Solution:
    def isLongPressedName(self, name: str, typed: str) -> bool:
        group1 = [(k, len(list(v))) for k, v in itertools.groupby(name)]
        group2 = [(k, len(list(v))) for k, v in itertools.groupby(typed)]
        if len(group1) != len(group2):
            return False

        for g1, g2 in zip(group1, group2):
            ch1, c1 = g1
            ch2, c2 = g2
            if ch1 != ch2:
                return False

            if c1 > c2:
                return False

        return True


if __name__ == '__main__':
    name = "saeed"
    typed = "ssaaedd"
    print(Solution().isLongPressedName(name, typed))
