package leetcode

import (
	"fmt"

	"gitlab.com/RandomCivil/leetcode/common"
)

//The thief has found himself a new place for his thievery again. There is only one entrance to this area, called the "root." Besides the root, each house has one and only one parent house. After a tour, the smart thief realized that "all houses in this place forms a binary tree". It will automatically contact the police if two directly-linked houses were broken into on the same night.
//Determine the maximum amount of money the thief can rob tonight without alerting the police.

/*
Input: [3,2,3,null,3,null,1]

     3
    / \
   2   3
    \   \
     3   1

Output: 7
Explanation: Maximum amount of money the thief can rob = 3 + 3 + 1 = 7.

Input: [3,4,5,1,3,null,1]

     3
    / \
   4   5
  / \   \
 1   3   1

Output: 9
Explanation: Maximum amount of money the thief can rob = 4 + 5 = 9.
*/

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

//arr := []int{3, 2, 3, 0, 3, 0, 1}
//arr := []int{3, 4, 5, 1, 3, 0, 1}
//arr := []int{4, 1, 0, 2, 0, 3} 7

//func rob(root *TreeNode) int {
func Rob2(root *common.TreeNode) int {
	var dfs func(head *common.TreeNode) (int, int)
	dfs = func(head *common.TreeNode) (int, int) {
		fmt.Printf("head:%v\n", head)
		if head == nil {
			return 0, 0
		}
		leftVal, leftVal1 := dfs(head.Left)
		rightVal, rightVal1 := dfs(head.Right)
		fmt.Printf("head:%v,left:%v,right:%v,left:%v,right:%v\n", head, leftVal, rightVal, leftVal1, rightVal1)
		//count head,no count head
		return head.Val + leftVal1 + rightVal1, common.Max(leftVal+rightVal, leftVal1+rightVal1, rightVal+leftVal1, leftVal+rightVal1)
	}
	a, b := dfs(root)
	fmt.Println(a, b)
	if a > b {
		return a
	} else {
		return b
	}
}
