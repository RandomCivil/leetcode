# Given a non-negative integer n, find the largest number that is less than or equal to n with monotone increasing digits.

# (Recall that an integer has monotone increasing digits if and only if each pair of adjacent digits x and y satisfy x <= y.)

# Example 1:

# Input: n = 10
# Output: 9

# Example 2:

# Input: n = 1234
# Output: 1234

# Example 3:


# Input: n = 332
# Output: 299
class Solution:
    def monotoneIncreasingDigits(self, n: int) -> int:
        nums = list(map(int, str(n)))
        size = len(nums)
        i = 0
        while i < size - 1 and nums[i] <= nums[i + 1]:
            i += 1
        if i == size - 1:
            return n

        while i >= 1 and nums[i - 1] == nums[i]:
            i -= 1
        print(i)
        nums[i] -= 1
        nums[i + 1 :] = [9] * (size - i - 1)
        return int("".join(map(str, nums)))


if __name__ == "__main__":
    # 299
    n = 332

    # 9
    n = 10
    print(Solution().monotoneIncreasingDigits(n))
