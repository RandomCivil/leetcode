inst_440 = __import__("440")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "n,k,expected",
        [
            (13, 2, 10),
        ],
    )
    def test_findKthNumber(self, n, k, expected):
        assert inst_440.Solution().findKthNumber(n, k) == expected
