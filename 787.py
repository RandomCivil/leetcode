# There are n cities connected by m flights. Each flight starts from city u and arrives at v with a price w.

# Now given all the cities and flights, together with starting city src and the destination dst, your task is to find the cheapest price from src to dst with up to k stops. If there is no such route, output -1.

# Example 1:
# Input:
# n = 3, edges = [[0,1,100],[1,2,100],[0,2,500]]
# src = 0, dst = 2, k = 1
# Output: 200

# Example 2:
# Input:
# n = 3, edges = [[0,1,100],[1,2,100],[0,2,500]]
# src = 0, dst = 2, k = 0
# Output: 500

from collections import defaultdict, deque


class Solution:
    def findCheapestPrice(self, n: int, flights, src: int, dst: int,
                          k: int) -> int:
        d = defaultdict(list)
        for _src, _dst, price in flights:
            d[_src].append((price, _dst))
        print(d)

        result = [float("inf")] * n
        result[src] = 0
        q = deque([(src, 0, 0)])
        while len(q) > 0:
            cur, price, count = q.popleft()
            if count > k:
                continue
            for p, nxt in d[cur]:
                if price + p < result[nxt]:
                    result[nxt] = price + p
                    q.append((nxt, price + p, count + 1))

        print(result)
        r = result[dst]
        return r if r < float('inf') else -1


if __name__ == '__main__':
    # 200
    flights = [[0, 1, 100], [1, 2, 100], [0, 2, 500]]
    n, src, dst, k = 3, 0, 2, 1

    # 500
    flights = [[0, 1, 100], [1, 2, 100], [0, 2, 500]]
    n, src, dst, k = 3, 0, 2, 0

    flights = [[0, 1, 2], [1, 2, 1], [2, 0, 10]]
    n, src, dst, k = 3, 1, 2, 1

    # 6
    flights = [[0, 1, 1], [0, 2, 5], [1, 2, 1], [2, 3, 1]]
    n, src, dst, k = 4, 0, 3, 1
    print(Solution().findCheapestPrice(n, flights, src, dst, k))
