# There are n soldiers standing in a line. Each soldier is assigned a unique rating value.

# You have to form a team of 3 soldiers amongst them under the following rules:

# Choose 3 soldiers with index (i, j, k) with rating (rating[i], rating[j], rating[k]).
# A team is valid if: (rating[i] < rating[j] < rating[k]) or (rating[i] > rating[j] > rating[k]) where (0 <= i < j < k < n).
# Return the number of teams you can form given the conditions. (soldiers can be part of multiple teams).

# Example 1:

# Input: rating = [2,5,3,4,1]
# Output: 3
# Explanation: We can form three teams given the conditions. (2,3,4), (5,4,1), (5,3,1).

# Example 2:

# Input: rating = [2,1,3]
# Output: 0
# Explanation: We can't form any team given the conditions.

# Example 3:


# Input: rating = [1,2,3,4]
# Output: 4
class Solution:
    def numTeams(self, rating) -> int:
        size = len(rating)
        more = [0] * size
        less = [0] * size
        for i in range(size):
            for j in range(i + 1, size):
                if rating[j] > rating[i]:
                    more[i] += 1
                else:
                    less[i] += 1

        print(more, less)

        r = 0
        for i in range(size):
            for j in range(i + 1, size):
                if rating[i] < rating[j]:
                    r += more[j]
                else:
                    r += less[j]

        return r


if __name__ == '__main__':
    rating = [2, 5, 3, 4, 1]
    print(Solution().numTeams(rating))
