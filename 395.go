package leetcode

import "gitlab.com/RandomCivil/leetcode/common"

//Find the length of the longest substring T of a given string (consists of lowercase letters only) such that every character in T appears no less than k times.

/*
Input:
s = "aaabb", k = 3

Output:
3

The longest substring is "aaa", as 'a' is repeated 3 times.

Input:
s = "ababbc", k = 2

Output:
5

The longest substring is "ababb", as 'a' is repeated 2 times and 'b' is repeated 3 times.
*/

//s, k := "ababbc", 2

func longestSubstring(s string, k int) int {
	var result int
	size := len(s)
	counter := func(start, end int) []int {
		c := make([]int, 26)
		for i := start; i < end+1; i++ {
			c[s[i]-97]++
		}
		return c
	}

	var recurse func(start, end int)
	recurse = func(start, end int) {
		if start < 0 || end == size {
			return
		}
		var flag bool
		c := counter(start, end)
		for i := start; i < end+1; i++ {
			if c[s[i]-97] < k {
				flag = true
				recurse(start, i-1)
				recurse(i+1, end)
			}
		}
		if !flag {
			result = common.Max(result, end-start+1)
		}
	}

	recurse(0, size-1)
	return result
}
