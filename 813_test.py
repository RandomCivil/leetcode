inst_813 = __import__("813")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "nums, k,expected",
        [([9, 1, 2, 3, 9], 3, 20), ([1, 2, 3, 4, 5, 6, 7], 4, 20.5)],
    )
    def test_largestSumOfAverages(self, nums, k, expected):
        assert inst_813.Solution().largestSumOfAverages(nums, k) == expected
