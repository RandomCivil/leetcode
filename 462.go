package leetcode

import (
	"math"
	"sort"
)

//Given an integer array nums of size n, return the minimum number of moves required to make all array elements equal.

//In one move, you can increment or decrement an element of the array by 1.

//Example 1:

//Input: nums = [1,2,3]
//Output: 2
//Explanation:
//Only two moves are needed (remember each move increments or decrements one element):
//[1,2,3]  =>  [2,2,3]  =>  [2,2,2]

//Example 2:

//Input: nums = [1,10,2,9]
//Output: 16
func MinMoves2(nums []int) int {
	var (
		r      float64
		size   = len(nums)
		median int
	)
	sort.Ints(nums)
	if size%2 == 1 {
		median = nums[int(math.Ceil(float64(size/2)))]
	} else {
		median = (nums[size/2] + nums[size/2-1]) / 2
	}

	for _, n := range nums {
		r += math.Abs(float64(n - median))
	}

	return int(r)
}
