package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestValidTree(t *testing.T) {
	tests := []struct {
		n      int
		edges  [][]int
		expect bool
	}{
		{
			n: 5,
			edges: [][]int{
				[]int{0, 1},
				[]int{0, 2},
				[]int{0, 3},
				[]int{1, 4},
			},
			expect: true,
		},
		{
			n: 5,
			edges: [][]int{
				[]int{0, 1},
				[]int{1, 2},
				[]int{2, 3},
				[]int{1, 3},
				[]int{1, 4},
			},
			expect: false,
		},

		{
			n: 6,
			edges: [][]int{
				[]int{0, 1},
				[]int{0, 2},
				[]int{0, 3},
				[]int{1, 5},
				[]int{2, 5},
			},
			expect: false,
		},
		{
			n: 4,
			edges: [][]int{
				[]int{2, 3},
				[]int{1, 2},
				[]int{1, 3},
			},
			expect: false,
		},
		{
			n: 3,
			edges: [][]int{
				[]int{1, 0},
				[]int{2, 0},
			},
			expect: true,
		},
		{
			n: 6,
			edges: [][]int{
				[]int{0, 1},
				[]int{0, 2},
				[]int{2, 5},
				[]int{3, 4},
				[]int{3, 5},
			},
			expect: true,
		},
	}
	Convey("TestValidTree", t, func() {
		for _, tt := range tests {
			So(validTree(tt.n, tt.edges), ShouldEqual, tt.expect)
		}
	})
}
