# Given a binary tree, return all root-to-leaf paths.
# Note: A leaf is a node with no children.

# Example:
"""
Input:

   1
 /   \
2     3
 \
  5

Output: ["1->2->5", "1->3"]
"""

# Explanation: All root-to-leaf paths are: 1->2->5, 1->3
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right


class Solution:
    def binaryTreePaths(self, root):
        if not root:
            return []
        self.r = set()

        def dfs(head, parent, path):
            if not head:
                if not parent.left and not parent.right:
                    print(path)
                    self.r.add("->".join(map(str, path)))
                return
            path.append(head.val)
            dfs(head.left, head, path)
            dfs(head.right, head, path)
            path.pop()

        dfs(root, root, [])
        return list(self.r)
