inst_224 = __import__("224")
inst_224_me = __import__("224_me")


import pytest


class TestClass:
    @pytest.mark.parametrize(
        "s,expected",
        [
            ("(1+(4+5+2)-3)+(6+8)", 23),
            ("(1+(4-5+2)-3)+(6+8)", 13),
            ("(1+(4-10+2)-3)+(6+8)", 8),
            ("(1+(4+5+2)-3)+(6-8)", 7),
            ("(1+2-3+5)", 5),
            ("1+1", 2),
            (" 1  + 1 ", 2),
            ("(1+(-4+5+2)-3)+(6-8)", -1),
            ("-1-3-5+4", -5),
            ("-2-3", -5),
            ("-(2+3)", -5),
            ("-(3+4)+5", -2),
            ("- (3 - (4 + 5) )", 6),
            ("- (3 - (- (4 + 5) ) )", -12),
        ],
    )
    def test_calculate(self, s, expected):
        assert inst_224_me.Solution().calculate(s) == expected
        assert inst_224.Solution().calculate(s) == expected
