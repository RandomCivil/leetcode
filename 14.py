# Write a function to find the longest common prefix string amongst an array of strings.
# If there is no common prefix, return an empty string "".

# Example 1:

# Input: strs = ["flower","flow","flight"]
# Output: "fl"

# Example 2:

# Input: strs = ["dog","racecar","car"]
# Output: ""
# Explanation: There is no common prefix among the input strings.

# Constraints:

# 0 <= strs.length <= 200
# 0 <= strs[i].length <= 200
# strs[i] consists of only lower-case English letters.


class Solution:
    def longestCommonPrefix(self, strs) -> str:
        r = ''
        for col in zip(*strs):
            flag = True
            for i in range(1, len(col)):
                print(col[i], col[i - 1])
                if col[i] != col[i - 1]:
                    flag = False
                    break
            else:
                r += col[0]

            if not flag:
                break
        return r


if __name__ == '__main__':
    strs = ["flower", "flow", "flight"]
    # strs = ["dog", "racecar", "car"]
    print(Solution().longestCommonPrefix(strs))
