# For a binary tree T, we can define a flip operation as follows: choose any node, and swap the left and right child subtrees.
# A binary tree X is flip equivalent to a binary tree Y if and only if we can make X equal to Y after some number of flip operations.
# Given the roots of two binary trees root1 and root2, return true if the two trees are flip equivelent or false otherwise.


# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def flipEquiv(self, root1: Optional[TreeNode], root2: Optional[TreeNode]) -> bool:
        if not root1 and not root2:
            return True

        self.result = True

        def dfs(cur1, cur2):
            l1, r1 = -1, -1
            l2, r2 = -1, -1
            if cur1 and cur1.left:
                l1 = cur1.left.val
            if cur1 and cur1.right:
                r1 = cur1.right.val
            if cur2 and cur2.left:
                l2 = cur2.left.val
            if cur2 and cur2.right:
                r2 = cur2.right.val
            if not (cur1 and cur2 and cur1.val == cur2.val):
                self.result = False
                return

            if l1 == l2 and r1 == r2:
                if l1 > -1:
                    dfs(cur1.left, cur2.left)
                if r1 > -1:
                    dfs(cur1.right, cur2.right)
            elif l1 == r2 and r1 == l2:
                if l1 > -1:
                    dfs(cur1.left, cur2.right)
                if r1 > -1:
                    dfs(cur1.right, cur2.left)
            else:
                self.result = False
                return

        dfs(root1, root2)
        return self.result
