# Given an m x n picture consisting of black 'B' and white 'W' pixels, return the number of black lonely pixels.

# A black lonely pixel is a character 'B' that located at a specific position where the same row and same column don't have any other black pixels.

# Example 1:


# Input: picture = [["W","W","B"],["W","B","W"],["B","W","W"]]
# Output: 3
# Explanation: All the three 'B's are black lonely pixels.
# Example 2:


# Input: picture = [["B","B","B"],["B","B","W"],["B","B","B"]]
# Output: 0


# Constraints:


# m == picture.length
# n == picture[i].length
# 1 <= m, n <= 500
# picture[i][j] is 'W' or 'B'.
class Solution:
    def findLonelyPixel(self, picture: list[list[str]]) -> int:
        result = 0
        m, n = len(picture), len(picture[0])
        row_set = [set() for _ in range(m)]
        col_set = [set() for _ in range(n)]
        l = []
        for i in range(m):
            for j in range(n):
                if picture[i][j] == "B":
                    row_set[i].add(j)
                    col_set[j].add(i)
                    l.append((i, j))
        for i, j in l:
            if len(row_set[i]) == 1 and len(col_set[j]) == 1:
                result += 1
        return result
