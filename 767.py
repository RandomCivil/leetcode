# Given a string s, check if the letters can be rearranged so that two characters that are adjacent to each other are not the same.

# If possible, output any possible result.  If not possible, return the empty string.

# Example 1:

# Input: s = "aab"
# Output: "aba"

# Example 2:

# Input: s = "aaab"
# Output: ""

# Note:

# s will consist of lowercase letters and have length in range [1, 500].
from collections import Counter
import heapq


class Solution:
    def reorganizeString(self, s: str) -> str:
        r = ''
        c = Counter(s)
        pc, pch = 0, ''
        heap = [(-v, k) for k, v in c.items()]
        heapq.heapify(heap)
        while len(heap) > 0:
            c, ch = heapq.heappop(heap)
            if c == 0:
                continue
            print(c, ch, r)
            r += ch
            if c < 0:
                heapq.heappush(heap, (pc, pch))
            c += 1
            pc, pch = c, ch

        if len(r) != len(s):
            return ''
        return r


if __name__ == '__main__':
    s = 'aaabbc'
    # s = 'aab'
    # s = 'aaab'
    print(Solution().reorganizeString(s))
