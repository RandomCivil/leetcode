inst_1293 = __import__("1293")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "grid,k,expected",
        [
            ([[0, 0, 0], [1, 1, 0], [0, 0, 0], [0, 1, 1], [0, 0, 0]], 1, 6),
            ([[0, 1, 1], [1, 1, 1], [1, 0, 0]], 1, -1),
            (
                [
                    [0, 0],
                    [1, 0],
                    [1, 0],
                    [1, 0],
                    [1, 0],
                    [1, 0],
                    [0, 0],
                    [0, 1],
                    [0, 1],
                    [0, 1],
                    [0, 0],
                    [1, 0],
                    [1, 0],
                    [0, 0],
                ],
                4,
                14,
            ),
            (
                [
                    [0, 1, 0, 0, 0, 1, 0, 0],
                    [0, 1, 0, 1, 0, 1, 0, 1],
                    [0, 0, 0, 1, 0, 0, 1, 0],
                ],
                1,
                13,
            ),
        ],
    )
    def test_shortestPath(self, grid, k, expected):
        assert inst_1293.Solution().shortestPath(grid, k) == expected
