import pytest


inst_1136 = __import__("1136")


class TestClass:
    @pytest.mark.parametrize(
        "n, relations, expected",
        [
            (3, [[1, 3], [2, 3]], 2),
            (4, [[1, 2], [1, 3], [2, 4], [3, 2], [3, 4]], 4),
            (3, [[1, 2], [2, 3], [3, 1]], -1),
        ],
    )
    def test_minimumSemesters(self, n, relations, expected):
        assert inst_1136.Solution().minimumSemesters(n, relations) == expected
