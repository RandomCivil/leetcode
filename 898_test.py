inst_898 = __import__("898")
inst_898_me = __import__("898_me")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "arr, expected",
        [
            # ([0], 1),
            # ([1, 1, 2], 3),
            # ([1, 2, 4], 6),
            ([1, 3, 5], 4),
            # ([1, 6, 4], 4),
        ],
    )
    def test_subarrayBitwiseORs(self, arr, expected):
        assert inst_898_me.Solution().subarrayBitwiseORs(arr) == expected
        assert inst_898.Solution().subarrayBitwiseORs(arr) == expected
