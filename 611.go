package leetcode

import "sort"

//Given an integer array nums, return the number of triplets chosen from the array that can make triangles if we take them as side lengths of a triangle.

//Example 1:

//Input: nums = [2,2,3,4]
//Output: 3
//Explanation: Valid combinations are:
//2,3,4 (using the first 2)
//2,3,4 (using the second 2)
//2,2,3

//Example 2:

//Input: nums = [4,2,3,4]
//Output: 4

//Constraints:

//1 <= nums.length <= 1000
//0 <= nums[i] <= 1000
func TriangleNumber(nums []int) int {
	var (
		size = len(nums)
		r, k int
	)
	sort.Ints(nums)
	for i := 0; i < size; i++ {
		for j := i + 1; j < size; j++ {
			k = j + 1
			for k < size && nums[i]+nums[j] > nums[k] {
				k++
			}
			r += (k - j - 1)
		}
	}
	return r
}
