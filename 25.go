package leetcode

import (
	"gitlab.com/RandomCivil/leetcode/common"
)

// Given the head of a linked list, reverse the nodes of the list k at a time, and return the modified list.

// k is a positive integer and is less than or equal to the length of the linked list. If the number of nodes is not a multiple of k then left-out nodes, in the end, should remain as it is.

// You may not alter the values in the list's nodes, only nodes themselves may be changed.

// Example 1:

// Input: head = [1,2,3,4,5], k = 2
// Output: [2,1,4,3,5]
// Example 2:

// Input: head = [1,2,3,4,5], k = 3
// Output: [3,2,1,4,5]

// Constraints:

// The number of nodes in the list is n.
// 1 <= k <= n <= 5000
// 0 <= Node.val <= 1000

// Follow-up: Can you solve the problem in O(1) extra memory space?
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func reverseKGroup(head *common.ListNode, k int) *common.ListNode {
	reverse := func(head *common.ListNode) (*common.ListNode, bool) {
		c := 1
		var t, prev *common.ListNode
		last := head
		end := head
		for end != nil && c <= k {
			c++
			end = end.Next
		}
		if c <= k {
			return nil, false
		}

		c = 1
		for head != nil && c <= k {
			t = head.Next
			head.Next = prev
			c++

			prev = head
			head = t
		}
		if c <= k {
			return nil, false
		}
		last.Next = t
		return prev, c == k+1
	}

	preRoot := new(common.ListNode)
	prev, cur := preRoot, head
	for cur != nil {
		rhead, isReachK := reverse(cur)
		if isReachK {
			prev.Next = rhead
		}

		prev = cur
		cur = cur.Next
	}
	return preRoot.Next
}
