package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestSearchRange(t *testing.T) {
	tests := []struct {
		target       int
		nums, expect []int
	}{
		{
			nums:   []int{5, 7, 7, 8, 8, 10},
			target: 8,
			expect: []int{3, 4},
		},
		{
			nums:   []int{5, 7, 7, 8, 8, 10},
			target: 7,
			expect: []int{1, 2},
		},
		{
			nums:   []int{1, 2},
			target: 1,
			expect: []int{0, 0},
		},
		{
			nums:   []int{2, 2},
			target: 2,
			expect: []int{0, 1},
		},
		{
			nums:   []int{5, 7, 7, 8, 8, 10},
			target: 6,
			expect: []int{-1, -1},
		},
	}
	Convey("TestSearchRange", t, func() {
		for _, tt := range tests {
			So(searchRange(tt.nums, tt.target), ShouldResemble, tt.expect)
		}
	})
}
