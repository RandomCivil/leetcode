package leetcode

import "fmt"

//Given an array of integers A, a move consists of choosing any A[i], and incrementing it by 1.
//Return the least number of moves to make every value in A unique.

//Example 1:

//Input: [1,2,2]
//Output: 1
//Explanation:  After 1 move, the array could be [1, 2, 3].

//Example 2:

//Input: [3,2,1,2,1,7]
//Output: 6
//Explanation:  After 6 moves, the array could be [3, 4, 1, 2, 5, 7].
//It can be shown with 5 or less moves that it is impossible for the array to have all unique values.

func MinIncrementForUnique(A []int) int {
	counter := make(map[int]int)
	for _, n := range A {
		if _, ok := counter[n]; !ok {
			counter[n] = 1
		} else {
			counter[n]++
		}
	}
	fmt.Println(counter)

	r := 0
	var taken []int
	for i := 0; i <= 40000; i++ {
		if val, ok := counter[i]; ok {
			if val > 1 {
				for j := 0; j < val-1; j++ {
					taken = append(taken, i)
				}
			}
		} else {
			if len(taken) > 0 {
				fmt.Println(taken)
				fmt.Println(i, taken[0])
				r += i - taken[0]
				taken = taken[1:]
			}
		}
	}
	return r
}
