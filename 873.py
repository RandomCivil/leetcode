# A sequence X1, X2, ..., Xn is Fibonacci-like if:

# n >= 3
# Xi + Xi+1 = Xi+2 for all i + 2 <= n
# Given a strictly increasing array arr of positive integers forming a sequence, return the length of the longest Fibonacci-like subsequence of arr. If one does not exist, return 0.

# A subsequence is derived from another sequence arr by deleting any number of elements (including none) from arr, without changing the order of the remaining elements. For example, [3, 5, 8] is a subsequence of [3, 4, 5, 6, 7, 8].

# Example 1:

# Input: arr = [1,2,3,4,5,6,7,8]
# Output: 5
# Explanation: The longest subsequence that is fibonacci-like: [1,2,3,5,8].

# Example 2:

# Input: arr = [1,3,7,11,12,14,18]
# Output: 3
# Explanation: The longest subsequence that is fibonacci-like: [1,11,12], [3,11,14] or [7,11,18].


class Solution:
    def lenLongestFibSubseq(self, arr) -> int:
        print(arr)
        r = 0
        s = set(arr)
        size = len(arr)
        for i, x in enumerate(arr):
            for j in range(i + 1, size):
                num = 2
                x, y = arr[j], arr[i] + arr[j]
                while y in s:
                    x, y = y, x + y
                    num += 1

                # print('num', num, path)
                if num > r:
                    r = num
        return r if r >= 3 else 0


if __name__ == '__main__':
    arr = [1, 2, 3, 4, 5, 6, 7, 8]
    arr = [1, 3, 7, 11, 12, 14, 18]
    arr = [1, 3, 7]
    # arr = [2, 4, 7, 8, 9, 10, 14, 15, 18, 23, 32, 50]
    print(Solution().lenLongestFibSubseq(arr))
