# Given a matrix consists of 0 and 1, find the distance of the nearest 0 for each cell.
# The distance between two adjacent cells is 1.
"""
Input:
[[0,0,0],
 [0,1,0],
 [0,0,0]]

Output:
[[0,0,0],
 [0,1,0],
 [0,0,0]]

Input:
[[0,0,0],
 [0,1,0],
 [1,1,1]]

Output:
[[0,0,0],
 [0,1,0],
 [1,2,1]]
"""
import sys


class Solution:
    def updateMatrix(self, mat):
        m, n = len(mat), len(mat[0])
        dp = [[2 * 10**4] * (n) for _ in range(m)]

        # down,right
        for i in range(m):
            for j in range(n):
                if mat[i][j] == 1:
                    if i > 0:
                        dp[i][j] = min(dp[i][j], dp[i - 1][j] + 1)
                    if j > 0:
                        dp[i][j] = min(dp[i][j], dp[i][j - 1] + 1)
                else:
                    dp[i][j] = 0
        print(dp)

        # up,left
        for i in range(m - 1, -1, -1):
            for j in range(n - 1, -1, -1):
                if mat[i][j] == 1:
                    if i < m - 1:
                        dp[i][j] = min(dp[i][j], dp[i + 1][j] + 1)
                    if j < n - 1:
                        dp[i][j] = min(dp[i][j], dp[i][j + 1] + 1)
        return dp


if __name__ == "__main__":
    matrix = [[0, 0, 0], [0, 1, 0], [1, 1, 1]]
    # matrix = [[0, 0, 0], [0, 1, 0], [0, 0, 0]]
    # matrix=[[0,1],[1,0]]

    print(Solution().updateMatrix(matrix))
