# We can shift a string by shifting each of its letters to its successive letter.

# For example, "abc" can be shifted to be "bcd".
# We can keep shifting the string to form a sequence.

# For example, we can keep shifting "abc" to form the sequence: "abc" -> "bcd" -> ... -> "xyz".
# Given an array of strings strings, group all strings[i] that belong to the same shifting sequence. You may return the answer in any order.

# Example 1:

# Input: strings = ["abc","bcd","acef","xyz","az","ba","a","z"]
# Output: [["acef"],["a","z"],["abc","bcd","xyz"],["az","ba"]]
# Example 2:

# Input: strings = ["a"]
# Output: [["a"]]


# Constraints:

# 1 <= strings.length <= 200
# 1 <= strings[i].length <= 50
# strings[i] consists of lowercase English letters.
from collections import defaultdict


class Solution:
    def groupStrings(self, strings):
        d = defaultdict(list)
        for s in strings:
            size = len(s)
            key = []
            for i in range(1, size):
                if ord(s[i - 1]) > ord(s[i]):
                    diff = ord(s[i]) - ord(s[i - 1]) + 26
                else:
                    diff = ord(s[i]) - ord(s[i - 1])
                key.append(diff)
            d[tuple(key)].append(s)
        print(d)
        return d.values()


if __name__ == "__main__":
    strings = ["abc", "bcd", "acef", "xyz", "az", "ba", "a", "z"]
    print(Solution().groupStrings(strings))
