# You are given a string s that consists of lower case English letters and brackets.

# Reverse the strings in each pair of matching parentheses, starting from the innermost one.

# Your result should not contain any brackets.

# Example 1:

# Input: s = "(abcd)"
# Output: "dcba"

# Example 2:

# Input: s = "(u(love)i)"
# Output: "iloveu"
# Explanation: The substring "love" is reversed first, then the whole string is reversed.

# Example 3:

# Input: s = "(ed(et(oc))el)"
# Output: "leetcode"
# Explanation: First, we reverse the substring "oc", then "etco", and finally, the whole string.

# Example 4:

# Input: s = "a(bcdefghijkl(mno)p)q"
# Output: "apmnolkjihgfedcbq"

# Constraints:

# 0 <= s.length <= 2000
# s only contains lower case English characters and parentheses.
# It's guaranteed that all parentheses are balanced.


class Solution:
    def reverseParentheses(self, s: str) -> str:
        if s == '':
            return ''

        stack = []
        for ch in s:
            if ch == '(':
                stack.append('')
            elif ch == ')':
                if len(stack) == 0:
                    continue
                p = stack.pop()
                if len(stack) == 0:
                    stack.append(''.join(reversed(p)))
                else:
                    stack[-1] += ''.join(reversed(p))
            else:
                if len(stack) == 0:
                    stack.append(ch)
                else:
                    stack[-1] += ch

        print(stack)
        return stack[0]


if __name__ == '__main__':
    # dcba
    s = "(abcd)"

    # iloveu
    s = "(u(love)i)"

    # leetcode
    s = "(ed(et(oc))el)"

    # apmnolkjihgfedcbq
    s = "a(bcdefghijkl(mno)p)q"
    print(Solution().reverseParentheses(s))
