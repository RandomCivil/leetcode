# Given an array of integers nums sorted in non-decreasing order, find the starting and ending position of a given target value.

# If target is not found in the array, return [-1, -1].

# You must write an algorithm with O(log n) runtime complexity.


# Example 1:

# Input: nums = [5,7,7,8,8,10], target = 8
# Output: [3,4]
# Example 2:

# Input: nums = [5,7,7,8,8,10], target = 6
# Output: [-1,-1]
# Example 3:

# Input: nums = [], target = 0
# Output: [-1,-1]


# Constraints:


# 0 <= nums.length <= 105
# -109 <= nums[i] <= 109
# nums is a non-decreasing array.
# -109 <= target <= 109
class Solution:
    def searchRange(self, nums, target: int):
        print(nums, target)
        size = len(nums)

        def searchLeftMost():
            lo, hi = 0, size - 1
            while lo <= hi:
                mid = (lo + hi) // 2
                if nums[mid] == target:
                    hi -= 1
                elif nums[mid] > target:
                    hi = mid - 1
                else:
                    lo = mid + 1
            return lo

        def searchRightMost():
            lo, hi = 0, size - 1
            while lo <= hi:
                mid = (lo + hi) // 2
                if nums[mid] == target:
                    lo += 1
                elif nums[mid] > target:
                    hi = mid - 1
                else:
                    lo = mid + 1
            return hi

        l, r = searchLeftMost(), searchRightMost()
        if l > r:
            return [-1, -1]
        return [l, r]
