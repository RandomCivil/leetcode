inst_695 = __import__("695")
inst_695_bfs = __import__("695_bfs")


import pytest


class TestClass:
    @pytest.mark.parametrize(
        "grid,expected",
        [
            (
                [
                    [0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
                    [0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0],
                    [0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0],
                ],
                6,
            ),
            ([[0, 1, 1, 0, 1, 0]], 2),
        ],
    )
    def test_maxAreaOfIsland(self, grid, expected):
        # assert inst_695.Solution().maxAreaOfIsland(grid) == expected
        assert inst_695_bfs.Solution().maxAreaOfIsland(grid) == expected
