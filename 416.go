package leetcode

import "fmt"

//Given a non-empty array containing only positive integers, find if the array can be partitioned into two subsets such that the sum of elements in both subsets is equal.

//Each of the array element will not exceed 100.
//The array size will not exceed 200.

/*
Input: [1, 5, 11, 5]
Output: true
Explanation: The array can be partitioned as [1, 5, 5] and [11].

Input: [1, 2, 3, 5]
Output: false
Explanation: The array cannot be partitioned into equal sum subsets.
*/

func canPartition(nums []int) bool {
	size := len(nums)
	counter := make(map[int]int)
	sum := 0
	for i := 0; i < size; i++ {
		counter[nums[i]]++
		sum += nums[i]
	}
	if sum%2 != 0 {
		return false
	}
	target := sum / 2

	var dfs func(sum int) bool
	dfs = func(sum int) bool {
		if sum > target {
			return false
		}
		if sum == target {
			return true
		}
		fmt.Println(counter)
		for k, v := range counter {
			if v <= 0 {
				continue
			}
			counter[k]--
			if dfs(sum + k) {
				return true
			}
			counter[k]++
		}
		return false
	}
	return dfs(0)
}
