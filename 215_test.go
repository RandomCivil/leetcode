package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestFindKthLargest(t *testing.T) {
	tests := []struct {
		nums      []int
		expect, k int
	}{
		{
			nums:   []int{3, 2, 1, 5, 6, 4},
			k:      2,
			expect: 5,
		},
		{
			nums:   []int{3, 2, 3, 1, 2, 4, 5, 5, 6},
			k:      4,
			expect: 4,
		},
		{
			nums:   []int{1},
			k:      1,
			expect: 1,
		},
		{
			nums:   []int{-1, 2, 0},
			k:      1,
			expect: 2,
		},
	}
	Convey("TestNumIslands", t, func() {
		for _, tt := range tests {
			So(findKthLargest(tt.nums, tt.k), ShouldEqual, tt.expect)
		}
	})
}
