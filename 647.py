# Given a string s, return the number of palindromic substrings in it.

# A string is a palindrome when it reads the same backward as forward.

# A substring is a contiguous sequence of characters within the string.

# Example 1:

# Input: s = "abc"
# Output: 3
# Explanation: Three palindromic strings: "a", "b", "c".

# Example 2:

# Input: s = "aaa"
# Output: 6
# Explanation: Six palindromic strings: "a", "a", "a", "aa", "aa", "aaa".

# Constraints:


# 1 <= s.length <= 1000
# s consists of lowercase English letters.
class Solution:
    def countSubstrings(self, s: str) -> int:
        size = len(s)
        dp = [[False] * size for _ in range(size)]
        r = 0

        for i in range(size):
            dp[i][i] = True
            r += 1

        for i in range(size - 1):
            if s[i] == s[i + 1]:
                dp[i][i + 1] = True
                r += 1

        for l in range(3, size + 1):
            for i in range(size - l + 1):
                j = i + l - 1
                dp[i][j] = dp[i + 1][j - 1] and s[i] == s[j]
                if dp[i][j]:
                    r += 1

        return r


if __name__ == '__main__':
    s = "abc"
    # s = "aaa"
    print(Solution().countSubstrings(s))
