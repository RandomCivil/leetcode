# Given an n x n array of integers matrix, return the minimum sum of any falling path through matrix.

# A falling path starts at any element in the first row and chooses the element in the next row that is either directly below or diagonally left/right. Specifically, the next element from position (row, col) will be (row + 1, col - 1), (row + 1, col), or (row + 1, col + 1).

# Example 1:

# Input: matrix = [[2,1,3],[6,5,4],[7,8,9]]
# Output: 13
# Explanation: There are two falling paths with a minimum sum underlined below:
# [[2,1,3],      [[2,1,3],
# [6,5,4],       [6,5,4],
# [7,8,9]]       [7,8,9]]

# Example 2:

# Input: matrix = [[-19,57],[-40,-5]]
# Output: -59
# Explanation: The falling path with a minimum sum is underlined below:
# [[-19,57],
# [-40,-5]]

# Example 3:

# Input: matrix = [[-48]]
# Output: -48

# Constraints:


# n == matrix.length
# n == matrix[i].length
# 1 <= n <= 100
# -100 <= matrix[i][j] <= 100
class Solution:
    def minFallingPathSum(self, matrix: list[list[int]]) -> int:
        m, n = len(matrix), len(matrix[0])
        dp = [[0] * n for _ in range(m + 1)]
        for i in range(1, m + 1):
            for j in range(n):
                dp[i][j] = dp[i - 1][j]
                if j > 0:
                    dp[i][j] = min(dp[i][j], dp[i - 1][j - 1])
                if j < n - 1:
                    dp[i][j] = min(dp[i][j], dp[i - 1][j + 1])
                dp[i][j] += matrix[i - 1][j]
        print(dp)
        return min(dp[-1])


if __name__ == "__main__":
    # 13
    matrix = [[2, 1, 3], [6, 5, 4], [7, 8, 9]]

    # -59
    matrix = [[-19, 57], [-40, -5]]

    matrix = [[-48]]
    print(Solution().minFallingPathSum(matrix))
