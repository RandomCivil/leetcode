# Given an integer array, your task is to find all the different possible increasing subsequences of the given array, and the length of an increasing subsequence should be at least 2.
"""
Input: [4, 6, 7, 7]
Output: [[4, 6], [4, 7], [4, 6, 7], [4, 6, 7, 7], [6, 7], [6, 7, 7], [7,7], [4,7,7]]
"""


class Solution:
    def findSubsequences(self, nums):
        size = len(nums)
        path = []
        s = set()

        def dfs(start):
            if start == size:
                return
            for i in range(start, size):
                if len(path) > 0 and nums[i] < path[-1]:
                    continue
                path.append(nums[i])
                if tuple(path) not in s:
                    if len(path) >= 2:
                        s.add(tuple(path))
                print(nums[i], path)
                dfs(i + 1)
                path.pop()

        dfs(0)
        return list(s)


if __name__ == '__main__':
    nums = [4, 6, 7, 7]
    # nums = []
    print(Solution().findSubsequences(nums))
