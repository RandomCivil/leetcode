package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestMaxAreaOfIsland(t *testing.T) {
	tests := []struct {
		grid   [][]int
		expect int
	}{
		{
			grid: [][]int{
				[]int{0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
				[]int{0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0},
				[]int{0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
				[]int{0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0},
				[]int{0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0},
				[]int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
				[]int{0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0},
				[]int{0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
			},
			expect: 6,
		},
		{
			grid: [][]int{
				{0, 1, 1, 0, 1, 0},
			},
			expect: 2,
		},
	}
	Convey("TestMaxAreaOfIsland", t, func() {
		for _, tt := range tests {
			So(maxAreaOfIsland(tt.grid), ShouldEqual, tt.expect)
		}
	})
}
