package leetcode

import (
	"fmt"
	"sort"
)

func CombinationSum2(candidates []int, target int) [][]int {
	sort.Ints(candidates)
	fmt.Println(candidates)
	var r [][]int
	var cp []int
	size := len(candidates)
	var dfs func(start, sum int, path []int)
	dfs = func(start, sum int, path []int) {
		if sum == target {
			fmt.Println(path)
			cp = make([]int, len(path))
			copy(cp, path)
			r = append(r, cp)
			return
		}

		for i := start; i < size; i++ {
			if i > start && candidates[i] == candidates[i-1] {
				fmt.Println("haha", i, start)
				continue
			}
			if candidates[i]+sum > target {
				break
			}
			path = append(path, candidates[i])
			fmt.Println("path", path, start)
			dfs(i+1, sum+candidates[i], path)
			path = path[:len(path)-1]
		}
	}
	dfs(0, 0, []int{})
	return r
}
