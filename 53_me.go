package leetcode

import (
	"fmt"

	"gitlab.com/RandomCivil/leetcode/common"
)

// Given an integer array nums, find the
// subarray with the largest sum, and return its sum.

// Example 1:

// Input: nums = [-2,1,-3,4,-1,2,1,-5,4]
// Output: 6
// Explanation: The subarray [4,-1,2,1] has the largest sum 6.
// Example 2:

// Input: nums = [1]
// Output: 1
// Explanation: The subarray [1] has the largest sum 1.
// Example 3:

// Input: nums = [5,4,-1,7,8]
// Output: 23
// Explanation: The subarray [5,4,-1,7,8] has the largest sum 23.

// Constraints:

// 1 <= nums.length <= 105
// -104 <= nums[i] <= 104
func maxSubArray1(nums []int) int {
	size := len(nums)
	if size == 1 {
		return nums[0]
	}
	mx := nums[0]
	dp := make([]int, size)
	dp[0] = nums[0]
	for i := 1; i < size; i++ {
		dp[i] = common.Max(dp[i-1]+nums[i], nums[i])
		if dp[i] > mx {
			mx = dp[i]
		}
	}
	fmt.Println(dp)
	return mx
}
