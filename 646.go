package leetcode

import (
	"sort"

	"gitlab.com/RandomCivil/leetcode/common"
)

//You are given n pairs of numbers. In every pair, the first number is always smaller than the second number.
//Now, we define a pair (c, d) can follow another pair (a, b) if and only if b < c. Chain of pairs can be formed in this fashion.
//Given a set of pairs, find the length longest chain which can be formed. You needn't use up all the given pairs. You can select pairs in any order.
/*
Input: [[1,2], [2,3], [3,4]]
Output: 2
Explanation: The longest chain is [1,2] -> [3,4]
*/
func FindLongestChain(pairs [][]int) int {
	size := len(pairs)
	sort.Sort(ByStart(pairs))
	dp := make([]int, size)
	for i := 0; i < size; i++ {
		dp[i] = 1
	}
	for i := 1; i < size; i++ {
		var l []int
		for j := 0; j < i; j++ {
			if pairs[i][0] > pairs[j][1] {
				l = append(l, dp[j]+1)
			} else {
				l = append(l, 1)
			}
		}
		dp[i] = common.Max(l...)
	}

	return common.Max(dp...)
}

type ByStart [][]int

func (a ByStart) Len() int           { return len(a) }
func (a ByStart) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByStart) Less(i, j int) bool { return a[i][1] < a[j][1] }
