inst_684 = __import__("684")
inst_684_union_find = __import__("684_union_find")


import pytest


class TestClass:
    @pytest.mark.parametrize(
        "edges,expected",
        [
            ([[1, 2], [1, 3], [2, 3]], [2, 3]),
            ([[1, 2], [2, 3], [3, 4], [1, 4], [1, 5]], [1, 4]),
            (
                [
                    [2, 7],
                    [7, 8],
                    [3, 6],
                    [2, 5],
                    [6, 8],
                    [4, 8],
                    [2, 8],
                    [1, 8],
                    [7, 10],
                    [3, 9],
                ],
                [2, 8],
            ),
        ],
    )
    def test_findRedundantConnection(self, edges, expected):
        assert inst_684.Solution().findRedundantConnection(edges) == expected
        assert inst_684_union_find.Solution().findRedundantConnection(edges) == expected
