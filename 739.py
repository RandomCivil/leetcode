# Given a list of daily temperatures temperatures, return a list such that, for each day in the input, tells you how many days you would have to wait until a warmer temperature. If there is no future day for which this is possible, put 0 instead.

# For example, given the list of temperatures temperatures = [73, 74, 75, 71, 69, 72, 76, 73], your output should be [1, 1, 4, 2, 1, 1, 0, 0].


# Note: The length of temperatures will be in the range [1, 30000]. Each temperature will be an integer in the range [30, 100].
class Solution:
    def dailyTemperatures(self, temperatures):
        stack = []
        size = len(temperatures)
        result = [0] * size
        for i in range(size - 1, -1, -1):
            while len(stack) > 0 and temperatures[i] > stack[-1][0]:
                stack.pop()
            if len(stack) > 0:
                result[i] = stack[-1][1] - i
            stack.append((temperatures[i], i))
        return result


if __name__ == "__main__":
    temperatures = [89, 62, 70, 58, 47, 47, 46, 76, 100, 70]
    print(Solution().dailyTemperatures(temperatures))
