inst_253 = __import__("253")


import pytest


class TestClass:
    @pytest.mark.parametrize(
        "intervals,expected",
        [
            ([[0, 30], [5, 10], [15, 20]], 2),
            ([[7, 10], [2, 4]], 1),
            ([[13,15],[1,13]],1),
        ],
    )
    def test_minMeetingRooms(self, intervals, expected):
        assert inst_253.Solution().minMeetingRooms(intervals) == expected
