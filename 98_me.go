package leetcode

import (
	"math"

	"gitlab.com/RandomCivil/leetcode/common"
)

// Given the root of a binary tree, determine if it is a valid binary search tree (BST).

// A valid BST is defined as follows:

// The left
// subtree
//  of a node contains only nodes with keys less than the node's key.
// The right subtree of a node contains only nodes with keys greater than the node's key.
// Both the left and right subtrees must also be binary search trees.

// Example 1:

// Input: root = [2,1,3]
// Output: true
// Example 2:

// Input: root = [5,1,4,null,null,3,6]
// Output: false
// Explanation: The root node's value is 5 but its right child's value is 4.

// Constraints:

// The number of nodes in the tree is in the range [1, 104].
// -231 <= Node.val <= 231 - 1
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func isValidBST(root *common.TreeNode) bool {
	result := true
	var dfs func(cur *common.TreeNode) (int, int)
	dfs = func(cur *common.TreeNode) (int, int) {
		var lmin, lmax, rmin, rmax, min, max int
		if cur.Left != nil {
			lmin, lmax = dfs(cur.Left)
			min = lmin
		} else {
			lmin = math.MinInt
			lmax = math.MinInt

			min = cur.Val
		}
		if cur.Right != nil {
			rmin, rmax = dfs(cur.Right)
			max = rmax
		} else {
			rmin = math.MaxInt
			rmax = math.MaxInt

			max = cur.Val
		}

		if !(cur.Val > lmin && cur.Val > lmax) {
			result = false
		}
		if !(cur.Val < rmin && cur.Val < rmax) {
			result = false
		}
		return min, max
	}
	dfs(root)
	return result
}
