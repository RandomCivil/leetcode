package leetcode

import "fmt"

//Given an array of integers nums and an integer k, return the total number of continuous subarrays whose sum equals to k.

//Example 1:

//Input: nums = [1,1,1], k = 2
//Output: 2

//Example 2:

//Input: nums = [1,2,3], k = 3
//Output: 2
func SubarraySum(nums []int, k int) int {
	var (
		r, sum int
		m      = make(map[int]int)
	)
	m[0] = 1
	for _, n := range nums {
		sum += n
		if _, ok := m[sum-k]; ok {
			r += m[sum-k]
		}
		if _, ok := m[sum]; !ok {
			m[sum] = 1
		} else {
			m[sum]++
		}
	}
	fmt.Println(m)
	return r
}
