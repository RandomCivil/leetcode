package leetcode

import "gitlab.com/RandomCivil/leetcode/common"

// Given the head of a sorted linked list, delete all duplicates such that each element appears only once. Return the linked list sorted as well.

// Example 1:

// Input: head = [1,1,2]
// Output: [1,2]
// Example 2:

// Input: head = [1,1,2,3,3]
// Output: [1,2,3]

// Constraints:

// The number of nodes in the list is in the range [0, 300].
// -100 <= Node.val <= 100
// The list is guaranteed to be sorted in ascending order.
func deleteDuplicates(head *common.ListNode) *common.ListNode {
	if head == nil {
		return nil
	}

	root := head
	cur, dup := head, head.Next
	for cur != nil && dup != nil {
		if dup != nil && cur.Val == dup.Val {
			for dup != nil && cur.Val == dup.Val {
				dup = dup.Next
			}
			cur.Next = dup
		}
		cur = dup
		if dup != nil {
			dup = dup.Next
		}
	}
	return root
}
