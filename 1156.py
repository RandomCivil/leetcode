# Given a string text, we are allowed to swap two of the characters in the string. Find the length of the longest substring with repeated characters.

# Example 1:

# Input: text = "ababa"
# Output: 3
# Explanation: We can swap the first 'b' with the last 'a', or the last 'b' with the first 'a'. Then, the longest repeated character substring is "aaa", which its length is 3.

# Example 2:

# Input: text = "aaabaaa"
# Output: 6
# Explanation: Swap 'b' with the last 'a' (or the first 'a'), and we get longest repeated character substring "aaaaaa", which its length is 6.

# Example 3:

# Input: text = "aaabbaaa"
# Output: 4

# Example 4:

# Input: text = "aaaaa"
# Output: 5
# Explanation: No need to swap, longest repeated character substring is "aaaaa", length is 5.

# Example 5:

# Input: text = "abcdef"
# Output: 1

# Constraints:

# 1 <= text.length <= 20000
# text consist of lowercase English characters only.
import itertools
from collections import Counter, defaultdict


class Solution:
    def maxRepOpt1(self, text: str) -> int:
        print(text)
        g = [(k, len(list(v))) for k, v in itertools.groupby(text)]
        print(g)

        d = defaultdict(list)
        for i, (k, _) in enumerate(g):
            d[k].append(i)
        print(d)

        c = Counter(text)
        r = max(g, key=lambda x: x[1])[1]
        for ch, l in d.items():
            size = len(l)
            for i in range(size - 1):
                if l[i + 1] - l[i] == 2 and g[l[i] + 1][1] == 1:
                    if c[g[l[i]][0]] > g[l[i]][1] + g[l[i + 1]][1]:
                        r = max(r, g[l[i]][1] + g[l[i + 1]][1] + 1)
                    else:
                        r = max(r, g[l[i]][1] + g[l[i + 1]][1])
                else:
                    r = max(r, max(g[l[i]][1], g[l[i + 1]][1]) + 1)

        return r


if __name__ == '__main__':
    # 3
    text = "ababa"
    # 6
    text = 'aaabaaa'
    # 4
    # text = 'aaabbaaa'
    # 5
    # text = 'aaaaa'
    print(Solution().maxRepOpt1(text))
