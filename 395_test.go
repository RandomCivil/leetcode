package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestLongestSubstring(t *testing.T) {
	tests := []struct {
		s         string
		expect, k int
	}{
		{
			s:      "bbaaacbd",
			k:      3,
			expect: 3,
		},
		{
			s:      "ababcabaaadc",
			k:      2,
			expect: 4,
		},
		{
			s:      "ababbc",
			k:      2,
			expect: 5,
		},
	}
	Convey("TestLongestSubstring", t, func() {
		for _, tt := range tests {
			So(longestSubstring(tt.s, tt.k), ShouldEqual, tt.expect)
		}
	})
}
