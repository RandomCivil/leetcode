# You are given an array points representing integer coordinates of some points on a 2D-plane, where points[i] = [xi, yi].

# The cost of connecting two points [xi, yi] and [xj, yj] is the manhattan distance between them: |xi - xj| + |yi - yj|, where |val| denotes the absolute value of val.

# Return the minimum cost to make all points connected. All points are connected if there is exactly one simple path between any two points.

# Example 1:


# Input: points = [[0,0],[2,2],[3,10],[5,2],[7,0]]
# Output: 20
# Explanation:

# We can connect the points as shown above to get the minimum cost of 20.
# Notice that there is a unique path between every pair of points.
# Example 2:

# Input: points = [[3,12],[-2,5],[-4,1]]
# Output: 18


# Constraints:

# 1 <= points.length <= 1000
# -106 <= xi, yi <= 106
# All pairs (xi, yi) are distinct.
from collections import defaultdict, deque
import heapq


class Solution:
    def minCostConnectPoints(self, points: list[list[int]]) -> int:
        print(points)
        size = len(points)
        h = []
        for i in range(1, size):
            heapq.heappush(
                h,
                (
                    abs(points[i][0] - points[0][0]) + abs(points[i][1] - points[0][1]),
                    i,
                ),
            )

        result = 0
        c = 0
        candidates = {i for i in range(1, size)}
        while len(h) > 0 and c < size:
            dis, cur = heapq.heappop(h)
            if cur not in candidates:
                continue
            candidates.remove(cur)
            result += dis
            c += 1
            for i in candidates:
                heapq.heappush(
                    h,
                    (
                        abs(points[i][0] - points[cur][0])
                        + abs(points[i][1] - points[cur][1]),
                        i,
                    ),
                )

        return result
