# Given two strings s and t, determine if they are isomorphic.
# Two strings s and t are isomorphic if the characters in s can be replaced to get t.
# All occurrences of a character must be replaced with another character while preserving the order of characters. No two characters may map to the same character, but a character may map to itself.

# Example 1:

# Input: s = "egg", t = "add"
# Output: true

# Example 2:

# Input: s = "foo", t = "bar"
# Output: false

# Example 3:

# Input: s = "paper", t = "title"
# Output: true

# Constraints:

# 1 <= s.length <= 5 * 104
# t.length == s.length
# s and t consist of any valid ascii character.


class Solution:
    def isIsomorphic(self, s: str, t: str) -> bool:
        d1 = {}
        d2 = {}
        for ch1, ch2 in zip(s, t):
            # print(ch1, ch2)
            if ch1 not in d1:
                d1[ch1] = ch2
            else:
                if d1[ch1] != ch2:
                    return False

            if ch2 not in d2:
                d2[ch2] = ch1
            else:
                if d2[ch2] != ch1:
                    return False
        return True


if __name__ == '__main__':
    s, t = 'egg', 'add'
    print(Solution().isIsomorphic(s, t))
