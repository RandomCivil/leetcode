inst_842 = __import__("842")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "nums, expected",
        [
            ("1101111", {(11, 0, 11, 11), (110, 1, 111)}),
            ("112358130", set()),
            ("0123", set()),
            ("539834657215398346785398346991079669377161950407626991734534318677529701785098211336528511",set())
        ],
    )
    def test_splitIntoFibonacci(self, nums, expected):
        result = inst_842.Solution().splitIntoFibonacci(nums)
        if len(result) > 0:
            assert tuple(result) in expected
        else:
            assert result == []
