inst_1530 = __import__("1530")
inst_1530_me = __import__("1530_me")

import pytest
import common


class TestClass:
    @pytest.mark.parametrize(
        "root, distance, expected",
        [
            ([1], 1, 0),
            ([1, 2, 3, 0, 4], 3, 1),
            ([1, 2, 3, 4, 5, 6, 7], 3, 2),
            ([1, 2, 3, 0, 4], 3, 1),
            ([7, 1, 4, 6, 0, 5, 3, 0, 0, 0, 0, 0, 2], 3, 1),
            ([7, 1, 4, 6, 0, 5, 3, 0, 0, 0, 0, 0, 2], 4, 2),
        ],
    )
    def test_countPairs(self, root, distance, expected):
        assert (
            inst_1530.Solution().countPairs(common.build_tree(root), distance)
            == expected
        )
        assert (
            inst_1530_me.Solution().countPairs(common.build_tree(root), distance)
            == expected
        )
