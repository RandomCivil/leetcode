# Given an integer n, return a list of all possible full binary trees with n nodes. Each node of each tree in the answer must have Node.val == 0.

# Each element of the answer is the root node of one possible tree. You may return the final list of trees in any order.

# A full binary tree is a binary tree where each node has exactly 0 or 2 children.


# Example 1:


# Input: n = 7
# Output: [[0,0,0,null,null,0,0,null,null,0,0],[0,0,0,null,null,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,null,null,null,null,0,0],[0,0,0,0,0,null,null,0,0]]
# Example 2:

# Input: n = 3
# Output: [[0,0,0]]


# Constraints:


# 1 <= n <= 20
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def allPossibleFBT(self, n: int):
        self.cache = {}

        def build(start, end):
            if (start, end) in self.cache:
                return self.cache[(start, end)]
            if start > end:
                return [None]
            res = []
            for i in range(start, end + 1):
                lefts = build(start, i - 1)
                rights = build(i + 1, end)
                for l in lefts:
                    for r in rights:
                        if (not l and not r) or (l and r):
                            node = TreeNode(val=0)
                            node.left = l
                            node.right = r
                            res.append(node)
            self.cache[(start, end)] = res
            return res

        return build(1, n)


if __name__ == "__main__":
    print(Solution().allPossibleFBT(3))
    print(Solution().allPossibleFBT(7))
