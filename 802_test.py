inst_802 = __import__("802")
inst_802_dfs = __import__("802_dfs")
inst_802_color = __import__("802_color")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "graph,expected",
        [
            ([[1, 2], [2, 3], [5], [0], [5], [], []], [2, 4, 5, 6]),
            ([[1, 2, 3, 4], [1, 2], [3, 4], [0, 4], []], [4]),
            ([[0, 1, 4], [0, 2], [3, 4], [2, 4], [3]], []),
        ],
    )
    def test_eventualSafeNodes(self, graph, expected):
        assert inst_802.Solution().eventualSafeNodes(graph) == expected
        assert inst_802_dfs.Solution().eventualSafeNodes(graph) == expected
        assert inst_802_color.Solution().eventualSafeNodes(graph) == expected
