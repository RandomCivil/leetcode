# On an 2 x 3 board, there are five tiles labeled from 1 to 5, and an empty square represented by 0. A move consists of choosing 0 and a 4-directionally adjacent number and swapping it.

# The state of the board is solved if and only if the board is [[1,2,3],[4,5,0]].

# Given the puzzle board board, return the least number of moves required so that the state of the board is solved. If it is impossible for the state of the board to be solved, return -1.


# Example 1:


# Input: board = [[1,2,3],[4,0,5]]
# Output: 1
# Explanation: Swap the 0 and the 5 in one move.
# Example 2:


# Input: board = [[1,2,3],[5,4,0]]
# Output: -1
# Explanation: No number of moves will make the board solved.
# Example 3:


# Input: board = [[4,1,2],[5,0,3]]
# Output: 5
# Explanation: 5 is the smallest number of moves that solves the board.
# An example path:
# After move 0: [[4,1,2],[5,0,3]]
# After move 1: [[4,1,2],[0,5,3]]
# After move 2: [[0,1,2],[4,5,3]]
# After move 3: [[1,0,2],[4,5,3]]
# After move 4: [[1,2,0],[4,5,3]]
# After move 5: [[1,2,3],[4,5,0]]


# Constraints:

# board.length == 2
# board[i].length == 3
# 0 <= board[i][j] <= 5
# Each value board[i][j] is unique.
from collections import deque


class Solution:
    def slidingPuzzle(self, board: list[list[int]]) -> int:
        m, n = len(board), len(board[0])

        def find_start_zero():
            for i in range(m):
                for j in range(n):
                    if board[i][j] == 0:
                        return i, j

        visited = set()
        directions = [(1, 0), (-1, 0), (0, 1), (0, -1)]
        target = [[1, 2, 3], [4, 5, 0]]
        q = deque([(*find_start_zero(), board, 0)])
        while len(q) > 0:
            x, y, cur, c = q.popleft()
            l = tuple(cur[0] + cur[1])
            if l in visited:
                continue
            visited.add(l)
            if cur == target:
                return c
            for xx, yy in directions:
                if 0 <= x + xx < m and 0 <= y + yy < n:
                    ncur = [cur[i].copy() for i in range(m)]
                    ncur[x][y], ncur[x + xx][y + yy] = ncur[x + xx][y + yy], ncur[x][y]
                    q.append((x + xx, y + yy, ncur, c + 1))
        return -1
