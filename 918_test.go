package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestMaxSubarraySumCircular(t *testing.T) {
	tests := []struct {
		nums   []int
		expect int
	}{
		{
			nums:   []int{1, -2, 3, -2},
			expect: 3,
		},
		{
			nums:   []int{5, -3, 5},
			expect: 10,
		},
		{
			nums:   []int{-2, 4, -5, 4, -5, 9, 4},
			expect: 15,
		},
		{
			nums:   []int{6, 9, -3},
			expect: 15,
		},
		{
			nums:   []int{-3, -2, -3},
			expect: -2,
		},
	}
	Convey("TestMaxSubarraySumCircular", t, func() {
		for _, tt := range tests {
			So(maxSubarraySumCircular(tt.nums), ShouldEqual, tt.expect)
		}
	})
}
