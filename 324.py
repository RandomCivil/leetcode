# Given an integer array nums, reorder it such that nums[0] < nums[1] > nums[2] < nums[3]....

# You may assume the input array always has a valid answer.

# Example 1:

# Input: nums = [1,5,1,1,6,4]
# Output: [1,6,1,5,1,4]
# Explanation: [1,4,1,5,1,6] is also accepted.

# Example 2:

# Input: nums = [1,3,2,2,3,1]
# Output: [2,3,1,3,1,2]
import heapq


class Solution:
    def wiggleSort(self, nums) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        arr = sorted(nums)
        size = len(nums)
        for i in range(1, size, 2):
            nums[i] = arr.pop()
        for i in range(0, size, 2):
            nums[i] = arr.pop()


if __name__ == '__main__':
    nums = [1, 5, 1, 1, 6, 4]
    nums = [1, 3, 2, 2, 3, 1]
    Solution().wiggleSort(nums)
    print(nums)
