inst_1774 = __import__("1774")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "baseCosts, toppingCosts, target,expected",
        [
            ([1, 7], [3, 4], 10, 10),
            ([3, 10], [2, 5], 9, 8),
            ([52, 48, 17, 44, 33, 58], [74, 28, 20, 98, 46, 9, 1, 22, 2], 93, 93),
            (
                [52, 48, 17, 44, 33, 17, 58, 52],
                [74, 28, 20, 98, 46, 9, 1, 1, 22, 2],
                93,
                93,
            ),
            ([2, 3], [4, 5, 100], 18, 17),
            ([10], [1], 1, 10),
        ],
    )
    def test_closestCost(self, baseCosts, toppingCosts, target, expected):
        assert (
            inst_1774.Solution().closestCost(baseCosts, toppingCosts, target)
            == expected
        )
