package leetcode

import (
	"container/heap"
	"fmt"
	"strings"
)

//Given a non-empty list of words, return the k most frequent elements.
//Your answer should be sorted by frequency from highest to lowest. If two words have the same frequency, then the word with the lower alphabetical order comes first.
/*
Input: ["i", "love", "leetcode", "i", "love", "coding"], k = 2
Output: ["i", "love"]
Explanation: "i" and "love" are the two most frequent words.
    Note that "i" comes before "love" due to a lower alphabetical order.

Input: ["the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"], k = 4
Output: ["the", "is", "sunny", "day"]
Explanation: "the", "is", "sunny" and "day" are the four most frequent words,
    with the number of occurrence being 4, 3, 2 and 1 respectively.
*/

type Item struct {
	Val  string
	Freq int
}

type PriorityQueue []*Item

func (h PriorityQueue) Len() int { return len(h) }
func (h PriorityQueue) Less(i, j int) bool {
	if h[i].Freq == h[j].Freq {
		return strings.Compare(h[i].Val, h[j].Val) < 0
	} else {
		return h[i].Freq > h[j].Freq
	}
}
func (h PriorityQueue) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
}
func (h *PriorityQueue) Push(x interface{}) {
	// Push and Pop use pointer receivers because they modify the slice's length,
	// not just its contents.
	*h = append(*h, x.(*Item))
}

func (h *PriorityQueue) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

func TopKFrequent(words []string, k int) []string {
	fmt.Println(words, k)
	var result []string
	counter := make(map[string]int)
	for _, w := range words {
		if _, ok := counter[w]; ok {
			counter[w]++
		} else {
			counter[w] = 1
		}
	}

	fmt.Println(counter)
	var pq PriorityQueue
	for w, freq := range counter {
		pq = append(pq, &Item{w, freq})
	}
	heap.Init(&pq)

	n := 0
	for n < k {
		p := heap.Pop(&pq)
		fmt.Println(p)
		result = append(result, p.(*Item).Val)
		n++
	}
	return result
}
