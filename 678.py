# Given a string containing only three types of characters: '(', ')' and '*', write a function to check whether this string is valid. We define the validity of a string by these rules:

# Any left parenthesis '(' must have a corresponding right parenthesis ')'.
# Any right parenthesis ')' must have a corresponding left parenthesis '('.
# Left parenthesis '(' must go before the corresponding right parenthesis ')'.
# '*' could be treated as a single right parenthesis ')' or a single left parenthesis '(' or an empty string.
# An empty string is also valid.
"""
Input: "()"
Output: True

Input: "(*)"
Output: True

Input: "(*))"
Output: True
"""


class Solution:
    def checkValidString(self, s: str) -> bool:
        size = len(s)
        print(s, size)
        cadidate = ['(', ')', '']
        self.ans = False

        def dfs(cur, i):
            if i == size:
                self.ans |= check(cur)
                return
            if s[i] == '*':
                for c in cadidate:
                    dfs(cur + c, i + 1)
                    if self.ans:
                        return
            else:
                dfs(cur + s[i], i + 1)

        def check(s):
            result = True
            l = []
            for s1 in s:
                if s1 == '(':
                    l.append(s1)
                else:
                    if len(l) == 0:
                        result = False
                        break
                    p = l.pop()
                    if p != '(':
                        result = False
                        break

            if len(l) > 0:
                result = False
            return result

        dfs('', 0)
        return self.ans


if __name__ == '__main__':
    s = '(*))'
    s = '((**)'
    s = '))())))*))())()(**(((())(()(*()((((())))*())(())*(*(()(*)))()*())**((()(()))())(*(*))*))())'
    print(Solution().checkValidString(s))
