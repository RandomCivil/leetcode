package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestWordBreak(t *testing.T) {
	tests := []struct {
		s        string
		wordDict []string
		expect   bool
	}{
		{
			s:        "leetcode",
			wordDict: []string{"leet", "code"},
			expect:   true,
		},
		{
			s:        "applepenapple",
			wordDict: []string{"apple", "pen"},
			expect:   true,
		},
		{
			s:        "catsandog",
			wordDict: []string{"cats", "dog", "sand", "and", "cat"},
			expect:   false,
		},
	}
	Convey("TestLongestConsecutive", t, func() {
		for _, tt := range tests {
			So(wordBreak(tt.s, tt.wordDict), ShouldEqual, tt.expect)
		}
	})
}
