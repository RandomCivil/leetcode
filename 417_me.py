# You are given an m x n integer matrix heights representing the height of each unit cell in a continent. The Pacific ocean touches the continent's left and top edges, and the Atlantic ocean touches the continent's right and bottom edges.

# Water can only flow in four directions: up, down, left, and right. Water flows from a cell to an adjacent one with an equal or lower height.

# Return a list of grid coordinates where water can flow to both the Pacific and Atlantic oceans.

# Example 1:

# Input: heights = [[1,2,2,3,5],[3,2,3,4,4],[2,4,5,3,1],[6,7,1,4,5],[5,1,1,2,4]]
# Output: [[0,4],[1,3],[1,4],[2,2],[3,0],[3,1],[4,0]]

# Example 2:


# Input: heights = [[2,1],[1,2]]
# Output: [[0,0],[0,1],[1,0],[1,1]]
class Solution:
    def pacificAtlantic(self, heights):
        m, n = len(heights), len(heights[0])
        directions = [(1, 0), (-1, 0), (0, 1), (0, -1)]

        self.reach_p = set()
        self.reach_a = set()

        def dfs(x, y, target_x, target_y, reach, visited, prevH):
            if x == target_x or y == target_y:
                return True
            if x < 0 or y < 0 or x == m or y == n:
                return False
            if prevH < heights[x][y]:
                return False
            if (x, y) in visited:
                return False
            visited.add((x, y))
            for xx, yy in directions:
                if dfs(
                    x + xx, y + yy, target_x, target_y, reach, visited, heights[x][y]
                ):
                    reach.add((x, y))
                    return True
            return False

        for i in range(m):
            for j in range(n):
                if (i, j) in self.reach_p:
                    continue
                dfs(i, j, -1, -1, self.reach_p, set(), float("inf"))

        for i in range(m):
            for j in range(n):
                if (i, j) in self.reach_a:
                    continue
                dfs(i, j, m, n, self.reach_a, set(), float("inf"))

        print(self.reach_a)
        print(self.reach_p)
        return self.reach_a & self.reach_p


if __name__ == "__main__":
    heights = [
        [1, 2, 2, 3, 5],
        [3, 2, 3, 4, 4],
        [2, 4, 5, 3, 1],
        [6, 7, 1, 4, 5],
        [5, 1, 1, 2, 4],
    ]
    print(Solution().pacificAtlantic(heights))
