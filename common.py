class UnionFind:
    def __init__(self, size) -> None:
        self.l = [None] * size
        self.rank = [0] * size

    def find(self, x):
        if self.l[x] is not None:
            self.l[x] = self.find(self.l[x])
            return self.l[x]
        return x

    def union(self, x, y):
        fx, fy = self.find(x), self.find(y)
        merged = False
        if fx != fy:
            if self.rank[fx] > self.rank[fy]:
                self.l[fy] = fx
            elif self.rank[fx] < self.rank[fy]:
                self.l[fx] = fy
            else:
                self.l[fy] = fx
                self.rank[fx] += 1

                # self.l[fx] = fy
                # self.rank[fy] += 1
            merged = True
        return merged

    def isconnected(self, x, y):
        return self.find(x) == self.find(y)


from collections import deque


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


def build_tree(arr):
    if len(arr) == 0:
        return None
    root = TreeNode(val=arr[0])
    q = deque([root])
    i = 0

    size = len(arr)
    if size % 2 == 0:
        arr.append(0)
        size += 1

    while 2 * i + 2 < size:
        head = q.popleft()
        left, right = arr[2 * i + 1], arr[2 * i + 2]
        if left != 0:
            head.left = TreeNode(val=arr[2 * i + 1])
            q.append(head.left)
        if right != 0:
            head.right = TreeNode(val=arr[2 * i + 2])
            q.append(head.right)
        i += 1
    return root


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class LinkList:
    def __init__(self, nums):
        self.nums = nums

    def build(self):
        size = len(self.nums)
        if size == 0:
            return None
        preRoot = ListNode()
        cur = preRoot
        for n in self.nums:
            cur.next = ListNode(val=n)
            cur = cur.next
        return preRoot.next

    def traversal(self, root):
        result = []
        head = root
        while head is not None:
            result.append(head.val)
            head = head.next
        return result
