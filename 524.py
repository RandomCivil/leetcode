# Given a string s and a string array dictionary, return the longest string in the dictionary that can be formed by deleting some of the given string characters. If there is more than one possible result, return the longest word with the smallest lexicographical order. If there is no possible result, return the empty string.

# Example 1:

# Input: s = "abpcplea", dictionary = ["ale","apple","monkey","plea"]
# Output: "apple"

# Example 2:

# Input: s = "abpcplea", dictionary = ["a","b","c"]
# Output: "a"

# Constraints:


# 1 <= s.length <= 1000
# 1 <= dictionary.length <= 1000
# 1 <= dictionary[i].length <= 1000
# s and dictionary[i] consist of lowercase English letters.
class Custom(str):
    def __lt__(x, y):
        if len(x) != len(y):
            return len(x) > len(y)
        else:
            return str(x) < str(y)


class Solution:
    def findLongestWord(self, s: str, dictionary) -> str:
        l = sorted(dictionary, key=lambda x: (-len(x), x))
        print(l)

        size = len(s)

        def compare(d):
            i, j = 0, 0
            while i < size and j < len(d):
                if s[i] == d[j]:
                    j += 1
                i += 1
            return len(d) == j

        for d in l:
            if compare(d):
                return d

        return ""


if __name__ == "__main__":
    s = "abpcplea"
    dictionary = ["ale", "apple", "monkey", "plea"]
    print(Solution().findLongestWord(s, dictionary))
