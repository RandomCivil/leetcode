package leetcode

import (
	"fmt"

	"gitlab.com/RandomCivil/leetcode/common"
)

//Given two words word1 and word2, find the minimum number of steps required to make word1 and word2 the same, where in each step you can delete one character in either string.
/*
Input: "sea", "eat"
Output: 2
Explanation: You need one step to make "sea" to "ea" and another step to make "eat" to "ea".
*/
func MinDistance(word1 string, word2 string) int {
	fmt.Println(word1, word2)
	size1, size2 := len(word1), len(word2)
	dp := make([][]int, size1+1)
	for i := 0; i < size1+1; i++ {
		dp[i] = make([]int, size2+1)
	}

	for i := 1; i <= size1; i++ {
		for j := 1; j <= size2; j++ {
			if word2[j-1] == word1[i-1] {
				dp[i][j] = dp[i-1][j-1] + 1
			} else {
				dp[i][j] = common.Max(dp[i-1][j], dp[i][j-1])
			}
		}
	}
	fmt.Println(dp)
	return size1 + size2 - dp[size1][size2]*2
}
