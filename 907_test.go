package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestSumSubarrayMins(t *testing.T) {
	tests := []struct {
		arr    []int
		expect int
	}{
		{
			arr:    []int{3, 1, 2, 4},
			expect: 17,
		},
		// {
		// 	arr:    []int{11, 81, 94, 43, 3},
		// 	expect: 444,
		// },
	}
	Convey("TestSumSubarrayMins", t, func() {
		for _, tt := range tests {
			So(sumSubarrayMins1(tt.arr), ShouldEqual, tt.expect)
			So(sumSubarrayMins(tt.arr), ShouldEqual, tt.expect)
		}
	})
}
