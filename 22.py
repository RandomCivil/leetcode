# Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.


# Example 1:

# Input: n = 3
# Output: ["((()))","(()())","(())()","()(())","()()()"]
# Example 2:

# Input: n = 1
# Output: ["()"]


# Constraints:

# 1 <= n <= 8
from collections import Counter


class Solution:
    def generateParenthesis(self, n: int):
        self.result = []

        def dfs(cur, c):
            if c["("] > n or c[")"] > n:
                return
            if c["("] == n and c[")"] == n:
                self.result.append(cur)
                return
            for s in ["(", ")"]:
                if s == ")" and c["("] <= c[")"]:
                    continue
                c[s] += 1
                dfs(cur + s, c)
                c[s] -= 1

        dfs("", Counter())
        return self.result
