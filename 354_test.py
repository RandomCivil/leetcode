inst_354 = __import__("354")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "envelopes,expected",
        [
            ([[5, 4], [6, 4], [6, 7], [2, 3]], 3),
            ([[1, 1], [1, 1], [1, 1]], 1),
            ([[1, 3], [3, 5], [6, 7], [6, 8], [8, 4], [9, 5]], 3),
        ],
    )
    def test_maxEnvelopes(self, envelopes, expected):
        assert inst_354.Solution().maxEnvelopes(envelopes) == expected
