# Given a non-empty binary tree, return the average value of the nodes on each level in the form of an array.
# Example 1:
"""
Input:
    3
   / \
  9  20
    /  \
   15   7
Output: [3, 14.5, 11]
"""
# Explanation:
# The average value of nodes on level 0 is 3,  on level 1 is 14.5, and on level 2 is 11. Hence return [3, 14.5, 11].
# Note:
# The range of node's value is in the range of 32-bit signed integer.
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
from collections import deque


class Solution:
    def averageOfLevels(self, root):
        r = []
        queue = deque([root])
        next_queue = deque([])
        level = []
        while len(queue) > 0:
            pop = queue.popleft()
            level.append(pop.val)
            if pop.left:
                next_queue.append(pop.left)
            if pop.right:
                next_queue.append(pop.right)

            if len(queue) == 0:
                print(level)
                r.append(sum(level) / len(level))
                level = []
                queue = next_queue
                next_queue = deque([])

        return r
