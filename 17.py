# Given an array nums of n integers and an integer target, are there elements a, b, c, and d in nums such that a + b + c + d = target? Find all unique quadruplets in the array which gives the sum of target.

# Notice that the solution set must not contain duplicate quadruplets.

# Example 1:

# Input: nums = [1,0,-1,0,-2,2], target = 0
# Output: [[-2,-1,1,2],[-2,0,0,2],[-1,0,0,1]]

# Example 2:

# Input: nums = [], target = 0
# Output: []
from collections import defaultdict, Counter


class Solution:
    def fourSum(self, nums, target: int):
        if len(nums) < 4:
            return []

        r = set()
        nums.sort()
        size = len(nums)
        c = Counter(nums)
        d = defaultdict(list)
        for i in range(size):
            for j in range(i + 1, size):
                d[nums[i] + nums[j]].append((nums[i], nums[j]))

        print(d)
        for s, l1 in d.items():
            if target - s not in d:
                continue

            l2 = d[target - s]
            for n1, n2 in l1:
                for n3, n4 in l2:
                    cc = c.copy()
                    cc[n1] -= 1
                    cc[n2] -= 1
                    cc[n3] -= 1
                    cc[n4] -= 1

                    if cc[n1] < 0 or cc[n2] < 0:
                        continue
                    if cc[n3] < 0 or cc[n4] < 0:
                        continue

                    item = [n1, n2, n3, n4]
                    r.add(tuple(sorted(item)))

        return list(r)


if __name__ == '__main__':
    # [[-2,-1,1,2],[-2,0,0,2],[-1,0,0,1]]
    nums = [1, 0, -1, 0, -2, 2]
    target = 0

    # [[-5,0,4,5],[-3,-2,4,5]]
    nums = [-5, 5, 4, -3, 0, 0, 4, -2]
    target = 4

    nums = [1, -2, -5, -4, -3, 3, 3, 5]
    target = -11
    print(Solution().fourSum(nums, target))
