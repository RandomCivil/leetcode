package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestGenerateParenthesis(t *testing.T) {
	tests := []struct {
		n      int
		expect []string
	}{
		{
			n:      3,
			expect: []string{"((()))", "(()())", "(())()", "()(())", "()()()"},
		},
		{
			n:      1,
			expect: []string{"()"},
		},
	}
	Convey("TestGenerateParenthesis", t, func() {
		for _, tt := range tests {
			r := generateParenthesis(tt.n)
			So(r, ShouldResemble, tt.expect)
		}
	})
}
