package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestNumDecodings(t *testing.T) {
	tests := []struct {
		s      string
		expect int
	}{
		{
			s:      "226",
			expect: 3,
		},
		{
			s:      "12",
			expect: 2,
		},
		{
			s:      "06",
			expect: 0,
		},
		{
			s:      "106",
			expect: 1,
		},
		{
			s:      "12345",
			expect: 3,
		},
	}
	Convey("TestNumDecodings", t, func() {
		for _, tt := range tests {
			So(numDecodings(tt.s), ShouldEqual, tt.expect)
		}
	})
}
