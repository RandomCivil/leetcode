package leetcode

import "fmt"

// There is a robot on an m x n grid. The robot is initially located at the top-left corner (i.e., grid[0][0]). The robot tries to move to the bottom-right corner (i.e., grid[m - 1][n - 1]). The robot can only move either down or right at any point in time.

// Given the two integers m and n, return the number of possible unique paths that the robot can take to reach the bottom-right corner.

// The test cases are generated so that the answer will be less than or equal to 2 * 109.

// Example 1:

// Input: m = 3, n = 7
// Output: 28
// Example 2:

// Input: m = 3, n = 2
// Output: 3
// Explanation: From the top-left corner, there are a total of 3 ways to reach the bottom-right corner:
// 1. Right -> Down -> Down
// 2. Down -> Down -> Right
// 3. Down -> Right -> Down

// Constraints:

// 1 <= m, n <= 100
func uniquePaths(m int, n int) int {
	var result int
	visited := make(map[[2]int]struct{})
	directions := [][]int{
		[]int{1, 0},
		[]int{0, 1},
	}
	var dfs func(x, y int, path [][]int)
	dfs = func(x, y int, path [][]int) {
		if x < 0 || x > m-1 || y < 0 || y > n-1 {
			return
		}

		if _, ok := visited[[2]int{x, y}]; ok {
			return
		}
		visited[[2]int{x, y}] = struct{}{}
		path = append(path, []int{x, y})

		if x == m-1 && y == n-1 {
			result++
			fmt.Println("find", path)
			return
		}
		for _, d := range directions {
			dfs(x+d[0], y+d[1], path)
			delete(visited, [2]int{x + d[0], y + d[1]})
		}
	}
	dfs(0, 0, make([][]int, 0))
	return result
}
