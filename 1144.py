# Given an array nums of integers, a move consists of choosing any element and decreasing it by 1.

# An array A is a zigzag array if either:

# Every even-indexed element is greater than adjacent elements, ie. A[0] > A[1] < A[2] > A[3] < A[4] > ...
# OR, every odd-indexed element is greater than adjacent elements, ie. A[0] < A[1] > A[2] < A[3] > A[4] < ...
# Return the minimum number of moves to transform the given array nums into a zigzag array.

# Example 1:

# Input: nums = [1,2,3]
# Output: 2
# Explanation: We can decrease 2 to 0 or 3 to 1.

# Example 2:

# Input: nums = [9,6,1,6,2]
# Output: 4

# Constraints:

# 1 <= nums.length <= 1000
# 1 <= nums[i] <= 1000


class Solution:
    def movesToMakeZigzag(self, nums) -> int:
        nums = [float('inf')] + nums + [float('inf')]
        size = len(nums)
        even = odd = 0
        for i in range(1, size - 1):
            if nums[i] >= nums[i - 1] or nums[i] >= nums[i + 1]:
                if i % 2 == 0:
                    odd += (nums[i] - min(nums[i - 1], nums[i + 1]) + 1)
                else:
                    even += (nums[i] - min(nums[i - 1], nums[i + 1]) + 1)

        print(even, odd)
        return min(even, odd)


if __name__ == '__main__':
    nums = [1, 2, 3]
    nums = [9, 6, 1, 6, 2]
    print(Solution().movesToMakeZigzag(nums))
