package leetcode

import "gitlab.com/RandomCivil/leetcode/common"

// Given the head of a linked list, remove the nth node from the end of the list and return its head.

// Example 1:

// Input: head = [1,2,3,4,5], n = 2
// Output: [1,2,3,5]
// Example 2:

// Input: head = [1], n = 1
// Output: []
// Example 3:

// Input: head = [1,2], n = 1
// Output: [1]

// Constraints:

// The number of nodes in the list is sz.
// 1 <= sz <= 30
// 0 <= Node.val <= 100
// 1 <= n <= sz

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func removeNthFromEnd1(head *common.ListNode, n int) *common.ListNode {
	root := head
	start, end := head, head
	var i, ii int
	for i < n {
		end = end.Next
		i++
		ii++
	}
	var prev *common.ListNode
	for end != nil {
		prev = start
		start = start.Next
		end = end.Next
		ii++
	}
	if prev != nil {
		prev.Next = start.Next
	}
	if ii == n {
		return root.Next
	}
	return root
}
