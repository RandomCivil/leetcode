# Given two arrays A and B of equal size, the advantage of A with respect to B is the number of indices i for which A[i] > B[i].

# Return any permutation of A that maximizes its advantage with respect to B.

# Example 1:
# Input: A = [2,7,11,15], B = [1,10,4,11]
# Output: [2,11,7,15]

# Example 2:
# Input: A = [12,24,8,32], B = [13,25,32,11]
# Output: [24,32,8,12]

from collections import deque


class Solution:
    def advantageCount(self, nums1: list[int], nums2: list[int]) -> list[int]:
        q = deque(sorted(nums1))
        sn2 = sorted([(n, i) for i, n in enumerate(nums2)], reverse=True)
        for n2, i in sn2:
            if n2 < q[-1]:
                nums2[i] = q.pop()
            else:
                nums2[i] = q.popleft()
        return nums2


if __name__ == "__main__":
    # A = [2, 7, 11, 15]
    # B = [1, 10, 4, 11]

    # A = [12, 24, 8, 32]
    # B = [13, 25, 32, 11]

    A = [2, 0, 4, 1, 2]
    B = [1, 3, 0, 0, 2]
    print(Solution().advantageCount(A, B))
