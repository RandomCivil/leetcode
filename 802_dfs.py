# We start at some node in a directed graph, and every turn, we walk along a directed edge of the graph. If we reach a terminal node (that is, it has no outgoing directed edges), we stop.

# We define a starting node to be safe if we must eventually walk to a terminal node. More specifically, there is a natural number k, so that we must have stopped at a terminal node in less than k steps for any choice of where to walk.

# Return an array containing all the safe nodes of the graph. The answer should be sorted in ascending order.

# The directed graph has n nodes with labels from 0 to n - 1, where n is the length of graph. The graph is given in the following form: graph[i] is a list of labels j such that (i, j) is a directed edge of the graph, going from node i to node j.

# Input: graph = [[1,2],[2,3],[5],[0],[5],[],[]]
# Output: [2,4,5,6]
# Explanation: The given graph is shown above.

# Input: graph = [[1,2,3,4],[1,2],[3,4],[0,4],[]]
# Output: [4]

# n == graph.length
# 1 <= n <= 104
# 0 <= graph[i].legnth <= n
# graph[i] is sorted in a strictly increasing order.
# The graph may contain self-loops.
# The number of edges in the graph will be in the range [1, 4 * 104].


class Solution:
    def eventualSafeNodes(self, graph):
        def dfs(cur, track, visited):
            if track[cur]:
                return True
            if visited[cur]:
                return False
            track[cur] = True
            visited[cur] = True
            for nxt in graph[cur]:
                if dfs(nxt, track, visited):
                    return True
            track[cur] = False
            return False

        size = len(graph)
        visited = [False] * size
        track = [False] * size
        for i in range(size):
            if not visited[i]:
                dfs(i, track, visited)

        result = []
        for i in range(size):
            if not track[i]:
                result.append(i)
        return result
