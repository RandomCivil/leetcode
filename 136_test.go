package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestSingleNumber(t *testing.T) {
	tests := []struct {
		nums   []int
		expect int
	}{
		{
			nums:   []int{1},
			expect: 1,
		},
		{
			nums:   []int{1, 2, 2},
			expect: 1,
		},
		{
			nums:   []int{4, 1, 2, 1, 2},
			expect: 4,
		},
	}
	Convey("TestSingleNumber", t, func() {
		for _, tt := range tests {
			So(singleNumber(tt.nums), ShouldEqual, tt.expect)
		}
	})
}
