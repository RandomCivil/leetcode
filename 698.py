# Given an integer array nums and an integer k, return true if it is possible to divide this array into k non-empty subsets whose sums are all equal.

# Example 1:

# Input: nums = [4,3,2,3,5,2,1], k = 4
# Output: true
# Explanation: It's possible to divide it into 4 subsets (5), (1, 4), (2,3), (2,3) with equal sums.

# Example 2:


# Input: nums = [1,2,3,4], k = 3
# Output: false
from collections import Counter


class Solution:
    def canPartitionKSubsets(self, nums, k: int) -> bool:
        size = len(nums)
        if size == 0:
            return False

        s = sum(nums)
        target, mod = divmod(s, k)
        if mod != 0:
            return False
        print(nums, target)

        def dfs(step, s, remain):
            if step == k:
                return True
            if s > target:
                return False
            elif s == target:
                return dfs(step + 1, 0, remain)

            for n, v in remain.items():
                if v <= 0:
                    continue
                remain[n] -= 1
                if dfs(step, s + n, remain):
                    return True
                remain[n] += 1
            return False

        return dfs(1, 0, Counter(nums))
