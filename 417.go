package leetcode

import (
	"math"
)

//Given an m x n matrix of non-negative integers representing the height of each unit cell in a continent, the "Pacific ocean" touches the left and top edges of the matrix and the "Atlantic ocean" touches the right and bottom edges.
//Water can only flow in four directions (up, down, left, or right) from a cell to another one with height equal or lower.
//Find the list of grid coordinates where water can flow to both the Pacific and Atlantic ocean.

//The order of returned grid coordinates does not matter.
//Both m and n are less than 150.

/*
Given the following 5x5 matrix:

  Pacific ~   ~   ~   ~   ~
       ~  1   2   2   3  (5) *
       ~  3   2   3  (4) (4) *
       ~  2   4  (5)  3   1  *
       ~ (6) (7)  1   4   5  *
       ~ (5)  1   1   2   4  *
          *   *   *   *   * Atlantic

Return:

[[0, 4], [1, 3], [1, 4], [2, 2], [3, 0], [3, 1], [4, 0]] (positions with parentheses in above matrix).
*/

/*
	matrix := [][]int{
		[]int{1, 2, 2, 3, 5},
		[]int{3, 2, 3, 4, 4},
		[]int{2, 4, 5, 3, 1},
		[]int{6, 7, 1, 4, 5},
		[]int{5, 1, 1, 2, 4},
	}
	//[[1,2,2,3,5],[3,2,3,4,4],[2,4,5,3,1],[6,7,1,4,5],[5,1,1,2,4]]
	r := leetcode.PacificAtlantic(matrix)
*/

func PacificAtlantic(matrix [][]int) [][]int {
	m, n := len(matrix), 0
	if m > 0 {
		n = len(matrix[0])
	}

	var result [][]int
	if m == 1 && n == 1 {
		result = append(result, []int{0, 0})
		return result
	}

	cache := make([][]bool, m)
	for i := 0; i < m; i++ {
		cache[i] = make([]bool, n)
	}

	path := make(map[[2]int]struct{})
	directions := [][]int{
		[]int{1, 0},
		[]int{0, -1},
		[]int{-1, 0},
		[]int{0, 1},
	}

	var reachPacific, reachAtlantic bool
	var dfs func(x, y, prevVal int)
	dfs = func(x, y, prevVal int) {
		if x == -1 || y == -1 {
			reachPacific = true
			return
		}
		if x == m || y == n {
			reachAtlantic = true
			return
		}
		if matrix[x][y] > prevVal {
			return
		}
		if cache[x][y] {
			reachPacific = true
			reachAtlantic = true
			return
		}

		path[[2]int{x, y}] = struct{}{}
		for _, direction := range directions {
			x1 := x + direction[0]
			y1 := y + direction[1]
			if _, ok := path[[2]int{x1, y1}]; ok {
				continue
			}

			dfs(x1, y1, matrix[x][y])
			if reachPacific && reachAtlantic {
				delete(path, [2]int{x, y})
				return
			}
		}
		delete(path, [2]int{x, y})
		return
	}

	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			reachPacific, reachAtlantic = false, false
			dfs(i, j, math.MaxInt32)
			//fmt.Println(i, j, reachPacific, reachAtlantic)
			if reachPacific && reachAtlantic {
				cache[i][j] = true
				result = append(result, []int{i, j})
			}
		}
	}
	//fmt.Println(cache)
	return result
}
