package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestCountBits(t *testing.T) {
	tests := []struct {
		n      int
		expect []int
	}{
		{
			n:      2,
			expect: []int{0, 1, 1},
		},
		{
			n:      5,
			expect: []int{0, 1, 1, 2, 1, 2},
		},
	}
	Convey("TestCountBits", t, func() {
		for _, tt := range tests {
			So(countBits(tt.n), ShouldResemble, tt.expect)
		}
	})
}
