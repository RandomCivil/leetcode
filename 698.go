package leetcode

import (
	"fmt"
	"sort"
)

// Given an integer array nums and an integer k, return true if it is possible to divide this array into k non-empty subsets whose sums are all equal.

// Example 1:

// Input: nums = [4,3,2,3,5,2,1], k = 4
// Output: true
// Explanation: It is possible to divide it into 4 subsets (5), (1, 4), (2,3), (2,3) with equal sums.
// Example 2:

// Input: nums = [1,2,3,4], k = 3
// Output: false

// Constraints:

// 1 <= k <= nums.length <= 16
// 1 <= nums[i] <= 104
// The frequency of each element is in the range [1, 4].
func canPartitionKSubsets(nums []int, k int) bool {
	var sum int
	for _, n := range nums {
		sum += n
	}
	if sum%k != 0 {
		return false
	}

	target := sum / k
	fmt.Println(nums, k, target)

	sort.Slice(nums, func(i, j int) bool {
		return nums[i] > nums[j]
	})

	size := len(nums)
	// visited使用map会很慢
	visited := make([]int, size)
	var dfs func(start, group, sum int) bool
	dfs = func(start, group, sum int) bool {
		if sum > target {
			return false
		}
		if sum == target {
			if group == k {
				return true
			}
			return dfs(0, group+1, 0)
		}
		for i := start; i < size; i++ {
			if visited[i] == 1 {
				continue
			}
			visited[i] = 1
			if dfs(i+1, group, sum+nums[i]) {
				return true
			}
			visited[i] = 0
		}
		return false
	}
	return dfs(0, 1, 0)
}
