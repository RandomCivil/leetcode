# Given the root of a binary tree, determine if it is a complete binary tree.

# In a complete binary tree, every level, except possibly the last, is completely filled, and all nodes in the last level are as far left as possible. It can have between 1 and 2h nodes inclusive at the last level h.

# Input: root = [1,2,3,4,5,6]
# Output: true
# Explanation: Every level before the last is full (ie. levels with node-values {1} and {2, 3}), and all nodes in the last level ({4, 5, 6}) are as far left as possible.

# Input: root = [1,2,3,4,5,null,7]
# Output: false
# Explanation: The node with value 7 isn't as far left as possible.
from collections import deque


class Solution:
    def isCompleteTree(self, root) -> bool:
        que = deque()
        que.append(root)
        while que:
            node = que.popleft()
            if node == None:
                print(que)
                return not any(que)
            else:
                que.extend([node.left, node.right])
        return True
