package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestMakesquare(t *testing.T) {
	tests := []struct {
		matchsticks []int
		expect      bool
	}{
		{
			matchsticks: []int{1, 1, 2, 2, 2},
			expect:      true,
		},
		{
			matchsticks: []int{3, 3, 3, 3, 4},
			expect:      false,
		},
	}
	Convey("TestMakesquare", t, func() {
		for _, tt := range tests {
			So(makesquare(tt.matchsticks), ShouldEqual, tt.expect)
		}
	})
}
