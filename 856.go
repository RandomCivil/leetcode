package leetcode

import (
	"fmt"
	"reflect"
)

//Given a balanced parentheses string S, compute the score of the string based on the following rule:

//() has score 1
//AB has score A + B, where A and B are balanced parentheses strings.
//(A) has score 2 * A, where A is a balanced parentheses string.

//Example 1:

//Input: "()"
//Output: 1

//Example 2:
//Input: "(())"
//Output: 2

//Example 3:
//Input: "()()"
//Output: 2

//Example 4:
//Input: "(()(()))"
//Output: 6

func ScoreOfParentheses(S string) int {
	fmt.Println(S, []rune(S), reflect.TypeOf(S[0]))
	var pop, v int
	stack := []int{0}
	for _, s := range S {
		fmt.Println(s, reflect.TypeOf(s), string(s) == "(")
		if string(s) == "(" {
			stack = append(stack, 0)
		} else {
			pop = stack[len(stack)-1]
			stack = stack[:len(stack)-1]
			v = 2 * pop
			if v < 1 {
				v = 1
			}
			stack[len(stack)-1] += v
		}
		fmt.Println("vvvv", stack)
	}
	fmt.Println(stack)
	return stack[len(stack)-1]
}
