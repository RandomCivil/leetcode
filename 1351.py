# Given a m x n matrix grid which is sorted in non-increasing order both row-wise and column-wise, return the number of negative numbers in grid.

# Example 1:

# Input: grid = [[4,3,2,-1],[3,2,1,-1],[1,1,-1,-2],[-1,-1,-2,-3]]
# Output: 8
# Explanation: There are 8 negatives number in the matrix.

# Example 2:

# Input: grid = [[3,2],[1,0]]
# Output: 0

# Example 3:

# Input: grid = [[1,-1],[-1,-1]]
# Output: 3

# Example 4:

# Input: grid = [[-1]]
# Output: 1

# Constraints:

# m == grid.length
# n == grid[i].length
# 1 <= m, n <= 100
# -100 <= grid[i][j] <= 100
import bisect


class Solution:
    def countNegatives(self, grid) -> int:
        print(grid)
        cnt = 0
        m = len(grid)
        n = len(grid[0])
        r = m - 1
        c = 0
        while r >= 0 and c < n:
            if grid[r][c] < 0:
                cnt += n - c
                r -= 1
            else:
                c += 1

        return cnt


if __name__ == '__main__':
    grid = [[4, 3, 2, -1], [3, 2, 1, -1], [1, 1, -1, -2], [-1, -1, -2, -3]]
    grid = [[3, 2], [1, 0]]
    grid = [[1, -1], [-1, -1]]
    grid = [[-1]]
    grid = [[3, -1, -3, -3, -3], [2, -2, -3, -3, -3], [1, -2, -3, -3, -3],
            [0, -3, -3, -3, -3]]
    print(Solution().countNegatives(grid))
