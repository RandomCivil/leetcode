# There are N network nodes, labelled 1 to N.
# Given times, a list of travel times as directed edges times[i] = (u, v, w), where u is the source node, v is the target node, and w is the time it takes for a signal to travel from source to target.
# Now, we send a signal from a certain node K. How long will it take for all
# nodes to receive the signal? If it is impossible, return -1.

# Input: times = [[2,1,1],[2,3,1],[3,4,1]], N = 4, K = 2
# Output: 2

from collections import defaultdict


class Solution:
    def networkDelayTime(self, times, n: int, k: int) -> int:
        d = defaultdict(list)
        for src, dst, w in times:
            d[src].append((w, dst))
        print(d)

        if k not in d:
            return -1

        result = [0] + [float('inf')] * n
        result[k] = 0

        def dfs(cur, s):
            for w, nxt in sorted(d[cur]):
                if w + s < result[nxt]:
                    result[nxt] = w + s
                    dfs(nxt, w + s)

        dfs(k, 0)
        print(result)
        m = max(result)
        return -1 if m == float('inf') else m


if __name__ == '__main__':
    times = [[2, 1, 1], [2, 3, 1], [3, 4, 1]]
    N, K = 4, 2

    # times = [[1, 2, 1]]
    # N, K = 2, 1

    # times = [[1,2,1],[2,3,2],[1,3,4]]
    # times = [[1, 2, 1], [2, 3, 2], [1, 3, 2]]
    # N, K = 3, 1
    print(Solution().networkDelayTime(times, N, K))
