# Given a string containing just the characters '(' and ')', return the length of the longest valid (well-formed) parentheses substring.

# Example 1:

# Input: s = "(()"
# Output: 2
# Explanation: The longest valid parentheses substring is "()".
# Example 2:

# Input: s = ")()())"
# Output: 4
# Explanation: The longest valid parentheses substring is "()()".
# Example 3:

# Input: s = ""
# Output: 0


# Constraints:


# 0 <= s.length <= 3 * 104
# s[i] is '(', or ')'.
class Solution:
    def longestValidParentheses(self, s: str) -> int:
        result = 0
        stack = [["", 0]]
        for ss in s:
            if ss == ")" and len(stack) > 1 and stack[-1][0] == "(":
                _, n = stack.pop()
                stack[-1][1] += n + 2
                result = max(result, stack[-1][1])
            else:
                stack.append([ss, 0])
        print(stack)
        return result
