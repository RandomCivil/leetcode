# Given a m * n matrix of distinct numbers, return all lucky numbers in the matrix in any order.

# A lucky number is an element of the matrix such that it is the minimum element in its row and maximum in its column.

# Example 1:

# Input: matrix = [[3,7,8],[9,11,13],[15,16,17]]
# Output: [15]
# Explanation: 15 is the only lucky number since it is the minimum in its row and the maximum in its column

# Example 2:

# Input: matrix = [[1,10,4,2],[9,3,8,7],[15,16,17,12]]
# Output: [12]
# Explanation: 12 is the only lucky number since it is the minimum in its row and the maximum in its column.

# Example 3:


# Input: matrix = [[7,8],[1,2]]
# Output: [7]
class Solution:
    def luckyNumbers(self, matrix):
        row_mn = [min(row) for row in matrix]
        col_mx = [max(col) for col in zip(*matrix)]
        print(row_mn, col_mx)
        return list(set(row_mn) & set(col_mx))


if __name__ == '__main__':
    matrix = [[3, 7, 8], [9, 11, 13], [15, 16, 17]]
    matrix = [[1, 10, 4, 2], [9, 3, 8, 7], [15, 16, 17, 12]]
    matrix = [[7, 8], [1, 2]]
    print(Solution().luckyNumbers(matrix))
