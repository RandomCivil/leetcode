package leetcode

import (
	"container/list"
)

// Design a data structure that follows the constraints of a Least Recently Used (LRU) cache.

// Implement the LRUCache class:

// LRUCache(int capacity) Initialize the LRU cache with positive size capacity.
// int get(int key) Return the value of the key if the key exists, otherwise return -1.
// void put(int key, int value) Update the value of the key if the key exists. Otherwise, add the key-value pair to the cache. If the number of keys exceeds the capacity from this operation, evict the least recently used key.
// The functions get and put must each run in O(1) average time complexity.

// Example 1:

// Input
// ["LRUCache", "put", "put", "get", "put", "get", "put", "get", "get", "get"]
// [[2], [1, 1], [2, 2], [1], [3, 3], [2], [4, 4], [1], [3], [4]]
// Output
// [null, null, null, 1, null, -1, null, -1, 3, 4]

// Explanation
// LRUCache lRUCache = new LRUCache(2);
// lRUCache.put(1, 1); // cache is {1=1}
// lRUCache.put(2, 2); // cache is {1=1, 2=2}
// lRUCache.get(1);    // return 1
// lRUCache.put(3, 3); // LRU key was 2, evicts key 2, cache is {1=1, 3=3}
// lRUCache.get(2);    // returns -1 (not found)
// lRUCache.put(4, 4); // LRU key was 1, evicts key 1, cache is {4=4, 3=3}
// lRUCache.get(1);    // return -1 (not found)
// lRUCache.get(3);    // return 3
// lRUCache.get(4);    // return 4

// Constraints:

// 1 <= capacity <= 3000
// 0 <= key <= 104
// 0 <= value <= 105
// At most 2 * 105 calls will be made to get and put.
type LRUCache struct {
	capacity int
	l        *list.List // store key
	d        map[int]*lrus
}

type lrus struct {
	ele *list.Element
	val int
}

func Constructor(capacity int) LRUCache {
	return LRUCache{
		capacity: capacity,
		l:        list.New(),
		d:        make(map[int]*lrus),
	}
}

func (this *LRUCache) Get(key int) int {
	if s, ok := this.d[key]; !ok {
		return -1
	} else {
		this.l.MoveToBack(s.ele)
		return s.val
	}
}

// 相同key时，value不覆盖，而是直接pushBack
func (this *LRUCache) Put(key int, value int) {
	if s, ok := this.d[key]; ok {
		this.l.Remove(s.ele)
	}
	this.d[key] = &lrus{
		ele: this.l.PushBack(key),
		val: value,
	}
	if this.l.Len() > this.capacity {
		front := this.l.Front()
		delete(this.d, front.Value.(int))
		this.l.Remove(front)
	}
}

/**
 * Your LRUCache object will be instantiated and called as such:
 * obj := Constructor(capacity);
 * param_1 := obj.Get(key);
 * obj.Put(key,value);
 */
