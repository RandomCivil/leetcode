package leetcode

import (
	"fmt"
	"math"
)

//Given an unsorted array return whether an increasing subsequence of length 3 exists or not in the array.

//Return true if there exists i, j, k
//such that arr[i] < arr[j] < arr[k] given 0 ≤ i < j < k ≤ n-1 else return false.

//Note: Your algorithm should run in O(n) time complexity and O(1) space complexity.

/*
Input: [1,2,3,4,5]
Output: true

Input: [5,4,3,2,1]
Output: false
*/

//func increasingTriplet(nums []int) bool {
func IncreasingTriplet(nums []int) bool {
	first, second := math.MaxInt32, math.MaxInt32
	for _, n := range nums {
		if n <= first {
			first = n
		} else if n <= second {
			second = n
		} else {
			fmt.Println(first, second, n)
			return true
		}
	}
	return false
}
