package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestOrderOfLargestPlusSign(t *testing.T) {
	tests := []struct {
		n, expect int
		mines     [][]int
	}{
		{
			n: 5,
			mines: [][]int{
				{4, 2},
			},
			expect: 2,
		},
		{
			n: 5,
			mines: [][]int{
				{3, 0},
				{3, 3},
			},
			expect: 3,
		},
	}
	Convey("TestOrderOfLargestPlusSign", t, func() {
		for _, tt := range tests {
			So(orderOfLargestPlusSign(tt.n, tt.mines), ShouldEqual, tt.expect)
		}
	})
}
