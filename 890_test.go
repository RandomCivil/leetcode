package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestFindAndReplacePattern(t *testing.T) {
	tests := []struct {
		expect, words []string
		pattern       string
	}{
		{
			words:   []string{"abc", "deq", "mee", "aqq", "dkd", "ccc"},
			pattern: "abb",
			expect:  []string{"mee", "aqq"},
		},
		{
			words:   []string{"a", "b", "c"},
			pattern: "a",
			expect:  []string{"a", "b", "c"},
		},
	}
	Convey("TestFindAndReplacePattern", t, func() {
		for _, tt := range tests {
			So(findAndReplacePattern(tt.words, tt.pattern), ShouldResemble, tt.expect)
		}
	})
}
