package leetcode

import "testing"

func TestLongestPalindrome(t *testing.T) {
	tests := []struct {
		s string
		e string
	}{
		{
			s: "babad",
			e: "bab",
		},
		{
			s: "cbbd",
			e: "bb",
		},
		{
			s: "a",
			e: "a",
		},
	}
	for _, tt := range tests {
		r := longestPalindrome(tt.s)
		t.Log(r)
		if r != tt.e {
			t.Fatal("TestLongestPalindrome error")
		}
	}
}
