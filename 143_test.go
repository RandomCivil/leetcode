package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/RandomCivil/leetcode/common"
)

func TestReorderList(t *testing.T) {
	tests := []struct {
		nums, expect []int
	}{
		{
			nums:   []int{1, 2, 3, 4},
			expect: []int{1, 4, 2, 3},
		},
		{
			nums:   []int{1, 2, 3, 4, 5},
			expect: []int{1, 5, 2, 4, 3},
		},
	}
	Convey("TestReorderList", t, func() {
		for _, tt := range tests {
			l := &common.LinkList{Nums: tt.nums}
			head := l.Build()
			reorderList(head)
			So(l.Traversal(head), ShouldResemble, tt.expect)
		}
	})
}
