package leetcode

import "gitlab.com/RandomCivil/leetcode/common"

//Given the head of a sorted linked list, delete all nodes that have duplicate numbers, leaving only distinct numbers from the original list. Return the linked list sorted as well.

//Example 1:

//Input: head = [1,2,3,3,4,4,5]
//Output: [1,2,5]

//Example 2:

//Input: head = [1,1,1,2,3]
//Output: [2,3]

//Constraints:

//The number of nodes in the list is in the range [0, 300].
//-100 <= Node.val <= 100
//The list is guaranteed to be sorted in ascending order.
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func DeleteDuplicates(head *common.ListNode) *common.ListNode {
	if head == nil || head.Next == nil {
		return head
	}
	// 至少两个节点
	var prev *common.ListNode
	root := head
	cur, tail := head, head.Next
	for cur != nil && cur.Next != nil {
		var hasDup bool
		for cur != nil && tail != nil && cur.Val == tail.Val {
			hasDup = true
			tail = tail.Next
		}
		if hasDup {
			if prev != nil {
				prev.Next = tail
			} else {
				root = tail
			}
			cur = tail
			if tail != nil {
				tail = tail.Next
			}
		} else {
			prev = cur
			cur = cur.Next
			tail = tail.Next
		}
	}
	return root
}
