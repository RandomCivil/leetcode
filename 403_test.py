inst_403 = __import__("403")
inst_403_me = __import__("403")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "stones,expected",
        [
            ([0, 1, 3, 5, 6, 8, 12, 17], True),
            ([0, 1, 2, 3, 4, 8, 9, 11], False),
            ([0, 2], False),
            ([0, 2147483647], False),
        ],
    )
    def test_canCross(self, stones, expected):
        assert inst_403.Solution().canCross(stones) == expected
        assert inst_403_me.Solution().canCross(stones) == expected
