# Given the root of a binary tree, return the sum of values of its deepest leaves.

# Example 1:

# Input: root = [1,2,3,4,5,null,6,7,null,null,null,null,8]
# Output: 15

# Example 2:


# Input: root = [6,7,8,2,7,1,3,9,null,1,4,null,null,null,5]
# Output: 19
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def deepestLeavesSum(self, root) -> int:
        self.leaves = []
        self.max_depth = 0

        def dfs(cur, depth):
            if not cur:
                return

            self.max_depth = max(self.max_depth, depth)
            self.leaves.append((cur, depth))

            dfs(cur.left, depth + 1)
            dfs(cur.right, depth + 1)

        dfs(root, 1)
        print(self.leaves)
        return sum([
            leaf.val for leaf, depth in self.leaves if depth == self.max_depth
        ])
