package leetcode

import "gitlab.com/RandomCivil/leetcode/common"

//Given the head of a singly linked list and two integers left and right where left <= right, reverse the nodes of the list from position left to position right, and return the reversed list.

//Example 1:

//Input: head = [1,2,3,4,5], left = 2, right = 4
//Output: [1,4,3,2,5]

//Example 2:

//Input: head = [5], left = 1, right = 1
//Output: [5]

//Constraints:

//The number of nodes in the list is n.
//1 <= n <= 500
//-500 <= Node.val <= 500
//1 <= left <= right <= n
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func reverseBetween(head *common.ListNode, left int, right int) *common.ListNode {
	root := head
	i := 1
	var startNode, prevStartNode *common.ListNode
	for head != nil && i < left {
		prevStartNode = head
		head = head.Next
		i++
	}
	startNode = head

	// 4,3,2
	reversedHead := reverse(startNode, i, right)
	// 1,4,3,2
	if prevStartNode != nil {
		prevStartNode.Next = reversedHead
	}

	if left == 1 {
		return reversedHead
	}
	return root
}

func reverse(head *common.ListNode, i, right int) *common.ListNode {
	first := head
	var prev *common.ListNode
	cur := head
	for cur != nil && i <= right {
		t := cur.Next
		cur.Next = prev

		prev = cur
		cur = t
		i++
	}
	first.Next = cur
	return prev
}
