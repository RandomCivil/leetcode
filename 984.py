# Given two integers a and b, return any string s such that:

# s has length a + b and contains exactly a 'a' letters, and exactly b 'b' letters,
# The substring 'aaa' does not occur in s, and
# The substring 'bbb' does not occur in s.


# Example 1:

# Input: a = 1, b = 2
# Output: "abb"
# Explanation: "abb", "bab" and "bba" are all correct answers.
# Example 2:

# Input: a = 4, b = 1
# Output: "aabaa"


# Constraints:


# 0 <= a, b <= 100
# It is guaranteed such an s exists for the given a and b.
class Solution:
    def strWithout3a3b(self, a: int, b: int) -> str:
        result = ""
        while a > 0 and b > 0:
            if a > b:
                result += "aab"
                a -= 2
                b -= 1
            elif a < b:
                result += "abb"
                a -= 1
                b -= 2
            else:
                result += "ab"
                a -= 1
                b -= 1
        if a > 0:
            if result[-1] == "a":
                result = "".join(["a"] * a) + result
            else:
                result += "".join(["a"] * a)
        if b > 0:
            if result[-1] == "b":
                result = "".join(["b"] * b) + result
            else:
                result += "".join(["b"] * b)
        return result
