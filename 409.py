# Given a string s which consists of lowercase or uppercase letters, return the length of the longest palindrome that can be built with those letters.
# Letters are case sensitive, for example, "Aa" is not considered a palindrome here.

# Example 1:

# Input: s = "abccccdd"
# Output: 7
# Explanation:
# One longest palindrome that can be built is "dccaccd", whose length is 7.

# Example 2:

# Input: s = "a"
# Output: 1

# Example 3:

# Input: s = "bb"
# Output: 2

# Constraints:

# 1 <= s.length <= 2000
# s consists of lowercase and/or uppercase English letters only.
from collections import Counter


class Solution:
    def longestPalindrome(self, s: str) -> int:
        c = Counter(s)
        print(c)
        odd_count = 0
        for ch, n in c.items():
            if n % 2 == 0:
                pass
            else:
                odd_count += 1

        return len(s) - odd_count + 1 if odd_count > 0 else len(s)


if __name__ == '__main__':
    s = 'abccccdd'
    s = 'bb'
    s = 'ccc'
    s = "aaabaaaa"
    print(Solution().longestPalindrome(s))
