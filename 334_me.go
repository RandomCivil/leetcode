package leetcode

import (
	"fmt"
	"math"
)

func increasingTriplet(nums []int) bool {
	fmt.Println(nums)
	size := len(nums)
	if size < 3 {
		return false
	}
	third := make([]int, size)
	third[size-1] = nums[size-1]
	for i := size - 1; i > 0; i-- {
		if nums[i-1] > third[i] {
			third[i-1] = nums[i-1]
		} else {
			third[i-1] = third[i]
		}
	}
	fmt.Println("third", third)

	second := make([]int, size)
	if nums[size-2] < third[size-1] {
		second[size-2] = nums[size-2]
	} else {
		second[size-2] = math.MinInt
	}
	for i := size - 2; i > 0; i-- {
		if nums[i-1] < third[i] && nums[i-1] > second[i] {
			second[i-1] = nums[i-1]
		} else {
			second[i-1] = second[i]
		}
	}
	fmt.Println("second", second)

	for i := size - 3; i >= 0; i-- {
		if nums[i] < second[i+1] {
			return true
		}
	}
	return false

}
