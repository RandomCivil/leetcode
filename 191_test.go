package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestHammingWeight(t *testing.T) {
	tests := []struct {
		num    uint32
		expect int
	}{
		{
			num:    00000000000000000000000000001011,
			expect: 3,
		},
	}
	Convey("TestHammingWeight", t, func() {
		for _, tt := range tests {
			So(hammingWeight(tt.num), ShouldEqual, tt.expect)
		}
	})
}
