# Given a string s, return true if a permutation of the string could form a palindrome and false otherwise.

# Example 1:

# Input: s = "code"
# Output: false
# Example 2:

# Input: s = "aab"
# Output: true
# Example 3:

# Input: s = "carerac"
# Output: true


# Constraints:


# 1 <= s.length <= 5000
# s consists of only lowercase English letters.
from collections import Counter


class Solution:
    def canPermutePalindrome(self, s: str) -> bool:
        counter = Counter(s)
        odd, even = 0, 0
        for v in counter.values():
            if v % 2 == 0:
                even += 1
            else:
                odd += 1
        return odd <= 1


if __name__ == "__main__":
    s = "aabb"
    print(Solution().canPermutePalindrome(s))
