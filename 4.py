# Given two sorted arrays nums1 and nums2 of size m and n respectively, return the median of the two sorted arrays.

# The overall run time complexity should be O(log (m+n)).


# Example 1:

# Input: nums1 = [1,3], nums2 = [2]
# Output: 2.00000
# Explanation: merged array = [1,2,3] and median is 2.
# Example 2:

# Input: nums1 = [1,2], nums2 = [3,4]
# Output: 2.50000
# Explanation: merged array = [1,2,3,4] and median is (2 + 3) / 2 = 2.5.


# Constraints:


# nums1.length == m
# nums2.length == n
# 0 <= m <= 1000
# 0 <= n <= 1000
# 1 <= m + n <= 2000
# -106 <= nums1[i], nums2[i] <= 106
class Solution:
    def findMedianSortedArrays(self, nums1, nums2) -> float:
        m, n = len(nums1), len(nums2)
        even = False
        count = (m + n) // 2 + 1
        if (m + n) % 2 == 0:
            even = True

        def cal(c, cur, prev):
            if c == count:
                if even:
                    return (cur + prev) / 2
                else:
                    return cur
            return None

        i, j, c = 0, 0, 0
        cur, prev = None, None

        while i < m and j < n:
            prev = cur
            if nums1[i] < nums2[j]:
                cur = nums1[i]
                i += 1
            elif nums1[i] > nums2[j]:
                cur = nums2[j]
                j += 1
            else:
                cur = nums1[i]
                i += 1
            c += 1
            result = cal(c, cur, prev)
            if result is not None:
                return result

        while i < m:
            prev = cur
            cur = nums1[i]
            c += 1
            result = cal(c, cur, prev)
            if result is not None:
                return result
            i += 1
        while j < n:
            prev = cur
            cur = nums2[j]
            c += 1
            result = cal(c, cur, prev)
            if result is not None:
                return result
            j += 1
