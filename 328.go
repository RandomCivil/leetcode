package leetcode

import "gitlab.com/RandomCivil/leetcode/common"

//Given the head of a singly linked list, group all the nodes with odd indices together followed by the nodes with even indices, and return the reordered list.

//The first node is considered odd, and the second node is even, and so on.

//Note that the relative order inside both the even and odd groups should remain as it was in the input.

//Example 1:

//Input: head = [1,2,3,4,5]
//Output: [1,3,5,2,4]

//Example 2:

//Input: head = [2,1,3,5,6,4,7]
//Output: [2,3,6,7,1,5,4]
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func OddEvenList(head *common.ListNode) *common.ListNode {
	if head == nil {
		return nil
	}

	odd, even := head, head.Next
	evenHead := even
	for odd.Next != nil && even.Next != nil {
		odd.Next = even.Next
		odd = odd.Next

		even.Next = odd.Next
		even = even.Next
	}
	odd.Next = evenHead
	return head
}
