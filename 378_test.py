inst_378 = __import__("378")
inst_378_heap = __import__("378_heap")


import pytest


class TestClass:
    @pytest.mark.parametrize(
        "matrix, k,expected",
        [
            ([[1, 5, 9], [10, 11, 13], [12, 13, 15]], 8, 13),
            ([[-5]], 1, -5),
            ([[1, 2], [1, 3]], 2, 1),
        ],
    )
    def test_kthSmallest(self, matrix, k, expected):
        assert inst_378.Solution().kthSmallest(matrix, k) == expected
        assert inst_378_heap.Solution().kthSmallest(matrix, k) == expected
