# Given an array nums of n integers, return an array of all the unique quadruplets [nums[a], nums[b], nums[c], nums[d]] such that:

# 0 <= a, b, c, d < n
# a, b, c, and d are distinct.
# nums[a] + nums[b] + nums[c] + nums[d] == target
# You may return the answer in any order.

# Example 1:

# Input: nums = [1,0,-1,0,-2,2], target = 0
# Output: [[-2,-1,1,2],[-2,0,0,2],[-1,0,0,1]]
# Example 2:

# Input: nums = [2,2,2,2,2], target = 8
# Output: [[2,2,2,2]]

# Constraints:

# 1 <= nums.length <= 200
# -109 <= nums[i] <= 109
# -109 <= target <= 109

from collections import defaultdict


class Solution:
    def fourSum(self, nums, target):
        def threeSum(nums, target):
            result = []
            size = len(nums)
            for i in range(size - 2):
                if i > 0 and nums[i] == nums[i - 1]:
                    continue
                l, r = i + 1, size - 1
                while l < r:
                    s = nums[i] + nums[l] + nums[r]
                    if s > target:
                        r -= 1
                    elif s < target:
                        l += 1
                    else:
                        result.append([nums[i], nums[l], nums[r]])
                        while l < r and nums[l] == nums[l + 1]:
                            l += 1
                        while l < r and r + 1 < size and nums[r] == nums[r + 1]:
                            r -= 1
                        l += 1
                        r -= 1
            return result

        nums.sort()
        result = set()
        for i, n in enumerate(nums):
            r = threeSum(nums[i + 1 :], target - nums[i])
            if len(r) == 0:
                continue
            # print(nums[i + 1 :], target - nums[i], r)
            for rr in r:
                result.add(tuple(sorted([n, *rr])))

        return list(result)


if __name__ == "__main__":
    # [[-2,-1,1,2],[-2,0,0,2],[-1,0,0,1]]
    nums = [1, 0, -1, 0, -2, 2]
    target = 0
    # [[2,2,2,2]]
    nums = [2, 2, 2, 2, 2]
    target = 8
    print(Solution().fourSum(nums, target))
