package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/RandomCivil/leetcode/common"
)

func TestMergeKLists(t *testing.T) {
	tests := []struct {
		lists  [][]int
		expect []int
	}{
		{
			lists: [][]int{
				{1, 4, 5}, {1, 3, 4}, {2, 6},
			},
			expect: []int{1, 1, 2, 3, 4, 4, 5, 6},
		},
		{
			lists: [][]int{
				{1, 4, 5}, {1, 3, 4},
			},
			expect: []int{1, 1, 3, 4, 4, 5},
		},
	}
	Convey("TestMergeKLists", t, func() {
		for _, tt := range tests {
			var rootList []*common.ListNode
			for _, l := range tt.lists {
				ll := &common.LinkList{
					Nums: l,
				}
				rootList = append(rootList, ll.Build())
			}

			var result []int
			head := mergeKLists(rootList)
			for head != nil {
				result = append(result, head.Val)
				head = head.Next
			}
			So(result, ShouldResemble, tt.expect)
		}
	})
}
