inst_269 = __import__("269")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "words,expected",
        [
            (["wrt","wrf","er","ett","rftt"],'wertf'),
            (['z','x'],'zx'),
            (["z","x","z"],''),
            (["abc","ab"],''),
            # (["c","ab","abc"],'bca'),
            (['z','z'],'z'),
            # (["ac","ab","zc","zb"],"acbz"),
            (["ri","xz","qxf","jhsguaw","dztqrbwbm","dhdqfb","jdv","fcgfsilnb","ooby"],""),
        ],
    )
    def test_alienOrder(self, words, expected):
        assert inst_269.Solution().alienOrder(words) == expected
