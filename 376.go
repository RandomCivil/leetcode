package leetcode

import "fmt"

//A sequence of numbers is called a wiggle sequence if the differences between successive numbers strictly alternate between positive and negative. The first difference (if one exists) may be either positive or negative. A sequence with fewer than two elements is trivially a wiggle sequence.
//For example, [1,7,4,9,2,5] is a wiggle sequence because the differences (6,-3,5,-7,3) are alternately positive and negative. In contrast, [1,4,7,2,5] and [1,7,4,5,5] are not wiggle sequences, the first because its first two differences are positive and the second because its last difference is zero.
//Given a sequence of integers, return the length of the longest subsequence that is a wiggle sequence. A subsequence is obtained by deleting some number of elements (eventually, also zero) from the original sequence, leaving the remaining elements in their original order.
//Can you do it in O(n) time?

/*
Input: [1,7,4,9,2,5]
Output: 6
Explanation: The entire sequence is a wiggle sequence.

Input: [1,17,5,10,13,15,10,5,16,8]
Output: 7
Explanation: There are several subsequences that achieve this length. One is [1,17,10,13,10,16,8].

Input: [1,2,3,4,5,6,7,8,9]
Output: 2
*/

//nums := []int{1, 17, 5, 10, 13, 15, 10, 5, 16, 8}
//nums := []int{1, 7, 4, 9, 2, 5}
//nums := []int{1,2,3,4,5,6,7,8,9}
//nums := []int{0,0}

//func wiggleMaxLength(nums []int) int {
func WiggleMaxLength(nums []int) int {
	size := len(nums)
	if size == 0 {
		return 1
	}
	p, n := make([]int, size), make([]int, size)
	p[0] = 1
	n[0] = 1
	maxp, maxn := 1, 1
	for i := 1; i < size; i++ {
		if nums[i] > nums[i-1] {
			p[i] = maxn + 1
			n[i] = n[i-1]
			maxp = p[i]
		}
		if nums[i] < nums[i-1] {
			n[i] = maxp + 1
			p[i] = p[i-1]
			maxn = n[i]
		}
	}
	fmt.Println(p, n, maxp, maxn)
	if maxp > maxn {
		return maxp
	} else {
		return maxn
	}
}
