import pytest


inst_1631 = __import__("1631")
inst_1631_bellman_ford = __import__("1631_bellman_ford")


class TestClass:
    @pytest.mark.parametrize(
        "heights, expected",
        [
            ([[1, 2, 2], [3, 8, 2], [5, 3, 5]], 2),
            ([[1, 2, 3], [3, 8, 4], [5, 3, 5]], 1),
            (
                [
                    [1, 2, 1, 1, 1],
                    [1, 2, 1, 2, 1],
                    [1, 2, 1, 2, 1],
                    [1, 2, 1, 2, 1],
                    [1, 1, 1, 2, 1],
                ],
                0,
            ),
        ],
    )
    def test_minimumEffortPath(self, heights, expected):
        assert inst_1631.Solution().minimumEffortPath(heights) == expected
        assert inst_1631_bellman_ford.Solution().minimumEffortPath(heights) == expected
