# We are given hours, a list of the number of hours worked per day for a given employee.

# A day is considered to be a tiring day if and only if the number of hours worked is (strictly) greater than 8.

# A well-performing interval is an interval of days for which the number of tiring days is strictly larger than the number of non-tiring days.

# Return the length of the longest well-performing interval.

# Example 1:

# Input: hours = [9,9,6,0,6,6,9]
# Output: 3
# Explanation: The longest well-performing interval is [9,9,6].

# Constraints:


# 1 <= hours.length <= 10000
# 0 <= hours[i] <= 16
class Solution:
    def longestWPI(self, hours) -> int:
        r = 0
        d = {}
        v = 0
        for i, h in enumerate(hours):
            if h > 8:
                v += 1
            else:
                v -= 1
            if v not in d:
                d[v] = i

            if v > 0:
                r = max(r, i + 1)
            else:
                if v - 1 in d:
                    r = max(r, i - d[v - 1])

        return r


if __name__ == '__main__':
    hours = [9, 9, 6, 0, 6, 6, 9]
    hours = [9, 9, 6, 0, 9, 6, 6, 9]
    print(Solution().longestWPI(hours))
