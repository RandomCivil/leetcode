import pytest


inst_1849 = __import__("1849")


class TestClass:
    @pytest.mark.parametrize(
        "s, expected",
        [
            ("050043", True),
            ("1234", False),
            ("9080701", False),
            ("0090089", True),
            ("001", False),
        ],
    )
    def test_splitString(self, s, expected):
        assert inst_1849.Solution().splitString(s) == expected
