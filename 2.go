package leetcode

import "gitlab.com/RandomCivil/leetcode/common"

//You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order, and each of their nodes contains a single digit. Add the two numbers and return the sum as a linked list.

//You may assume the two numbers do not contain any leading zero, except the number 0 itself.

//Example 1:

//Input: l1 = [2,4,3], l2 = [5,6,4]
//Output: [7,0,8]
//Explanation: 342 + 465 = 807.

//Example 2:

//Input: l1 = [0], l2 = [0]
//Output: [0]

//Example 3:

//Input: l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
//Output: [8,9,9,9,0,0,0,1]

//Constraints:

//The number of nodes in each linked list is in the range [1, 100].
//0 <= Node.val <= 9
//It is guaranteed that the list represents a number that does not have leading zeros.

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func addTwoNumbers(l1 *common.ListNode, l2 *common.ListNode) *common.ListNode {
	h1, h2, prev := l1, l2, l1
	var v, next int
	for h1 != nil || h2 != nil {
		v = next
		if h1 != nil {
			v += h1.Val
		}
		if h2 != nil {
			v += h2.Val
		}
		next = v / 10
		if h1 == nil {
			h1 = &common.ListNode{}
			prev.Next = h1
		}
		h1.Val = v % 10

		if h1 != nil {
			prev = h1
			h1 = h1.Next
		}
		if h2 != nil {
			h2 = h2.Next
		}
	}
	if next > 0 && prev != nil {
		prev.Next = &common.ListNode{Val: next}
	}
	return l1
}
