# In a gold mine grid of size m * n, each cell in this mine has an integer representing the amount of gold in that cell, 0 if it is empty.

# Return the maximum amount of gold you can collect under the conditions:

# Every time you are located in a cell you will collect all the gold in that cell.
# From your position you can walk one step to the left, right, up or down.
# You can't visit the same cell more than once.
# Never visit a cell with 0 gold.
# You can start and stop collecting gold from any position in the grid that has some gold.

# Example 1:

# Input: grid = [[0,6,0],[5,8,7],[0,9,0]]
# Output: 24
# Explanation:
# [[0,6,0],
# [5,8,7],
# [0,9,0]]
# Path to get the maximum gold, 9 -> 8 -> 7.

# Example 2:

# Input: grid = [[1,0,7],[2,0,6],[3,4,5],[0,3,0],[9,0,20]]
# Output: 28
# Explanation:
# [[1,0,7],
# [2,0,6],
# [3,4,5],
# [0,3,0],
# [9,0,20]]
# Path to get the maximum gold, 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7.

# Constraints:

# 1 <= grid.length, grid[i].length <= 15
# 0 <= grid[i][j] <= 100
# There are at most 25 cells containing gold.


class Solution:
    def getMaximumGold(self, grid) -> int:
        self.r = 0
        m, n = len(grid), len(grid[0])
        nxt = [(1, 0), (-1, 0), (0, 1), (0, -1)]
        visited = set()

        def cal(x, y, s):
            for nx, ny in nxt:
                if x + nx == -1 or y + ny == -1 or x + nx == m or y + ny == n:
                    # print('last', s, path)
                    self.r = max(self.r, s)
                    continue
                if grid[x + nx][y + ny] == 0 or (x + nx, y + ny) in visited:
                    self.r = max(self.r, s)
                    continue

                visited.add((x + nx, y + ny))
                cal(x + nx, y + ny, s + grid[x + nx][y + ny])
                visited.remove((x + nx, y + ny))

        for i in range(m):
            for j in range(n):
                if grid[i][j] == 0:
                    continue
                visited = {(i, j)}
                cal(i, j, grid[i][j])

        return self.r


if __name__ == '__main__':
    # 24
    grid = [[0, 6, 0], [5, 8, 7], [0, 9, 0]]
    # 28
    grid = [[1, 0, 7], [2, 0, 6], [3, 4, 5], [0, 3, 0], [9, 0, 20]]
    # 45
    grid = [[0, 0, 0, 3, 0, 0, 17, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 8, 5, 15, 0, 0],
            [0, 0, 0, 0, 0, 17, 0, 17, 0, 0, 12, 0],
            [0, 0, 0, 4, 0, 0, 20, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 7, 13, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 16, 20],
            [2, 0, 0, 4, 0, 0, 0, 16, 0, 4, 0, 0],
            [0, 0, 11, 0, 0, 10, 0, 0, 5, 0, 0, 18],
            [2, 0, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
    print(Solution().getMaximumGold(grid))
