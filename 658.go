package leetcode

import (
	"fmt"

	"gitlab.com/RandomCivil/leetcode/common"
)

//Given a sorted array arr, two integers k and x, find the k closest elements to x in the array. The result should also be sorted in ascending order. If there is a tie, the smaller elements are always preferred.
/*
Input: arr = [1,2,3,4,5], k = 4, x = 3
Output: [1,2,3,4]

Input: arr = [1,2,3,4,5], k = 4, x = -1
Output: [1,2,3,4]
*/
func findClosestElements(arr []int, k int, x int) []int {
	size := len(arr)
	i := common.BinarySearchLowerBound(arr, x)
	fmt.Println(arr, i, x)
	left, right := i-1, i
	for right-left-1 < k {
		if left < 0 {
			right++
			continue
		}
		if right == size {
			left--
			continue
		}
		if x-arr[left] <= arr[right]-x {
			left--
		} else {
			right++
		}
	}
	return arr[left+1 : right]
}
