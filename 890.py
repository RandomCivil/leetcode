# You have a list of words and a pattern, and you want to know which words in words matches the pattern.
# A word matches the pattern if there exists a permutation of letters p so that after replacing every letter x in the pattern with p(x), we get the desired word.
# (Recall that a permutation of letters is a bijection from letters to letters: every letter maps to another letter, and no two letters map to the same letter.)
# Return a list of the words in words that match the given pattern.
# You may return the answer in any order.

# Example 1:

# Input: words = ["abc","deq","mee","aqq","dkd","ccc"], pattern = "abb"
# Output: ["mee","aqq"]
# Explanation: "mee" matches the pattern because there is a permutation {a -> m, b -> e, ...}.
# "ccc" does not match the pattern because {a -> c, b -> c, ...} is not a permutation,
# since a and b map to the same letter.

# 1 <= words.length <= 50
# 1 <= pattern.length = words[i].length <= 20


class Solution:
    def findAndReplacePattern(self, words, pattern: str):
        r = []
        for word in words:
            d1 = {}
            d2 = {}
            for ch, p in zip(word, pattern):
                if p not in d1:
                    d1[p] = ch
                if ch not in d2:
                    d2[ch] = p
                if d1[p] != ch or d2[ch] != p:
                    break
            else:
                r.append(word)

        return r


if __name__ == '__main__':
    words = ["abc", "deq", "mee", "aqq", "dkd", "ccc"]
    pattern = "abb"

    words = ["abc", "deq", "mee", "aqq", "dkd", "ccc"]
    pattern = "abd"
    print(Solution().findAndReplacePattern(words, pattern))
