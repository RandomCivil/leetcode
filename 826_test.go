package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestMaxProfitAssignment(t *testing.T) {
	tests := []struct {
		difficulty []int
		profit     []int
		worker     []int
		expect     int
	}{
		{
			difficulty: []int{2, 4, 6, 8, 10},
			profit:     []int{10, 20, 30, 40, 50},
			worker:     []int{4, 5, 6, 7},
			expect:     100,
		},
		{
			difficulty: []int{85, 47, 57},
			profit:     []int{24, 66, 99},
			worker:     []int{40, 25, 25},
			expect:     0,
		},
		{
			difficulty: []int{13, 37, 58},
			profit:     []int{4, 90, 96},
			worker:     []int{34, 73, 45},
			expect:     190,
		},
	}
	Convey("TestMaxProfitAssignment", t, func() {
		for _, tt := range tests {
			So(maxProfitAssignment(tt.difficulty, tt.profit, tt.worker), ShouldEqual, tt.expect)
		}
	})
}
