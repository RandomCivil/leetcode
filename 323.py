# You have a graph of n nodes. You are given an integer n and an array edges where edges[i] = [ai, bi] indicates that there is an edge between ai and bi in the graph.

# Return the number of connected components in the graph.

# Example 1:


# Input: n = 5, edges = [[0,1],[1,2],[3,4]]
# Output: 2
# Example 2:


# Input: n = 5, edges = [[0,1],[1,2],[2,3],[3,4]]
# Output: 1


# Constraints:

# 1 <= n <= 2000
# 1 <= edges.length <= 5000
# edges[i].length == 2
# 0 <= ai <= bi < n
# ai != bi
# There are no repeated edges.
from collections import defaultdict


class Solution:
    def countComponents(self, n: int, edges) -> int:
        d = defaultdict(list)
        for s, e in edges:
            d[s].append(e)
            d[e].append(s)

        result = 0
        self.visited = set()

        def dfs(cur, parent):
            self.visited.add(cur)
            for nxt in d[cur]:
                if nxt == parent:
                    continue
                if nxt in self.visited:
                    continue
                dfs(nxt, cur)

        for i in range(n):
            if i in self.visited:
                continue
            dfs(i, None)
            result += 1
        return result


if __name__ == "__main__":
    # 2
    n = 5
    edges = [[0, 1], [1, 2], [3, 4]]

    # 1
    n = 5
    edges = [[0, 1], [1, 2], [2, 3], [3, 4]]

    # 2
    n = 5
    edges = [[0, 1], [1, 2], [0, 2], [3, 4]]
    print(Solution().countComponents(n, edges))
