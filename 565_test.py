inst_565 = __import__("565")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "nums,expected",
        [
            ([5, 4, 0, 3, 1, 6, 2], 4),
            ([0, 1, 2], 1),
        ],
    )
    def test_arrayNesting(self, nums, expected):
        assert inst_565.Solution().arrayNesting(nums) == expected
