package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestRangeSum(t *testing.T) {
	tests := []struct {
		matrix     [][]int
		row1, col1 int
		row2, col2 int
		expect     int
	}{
		{
			matrix: [][]int{
				{3, 0, 1, 4, 2},
				{5, 6, 3, 2, 1},
				{1, 2, 0, 1, 5},
				{4, 1, 0, 1, 7},
				{1, 0, 3, 0, 5},
			},
			row1:   2,
			col1:   1,
			row2:   4,
			col2:   3,
			expect: 8,
		},
		{
			matrix: [][]int{
				{3, 0, 1, 4, 2},
				{5, 6, 3, 2, 1},
				{1, 2, 0, 1, 5},
				{4, 1, 0, 1, 7},
				{1, 0, 3, 0, 5},
			},
			row1:   1,
			col1:   1,
			row2:   2,
			col2:   2,
			expect: 11,
		},
		{
			matrix: [][]int{
				{3, 0, 1, 4, 2},
				{5, 6, 3, 2, 1},
				{1, 2, 0, 1, 5},
				{4, 1, 0, 1, 7},
				{1, 0, 3, 0, 5},
			},
			row1:   1,
			col1:   2,
			row2:   2,
			col2:   4,
			expect: 12,
		},
	}
	Convey("TestRangeSum", t, func() {
		for _, tt := range tests {
			nm := NumMatrixConstructor(tt.matrix)
			So(nm.SumRegion(tt.row1, tt.col1, tt.row2, tt.col2), ShouldEqual, tt.expect)
		}
	})
}
