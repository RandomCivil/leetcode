package leetcode

import "fmt"

//Given a list of daily temperatures T, return a list such that, for each day in the input, tells you how many days you would have to wait until a warmer temperature. If there is no future day for which this is possible, put 0 instead.
//For example, given the list of temperatures T = [73, 74, 75, 71, 69, 72, 76, 73], your output should be [1, 1, 4, 2, 1, 1, 0, 0].
//Note: The length of temperatures will be in the range [1, 30000]. Each temperature will be an integer in the range [30, 100].

func DailyTemperatures(T []int) []int {
	fmt.Println(T, len(T))
	size := len(T)
	result := make([]int, size)
	for i := 0; i < size; i++ {
		result[i] = size + 1
	}

	m := make(map[int]int)
	for i := size - 1; i >= 0; i-- {
		m[T[i]] = i
		for t := T[i] + 1; t <= 100; t++ {
			var val int
			var ok bool
			if val, ok = m[t]; !ok {
				continue
			}
			if val-i < result[i] {
				//val = result[i]
				result[i] = val - i
			}
		}
		if result[i] == size+1 {
			result[i] = 0
		}
	}
	return result
}
