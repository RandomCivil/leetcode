inst_207_topological_sort = __import__("207_topological_sort")


import pytest


class TestClass:
    @pytest.mark.parametrize(
        "numCourses,prerequisites,expected",
        [
            (2, [[1, 0]], True),
            (2, [[1, 0], [0, 1]], False),
            (3, [[0, 2], [1, 2], [2, 0]], False),
        ],
    )
    def test_canFinish(self, numCourses: int, prerequisites, expected):
        assert (
            inst_207_topological_sort.Solution().canFinish(numCourses, prerequisites)
            == expected
        )
