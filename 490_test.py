inst_490 = __import__("490")
inst_490_me = __import__("490_me")


import pytest


class TestClass:
    @pytest.mark.parametrize(
        " maze, start, destination,expected",
        [
            (
                [
                    [0, 0, 1, 0, 0],
                    [0, 0, 0, 0, 0],
                    [0, 0, 0, 1, 0],
                    [1, 1, 0, 1, 1],
                    [0, 0, 0, 0, 0],
                ],
                [0, 4],
                [4, 4],
                True,
            ),
            (
                [
                    [0, 0, 1, 0, 0],
                    [0, 0, 0, 0, 0],
                    [0, 0, 0, 1, 0],
                    [1, 1, 0, 1, 1],
                    [0, 0, 0, 0, 0],
                ],
                [0, 4],
                [3, 2],
                False,
            ),
            (
                [
                    [0, 0, 1, 0, 0],
                    [0, 0, 0, 0, 0],
                    [0, 0, 0, 1, 0],
                    [1, 1, 0, 1, 1],
                    [0, 0, 0, 0, 0],
                ],
                [0, 4],
                [1, 2],
                True,
            ),
        ],
    )
    def test_hasPath(self, maze, start, destination, expected):
        assert inst_490_me.Solution().hasPath(maze, start, destination) == expected
        assert inst_490.Solution().hasPath(maze, start, destination) == expected
