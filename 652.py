# Given a binary tree, return all duplicate subtrees. For each kind of duplicate subtrees, you only need to return the root node of any one of them.
# Two trees are duplicate if they have the same structure with same node values.
from collections import defaultdict


class Solution:
    def findDuplicateSubtrees(self, root):
        self.result = []
        d = defaultdict(int)

        def dfs(root):
            if root is None:
                return '#'
            s = '{} {} {}'.format(root.val, dfs(root.left), dfs(root.right))
            d[s] += 1
            if d[s] == 2:
                self.result.append(root)
            return s

        dfs(root)
        return self.result
