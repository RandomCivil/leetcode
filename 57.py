# Given a set of non-overlapping intervals, insert a new interval into the intervals (merge if necessary).

# You may assume that the intervals were initially sorted according to their start times.

# Example 1:

# Input: intervals = [[1,3],[6,9]], newInterval = [2,5]
# Output: [[1,5],[6,9]]

# Example 2:

# Input: intervals = [[1,2],[3,5],[6,7],[8,10],[12,16]], newInterval = [4,8]
# Output: [[1,2],[3,10],[12,16]]
# Explanation: Because the new interval [4,8] overlaps with [3,5],[6,7],[8,10].

# Example 3:

# Input: intervals = [], newInterval = [5,7]
# Output: [[5,7]]

# Example 4:

# Input: intervals = [[1,5]], newInterval = [2,3]
# Output: [[1,5]]

# Example 5:


# Input: intervals = [[1,5]], newInterval = [2,7]
# Output: [[1,7]]
class Solution:
    def insert(self, intervals, newInterval):
        ns, ne = newInterval
        r = []
        size = len(intervals)

        i = 0
        while i < size and intervals[i][0] < ns:
            r.append(intervals[i])
            i += 1

        intervals = intervals[:i] + [newInterval] + intervals[i:]

        if i == 0:
            r.append(intervals[i])
            i += 1

        print(i, r, intervals)
        for j in range(i, size + 1):
            cs, ce = intervals[j]
            ls, le = r[-1]
            if le >= cs:
                r[-1][1] = max(ce, le)
            else:
                r.append(intervals[j])
        return r


if __name__ == '__main__':
    # [[1,2],[3,10],[12,16]]
    intervals = [[1, 2], [3, 5], [6, 7], [8, 10], [12, 16]]
    # newInterval = [4, 8]
    newInterval = [15, 17]
    print(Solution().insert(intervals, newInterval))
