package leetcode

import (
	"fmt"

	"gitlab.com/RandomCivil/leetcode/common"
)

// You are given the root of a binary search tree (BST), where the values of exactly two nodes of the tree were swapped by mistake. Recover the tree without changing its structure.

// Example 1:

// Input: root = [1,3,null,null,2]
// Output: [3,1,null,null,2]
// Explanation: 3 cannot be a left child of 1 because 3 > 1. Swapping 1 and 3 makes the BST valid.
// Example 2:

// Input: root = [3,1,4,null,null,2]
// Output: [2,1,4,null,null,3]
// Explanation: 2 cannot be in the right subtree of 3 because 2 < 3. Swapping 2 and 3 makes the BST valid.

// Constraints:

// The number of nodes in the tree is in the range [2, 1000].
// -231 <= Node.val <= 231 - 1
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func recoverTree(root *common.TreeNode) {
	m := make(map[int]*common.TreeNode)
	var traversalInOrder func(head *common.TreeNode) (vals []int)
	traversalInOrder = func(head *common.TreeNode) (vals []int) {
		if head != nil {
			traversalInOrder(head.Left)
			vals = append(vals, head.Val)
			m[head.Val] = head
			traversalInOrder(head.Right)
		}
		return
	}

	findSwap := func(vals []int) (n1, n2 int) {
		size := len(vals)
		var start bool
		for i := 0; i < size-1; i++ {
			if vals[i] > vals[i+1] {
				if !start {
					n1 = vals[i]
					start = true
				}
				n2 = vals[i+1]
			}
		}
		return
	}

	vals := traversalInOrder(root)
	n1, n2 := findSwap(vals)

	fmt.Println(n1, n2)
	m[n1].Val = n2
	m[n2].Val = n1
}
