inst_1003 = __import__("1003")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "s, expected",
        [
            ("aabcbc", True),
            ("abcabcababcc", True),
            ("aaabcbcbc", True),
            ("abacbcabcc", False),
        ],
    )
    def test_isValid(self, s, expected):
        assert inst_1003.Solution().isValid(s) == expected
