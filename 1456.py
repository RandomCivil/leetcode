# Given a string s and an integer k.

# Return the maximum number of vowel letters in any substring of s with length k.

# Vowel letters in English are (a, e, i, o, u).

# Example 1:

# Input: s = "abciiidef", k = 3
# Output: 3
# Explanation: The substring "iii" contains 3 vowel letters.

# Example 2:

# Input: s = "aeiou", k = 2
# Output: 2
# Explanation: Any substring of length 2 contains 2 vowels.

# Example 3:

# Input: s = "leetcode", k = 3
# Output: 2
# Explanation: "lee", "eet" and "ode" contain 2 vowels.

# Example 4:

# Input: s = "rhythms", k = 4
# Output: 0
# Explanation: We can see that s doesn't have any vowel letters.

# Example 5:


# Input: s = "tryhard", k = 4
# Output: 1
class Solution:
    def maxVowels(self, s: str, k: int) -> int:
        size = len(s)
        st = {'a', 'e', 'i', 'o', 'u'}
        r = 0
        n = 0
        for i in range(k):
            if s[i] in st:
                n += 1

        i, j = 0, k
        r = n
        while j < size:
            if s[j] in st:
                n += 1
            if s[i] in st:
                n -= 1
            r = max(r, n)
            j += 1
            i += 1

        return r


if __name__ == '__main__':
    # 3
    s = "abciiidef"
    k = 3
    # 2
    s = "aeiou"
    k = 2
    # 2
    s = "leetcode"
    k = 3
    # 0
    s = "rhythms"
    k = 4
    # 1
    s = "tryhard"
    k = 4
    print(Solution().maxVowels(s, k))
