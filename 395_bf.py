# Given a string s and an integer k, return the length of the longest substring of s such that the frequency of each character in this substring is greater than or equal to k.

# Example 1:

# Input: s = "aaabb", k = 3
# Output: 3
# Explanation: The longest substring is "aaa", as 'a' is repeated 3 times.

# Example 2:

# Input: s = "ababbc", k = 2
# Output: 5
# Explanation: The longest substring is "ababb", as 'a' is repeated 2 times and 'b' is repeated 3 times.
from collections import Counter


class Solution:
    def longestSubstring(self, s: str, k: int) -> int:
        result = 0
        size = len(s)
        for start in range(0, size):
            c = Counter()
            for end in range(start, size):
                c[s[end]] += 1
                if all(v >= k for _, v in c.items()):
                    result = max(result, end - start + 1)
        return result


if __name__ == '__main__':
    # 3
    s = "aaabb"
    k = 3
    print(Solution().longestSubstring(s, k))
