# Given an integer array nums, you need to find one continuous subarray that if you only sort this subarray in ascending order, then the whole array will be sorted in ascending order.

# Return the shortest such subarray and output its length.

# Example 1:

# Input: nums = [2,6,4,8,10,9,15]
# Output: 5
# Explanation: You need to sort [6, 4, 8, 10, 9] in ascending order to make the whole array sorted in ascending order.

# Example 2:

# Input: nums = [1,2,3,4]
# Output: 0

# Example 3:


# Input: nums = [1]
# Output: 0
class Solution:
    def findUnsortedSubarray(self, nums) -> int:
        size = len(nums)
        if size == 1:
            return 0
        lo, hi = 0, size - 1
        sorted_num = sorted(nums)
        while lo < size and nums[lo] == sorted_num[lo]:
            lo += 1
        while hi > 0 and nums[hi] == sorted_num[hi]:
            hi -= 1

        print(lo, hi)
        if lo > hi:
            return 0
        return hi - lo + 1


if __name__ == "__main__":
    # 5
    nums = [2, 6, 4, 8, 10, 9, 15]

    # 0
    # nums = [1, 2, 3, 4]
    print(Solution().findUnsortedSubarray(nums))
