package leetcode

import "fmt"

//Given a binary array, find the maximum number of consecutive 1s in this array.

//Example 1:
//Input: [1,1,0,1,1,1]
//Output: 3
//Explanation: The first two digits or the last three digits are consecutive 1s.
//The maximum number of consecutive 1s is 3.
//Note:

//The input array will only contain 0 and 1.
//The length of input array is a positive integer and will not exceed 10,000

func FindMaxConsecutiveOnes1(nums []int) int {
	fmt.Println(nums)
	r := 0
	var dis int
	for _, num := range nums {
		if dis > r {
			r = dis
		}
		if num == 1 {
			dis++
		} else {
			dis = 0
		}
	}
	if dis > r {
		r = dis
	}
	return r
}
