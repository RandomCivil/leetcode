inst_82 = __import__("82")

import pytest, common


class TestClass:
    @pytest.mark.parametrize(
        "nums,expected",
        [
            ([1, 2, 3, 3, 4, 4, 5], [1, 2, 5]),
            ([1, 1, 1, 2, 3], [2, 3]),
            ([1, 1], []),
            ([1, 1, 1], []),
        ],
    )
    def test_deleteDuplicates(self, nums, expected):
        l = common.LinkList(nums)
        assert l.traversal(inst_82.Solution().deleteDuplicates(l.build())) == expected
