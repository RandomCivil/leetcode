inst_987 = __import__("987")

import pytest
import common


class TestClass:
    @pytest.mark.parametrize(
        "nums,expected",
        [
            ([3, 9, 20, 0, 0, 15, 7], [[9], [3, 15], [20], [7]]),
            ([1, 2, 3, 4, 5, 6, 7], [[4], [2], [1, 5, 6], [3], [7]]),
            ([1, 2, 3, 4, 6, 5, 7], [[4], [2], [1, 5, 6], [3], [7]]),
            ([3, 1, 4, 1, 2, 2], [[1], [1], [3, 2, 2], [4]]),
        ],
    )
    def test_verticalTraversal(self, nums, expected):
        root = common.build_tree(nums)
        assert inst_987.Solution().verticalTraversal(root) == expected
