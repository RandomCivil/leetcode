# Given an array nums of size n, return the majority element.
# The majority element is the element that appears more than ⌊n / 2⌋ times. You may assume that the majority element always exists in the array.

# Example 1:

# Input: nums = [3,2,3]
# Output: 3

# Example 2:

# Input: nums = [2,2,1,1,1,2,2]
# Output: 2

# Constraints:

# n == nums.length
# 1 <= n <= 5 * 104
# -231 <= nums[i] <= 231 - 1

# Follow-up: Could you solve the problem in linear time and in O(1) space?
from collections import Counter


class Solution:
    def majorityElement(self, nums) -> int:
        print(nums)
        c = Counter(nums)
        print(c)
        mx = max(c.items(), key=lambda x: x[1])
        return mx[0]


if __name__ == '__main__':
    nums = [3, 3, 4]
    print(Solution().majorityElement(nums))
