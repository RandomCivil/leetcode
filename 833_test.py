inst_833 = __import__("833")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "s,indices,sources,targets,expected",
        [
            ("abcd", [0, 2], ["a", "cd"], ["eee", "ffff"], "eeebffff"),
            ("abcd", [0, 2], ["ab", "ec"], ["eee", "ffff"], "eeecd"),
        ],
    )
    def test_findReplaceString(
        self,
        s: str,
        indices: list[int],
        sources: list[str],
        targets: list[str],
        expected,
    ):
        assert (
            inst_833.Solution().findReplaceString(s, indices, sources, targets)
            == expected
        )
