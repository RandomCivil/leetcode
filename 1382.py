# Given a binary search tree, return a balanced binary search tree with the same node values.

# A binary search tree is balanced if and only if the depth of the two subtrees of every node never differ by more than 1.

# If there is more than one answer, return any of them.

# Example 1:


# Input: root = [1,null,2,null,3,null,4,null,null]
# Output: [2,1,3,null,null,null,4]
# Explanation: This is not the only correct answer, [3,1,4,null,2,null,null] is also correct.
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def balanceBST(self, root):
        self.arr = []

        def tranversal(cur):
            if not cur:
                return
            tranversal(cur.left)
            self.arr.append(cur.val)
            tranversal(cur.right)

        def recurse(start, end):
            if start > end:
                return None
            mid = (start + end) // 2
            node = TreeNode(self.arr[mid])
            l = recurse(start, mid - 1)
            r = recurse(mid + 1, end)
            node.left = l
            node.right = r
            return node

        tranversal(root)
        start, end = 0, len(self.arr) - 1
        return recurse(start, end)
