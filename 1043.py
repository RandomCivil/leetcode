# Given an integer array arr, you should partition the array into (contiguous) subarrays of length at most k. After partitioning, each subarray has their values changed to become the maximum value of that subarray.

# Return the largest sum of the given array after partitioning.

# Example 1:

# Input: arr = [1,15,7,9,2,5,10], k = 3
# Output: 84
# Explanation: arr becomes [15,15,15,9,10,10,10]

# Example 2:

# Input: arr = [1,4,1,5,7,3,6,1,9,9,3], k = 4
# Output: 83

# Example 3:

# Input: arr = [1], k = 1
# Output: 1

# Constraints:


# 1 <= arr.length <= 500
# 0 <= arr[i] <= 109
# 1 <= k <= arr.length
class Solution:
    def maxSumAfterPartitioning(self, arr, k: int) -> int:
        size = len(arr)
        dp = [0] * size
        dp[0] = arr[0]
        mx = 0
        for i in range(0, k):
            mx = max(mx, arr[i])
            dp[i] = mx * (i + 1)

        for i in range(k, size):
            v = 0
            l = []
            for j in range(1, k + 1):
                l.append(arr[i - j + 1])
                v = max(v, dp[i - j] + max(l) * j)
            dp[i] = v
        print(dp)
        return dp[-1]


if __name__ == '__main__':
    arr = [1, 15, 7, 9, 2, 5, 10]
    k = 3

    arr = [1, 4, 1, 5, 7, 3, 6, 1, 9, 9, 3]
    k = 4
    print(Solution().maxSumAfterPartitioning(arr, k))
