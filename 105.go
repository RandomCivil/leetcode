package leetcode

import (
	"gitlab.com/RandomCivil/leetcode/common"
)

// Given two integer arrays preorder and inorder where preorder is the preorder traversal of a binary tree and inorder is the inorder traversal of the same tree, construct and return the binary tree.

// Example 1:

// Input: preorder = [3,9,20,15,7], inorder = [9,3,15,20,7]
// Output: [3,9,20,null,null,15,7]
// Example 2:

// Input: preorder = [-1], inorder = [-1]
// Output: [-1]

// Constraints:

// 1 <= preorder.length <= 3000
// inorder.length == preorder.length
// -3000 <= preorder[i], inorder[i] <= 3000
// preorder and inorder consist of unique values.
// Each value of inorder also appears in preorder.
// preorder is guaranteed to be the preorder traversal of the tree.
// inorder is guaranteed to be the inorder traversal of the tree.
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func buildTree(preorder []int, inorder []int) *common.TreeNode {
	pos := make(map[int]int)
	for i, n := range inorder {
		pos[n] = i
	}

	var i int
	var build func(start, end int) *common.TreeNode
	build = func(start, end int) *common.TreeNode {
		if start > end {
			return nil
		}
		val := preorder[i]
		cur := pos[val]
		i++
		l := build(start, cur-1)
		r := build(cur+1, end)
		return &common.TreeNode{
			Val:   val,
			Left:  l,
			Right: r,
		}
	}
	return build(0, len(preorder)-1)
}
