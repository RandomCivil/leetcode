# You are given a sorted array consisting of only integers where every element appears exactly twice, except for one element which appears exactly once. Find this single element that appears only once.

# Follow up: Your solution should run in O(log n) time and O(1) space.

# Example 1:

# Input: nums = [1,1,2,3,3,4,4,8,8]
# Output: 2

# Example 2:


# Input: nums = [3,3,7,7,10,11,11]
# Output: 10
class Solution:
    def singleNonDuplicate(self, nums) -> int:
        size = len(nums)
        lo, hi = 0, size - 1
        while lo < hi:
            mid = (lo + hi) // 2
            if mid % 2 == 0:
                if nums[mid] == nums[mid + 1]:
                    lo = mid + 2
                else:
                    hi = mid
            else:
                if nums[mid] == nums[mid + 1]:
                    hi = mid
                else:
                    lo = mid + 1

        return nums[lo]


if __name__ == '__main__':
    # 2
    nums = [1, 1, 2, 3, 3, 4, 4, 8, 8]

    # 10
    # nums = [3, 3, 7, 7, 10, 11, 11]
    print(Solution().singleNonDuplicate(nums))
