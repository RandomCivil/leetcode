inst_135 = __import__("135")


import pytest


class TestClass:
    @pytest.mark.parametrize(
        "ratings,expected",
        [
            ([1, 0, 2], 5),
            ([1, 2, 2], 4),
            ([1, 3, 2], 4),
            ([3, 2, 1], 6),
            ([1, 6, 10, 8, 7, 3, 2], 18),
        ],
    )
    def test_ladderLength(self, ratings, expected):
        assert inst_135.Solution().candy(ratings) == expected
