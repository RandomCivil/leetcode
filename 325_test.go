package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestMaxSubArrayLen(t *testing.T) {
	tests := []struct {
		nums      []int
		k, expect int
	}{
		{
			nums:   []int{1, -1, 5, -2, 3},
			k:      3,
			expect: 4,
		},
		{
			nums:   []int{-2, -1, 2, 1},
			k:      1,
			expect: 2,
		},
	}
	Convey("TestMaxSubArrayLen", t, func() {
		for _, tt := range tests {
			So(maxSubArrayLen(tt.nums, tt.k), ShouldEqual, tt.expect)
		}
	})
}
