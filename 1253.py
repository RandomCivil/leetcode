# Given the following details of a matrix with n columns and 2 rows :

# The matrix is a binary matrix, which means each element in the matrix can be 0 or 1.
# The sum of elements of the 0-th(upper) row is given as upper.
# The sum of elements of the 1-st(lower) row is given as lower.
# The sum of elements in the i-th column(0-indexed) is colsum[i], where colsum is given as an integer array with length n.
# Your task is to reconstruct the matrix with upper, lower and colsum.

# Return it as a 2-D integer array.

# If there are more than one valid solution, any of them will be accepted.

# If no valid solution exists, return an empty 2-D array.

# Example 1:

# Input: upper = 2, lower = 1, colsum = [1,1,1]
# Output: [[1,1,0],[0,0,1]]
# Explanation: [[1,0,1],[0,1,0]], and [[0,1,1],[1,0,0]] are also correct answers.

# Example 2:

# Input: upper = 2, lower = 3, colsum = [2,2,1,1]
# Output: []

# Example 3:

# Input: upper = 5, lower = 5, colsum = [2,1,2,0,1,0,1,2,0,1]
# Output: [[1,1,1,0,1,0,0,1,0,0],[1,0,1,0,0,0,1,1,0,1]]


class Solution:
    def reconstructMatrix(self, upper: int, lower: int, colsum):
        size = len(colsum)
        r = [[0] * size for _ in range(2)]

        s1 = []
        for i, s in enumerate(colsum):
            print(i, upper, lower, s)
            if s == 2:
                r[0][i] = 1
                r[1][i] = 1
                if upper <= 0 or lower <= 0:
                    print('hehe', i, upper, lower)
                    return []
                upper -= 1
                lower -= 1
            elif s == 1:
                s1.append(i)

        for i in s1:
            if upper > 0:
                r[0][i] = 1
                upper -= 1
            elif lower > 0:
                r[1][i] = 1
                lower -= 1
            else:
                return []

        print(colsum)
        print(upper, lower)
        return r if not upper and not lower else []


if __name__ == '__main__':
    upper = 2
    lower = 1
    colsum = [1, 1, 1]

    # upper = 2
    # lower = 3
    # colsum = [2, 2, 1, 1]

    # upper = 5
    # lower = 5
    # colsum = [2, 1, 2, 0, 1, 0, 1, 2, 0, 1]

    # []
    # upper = 4
    # lower = 7
    # colsum = [2, 1, 2, 2, 1, 1, 1]
    print(Solution().reconstructMatrix(upper, lower, colsum))
