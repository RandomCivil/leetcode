inst_424 = __import__("424")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "s,k,expected",
        [
            ("ABAB", 2, 4),
            ("AABABBA", 1, 4),
            ("AAAA", 2, 4),
            ("ABBB", 2, 4),
            ("ABAA", 0, 2),
        ],
    )
    def test_characterReplacement(self, s, k, expected):
        assert inst_424.Solution().characterReplacement(s, k) == expected
