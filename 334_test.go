package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestIncreasingTriplet(t *testing.T) {
	tests := []struct {
		nums   []int
		expect bool
	}{
		{
			nums:   []int{20, 100, 10, 12, 5, 13},
			expect: true,
		},
		{
			nums:   []int{2, 1, 5, 0, 4, 6},
			expect: true,
		},
		{
			nums:   []int{1, 2, 3, 4, 5},
			expect: true,
		},
		{
			nums:   []int{1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2},
			expect: false,
		},
		{
			nums:   []int{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 7},
			expect: true,
		},
		{
			nums:   []int{4, 5, 2147483647, 1, 2},
			expect: true,
		},
	}
	Convey("TestIncreasingTriplet", t, func() {
		for _, tt := range tests {
			So(increasingTriplet(tt.nums), ShouldEqual, tt.expect)
		}
	})
}
