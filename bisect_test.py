import pytest
import bisect


class TestClass:
    @pytest.mark.parametrize(
        "arr,x,expected",
        [
            ([1, 3, 4, 5], 2, 1),
            ([1, 2, 3, 4, 5], 2, 1),
            ([1, 2, 3, 4, 5], 6, 5),
            ([1, 2, 3, 4, 5], 0, 0),
        ],
    )
    def test_bisect_left(self, arr, x, expected):
        assert bisect.bisect_left(arr, x) == expected

    @pytest.mark.parametrize(
        "arr,x,expected",
        [
            ([1, 3, 4, 5], 2, 1),
            ([1, 2, 3, 4, 5], 2, 2),
            ([1, 2, 3, 4, 5], 6, 5),
            ([1, 2, 3, 4, 5], 0, 0),
        ],
    )
    def test_bisect(self, arr, x, expected):
        assert bisect.bisect(arr, x) == expected

    @pytest.mark.parametrize(
        "arr,x,expected",
        [
            ([1, 3, 4, 5], 2, 1),
            ([1, 2, 3, 4, 5], 2, 2),
            ([1, 2, 3, 4, 5], 6, 5),
            ([1, 2, 3, 4, 5], 0, 0),
        ],
    )
    # 和bisect一样
    def test_bisect_right(self, arr, x, expected):
        assert bisect.bisect_right(arr, x) == expected
