# Given a binary array nums and an integer k, return the maximum number of consecutive 1's in the array if you can flip at most k 0's.

# Example 1:

# Input: nums = [1,1,1,0,0,0,1,1,1,1,0], k = 2
# Output: 6
# Explanation: [1,1,1,0,0,1,1,1,1,1,1]
# Bolded numbers were flipped from 0 to 1. The longest subarray is underlined.

# Example 2:


# Input: nums = [0,0,1,1,0,0,1,1,1,0,1,1,0,0,0,1,1,1,1], k = 3
# Output: 10
# Explanation: [0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,1,1,1,1]
# Bolded numbers were flipped from 0 to 1. The longest subarray is underlined.
class Solution:
    def longestOnes(self, nums, k: int) -> int:
        print(nums, k)
        result = 0
        size = len(nums)
        start, end = 0, 0
        while end < size + 1:
            if k >= 0:
                print(start, end, end - start, k)
                result = max(result, end - start)
                if end < size and nums[end] == 0:
                    k -= 1
                end += 1
            else:
                if nums[start] == 0:
                    k += 1
                start += 1
        return result


if __name__ == '__main__':
    # 6
    nums = [1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0]
    k = 2

    # 10
    nums = [0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1]
    k = 3
    # k = 0
    print(Solution().longestOnes(nums, k))
