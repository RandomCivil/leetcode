package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/RandomCivil/leetcode/common"
)

func TestSwapPairs(t *testing.T) {
	tests := []struct {
		nums, expect []int
	}{
		{
			nums:   []int{1, 2},
			expect: []int{2, 1},
		},
		{
			nums:   []int{1, 2, 3},
			expect: []int{2, 1, 3},
		},
		{
			nums:   []int{1, 2, 3, 4},
			expect: []int{2, 1, 4, 3},
		},
		{
			nums:   []int{1, 2, 3, 4, 5},
			expect: []int{2, 1, 4, 3, 5},
		},
	}
	for _, tt := range tests {
		Convey("TestSwapPairs", t, func() {
			l := &common.LinkList{
				Nums: tt.nums,
			}
			So(l.Traversal(swapPairs((l.Build()))), ShouldResemble, tt.expect)
		})
	}
}
