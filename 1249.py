# Given a string s of '(' , ')' and lowercase English characters.

# Your task is to remove the minimum number of parentheses ( '(' or ')', in any positions ) so that the resulting parentheses string is valid and return any valid string.

# Formally, a parentheses string is valid if and only if:

# It is the empty string, contains only lowercase characters, or
# It can be written as AB (A concatenated with B), where A and B are valid strings, or
# It can be written as (A), where A is a valid string.

# Example 1:

# Input: s = "lee(t(c)o)de)"
# Output: "lee(t(c)o)de"
# Explanation: "lee(t(co)de)" , "lee(t(c)ode)" would also be accepted.

# Example 2:

# Input: s = "a)b(c)d"
# Output: "ab(c)d"

# Example 3:

# Input: s = "))(("
# Output: ""
# Explanation: An empty string is also valid.

# Example 4:

# Input: s = "(a(b(c)d)"
# Output: "a(b(c)d)"


class Solution:
    def minRemoveToMakeValid(self, s: str) -> str:
        stack = []
        for i, ch in enumerate(s):
            if ch == '(':
                stack.append((ch, i))
            elif ch == ')':
                if len(stack) > 0 and stack[-1][0] == '(':
                    stack.pop()
                else:
                    stack.append((ch, i))

        print(stack)
        arr = list(s)
        for _, i in stack:
            arr[i] = ''
        return ''.join(arr)


if __name__ == '__main__':
    # "lee(t(c)o)de"
    s = "lee(t(c)o)de)"

    # "ab(c)d"
    s = "a)b(c)d"

    s = "))(("
    # "a(b(c)d)"
    s = "(a(b(c)d)"
    print(Solution().minRemoveToMakeValid(s))
