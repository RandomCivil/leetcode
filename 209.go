package leetcode

import (
	"math"

	"gitlab.com/RandomCivil/leetcode/common"
)

//Given an array of positive integers nums and a positive integer target, return the minimal length of a contiguous subarray [numsl, numsl+1, ..., numsr-1, numsr] of which the sum is greater than or equal to target. If there is no such subarray, return 0 instead.

//Example 1:

//Input: target = 7, nums = [2,3,1,2,4,3]
//Output: 2
//Explanation: The subarray [4,3] has the minimal length under the problem constraint.
//Example 2:

//Input: target = 4, nums = [1,4,4]
//Output: 1

//Example 3:

// Input: target = 11, nums = [1,1,1,1,1,1,1,1]
// Output: 0
func MinSubArrayLen(target int, nums []int) int {
	size := len(nums)
	left := 0
	var sum int
	result := math.MaxInt
	for right := 0; right < size; right++ {
		sum += nums[right]
		for left <= right && sum >= target {
			result = common.Min(result, right-left+1)
			sum -= nums[left]
			left++
		}
	}
	if result == math.MaxInt {
		return 0
	}
	return result
}
