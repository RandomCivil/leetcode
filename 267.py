# Given a string s, return all the palindromic permutations (without duplicates) of it.

# You may return the answer in any order. If s has no palindromic permutation, return an empty list.

# Example 1:

# Input: s = "aabb"
# Output: ["abba","baab"]
# Example 2:

# Input: s = "abc"
# Output: []


# Constraints:

# 1 <= s.length <= 16
# s consists of only lowercase English letters.
from collections import Counter


class Solution:
    def generatePalindromes(self, s: str):
        size = len(s)
        c = Counter(s)
        oddS = None
        odd, even = 0, 0
        for k, v in c.items():
            if v % 2 == 0:
                even += 1
            else:
                odd += 1
                oddS = k
        if odd > 1:
            return []

        result = []
        start = ""
        if oddS:
            start = oddS
            c[oddS] -= 1

        def dfs(ss, counter, arr):
            if len(ss) == size:
                result.append(ss)
                return
            for s in arr:
                if counter[s] <= 0:
                    continue
                counter[s] -= 2
                dfs(s + ss + s, counter, arr)
                counter[s] += 2

        dfs(start, c, c.keys())
        return result


if __name__ == "__main__":
    s = "aabb"
    s = "aaaabbccc"
    print(Solution().generatePalindromes(s))
