package leetcode

import (
	"fmt"

	"gitlab.com/RandomCivil/leetcode/common"
)

func LongestPalindromeSubseq(s string) int {
	fmt.Println(s)
	size := len(s)
	dp := make([][]int, size)
	for i := 0; i < size; i++ {
		dp[i] = make([]int, size)
	}
	for i := size - 1; i >= 0; i-- {
		dp[i][i] = 1
		for j := i + 1; j < size; j++ {
			if s[i] == s[j] {
				dp[i][j] = dp[i+1][j-1] + 2
			} else {
				dp[i][j] = common.Max(dp[i+1][j], dp[i][j-1])
			}
		}
	}
	fmt.Println(dp)
	var result int
	for i := 0; i < size; i++ {
		if dp[i][size-1] > result {
			result = dp[i][size-1]
		}
	}
	return result
}
