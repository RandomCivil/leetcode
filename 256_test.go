package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestMinCost(t *testing.T) {
	tests := []struct {
		costs  [][]int
		expect int
	}{
		{
			costs: [][]int{
				[]int{17, 2, 17},
				[]int{16, 16, 5},
				[]int{14, 3, 19},
			},
			expect: 10,
		},
	}
	Convey("TestMinCost", t, func() {
		for _, tt := range tests {
			So(minCost(tt.costs), ShouldEqual, tt.expect)
		}
	})
}
