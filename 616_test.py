inst_616 = __import__("616")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "s,words,expected",
        [
            ("abcxyz123", ["abc", "123"], "<b>abc</b>xyz<b>123</b>"),
            ("aaabbb", ["aa", "b"], "<b>aaabbb</b>"),
            ("a", [], "a"),
            ("aaabbcc", ["aaa", "aab", "bc"], "<b>aaabbc</b>c"),
        ],
    )
    def test_addBoldTag(self, s, words, expected):
        assert inst_616.Solution().addBoldTag(s, words) == expected
