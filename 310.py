# A tree is an undirected graph in which any two vertices are connected by exactly one path. In other words, any connected graph without simple cycles is a tree.

# Given a tree of n nodes labelled from 0 to n - 1, and an array of n - 1 edges where edges[i] = [ai, bi] indicates that there is an undirected edge between the two nodes ai and bi in the tree, you can choose any node of the tree as the root. When you select a node x as the root, the result tree has height h. Among all possible rooted trees, those with minimum height (i.e. min(h))  are called minimum height trees (MHTs).

# Return a list of all MHTs' root labels. You can return the answer in any order.

# The height of a rooted tree is the number of edges on the longest downward path between the root and a leaf.

# Example 1:

# Input: n = 4, edges = [[1,0],[1,2],[1,3]]
# Output: [1]
# Explanation: As shown, the height of the tree is 1 when the root is the node with label 1 which is the only MHT.

# Example 2:

# Input: n = 6, edges = [[3,0],[3,1],[3,2],[3,4],[5,4]]
# Output: [3,4]

# Example 3:

# Input: n = 1, edges = []
# Output: [0]

# Example 4:

# Input: n = 2, edges = [[0,1]]
# Output: [0,1]
from collections import defaultdict


class Solution:
    def findMinHeightTrees(self, n: int, edges):
        if n <= 2:
            return [i for i in range(n)]

        neighbors = defaultdict(set)

        for s, e in edges:
            neighbors[s].add(e)
            neighbors[e].add(s)

        print(neighbors)
        leaves = [e for e, path in neighbors.items() if len(path) == 1]

        while len(neighbors) > 2:
            next_leaves = []
            print(leaves)
            for leaf in leaves:
                s = neighbors[leaf].pop()
                neighbors[s].remove(leaf)
                del neighbors[leaf]
                if len(neighbors[s]) == 1:
                    next_leaves.append(s)

            leaves = next_leaves

        return leaves


if __name__ == '__main__':
    n = 4
    edges = [[1, 0], [1, 2], [1, 3]]

    n = 6
    edges = [[3, 0], [3, 1], [3, 2], [3, 4], [5, 4]]

    n = 3
    edges = [[0, 1], [0, 2]]
    print(Solution().findMinHeightTrees(n, edges))
