# Given the root of a binary tree, return the most frequent subtree sum. If there is a tie, return all the values with the highest frequency in any order.

# The subtree sum of a node is defined as the sum of all the node values formed by the subtree rooted at that node (including the node itself).

# Example 1:

# Input: root = [5,2,-3]
# Output: [2,-3,4]

# Example 2:

# Input: root = [5,2,-5]
# Output: [2]
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
from collections import defaultdict


class Solution:
    def findFrequentTreeSum(self, root):
        self.max_freq = 0
        self.d = defaultdict(int)

        def dfs(head):
            if not head:
                return 0
            l = dfs(head.left)
            r = dfs(head.right)
            v = l + r + head.val
            self.d[v] += 1
            self.max_freq = max(self.max_freq, self.d[v])
            return v

        dfs(root)
        return [s for s, f in self.d.items() if f == self.max_freq]
