# You are given an m x n grid grid where:

# '.' is an empty cell.
# '#' is a wall.
# '@' is the starting point.
# Lowercase letters represent keys.
# Uppercase letters represent locks.
# You start at the starting point and one move consists of walking one space in one of the four cardinal directions. You cannot walk outside the grid, or walk into a wall.

# If you walk over a key, you can pick it up and you cannot walk over a lock unless you have its corresponding key.

# For some 1 <= k <= 6, there is exactly one lowercase and one uppercase letter of the first k letters of the English alphabet in the grid. This means that there is exactly one key for each lock, and one lock for each key; and also that the letters used to represent the keys and locks were chosen in the same order as the English alphabet.

# Return the lowest number of moves to acquire all keys. If it is impossible, return -1.


# Example 1:


# Input: grid = ["@.a..","###.#","b.A.B"]
# Output: 8
# Explanation: Note that the goal is to obtain all the keys not to open all the locks.
# Example 2:


# Input: grid = ["@..aA","..B#.","....b"]
# Output: 6
# Example 3:


# Input: grid = ["@Aa"]
# Output: -1


# Constraints:

# m == grid.length
# n == grid[i].length
# 1 <= m, n <= 30
# grid[i][j] is either an English letter, '.', '#', or '@'.
# The number of keys in the grid is in the range [1, 6].
# Each key in the grid is unique.
# Each key in the grid has a matching lock.
from collections import deque
import functools


class Solution:
    def shortestPathAllKeys(self, grid: list[str]) -> int:
        m, n = len(grid), len(grid[0])
        keys = [True] * 6
        start_x, start_y = None, None
        q = deque()
        for i in range(m):
            for j in range(n):
                if grid[i][j] == "@":
                    start_x, start_y = i, j
                if grid[i][j].islower():
                    keys[ord(grid[i][j]) - ord("a")] = False
        print(start_x, start_y)
        if start_x is None and start_y is None:
            return -1
        q.append((start_x, start_y, keys, 0))

        directions = [(1, 0), (-1, 0), (0, 1), (0, -1)]
        visited = set()
        while len(q) > 0:
            x, y, cur, c = q.popleft()
            print(x, y, cur)
            if (x, y, tuple(cur)) in visited:
                continue
            visited.add((x, y, tuple(cur)))
            if functools.reduce(lambda x, y: x & y, cur):
                return c
            for xx, yy in directions:
                if 0 <= x + xx < m and 0 <= y + yy < n and grid[x + xx][y + yy] != "#":
                    if grid[x + xx][y + yy].islower():  # key
                        new_keys = cur.copy()
                        new_keys[ord(grid[x + xx][y + yy]) - ord("a")] = True
                        q.append((x + xx, y + yy, new_keys, c + 1))
                    elif grid[x + xx][y + yy].isupper():  # lock
                        if cur[ord(grid[x + xx][y + yy]) - ord("A")]:  # has key
                            q.append((x + xx, y + yy, cur, c + 1))
                    else:  # empty cell
                        q.append((x + xx, y + yy, cur, c + 1))
        return -1
