package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestIsPowerOfTwo(t *testing.T) {
	tests := []struct {
		n      int
		expect bool
	}{
		{
			n:      0,
			expect: false,
		},
		{
			n:      1,
			expect: true,
		},
		{
			n:      3,
			expect: false,
		},
		{
			n:      -3,
			expect: false,
		},
		{
			n:      16,
			expect: true,
		},
		{
			n:      -16,
			expect: false,
		},
	}
	Convey("TestIsPowerOfTwo", t, func() {
		for _, tt := range tests {
			So(isPowerOfTwo(tt.n), ShouldEqual, tt.expect)
		}
	})
}
