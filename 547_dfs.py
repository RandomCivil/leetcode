# There are N students in a class. Some of them are friends, while some are not. Their friendship is transitive in nature. For example, if A is a direct friend of B, and B is a direct friend of C, then A is an indirect friend of C. And we defined a friend circle is a group of students who are direct or indirect friends.
# Given a N*N matrix M representing the friend relationship between students in the class. If M[i][j] = 1, then the ith and jth students are direct friends with each other, otherwise not. And you have to output the total number of friend circles among all the students.
"""
Input:
[[1,1,0],
 [1,1,0],
 [0,0,1]]
Output: 2
Explanation:The 0th and 1st students are direct friends, so they are in a friend circle.
The 2nd student himself is in a friend circle. So return 2.

Input:
[[1,1,0],
 [1,1,1],
 [0,1,1]]
Output: 1
Explanation:The 0th and 1st students are direct friends, the 1st and 2nd students are direct friends,
so the 0th and 2nd students are indirect friends. All of them are in the same friend circle, so return 1.
"""
from collections import defaultdict


class Solution:
    def findCircleNum(self, isConnected) -> int:
        result = 0
        d = defaultdict(list)
        self.visited = set()
        m, n = len(isConnected), len(isConnected[0])
        for i in range(m):
            for j in range(n):
                if i == j:
                    continue
                if isConnected[i][j] == 1:
                    d[i].append(j)
                    d[j].append(i)

        def dfs(cur, parent):
            if cur in self.visited:
                return
            self.visited.add(cur)
            for nxt in d[cur]:
                if nxt == parent:
                    continue
                dfs(nxt, cur)

        for i in range(m):
            if i in self.visited:
                continue
            dfs(i, -1)
            result += 1
        return result


if __name__ == "__main__":
    M = [[1, 1, 0], [1, 1, 0], [0, 0, 1]]
    # M = [[1, 1, 0],
    # [1, 1, 1],
    # [0, 1, 1]]

    print(Solution().findCircleNum(M))
