package leetcode

//Given the root of a binary search tree, and an integer k, return the kth (1-indexed) smallest element in the tree.

//Example 1:

//Input: root = [3,1,4,null,2], k = 1
//Output: 1

//Example 2:

//Input: root = [5,3,6,2,4,null,null,1], k = 3
//Output: 3
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func KthSmallest(root *TreeNode, k int) int {
	r := -1
	var dfs func(head *TreeNode)
	dfs = func(head *TreeNode) {
		if head == nil {
			return
		}
		if r != -1 {
			return
		}
		dfs(head.Left)
		k--
		if k == 0 {
			r = head.Val
			return
		}
		dfs(head.Right)
	}
	dfs(root)
	return r
}
