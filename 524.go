package leetcode

import (
	"fmt"
	"sort"
	"strings"
)

//Given a string and a string dictionary, find the longest string in the dictionary that can be formed by deleting
//some characters of the given string. If there are more than one possible results,
//return the longest word with the smallest lexicographical order. If there is no possible result, return the empty string.

/*
Input:
s = "abpcplea", d = ["ale","apple","monkey","plea"]

Output:
"apple"

Input:
s = "abpcplea", d = ["a","b","c"]

Output:
"a"
*/
func FindLongestWord(s string, d []string) string {
	size := len(s)
	sort.Sort(Dict(d))
	fmt.Println(d)
	for _, x := range d {
		var k, g int
		for g < size {
			if x[k] == s[g] {
				k++
			}
			if k == len(x) {
				return x
			}
			g++
		}
	}
	return ""
}

type Dict []string

func (a Dict) Len() int      { return len(a) }
func (a Dict) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a Dict) Less(i, j int) bool {
	if len(a[i]) != len(a[j]) {
		return len(a[i])-len(a[j]) > 0
	} else {
		return strings.Compare(a[i], a[j]) < 0
	}
}
