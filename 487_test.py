inst_487 = __import__("487")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "nums,expected",
        [
            ([0], 1),
            ([1], 1),
            ([0, 1], 2),
            ([1, 0], 2),
            ([1, 0, 1, 1, 0], 4),
            ([1, 0, 1, 1, 0, 1], 4),
        ],
    )
    def test_findMaxConsecutiveOnes(self, nums, expected):
        assert inst_487.Solution().findMaxConsecutiveOnes(nums) == expected
