package leetcode

import (
	"fmt"

	"gitlab.com/RandomCivil/leetcode/common"
)

// Given an array of integers nums sorted in non-decreasing order, find the starting and ending position of a given target value.

// If target is not found in the array, return [-1, -1].

// You must write an algorithm with O(log n) runtime complexity.

// Example 1:

// Input: nums = [5,7,7,8,8,10], target = 8
// Output: [3,4]
// Example 2:

// Input: nums = [5,7,7,8,8,10], target = 6
// Output: [-1,-1]
// Example 3:

// Input: nums = [], target = 0
// Output: [-1,-1]

// Constraints:

// 0 <= nums.length <= 105
// -109 <= nums[i] <= 109
// nums is a non-decreasing array.
// -109 <= target <= 109
func searchRange(nums []int, target int) []int {
	size := len(nums)
	searchLeftMost := func(find int) int {
		lo, hi := 0, size-1
		leftMost := -1
		for lo <= hi {
			mid := (lo + hi) / 2
			if nums[mid] > target {
				hi = mid - 1
			} else if nums[mid] < target {
				lo = mid + 1
			} else {
				hi = mid - 1
				leftMost = mid
			}
		}
		return leftMost
	}
	searchRightMost := func(find int) int {
		lo, hi := 0, size-1
		rightMost := -1
		for lo <= hi {
			mid := (lo + hi) / 2
			if nums[mid] > target {
				hi = mid - 1
			} else if nums[mid] < target {
				lo = mid + 1
			} else {
				lo = mid + 1
				rightMost = mid
			}
		}
		return rightMost
	}
	find := common.BinarySearch(nums, target)
	if find == -1 {
		return []int{-1, -1}
	}
	fmt.Println(nums, target, find)
	result := []int{searchLeftMost(find), searchRightMost(find)}
	return result
}
