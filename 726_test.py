inst_726 = __import__("726")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "formula,expected",
        [
            ("(Mg2)2", "Mg4"),
            ("Mg(H2OH10)2", "H24MgO2"),
            ("Mg(OH10)2", "H20MgO2"),
            ("K4(ON(SO3)2)2", "K4N2O14S4"),
            ("Mg(OH)2", "H2MgO2"),
            ("H2O", "H2O"),
            ("(H2O2)3", "H6O6"),
            ("a", ""),
        ],
    )
    def test_countOfAtoms(self, formula, expected):
        assert inst_726.Solution().countOfAtoms(formula) == expected
