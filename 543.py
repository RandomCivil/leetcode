# Given a binary tree, you need to compute the length of the diameter of the tree. The diameter of a binary tree is the length of the longest path between any two nodes in a tree. This path may or may not pass through the root.

# Example:
# Given a binary tree
"""
          1
         / \
        2   3
       / \     
      4   5    
"""
# Return 3, which is the length of the path [4,2,1,3] or [5,2,1,3].
# Note: The length of path between two nodes is represented by the number of edges between them.

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right


class Solution:
    def diameterOfBinaryTree(self, root) -> int:
        self.r = 0

        def dfs(head, parent, cur_depth):
            if not head:
                return cur_depth
            v1 = dfs(head.left, head, cur_depth + 1)
            v2 = dfs(head.right, head, cur_depth + 1)
            dis = (v1 - cur_depth) + (v2 - cur_depth) - 2
            print(head.val, v1, v2, dis, cur_depth)
            self.r = max(self.r, dis)
            return max(v1, v2)

        dfs(root, root, 0)

        return self.r
