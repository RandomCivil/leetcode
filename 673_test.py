inst_673 = __import__("673")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "nums,expected",
        [
            ([1, 3, 5, 4, 7], 2),
            ([1, 2, 4, 3, 5, 4, 7, 2], 3),
            ([2, 2, 2, 2, 2], 5),
        ],
    )
    def test_findNumberOfLIS(self, nums, expected):
        assert inst_673.Solution().findNumberOfLIS(nums) == expected
