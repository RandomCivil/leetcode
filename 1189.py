# Given a string text, you want to use the characters of text to form as many instances of the word "balloon" as possible.
# You can use each character in text at most once. Return the maximum number of instances that can be formed.

# Example 1:

# Input: text = "nlaebolko"
# Output: 1

# Example 2:

# Input: text = "loonbalxballpoon"
# Output: 2

# Example 3:

# Input: text = "leetcode"
# Output: 0
from collections import Counter


class Solution:
    def maxNumberOfBalloons(self, text: str) -> int:
        c = Counter(text)
        balloon_counter = Counter('balloon')
        r = float('inf')
        for ch, times in balloon_counter.items():
            if ch not in c:
                return 0
            r = min(r, c[ch] // times)

        return r
