inst_474 = __import__("474")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "strs,m,n,expected",
        [
            (["10", "0001", "111001", "1", "0"], 5, 3, 4),
            (["10", "0", "1"], 1, 1, 2),
            (["00101011"], 36, 39, 1),
        ],
    )
    def test_findMaxForm(self, strs, m: int, n: int, expected):
        assert inst_474.Solution().findMaxForm(strs, m, n) == expected
