package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestFindUnsortedSubarray(t *testing.T) {
	tests := []struct {
		nums   []int
		expect int
	}{
		{
			nums:   []int{2, 6, 4, 8, 10, 9, 15},
			expect: 5,
		},
		{
			nums:   []int{1, 2, 3, 4},
			expect: 0,
		},
		{
			nums:   []int{1},
			expect: 0,
		},
		{
			nums:   []int{1, 3, 2, 2, 2},
			expect: 4,
		},
	}
	Convey("TestFindUnsortedSubarray", t, func() {
		for _, tt := range tests {
			So(FindUnsortedSubarray(tt.nums), ShouldEqual, tt.expect)
		}
	})
}
