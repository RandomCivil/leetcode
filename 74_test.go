package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestSearchMatrix(t *testing.T) {
	tests := []struct {
		matrix [][]int
		expect bool
		target int
	}{
		{
			matrix: [][]int{
				[]int{1, 3, 5, 7},
				[]int{10, 11, 16, 20},
				[]int{23, 30, 34, 60},
			},
			expect: true,
			target: 3,
		},
		{
			matrix: [][]int{
				[]int{1, 3, 5, 7},
				[]int{10, 11, 16, 20},
				[]int{23, 30, 34, 60},
			},
			expect: false,
			target: 13,
		},
	}
	Convey("TestSearchMatrix", t, func() {
		for _, tt := range tests {
			So(searchMatrix(tt.matrix, tt.target), ShouldEqual, tt.expect)
		}
	})
}
