inst_1366 = __import__("1366")


import pytest


class TestClass:
    @pytest.mark.parametrize(
        "votes,expected",
        [
            (["ABC", "ACB", "ABC", "ACB", "ACB"], "ACB"),
            (["WXYZ", "XYZW"], "XWYZ"),
            (["ZMNAGUEDSJYLBOPHRQICWFXTVK"], "ZMNAGUEDSJYLBOPHRQICWFXTVK"),
            (
                [
                    "FVSHJIEMNGYPTQOURLWCZKAX",
                    "AITFQORCEHPVJMXGKSLNZWUY",
                    "OTERVXFZUMHNIYSCQAWGPKJL",
                    "VMSERIJYLZNWCPQTOKFUHAXG",
                    "VNHOZWKQCEFYPSGLAMXJIUTR",
                    "ANPHQIJMXCWOSKTYGULFVERZ",
                    "RFYUXJEWCKQOMGATHZVILNSP",
                    "SCPYUMQJTVEXKRNLIOWGHAFZ",
                    "VIKTSJCEYQGLOMPZWAHFXURN",
                    "SVJICLXKHQZTFWNPYRGMEUAO",
                    "JRCTHYKIGSXPOZLUQAVNEWFM",
                    "NGMSWJITREHFZVQCUKXYAPOL",
                    "WUXJOQKGNSYLHEZAFIPMRCVT",
                    "PKYQIOLXFCRGHZNAMJVUTWES",
                    "FERSGNMJVZXWAYLIKCPUQHTO",
                    "HPLRIUQMTSGYJVAXWNOCZEKF",
                    "JUVWPTEGCOFYSKXNRMHQALIZ",
                    "MWPIAZCNSLEYRTHFKQXUOVGJ",
                    "EZXLUNFVCMORSIWKTYHJAQPG",
                    "HRQNLTKJFIEGMCSXAZPYOVUW",
                    "LOHXVYGWRIJMCPSQENUAKTZF",
                    "XKUTWPRGHOAQFLVYMJSNEIZC",
                    "WTCRQMVKPHOSLGAXZUEFYNJI",
                ],
                "VWFHSJARNPEMOXLTUKICZGYQ",
            ),
            (["BCA", "CAB", "CBA", "ABC", "ACB", "BAC"], "ABC"),
        ],
    )
    def test_rankTeams(self, votes, expected):
        assert inst_1366.Solution().rankTeams(votes) == expected
