# Given an array A, partition it into two (contiguous) subarrays left and right so that:

# Every element in left is less than or equal to every element in right.
# left and right are non-empty.
# left has the smallest possible size.
# Return the length of left after such a partitioning.  It is guaranteed that such a partitioning exists.

# Example 1:

# Input: [5,0,3,8,6]
# Output: 3
# Explanation: left = [5,0,3], right = [8,6]

# Example 2:

# Input: [1,1,1,0,6,12]
# Output: 4
# Explanation: left = [1,1,1,0], right = [6,12]

# Note:

# 2 <= A.length <= 30000
# 0 <= A[i] <= 10^6
# It is guaranteed there is at least one way to partition A as described.


class Solution:
    def partitionDisjoint(self, nums: list[int]) -> int:
        size = len(nums)
        mx = [float("-inf")] * (size + 1)
        mn = [float("inf")] * (size + 1)
        for i in range(1, size + 1):
            mx[i] = max(mx[i - 1], nums[i - 1])
        for i in range(size - 1, -1, -1):
            mn[i] = min(mn[i + 1], nums[i])
        print(mx)
        print(mn)

        for i in range(1, size + 1):
            if mx[i] <= mn[i]:
                return i
        return 0


if __name__ == "__main__":
    # 3
    nums = [5, 0, 3, 8, 6]

    # 4
    nums = [1, 1, 1, 0, 6, 12]

    # 2
    nums = [6, 0, 8, 30, 37, 6, 75, 98, 39, 90, 63, 74, 52, 92, 64]

    # 1
    nums = [26, 51, 40, 58, 42, 76, 30, 48, 79, 91]
    print(Solution().partitionDisjoint(nums))
