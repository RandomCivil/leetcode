package leetcode

import (
	"math"
	"sort"
)

//Given an array nums of n integers and an integer target, find three integers in nums such that the sum is closest to target. Return the sum of the three integers. You may assume that each input would have exactly one solution.

//Example 1:

//Input: nums = [-1,2,1,-4], target = 1
//Output: 2
//Explanation: The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
func ThreeSumClosest(nums []int, target int) int {
	size := len(nums)
	sort.Ints(nums)
	dis := int(math.Pow10(5))
	var sum, l, r, result, v int
	for i, num := range nums {
		l, r = i+1, size-1
		for l < r {
			sum = num + nums[l] + nums[r]
			v = int(math.Abs(float64(target - sum)))
			if v < dis {
				dis = v
				result = sum
			}

			if sum < target {
				l++
			} else if sum > target {
				r--
			} else {
				return target
			}
		}
	}
	return result
}
