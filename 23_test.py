inst_23 = __import__("23")

import pytest, common


class TestClass:
    @pytest.mark.parametrize(
        "lists,expected",
        [
            ([[1, 4, 5], [1, 3, 4], [2, 6]], [1, 1, 2, 3, 4, 4, 5, 6]),
            ([], []),
            ([[]], []),
        ],
    )
    def test_mergeKLists(self, lists, expected):
        rootList = []
        for nums in lists:
            l = common.LinkList(nums)
            rootList.append(l.build())
        r = inst_23.Solution().mergeKLists(rootList)

        result = []
        while r:
            result.append(r.val)
            r = r.next
        assert result == expected
