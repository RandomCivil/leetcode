# Given an array consists of non-negative integers, your task is to count the number of triplets chosen from the array that can make triangles if we take them as side lengths of a triangle.
"""
Input: [2,2,3,4]
Output: 3
Explanation:
Valid combinations are:
2,3,4 (using the first 2)
2,3,4 (using the second 2)
2,2,3
"""
# The length of the given array won't exceed 1000.
# The integers in the given array are in the range of [0, 1000].
import bisect


class Solution:
    def triangleNumber(self, nums) -> int:
        size = len(nums)
        result = 0
        nums = sorted(nums)
        for i in range(size - 2):
            if nums[i] == 0:
                continue
            for j in range(i + 1, size - 1):
                if nums[j] == 0:
                    continue
                k = bisect.bisect_left(nums, nums[i] + nums[j])
                print(nums[i], nums[j], k, j)
                result += k - j - 1
        return result


if __name__ == "__main__":
    nums = [2, 2, 3, 4]
    # nums = [2, 5, 3, 4]
    print(Solution().triangleNumber(nums))
