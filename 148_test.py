inst_148 = __import__("148")


import pytest, common


class TestClass:
    @pytest.mark.parametrize(
        "nums,expected",
        [
            ([4, 2, 1, 3], [1, 2, 3, 4]),
            ([-1, 5, 3, 4, 0], [-1, 0, 3, 4, 5]),
            ([], []),
        ],
    )
    def test_sortList(self, nums, expected):
        l = common.LinkList(nums)
        result = inst_148.Solution().sortList(l.build())
        assert l.traversal(result) == expected
