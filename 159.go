package leetcode

import (
	"gitlab.com/RandomCivil/leetcode/common"
)

// Given a string s, return the length of the longest substring that contains at most two distinct characters.

// Example 1:

// Input: s = "eceba"
// Output: 3
// Explanation: The substring is "ece" which its length is 3.
// Example 2:

// Input: s = "ccaabbb"
// Output: 5
// Explanation: The substring is "aabbb" which its length is 5.

// Constraints:

// 1 <= s.length <= 105
// s consists of English letters.
func lengthOfLongestSubstringTwoDistinct(s string) int {
	size := len(s)
	counter := make(map[string]int)
	var result int
	left := 0
	for right := 0; right < size; right++ {
		counter[string(s[right])]++
		if len(counter) > 2 {
			counter[string(s[left])]--
			if counter[string(s[left])] == 0 {
				delete(counter, string(s[left]))
			}
			left++
		} else {
			result = common.Max(result, right-left+1)
		}
	}
	return result
}
