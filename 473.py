#! /usr/bin/env python3

# Remember the story of Little Match Girl? By now, you know exactly what matchsticks the little match girl has, please find out a way you can make one square by using up all those matchsticks. You should not break any stick, but you can link them up, and each matchstick must be used exactly one time.
# Your input will be several matchsticks the girl has, represented with their stick length. Your output will either be true or false, to represent whether you could make one square using all the matchsticks the little match girl has.
"""
Input: [1,1,2,2,2]
Output: true

Explanation: You can form a square with length 2, one side of the square came two sticks with length 1.

Input: [3,3,3,3,4]
Output: false

Explanation: You cannot find a way to form a square with all the matchsticks.
"""


class Solution:
    def makesquare(self, matchsticks) -> bool:
        if len(matchsticks) < 4:
            return False

        s = sum(matchsticks)
        target, d = divmod(s, 4)
        if d != 0:
            return False

        self.sums = [0] * 4
        self.visited = set()

        def dfs(step, start):
            if self.sums[step] > target:
                return False
            if self.sums[step] == target:
                if step == 3:
                    return True
                return dfs(step + 1, 0)
            for i, n in enumerate(matchsticks):
                if i < start:
                    continue
                if i in self.visited:
                    continue
                self.visited.add(i)
                self.sums[step] += n
                if dfs(step, i + 1):
                    return True
                self.sums[step] -= n
                self.visited.remove(i)
            return False

        return dfs(0, 0)


if __name__ == "__main__":
    matchsticks = [1, 1, 2, 2, 2]
    # matchsticks = [3, 3, 3, 3, 4]
    # matchsticks = [5, 5, 5, 5, 4, 4, 4, 4, 3, 3, 3, 3]
    # matchsticks = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
    print(Solution().makesquare(matchsticks))
