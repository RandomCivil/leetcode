package leetcode

// You are given an integer array matchsticks where matchsticks[i] is the length of the ith matchstick. You want to use all the matchsticks to make one square. You should not break any stick, but you can link them up, and each matchstick must be used exactly one time.

// Return true if you can make this square and false otherwise.

// Example 1:

// Input: matchsticks = [1,1,2,2,2]
// Output: true
// Explanation: You can form a square with length 2, one side of the square came two sticks with length 1.
// Example 2:

// Input: matchsticks = [3,3,3,3,4]
// Output: false
// Explanation: You cannot find a way to form a square with all the matchsticks.
func makesquare(matchsticks []int) bool {
	size := len(matchsticks)
	if size < 4 {
		return false
	}
	var sum int
	for _, n := range matchsticks {
		sum += n
	}
	if sum%4 > 0 {
		return false
	}
	target := sum / 4

	sums := make([]int, 4)
	visited := make(map[int]struct{})

	var dfs func(step, start int) bool
	dfs = func(step, start int) bool {
		if sums[step] > target {
			return false
		}
		if sums[step] == target {
			if step == 3 {
				return true
			}
			return dfs(step+1, 0)
		}
		for i := start; i < size; i++ {
			if _, ok := visited[i]; ok {
				continue
			}
			visited[i] = struct{}{}
			sums[step] += matchsticks[i]
			if dfs(step, i+1) {
				return true
			}
			sums[step] -= matchsticks[i]
			delete(visited, i)
		}
		return false
	}
	return dfs(0, 0)
}
