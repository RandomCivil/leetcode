package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestMaxProfitMultiple(t *testing.T) {
	tests := []struct {
		prices []int
		expect int
	}{
		{
			prices: []int{7, 1, 5, 3, 6, 4},
			expect: 7,
		},
		{
			prices: []int{1, 2, 3, 4, 5},
			expect: 4,
		},
		{
			prices: []int{3, 3},
			expect: 0,
		},
	}
	Convey("TestMaxProfitMultiple", t, func() {
		for _, tt := range tests {
			So(maxProfitMultiple(tt.prices), ShouldEqual, tt.expect)
		}
	})
}
