package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/RandomCivil/leetcode/common"
)

func TestMergeTwoLists(t *testing.T) {
	tests := []struct {
		list1, list2, expect []int
	}{
		{
			list1:  []int{1, 2, 4},
			list2:  []int{1, 3, 4},
			expect: []int{1, 1, 2, 3, 4, 4},
		},
		{
			list1:  []int{1, 2, 4},
			list2:  []int{1, 3, 5},
			expect: []int{1, 1, 2, 3, 4, 5},
		},
		{
			list1:  []int{1},
			list2:  []int{},
			expect: []int{1},
		},
		{
			list1:  []int{-9, 3},
			list2:  []int{5, 7},
			expect: []int{-9, 3, 5, 7},
		},
	}
	Convey("TestMergeTwoLists", t, func() {
		for _, tt := range tests {
			l1, l2 := &common.LinkList{Nums: tt.list1}, &common.LinkList{Nums: tt.list2}
			root1, root2 := l1.Build(), l2.Build()
			cur := mergeTwoLists(root1, root2)
			var result []int
			for cur != nil {
				result = append(result, cur.Val)
				cur = cur.Next
			}
			So(result, ShouldResemble, tt.expect)
		}
	})
}
