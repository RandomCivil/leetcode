# Given the root of a binary tree, the level of its root is 1, the level of its children is 2, and so on.

# Return the smallest level x such that the sum of all the values of nodes at level x is maximal.

# Example 1:

# Input: root = [1,7,0,7,-8,null,null]
# Output: 2
# Explanation:
# Level 1 sum = 1.
# Level 2 sum = 7 + 0 = 7.
# Level 3 sum = 7 + -8 = -1.
# So we return the level with the maximum sum which is level 2.

# Example 2:

# Input: root = [989,null,10250,98693,-89388,null,null,null,-32127]
# Output: 2
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
from collections import deque


class Solution:
    def maxLevelSum(self, root) -> int:
        r = 0
        queue = deque([root])
        next_queue = deque([])
        mx = float('-inf')
        n = 0
        s = 0
        while len(queue) > 0:
            pop = queue.popleft()
            s += pop.val
            if pop.left:
                next_queue.append(pop.left)
            if pop.right:
                next_queue.append(pop.right)

            if len(queue) == 0:
                if s > mx:
                    mx = s
                    r = n

                s = 0
                n += 1
                queue = next_queue
                next_queue = deque([])

        return r
