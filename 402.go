package leetcode

import "fmt"

//Given a non-negative integer num represented as a string, remove k digits from the number so that the new number is the smallest possible.

//The length of num is less than 10002 and will be ≥ k.
//The given num does not contain any leading zero.

/*
Input: num = "1432219", k = 3
Output: "1219"
Explanation: Remove the three digits 4, 3, and 2 to form the new number 1219 which is the smallest.

Input: num = "10200", k = 1
Output: "200"
Explanation: Remove the leading 1 and the number is 200. Note that the output must not contain leading zeroes.

Input: num = "10", k = 2
Output: "0"
Explanation: Remove all the digits from the number and it is left with nothing which is 0.
*/

//num, k := "12741516", 3
//num, k := "1421532", 3
//num, k := "10200", 1
//num, k := "10", 2
//num, k := "10", 1
//num, k := "112", 1
//num, k := "1173", 2

//func removeKdigits(num string, k int) string {
func RemoveKdigits(num string, k int) string {
	fmt.Println(num, k)
	size := len(num)
	if size == k {
		return "0"
	}
	var stack []byte
	for i := 0; i < size; i++ {
		for k > 0 && len(stack) > 0 {
			fmt.Println(stack)
			last := stack[len(stack)-1]
			if last > num[i] {
				stack = stack[:len(stack)-1]
				k--
			} else {
				break
			}
		}
		stack = append(stack, num[i])
	}
	fmt.Println(string(stack))
	start := 0
	for i := 0; i < len(stack); i++ {
		if stack[i] == '0' {
			start++
		} else {
			break
		}
	}

	if size-len(stack) <= k {
		return string(stack[:len(stack)-k])
	}

	if string(stack[start:]) == "" {
		return "0"
	} else {
		return string(stack[start:])
	}
}
