inst_93 = __import__("93")


import pytest


class TestClass:
    @pytest.mark.parametrize(
        "s,expected",
        [
            ("25525511135", ["255.255.11.135", "255.255.111.35"]),
            ("0000", ["0.0.0.0"]),
            (
                "101023",
                ["1.0.10.23", "1.0.102.3", "10.1.0.23", "10.10.2.3", "101.0.2.3"],
            ),
            ("010010", ["0.10.0.10", "0.100.1.0"]),
        ],
    )
    def test_restoreIpAddresses(self, s, expected):
        assert inst_93.Solution().restoreIpAddresses(s) == expected
