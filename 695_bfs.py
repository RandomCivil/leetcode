# Given a non-empty 2D array grid of 0's and 1's, an island is a group of 1's (representing land)
# connected 4-directionally (horizontal or vertical.) You may assume all four edges of the grid are surrounded by water.
# Find the maximum area of an island in the given 2D array. (If there is no island, the maximum area is 0.)
"""
[[0,0,1,0,0,0,0,1,0,0,0,0,0],
 [0,0,0,0,0,0,0,1,1,1,0,0,0],
 [0,1,1,0,1,0,0,0,0,0,0,0,0],
 [0,1,0,0,1,1,0,0,1,0,1,0,0],
 [0,1,0,0,1,1,0,0,1,1,1,0,0],
 [0,0,0,0,0,0,0,0,0,0,1,0,0],
 [0,0,0,0,0,0,0,1,1,1,0,0,0],
 [0,0,0,0,0,0,0,1,1,0,0,0,0]]

Given the above grid, return 6. Note the answer is not 11, because the island must be connected 4-directionally.
"""

from collections import deque


class Solution:
    def maxAreaOfIsland(self, grid) -> int:
        m = len(grid)
        n = len(grid[0])
        result = 0

        def bfs(q):
            c = 0
            while len(q) > 0:
                x, y = q.popleft()
                c += 1
                for xx, yy in [(1, 0), (-1, 0), (0, 1), (0, -1)]:
                    if (
                        0 <= x + xx < m
                        and 0 <= y + yy < n
                        and grid[x + xx][y + yy] == 1
                    ):
                        grid[x + xx][y + yy] = 0
                        q.append((x + xx, y + yy))
            return c

        q = deque()
        for i in range(m):
            for j in range(n):
                if grid[i][j] == 0:
                    continue
                q.clear()
                q.append((i, j))
                grid[i][j] = 0
                result = max(result, bfs(q))
        return result
