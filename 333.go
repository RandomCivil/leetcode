package leetcode

import (
	"math"

	"gitlab.com/RandomCivil/leetcode/common"
)

// Given the root of a binary tree, find the largest
// subtree
// , which is also a Binary Search Tree (BST), where the largest means subtree has the largest number of nodes.

// A Binary Search Tree (BST) is a tree in which all the nodes follow the below-mentioned properties:

// The left subtree values are less than the value of their parent (root) node's value.
// The right subtree values are greater than the value of their parent (root) node's value.
// Note: A subtree must include all of its descendants.

// Example 1:

// Input: root = [10,5,15,1,8,null,7]
// Output: 3
// Explanation: The Largest BST Subtree in this case is the highlighted one. The return value is the subtree's size, which is 3.
// Example 2:

// Input: root = [4,2,7,2,3,5,null,2,null,null,null,null,null,1]
// Output: 2

// Constraints:

// The number of nodes in the tree is in the range [0, 104].
// -104 <= Node.val <= 104
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func largestBSTSubtree(root *common.TreeNode) int {
	var result int
	var dfs func(cur *common.TreeNode) (bool, int, int, int)
	dfs = func(cur *common.TreeNode) (bool, int, int, int) {
		if cur == nil {
			return true, math.MaxInt, math.MinInt, 0
		}
		// min,max都返回是因为不知道cur是左子节点还是右子节点
		lvalid, lmin, lmax, lcount := dfs(cur.Left)
		rvalid, rmin, rmax, rcount := dfs(cur.Right)
		if lvalid && rvalid && lmax < cur.Val && cur.Val < rmin {
			count := lcount + rcount + 1
			result = common.Max(result, count)
			return true, common.Min(lmin, cur.Val), common.Max(rmax, cur.Val), count
		}
		return false, 0, 0, 0
	}
	dfs(root)
	return result
}
