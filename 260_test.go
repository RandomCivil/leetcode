package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestSingleNumber3(t *testing.T) {
	tests := []struct {
		nums, expect []int
	}{
		{
			nums:   []int{1, 2, 1, 3, 2, 5},
			expect: []int{3, 5},
		},
	}
	Convey("TestSingleNumber3", t, func() {
		for _, tt := range tests {
			So(singleNumber3(tt.nums), ShouldResemble, tt.expect)
		}
	})

}
