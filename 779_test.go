package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestKthGrammar(t *testing.T) {
	tests := []struct {
		n, k, expect int
	}{
		{
			n:      2,
			k:      1,
			expect: 0,
		},
		{
			n:      4,
			k:      4,
			expect: 0,
		},
		{
			n:      4,
			k:      3,
			expect: 1,
		},
		{
			n:      4,
			k:      5,
			expect: 1,
		},
		{
			n:      4,
			k:      6,
			expect: 0,
		},
		{
			n:      4,
			k:      7,
			expect: 0,
		},
		{
			n:      4,
			k:      8,
			expect: 1,
		},
	}
	Convey("TestKthGrammar", t, func() {
		for _, tt := range tests {
			So(kthGrammar(tt.n, tt.k), ShouldEqual, tt.expect)
		}
	})
}
