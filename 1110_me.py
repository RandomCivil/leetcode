# Given the root of a binary tree, each node in the tree has a distinct value.

# After deleting all nodes with a value in to_delete, we are left with a forest (a disjoint union of trees).

# Return the roots of the trees in the remaining forest. You may return the result in any order.

# Example 1:

# Input: root = [1,2,3,4,5,6,7], to_delete = [3,5]
# Output: [[1,2,null,4],[6],[7]]

# Example 2:

# Input: root = [1,2,4,null,3], to_delete = [3]
# Output: [[1,2,4]]

# Constraints:


# The number of nodes in the given tree is at most 1000.
# Each node has a distinct value between 1 and 1000.
# to_delete.length <= 1000
# to_delete contains distinct values between 1 and 1000.
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def delNodes(self, root, to_delete):
        self.result = []
        s = set(to_delete)
        if not root:
            return None
        if root.val not in s:
            self.result.append(root)

        def dfs(cur):
            if not cur:
                return None, False
            l, ld = dfs(cur.left)
            r, rd = dfs(cur.right)
            if ld:
                cur.left = None
            if rd:
                cur.right = None
            delete = False
            if cur.val in s:
                delete = True
                if l and l.val not in s:
                    self.result.append(l)
                if r and r.val not in s:
                    self.result.append(r)
            return cur, delete

        dfs(root)
        return self.result
