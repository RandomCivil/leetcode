# Given a non-empty array of non-negative integers nums, the degree of this array is defined as the maximum frequency of any one of its elements.
# Your task is to find the smallest possible length of a (contiguous) subarray of nums, that has the same degree as nums.

# Example 1:

# Input: nums = [1,2,2,3,1]
# Output: 2
# Explanation:
# The input array has a degree of 2 because both elements 1 and 2 appear twice.
# Of the subarrays that have the same degree:
# [1, 2, 2, 3, 1], [1, 2, 2, 3], [2, 2, 3, 1], [1, 2, 2], [2, 2, 3], [2, 2]
# The shortest length is 2. So return 2.

# Example 2:

# Input: nums = [1,2,2,3,1,4,2]
# Output: 6
# Explanation:
# The degree is 3 because the element 2 is repeated 3 times.
# So [2,2,3,1,4,2] is the shortest subarray, therefore returning 6.

# Constraints:

# nums.length will be between 1 and 50,000.
# nums[i] will be an integer between 0 and 49,999.
from collections import defaultdict


class Solution:
    def findShortestSubArray(self, nums: List[int]) -> int:
        d = defaultdict(list)
        for i, n in enumerate(nums):
            d[n].append(i)
            
        l = [v for k, v in sorted(d.items(), key=lambda x: -len(x[1]))]
        max_freq = len(l[0])
        print(l, max_freq)

        result = float("inf")
        for indexes in l:
            if len(indexes) == max_freq:
                result = min(result, indexes[-1] - indexes[0] + 1)
            else:
                break
        return result


if __name__ == "__main__":
    nums = [1, 2, 2, 3, 1]
    nums = [1, 2, 2, 3, 1, 4, 2]
    print(Solution().findShortestSubArray(nums))
