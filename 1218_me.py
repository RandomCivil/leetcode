# Given an integer array arr and an integer difference, return the length of the longest subsequence in arr which is an arithmetic sequence such that the difference between adjacent elements in the subsequence equals difference.

# A subsequence is a sequence that can be derived from arr by deleting some or no elements without changing the order of the remaining elements.

# Example 1:

# Input: arr = [1,2,3,4], difference = 1
# Output: 4
# Explanation: The longest arithmetic subsequence is [1,2,3,4].

# Example 2:

# Input: arr = [1,3,5,7], difference = 1
# Output: 1
# Explanation: The longest arithmetic subsequence is any single element.

# Example 3:

# Input: arr = [1,5,7,8,5,3,4,2,1], difference = -2
# Output: 4
# Explanation: The longest arithmetic subsequence is [7,5,3,1].


class Solution:
    def longestSubsequence(self, arr, difference: int) -> int:
        r = 0
        size = len(arr)
        dp = [0] * size
        for i in range(size):
            v = 0
            for j in range(i):
                if arr[i] - arr[j] == difference:
                    v = max(v, dp[j])

            dp[i] = v + 1
            r = max(r, dp[i])

        print(dp)
        return r


if __name__ == '__main__':
    arr = [1, 5, 7, 8, 5, 3, 4, 2, 1]
    difference = -2

    arr = [1, 2, 3, 4]
    difference = 1

    arr = [1, 3, 5, 7]
    difference = 1
    print(Solution().longestSubsequence(arr, difference))
