package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestMinEatingSpeed(t *testing.T) {
	tests := []struct {
		piles     []int
		expect, h int
	}{
		{
			piles:  []int{3, 6, 7, 11},
			h:      8,
			expect: 4,
		},
		{
			piles:  []int{30, 11, 23, 4, 20},
			h:      5,
			expect: 30,
		},
		{
			piles:  []int{30, 11, 23, 4, 20},
			h:      6,
			expect: 23,
		},
		{
			piles:  []int{1, 1, 1, 999999999},
			h:      10,
			expect: 142857143,
		},
	}
	Convey("TestMinEatingSpeed", t, func() {
		for _, tt := range tests {
			So(minEatingSpeed(tt.piles, tt.h), ShouldEqual, tt.expect)
		}
	})
}
