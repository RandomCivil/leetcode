# You are given an integer num. You can swap two digits at most once to get the maximum valued number.

# Return the maximum valued number you can get.

# Example 1:

# Input: num = 2736
# Output: 7236
# Explanation: Swap the number 2 and the number 7.

# Example 2:


# Input: num = 9973
# Output: 9973
# Explanation: No swap.
class Solution:
    def maximumSwap(self, num: int) -> int:
        arr = list(map(int, str(num)))
        d = {n: i for i, n in enumerate(arr)}
        stop = False
        for i, n in enumerate(arr):
            for digit in range(9, n, -1):
                pos = d.get(digit, -1)
                if pos > i:  # 后面还有这个数
                    arr[i], arr[pos] = arr[pos], arr[i]
                    stop = True
                    break
            if stop:
                break

        print(arr)
        return int("".join(map(str, arr)))
