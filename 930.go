package leetcode

import "fmt"

//In an array A of 0s and 1s, how many non-empty subarrays have sum S?
//Input: A = [1,0,1,0,1], S = 2
//Output: 4

//Note:

//A.length <= 30000
//0 <= S <= A.length
//A[i] is either 0 or 1.

//Let P[i] = A[0] + A[1] + ... + A[i-1]. Then P[j+1] - P[i] = A[i] + A[i+1] + ... + A[j], the sum of the subarray [i, j].
//Hence, we are looking for the number of i < j with P[j] - P[i] = S.
func NumSubarraysWithSum(A []int, S int) int {
	fmt.Println(A, S)
	size := len(A)
	p := make([]int, size+1)
	for i, n := range A {
		if n == 1 {
			p[i+1] = p[i] + 1
		} else {
			p[i+1] = p[i]
		}
	}
	fmt.Println(p)

	r := 0
	counter := make(map[int]int)
	for _, n := range p {
		r += counter[n]
		if _, ok := counter[n+S]; !ok {
			counter[n+S] = 1
		} else {
			counter[n+S]++
		}
	}
	return r
}
