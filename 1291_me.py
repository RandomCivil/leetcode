# An integer has sequential digits if and only if each digit in the number is one more than the previous digit.

# Return a sorted list of all the integers in the range [low, high] inclusive that have sequential digits.

# Example 1:

# Input: low = 100, high = 300
# Output: [123,234]

# Example 2:


# Input: low = 1000, high = 13000
# Output: [1234,2345,3456,4567,5678,6789,12345]
class Solution:
    def sequentialDigits(self, low: int, high: int):
        self.result = []
        max_size = len(str(high))

        def dfs(arr):
            if len(arr) > max_size:
                return
            if len(arr) > 0:
                v = int(''.join(map(str, arr)))
                if low <= v <= high and arr[0] != 0:
                    self.result.append(v)

            if len(arr) > 0:
                if 0 < arr[-1] + 1 < 10:
                    arr.append(arr[-1] + 1)
                    dfs(arr)
                    arr.pop()
            else:
                for i in range(10):
                    dfs([i])

        dfs([])
        return sorted(self.result)


if __name__ == '__main__':
    # [123,234]
    low = 100
    high = 300

    # [1234,2345,3456,4567,5678,6789,12345]
    low = 1000
    high = 13000

    # low = 58
    # high = 155
    print(Solution().sequentialDigits(low, high))
