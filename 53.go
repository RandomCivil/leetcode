package leetcode

//Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.

//Example 1:

//Input: nums = [-2,1,-3,4,-1,2,1,-5,4]
//Output: 6
//Explanation: [4,-1,2,1] has the largest sum = 6.

//Example 2:

//Input: nums = [1]
//Output: 1

//Example 3:

//Input: nums = [0]
//Output: 0

//Example 4:

//Input: nums = [-1]
//Output: -1

//Example 5:

//Input: nums = [-100000]
//Output: -100000

//Constraints:

//1 <= nums.length <= 3 * 104
//-105 <= nums[i] <= 105

func maxSubArray(nums []int) int {
	r := nums[0]
	size := len(nums)
	dp := make([]int, size)
	dp[0] = nums[0]
	for i, n := range nums {
		if i == 0 {
			continue
		}
		if dp[i-1] < 0 {
			dp[i] = n
		} else {
			dp[i] = dp[i-1] + n
		}
		if dp[i] > r {
			r = dp[i]
		}
	}
	return r
}
