package leetcode

import (
	"fmt"

	"gitlab.com/RandomCivil/leetcode/common"
)

// Given an m x n binary matrix filled with 0's and 1's, find the largest square containing only 1's and return its area.

// Example 1:

// Input: matrix = [["1","0","1","0","0"],["1","0","1","1","1"],["1","1","1","1","1"],["1","0","0","1","0"]]
// Output: 4
// Example 2:

// Input: matrix = [["0","1"],["1","0"]]
// Output: 1
// Example 3:

// Input: matrix = [["0"]]
// Output: 0

// Constraints:

// m == matrix.length
// n == matrix[i].length
// 1 <= m, n <= 300
// matrix[i][j] is '0' or '1'.
func maximalSquare(matrix [][]byte) int {
	m, n := len(matrix), len(matrix[0])
	dp := make([][]int, m+1)
	for i := 0; i < m+1; i++ {
		dp[i] = make([]int, n+1)
	}

	var result int
	for i := 1; i < m+1; i++ {
		for j := 1; j < n+1; j++ {
			if matrix[i-1][j-1] == '0' {
				continue
			}
			dp[i][j] = common.Min(dp[i-1][j], dp[i][j-1], dp[i-1][j-1]) + 1
			if dp[i][j]*dp[i][j] > result {
				result = dp[i][j] * dp[i][j]
			}
		}
	}
	fmt.Println(dp)
	return result
}
