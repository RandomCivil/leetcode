# Given a binary search tree with non-negative values, find the minimum absolute difference between values of any two nodes.
# Example:
"""
Input:

   1
    \
     3
    /
   2

Output:
1
"""

# Explanation:
# The minimum absolute difference is 1, which is the difference between 2 and 1 (or between 2 and 3).

# Note:

# There are at least two nodes in this BST.
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right


class Solution:
    def getMinimumDifference(self, root) -> int:
        self.r = float('inf')

        self.arr = []

        def dfs(head):
            if not head:
                return
            self.arr.append(head.val)
            dfs(head.left)
            dfs(head.right)

        dfs(root)
        size = len(self.arr)
        for i in range(1, size):
            self.r = min(self.r, abs(self.arr[i] - self.arr[i - 1]))
        return self.r
