package leetcode

// Given an m x n matrix, return all elements of the matrix in spiral order.

// Example 1:

// Input: matrix = [[1,2,3],[4,5,6],[7,8,9]]
// Output: [1,2,3,6,9,8,7,4,5]
// Example 2:

// Input: matrix = [[1,2,3,4],[5,6,7,8],[9,10,11,12]]
// Output: [1,2,3,4,8,12,11,10,9,5,6,7]

// Constraints:

// m == matrix.length
// n == matrix[i].length
// 1 <= m, n <= 10
// -100 <= matrix[i][j] <= 100
func spiralOrder(matrix [][]int) []int {
	var result []int
	m, n := len(matrix), len(matrix[0])
	left, right := 0, n-1
	up, down := 0, m-1
	for len(result) < m*n {
		for i := left; i <= right; i++ {
			result = append(result, matrix[up][i])
		}
		for i := up + 1; i <= down; i++ {
			result = append(result, matrix[i][right])
		}
		if up != down {
			for i := right - 1; i >= left; i-- {
				result = append(result, matrix[down][i])
			}
		}
		if left != right {
			for i := down - 1; i > up; i-- {
				result = append(result, matrix[i][left])
			}
		}
		left++
		right--
		up++
		down--
	}
	return result
}
