package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestMaxProduct(t *testing.T) {
	tests := []struct {
		words  []string
		expect int
	}{
		{
			words:  []string{"abcw", "baz", "foo", "bar", "xtfn", "abcdef"},
			expect: 16,
		},
		{
			words:  []string{"a", "ab", "abc", "d", "cd", "bcd", "abcd"},
			expect: 4,
		},
		{
			words:  []string{"a", "aa", "aaa", "aaaa"},
			expect: 0,
		},
	}
	Convey("TestMaxProduct", t, func() {
		for _, tt := range tests {
			So(maxProduct(tt.words), ShouldEqual, tt.expect)
		}
	})
}
