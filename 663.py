# Given the root of a binary tree, return true if you can partition the tree into two trees with equal sums of values after removing exactly one edge on the original tree.

# Example 1:


# Input: root = [5,10,10,null,null,2,3]
# Output: true
# Example 2:


# Input: root = [1,2,10,null,null,2,20]
# Output: false
# Explanation: You cannot split the tree into two trees with equal sums after removing exactly one edge on the tree.


# Constraints:


# The number of nodes in the tree is in the range [1, 104].
# -105 <= Node.val <= 105
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def checkEqualTree(self, root: Optional[TreeNode]) -> bool:
        self.d = defaultdict(list)
        self.sum = 0
        self.total = 0

        def dfs(cur):
            if not cur:
                return 0, 0
            self.sum += cur.val
            self.total += 1
            # 叶子节点
            if not cur.left and not cur.right:
                self.d[cur.val].append(1)

            l, lused = dfs(cur.left)
            r, rused = dfs(cur.right)

            self.d[cur.val + l].append(lused + 1)
            self.d[cur.val + r].append(rused + 1)
            self.d[cur.val + l + r].append(lused + rused + 1)
            return l + r + cur.val, lused + rused + 1

        dfs(root)
        print(self.d)
        print(self.sum)
        print(self.total)

        if self.sum % 2 != 0:
            return False
        if len(self.d[int(self.sum / 2)]) < 2:
            return False
        for n in self.d[int(self.sum / 2)]:
            if n < self.total:
                break
        else:
            return False
        return True
