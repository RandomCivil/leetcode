package leetcode

//We are given the head node root of a binary tree, where additionally every node's value is either a 0 or a 1.
//Return the same tree where every subtree (of the given tree) not containing a 1 has been removed.
//(Recall that the subtree of a node X is X, plus every node that is a descendant of X.)

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func pruneTree(root *TreeNode) *TreeNode {
	var walk func(node *TreeNode) bool
	walk = func(node *TreeNode) bool {
		if node == nil {
			return false
		}
		//返回是否存在1的subtree
		r1 := walk(node.Left)
		r2 := walk(node.Right)
		if !r1 {
			node.Left = nil
		}
		if !r2 {
			node.Right = nil
		}
		return r1 || r2 || node.Val == 1
	}
	r := root
	if !walk(root) {
		r = nil
	}
	return r
}
