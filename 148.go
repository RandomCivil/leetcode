package leetcode

import "gitlab.com/RandomCivil/leetcode/common"

// Given the head of a linked list, return the list after sorting it in ascending order.

// Example 1:

// Input: head = [4,2,1,3]
// Output: [1,2,3,4]
// Example 2:

// Input: head = [-1,5,3,4,0]
// Output: [-1,0,3,4,5]
// Example 3:

// Input: head = []
// Output: []

// Constraints:

// The number of nodes in the list is in the range [0, 5 * 104].
// -105 <= Node.val <= 105

// Follow up: Can you sort the linked list in O(n logn) time and O(1) memory (i.e. constant space)?
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func sortList(head *common.ListNode) *common.ListNode {
	findMiddle := func(head *common.ListNode) (*common.ListNode, *common.ListNode) {
		slow, fast := head, head
		var prev *common.ListNode
		for fast != nil && fast.Next != nil {
			prev = slow
			slow = slow.Next
			fast = fast.Next.Next
		}
		return prev, slow
	}
	var merge func(head *common.ListNode) *common.ListNode
	merge = func(head *common.ListNode) *common.ListNode {
		if head == nil || head.Next == nil {
			return head
		}

		mid, midNext := findMiddle(head)
		mid.Next = nil
		cur1, cur2 := merge(head), merge(midNext)
		preRoot := &common.ListNode{}
		cur := preRoot
		for cur1 != nil && cur2 != nil {
			if cur1.Val <= cur2.Val {
				cur.Next = &common.ListNode{Val: cur1.Val}
				cur = cur.Next
				cur1 = cur1.Next
			} else {
				cur.Next = &common.ListNode{Val: cur2.Val}
				cur = cur.Next
				cur2 = cur2.Next
			}
		}
		for cur1 != nil {
			cur.Next = &common.ListNode{Val: cur1.Val}
			cur = cur.Next
			cur1 = cur1.Next
		}
		for cur2 != nil {
			cur.Next = &common.ListNode{Val: cur2.Val}
			cur = cur.Next
			cur2 = cur2.Next
		}
		return preRoot.Next
	}
	return merge(head)
}
