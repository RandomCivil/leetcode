package leetcode

import "fmt"

// Given an integer array nums and an integer k, return the kth largest element in the array.

// Note that it is the kth largest element in the sorted order, not the kth distinct element.

// You must solve it in O(n) time complexity.

// Example 1:

// Input: nums = [3,2,1,5,6,4], k = 2
// Output: 5
// Example 2:

// Input: nums = [3,2,3,1,2,4,5,5,6], k = 4
// Output: 4

// Constraints:

// 1 <= k <= nums.length <= 105
// -104 <= nums[i] <= 104
func findKthLargest(nums []int, k int) int {
	size := len(nums)
	var quickSelect func(a []int, lo, hi int) int
	quickSelect = func(a []int, lo, hi int) int {
		if lo >= hi {
			return a[lo]
		}
		p := partition(a, lo, hi)
		fmt.Println(a, p)
		if p+1 == size-k+1 {
			return a[p]
		} else if p+1 > size-k+1 {
			return quickSelect(a, lo, p-1)
		} else {
			return quickSelect(a, p+1, hi)
		}
	}
	return quickSelect(nums, 0, size-1)
}

func partition(a []int, lo, hi int) int {
	pivot := a[hi]
	i := lo - 1
	for j := lo; j < hi; j++ {
		if a[j] < pivot {
			i++
			a[j], a[i] = a[i], a[j]
		}
	}
	a[i+1], a[hi] = a[hi], a[i+1]
	return i + 1
}
