package leetcode

import (
	"fmt"

	"gitlab.com/RandomCivil/leetcode/common"
)

//Given a binary array nums, return the maximum length of a contiguous subarray with an equal number of 0 and 1.

//Example 1:

//Input: nums = [0,1]
//Output: 2
//Explanation: [0, 1] is the longest contiguous subarray with an equal number of 0 and 1.

//Example 2:

//Input: nums = [0,1,0]
//Output: 2
//Explanation: [0, 1] (or [1, 0]) is a longest contiguous subarray with equal number of 0 and 1.
func FindMaxLength(nums []int) int {
	var (
		v, r int
		m    = make(map[int]int)
	)
	m[0] = -1
	for i, n := range nums {
		if n == 0 {
			v++
		} else {
			v--
		}
		if _, ok := m[v]; ok {
			fmt.Println(i, m[v])
			r = common.Max(r, i-m[v])
		} else {
			m[v] = i
		}
		//m[v] = i
	}
	fmt.Println(m)
	return r
}
