package leetcode

import "fmt"

// You have a graph of n nodes labeled from 0 to n - 1. You are given an integer n and a list of edges where edges[i] = [ai, bi] indicates that there is an undirected edge between nodes ai and bi in the graph.

// Return true if the edges of the given graph make up a valid tree, and false otherwise.

// Example 1:

// Input: n = 5, edges = [[0,1],[0,2],[0,3],[1,4]]
// Output: true
// Example 2:

// Input: n = 5, edges = [[0,1],[1,2],[2,3],[1,3],[1,4]]
// Output: false

// Constraints:

// 1 <= n <= 2000
// 0 <= edges.length <= 5000
// edges[i].length == 2
// 0 <= ai, bi < n
// ai != bi
// There are no self-loops or repeated edges.
func validTree(n int, edges [][]int) bool {
	adj := make(map[int][]int)
	for _, edge := range edges {
		p, c := edge[0], edge[1]
		if _, ok := adj[p]; !ok {
			adj[p] = make([]int, 0)
		}
		if _, ok := adj[c]; !ok {
			adj[c] = make([]int, 0)
		}
		adj[p] = append(adj[p], c)
		adj[c] = append(adj[c], p)
	}
	fmt.Println(adj)

	visited := map[int]struct{}{
		0: struct{}{},
	}
	var dfs func(cur, parent int) bool
	dfs = func(cur, parent int) bool {
		for _, next := range adj[cur] {
			if next == parent {
				continue
			}
			if _, ok := visited[next]; ok {
				return false
			}
			visited[next] = struct{}{}
			if !dfs(next, cur) {
				return false
			}
		}
		return true
	}
	r := dfs(0, -1)
	fmt.Println(visited)
	if len(visited) != n {
		return false
	}
	return r
}
