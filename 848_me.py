# We have a string S of lowercase letters, and an integer array shifts.
# Call the shift of a letter, the next letter in the alphabet, (wrapping around so that 'z' becomes 'a').
# For example, shift('a') = 'b', shift('t') = 'u', and shift('z') = 'a'.
# Now for each shifts[i] = x, we want to shift the first i+1 letters of S, x times.
# Return the final string after all such shifts to S are applied.

# Example 1:

# Input: S = "abc", shifts = [3,5,9]
# Output: "rpl"
# Explanation:
# We start with "abc".
# After shifting the first 1 letters of S by 3, we have "dbc".
# After shifting the first 2 letters of S by 5, we have "igc".
# After shifting the first 3 letters of S by 9, we have "rpl", the answer.
# Note:

# 1 <= S.length = shifts.length <= 20000
# 0 <= shifts[i] <= 10 ^ 9
import itertools


class Solution:
    def shiftingLetters(self, s: str, shifts) -> str:
        print(s, shifts)
        r = ''
        times = list(itertools.accumulate(reversed(shifts)))
        times = list(reversed(times))
        print(list(times))
        for ch, t in zip(s, list(times)):
            v = ord(ch) + t % 26
            if v > 122:
                v = 97 + v % 123
            r += chr(v)
        return r


if __name__ == '__main__':
    s = 'abc'
    shifts = [3, 5, 9]

    s = "bad"
    shifts = [10, 20, 30]
    print(Solution().shiftingLetters(s, shifts))
