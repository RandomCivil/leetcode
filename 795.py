# We are given an array A of positive integers, and two positive integers L and R (L <= R).

# Return the number of (contiguous, non-empty) subarrays such that the value of the maximum array element in that subarray is at least L and at most R.

# Example :
# Input:
# A = [2, 1, 4, 3]
# L = 2
# R = 3
# Output: 3
# Explanation: There are three subarrays that meet the requirements: [2], [2, 1], [3].


class Solution:
    def numSubarrayBoundedMax(self, nums: list[int], left: int, right: int) -> int:
        mx = -1
        result = 0
        l = 0
        size = len(nums)
        dp = [0] * (size + 1)
        last = -1
        for i, n in enumerate(nums):
            if n > right:
                dp[i + 1] = 0
                last = i
            elif n >= left:
                dp[i + 1] = i - last
            else:
                dp[i + 1] = dp[i]
            result += dp[i + 1]
        print(dp)
        return result


if __name__ == "__main__":
    A = [2, 1, 4, 3]
    A = [2, 1, 4, 3, 5, 2, 3, 3]
    L = 2
    R = 3
    # L = 3
    # R = 4

    A = [7, 3, 6, 7, 1]
    L = 1
    R = 4
    print(Solution().numSubarrayBoundedMax(A, L, R))
