# Given an integer array arr, return the length of a maximum size turbulent subarray of arr.

# A subarray is turbulent if the comparison sign flips between each adjacent pair of elements in the subarray.

# More formally, a subarray [arr[i], arr[i + 1], ..., arr[j]] of arr is said to be turbulent if and only if:

# For i <= k < j:
# arr[k] > arr[k + 1] when k is odd, and
# arr[k] < arr[k + 1] when k is even.
# Or, for i <= k < j:
# arr[k] > arr[k + 1] when k is even, and
# arr[k] < arr[k + 1] when k is odd.

# Example 1:

# Input: arr = [9,4,2,10,7,8,8,1,9]
# Output: 5
# Explanation: arr[1] > arr[2] < arr[3] > arr[4] < arr[5]

# Example 2:

# Input: arr = [4,8,12,16]
# Output: 2

# Example 3:


# Input: arr = [100]
# Output: 1
class Solution:
    def maxTurbulenceSize(self, arr: list[int]) -> int:
        print(arr)
        size = len(arr)
        if size == 1:
            return 1

        diffs = []
        for i in range(1, size):
            diffs.append(arr[i] - arr[i - 1])
        print(diffs)

        size = len(diffs)
        dp = [0] * size
        dp[0] = 1
        result = 0
        for i in range(size):
            if diffs[i] == 0:
                dp[i] = 0
            elif (
                i > 0
                and (diffs[i] > 0 and diffs[i - 1] < 0)
                or (diffs[i] < 0 and diffs[i - 1] > 0)
            ):
                dp[i] = dp[i - 1] + 1
            else:
                dp[i] = 1
            result = max(result, dp[i])
        print(dp, result)
        return result + 1
