#! /usr/bin/env python3

# Given a string s and a non-empty string p, find all the start indices of p's anagrams in s.
# Strings consists of lowercase English letters only and the length of both strings s and p will not be larger than 20,100.
# The order of output does not matter.
"""
Input:
s: "cbaebabacd" p: "abc"

Output:
[0, 6]

Explanation:
The substring with start index = 0 is "cba", which is an anagram of "abc".
The substring with start index = 6 is "bac", which is an anagram of "abc".
"""

from collections import Counter, defaultdict


class Solution:
    def findAnagrams(self, s: str, p: str):
        result = []
        cp = Counter(p)
        size = len(s)
        i = len(p) - 1
        cs = Counter(s[0:i])
        while i < size:
            cs[s[i]] += 1
            if cs == cp:
                result.append(i - len(p) + 1)
            cs[s[i - len(p) + 1]] -= 1
            i += 1

        return result


if __name__ == "__main__":
    s, p = "cbaebabacd", "abc"
    s, p = "abab", "ab"
    print(Solution().findAnagrams(s, p))
