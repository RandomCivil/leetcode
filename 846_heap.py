# Alice has a hand of cards, given as an array of integers.
# Now she wants to rearrange the cards into groups so that each group is size W, and consists of W consecutive cards.
# Return true if and only if she can.

# Example 1:

# Input: hand = [1,2,3,6,2,3,4,7,8], W = 3
# Output: true
# Explanation: Alice's hand can be rearranged as [1,2,3],[2,3,4],[6,7,8]

# Example 2:

# Input: hand = [1,2,3,4,5], W = 4
# Output: false
# Explanation: Alice's hand can't be rearranged into groups of 4.

# Constraints:

# 1 <= hand.length <= 10000
# 0 <= hand[i] <= 10^9
# 1 <= W <= hand.length

from collections import Counter
import heapq


class Solution:
    def isNStraightHand(self, hand, groupSize: int) -> bool:
        size = len(hand)
        if size % groupSize != 0:
            return False

        div = size / groupSize
        result = [[] for _ in range(int(div))]
        c = Counter(hand)
        h = list(c.items())
        heapq.heapify(h)
        start = 0
        while len(h) > 0:
            k, v = heapq.heappop(h)
            for i in range(start, int(div)):
                if len(result[i]) == groupSize:
                    start = i
                    continue
                if len(result[i]) == 0 or result[i][-1] + 1 == k:
                    result[i].append(k)
                    v -= 1
                    break
            else:
                return False
            if v > 0:
                heapq.heappush(h, (k, v))

        for r in result:
            if len(r) != groupSize:
                return False
        return True
