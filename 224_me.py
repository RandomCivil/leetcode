# Given a string s representing a valid expression, implement a basic calculator to evaluate it, and return the result of the evaluation.

# Note: You are not allowed to use any built-in function which evaluates strings as mathematical expressions, such as eval().


# Example 1:

# Input: s = "1 + 1"
# Output: 2
# Example 2:

# Input: s = " 2-1 + 2 "
# Output: 3
# Example 3:

# Input: s = "(1+(4+5+2)-3)+(6+8)"
# Output: 23


# Constraints:


# 1 <= s.length <= 3 * 105
# s consists of digits, '+', '-', '(', ')', and ' '.
# s represents a valid expression.
# '+' is not used as a unary operation (i.e., "+1" and "+(2 + 3)" is invalid).
# '-' could be used as a unary operation (i.e., "-1" and "-(2 + 3)" is valid).
# There will be no two consecutive operators in the input.
# Every number and running calculation will fit in a signed 32-bit integer.
class Solution:
    def calculate(self, s: str) -> int:
        print(s)
        s = s.replace(" ", "")

        if s[0] != "-" and s[0] != "(":
            s = "(" + s + ")"
        size = len(s)

        def get_num(i):
            if i < size and s[i] == "-":
                i += 1
            while i < size and s[i].isdigit():
                i += 1
            return i

        i = 0
        stack = [["", 0]]
        while i < size:
            if s[i] == "(":
                j = get_num(i + 1)
                if s[i + 1 : j] == "-":
                    stack.append([s[i], 0])
                    stack.append(["-", 0])
                else:
                    stack.append([s[i], int(s[i + 1 : j])])
                i = j
            elif s[i] == ")":
                _sum = 0
                while stack[-1][0] != "(":
                    sign, n = stack.pop()
                    if sign == "-":
                        _sum -= n
                    else:
                        _sum += n

                _, n = stack.pop()
                stack[-1][1] += n + _sum
                i += 1
            else:  # +,-
                if i + 1 < size and s[i + 1].isdigit():
                    j = get_num(i + 1)
                    stack.append([s[i], int(s[i + 1 : j])])
                    i = j
                else:  # (
                    stack.append([s[i], 0])
                    i += 1

        print(stack)
        result = stack[0][1]
        for sign, n in stack[1:]:
            if sign == "-":
                result -= n
            else:
                result += n
        return result
