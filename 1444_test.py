inst_1444 = __import__("1444")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "pizza,k,expected",
        [
            (["A..", "AAA", "..."], 3, 3),
        ],
    )
    def test_ways(self, pizza, k, expected):
        assert inst_1444.Solution().ways(pizza, k) == expected
