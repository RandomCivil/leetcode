package leetcode

import (
	"fmt"
	"math"
)

//Given a non-negative integer, you could swap two digits at most once to get the maximum valued number. Return the maximum valued number you could get.
/*
Input: 2736
Output: 7236
Explanation: Swap the number 2 and the number 7.

Input: 9973
Output: 9973
Explanation: No swap.
*/
func maximumSwap(num int) int {
	if num == 0 {
		return 0
	}
	var arr []int
	n := num
	for n >= 1 {
		arr = append(arr, n%10)
		n = n / 10
	}
	fmt.Println(arr)
	max := math.MinInt32
	maxIndex := -1
	for i := len(arr) - 1; i >= 0; i-- {
		if arr[i] > max {
			max = arr[i]
			maxIndex = i
		}
	}
	fmt.Println(max, maxIndex)
	arr[len(arr)-1], arr[maxIndex] = arr[maxIndex], arr[len(arr)-1]
	fmt.Println(arr)

	n = 0
	for i := len(arr) - 1; i >= 0; i-- {
		n += arr[i] * int(math.Pow10(i))
	}
	return n
}
