# Given an m x n integers matrix, return the length of the longest increasing path in matrix.

# From each cell, you can either move in four directions: left, right, up, or down. You may not move diagonally or move outside the boundary (i.e., wrap-around is not allowed).


# Example 1:


# Input: matrix = [[9,9,4],[6,6,8],[2,1,1]]
# Output: 4
# Explanation: The longest increasing path is [1, 2, 6, 9].
# Example 2:


# Input: matrix = [[3,4,5],[3,2,6],[2,2,1]]
# Output: 4
# Explanation: The longest increasing path is [3, 4, 5, 6]. Moving diagonally is not allowed.
# Example 3:

# Input: matrix = [[1]]
# Output: 1


# Constraints:


# m == matrix.length
# n == matrix[i].length
# 1 <= m, n <= 200
# 0 <= matrix[i][j] <= 231 - 1
from collections import defaultdict


class Solution:
    def longestIncreasingPath(self, matrix: list[list[int]]) -> int:
        print(matrix)
        m, n = len(matrix), len(matrix[0])
        self.mem = defaultdict(lambda: 0)
        directions = [(1, 0), (-1, 0), (0, 1), (0, -1)]
        self.result = 0

        def dfs(x, y, prev):
            if x < 0 or x == m or y < 0 or y == n:
                return 0
            if matrix[x][y] <= prev:
                return 0
            if (x,y) in self.mem:
                return self.mem[(x, y)]
            for xx, yy in directions:
                self.mem[(x, y)] = max(
                    self.mem[(x, y)], dfs(x + xx, y + yy, matrix[x][y]) + 1
                )
            self.result = max(self.result, self.mem[(x, y)])
            return self.mem[(x, y)]

        for i in range(m):
            for j in range(n):
                dfs(i, j, float("-inf"))
        return self.result
