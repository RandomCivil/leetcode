inst_34 = __import__("34")

import pytest, common


class TestClass:
    @pytest.mark.parametrize(
        "nums,target,expected",
        [
            ([5, 7, 7, 8, 8, 10], 8, [3, 4]),
            ([5, 7, 7, 8, 8, 10], 6, [-1, -1]),
            ([1], 1, [0, 0]),
        ],
    )
    def test_searchRange(self, nums, target, expected):
        assert inst_34.Solution().searchRange(nums, target) == expected
