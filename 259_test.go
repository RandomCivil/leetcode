package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestThreeSumSmaller(t *testing.T) {
	tests := []struct {
		nums           []int
		expect, target int
	}{
		{
			nums:   []int{-2, 0, 1, 3},
			target: 2,
			expect: 2,
		},
		{
			nums:   []int{-1, 2, 1, -4},
			target: 2,
			expect: 3,
		},
	}
	Convey("TestThreeSumSmaller", t, func() {
		for _, tt := range tests {
			So(threeSumSmaller(tt.nums, tt.target), ShouldEqual, tt.expect)
		}
	})
}
