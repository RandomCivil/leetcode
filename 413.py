# An integer array is called arithmetic if it consists of at least three elements and if the difference between any two consecutive elements is the same.

# For example, [1,3,5,7,9], [7,7,7,7], and [3,-1,-5,-9] are arithmetic sequences.
# Given an integer array nums, return the number of arithmetic subarrays of nums.

# A subarray is a contiguous subsequence of the array.

# Example 1:

# Input: nums = [1,2,3,4]
# Output: 3
# Explanation: We have 3 arithmetic slices in nums: [1, 2, 3], [2, 3, 4] and [1,2,3,4] itself.
# Example 2:

# Input: nums = [1]
# Output: 0


# Constraints:


# 1 <= nums.length <= 5000
# -1000 <= nums[i] <= 1000
class Solution:
    def numberOfArithmeticSlices(self, nums) -> int:
        result = 0
        size = len(nums)
        if size < 3:
            return 0

        dp = [1] * size
        dp[1] = 2
        prev = nums[1] - nums[0]
        for i in range(1, size - 1):
            diff = nums[i + 1] - nums[i]
            if diff == prev:
                dp[i + 1] = dp[i] + 1
                result += max(0, dp[i + 1] - 2)
            else:
                dp[i + 1] = 2
            prev = diff

        print(dp)
        return result


if __name__ == "__main__":
    # 3
    nums = [1, 2, 3, 4]

    # 6
    nums = [0, 1, 3, 5, 7, 9]

    # 4
    nums = [0, 1, 3, 5, 7, 10, 14, 18]
    print(Solution().numberOfArithmeticSlices(nums))
