package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestLongestConsecutive(t *testing.T) {
	tests := []struct {
		nums   []int
		expect int
	}{
		{
			nums:   []int{100, 4, 200, 1, 3, 2},
			expect: 4,
		},
		{
			nums:   []int{0, 3, 7, 2, 5, 8, 4, 6, 0, 1},
			expect: 9,
		},
		{
			nums:   []int{1, 2, 0, 1},
			expect: 3,
		},
		{
			nums:   []int{0, 1, 2, 3, 6, 7, 9, 10, 8},
			expect: 5,
		},
	}
	Convey("TestLongestConsecutive", t, func() {
		for _, tt := range tests {
			So(longestConsecutive(tt.nums), ShouldEqual, tt.expect)
			So(longestConsecutiveWithMap(tt.nums), ShouldEqual, tt.expect)
		}
	})
}
