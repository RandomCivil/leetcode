# Given the head of a linked list, rotate the list to the right by k places.


# Example 1:


# Input: head = [1,2,3,4,5], k = 2
# Output: [4,5,1,2,3]
# Example 2:


# Input: head = [0,1,2], k = 4
# Output: [2,0,1]


# Constraints:


# The number of nodes in the list is in the range [0, 500].
# -100 <= Node.val <= 100
# 0 <= k <= 2 * 109
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def rotateRight(self, head, k: int):
        if not head or k==0:
            return head

        n = 0
        cur = head
        while cur:
            n += 1
            cur = cur.next

        k = k % n
        if k==0:
            return head
            
        i = 0
        start, end = head, head
        while end and i < k:
            end = end.next
            i += 1

        while end.next:
            start = start.next
            end = end.next

        t = start.next
        start.next = None
        end.next = head
        return t
