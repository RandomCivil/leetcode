package leetcode

import "gitlab.com/RandomCivil/leetcode/common"

// Given a non-negative integer represented as a linked list of digits, plus one to the integer.

// The digits are stored such that the most significant digit is at the head of the list.

// Example 1:

// Input: head = [1,2,3]
// Output: [1,2,4]
// Example 2:

// Input: head = [0]
// Output: [1]

// Constraints:

// The number of nodes in the linked list is in the range [1, 100].
// 0 <= Node.val <= 9
// The number represented by the linked list does not contain leading zeros except for the zero itself.
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func plusOne(head *common.ListNode) *common.ListNode {
	cur := head
	cur = reverseList(head)
	cur.Val++
	var next int
	var prev *common.ListNode
	for cur != nil {
		val := cur.Val + next
		cur.Val = val % 10
		next = val / 10

		t := cur.Next
		cur.Next = prev
		prev = cur
		cur = t
	}
	if next > 0 {
		node := &common.ListNode{Val: next}
		node.Next = prev
		return node
	}
	return prev
}
