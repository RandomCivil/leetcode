inst_456 = __import__("456")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "nums,expected",
        [
            ([3, 1, 4, 2], True),
            ([-1, 3, 2, 0], True),
            ([1, 2, 3, 4], False),
            ([1, 0, 1, -4, -3], False),
        ],
    )
    def test_find132pattern(self, nums, expected):
        assert inst_456.Solution().find132pattern(nums) == expected
