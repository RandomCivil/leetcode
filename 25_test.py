inst_25 = __import__("25")

import pytest, common


class TestClass:
    @pytest.mark.parametrize(
        "nums,k,expected",
        [
            ([1, 2, 3, 4], 2, [2, 1, 4, 3]),
            ([1, 2, 3, 4, 5], 2, [2, 1, 4, 3, 5]),
            ([1, 2, 3, 4, 5], 3, [3, 2, 1, 4, 5]),
        ],
    )
    def test_reverseKGroup(self, nums, k, expected):
        l = common.LinkList(nums)
        assert l.traversal(inst_25.Solution().reverseKGroup(l.build(), k)) == expected
