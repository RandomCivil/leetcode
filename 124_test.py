inst_124 = __import__("124")

import pytest
import common


class TestClass:
    @pytest.mark.parametrize(
        "nums,expected",
        [
            ([-10, 9, 20, 0, 0, 15, 7], 42),
            ([10, -9, 20, 0, 0, 15, 7], 45),
            ([1, 2, 3], 6),
            ([-3], -3),
            ([2, -1, -2], 2),
            ([5, 4, 8, 11, 0, 13, 4, 7, 2, 0, 0, 0, 1], 48),
        ],
    )
    def test_largestRectangleArea(self, nums, expected):
        root = common.build_tree(nums)
        assert inst_124.Solution().maxPathSum(root) == expected
