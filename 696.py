# Give a string s, count the number of non-empty (contiguous) substrings that have the same number of 0's and 1's, and all the 0's and all the 1's in these substrings are grouped consecutively.
# Substrings that occur multiple times are counted the number of times they occur.

# Example 1:
# Input: "00110011"
# Output: 6
# Explanation: There are 6 substrings that have equal number of consecutive 1's and 0's: "0011", "01", "1100", "10", "0011", and "01".

# Notice that some of these substrings repeat and are counted the number of times they occur.

# Also, "00110011" is not a valid substring because all the 0's (and 1's) are not grouped together.

# Example 2:
# Input: "10101"
# Output: 4
# Explanation: There are 4 substrings: "10", "01", "10", "01" that have equal number of consecutive 1's and 0's.
# Note:

# s.length will be between 1 and 50,000.
# s will only consist of "0" or "1" characters.
from collections import defaultdict


class Solution:
    def countBinarySubstrings(self, s: str) -> int:
        r = 0
        arr = []
        target = s[0]
        size = len(s)
        n = 1
        for i in range(1, size):
            if s[i] == target:
                n += 1
            else:
                arr.append(n)
                target = s[i]
                n = 1

        arr.append(n)
        print(arr, n)

        size = len(arr)
        for i in range(1, size):
            r += min(arr[i], arr[i - 1])
        return r


if __name__ == '__main__':
    s = "00110011"
    # s = "10101"
    # s = "110001111000000"
    print(Solution().countBinarySubstrings(s))
