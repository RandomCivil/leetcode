package leetcode

//A sequence of number is called arithmetic if it consists of at least three elements and if the difference between any two consecutive elements is the same.

//For example, these are arithmetic sequence:
/*
1, 3, 5, 7, 9
7, 7, 7, 7
3, -1, -5, -9
*/

//The function should return the number of arithmetic slices in the array A.

/*
A = [1, 2, 3, 4]
return: 3, for 3 arithmetic slices in A: [1, 2, 3], [2, 3, 4] and [1, 2, 3, 4] itself.

1-5 6
1-6 10
*/

func NumberOfArithmeticSlices1(nums []int) int {
	size := len(nums)
	if size < 3 {
		return 0
	}

	var diff, prev int
	sum := 0
	for i := 1; i < size; i++ {
		diff = nums[i] - nums[i-1]
		prev = i
		for j := i + 1; j < size; j++ {
			if nums[j]-nums[prev] != diff {
				break
			}
			sum++
			prev = j
		}
	}
	return sum
}
