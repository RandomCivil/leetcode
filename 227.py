# Given a string s which represents an expression, evaluate this expression and return its value.

# The integer division should truncate toward zero.

# You may assume that the given expression is always valid. All intermediate results will be in the range of [-231, 231 - 1].

# Note: You are not allowed to use any built-in function which evaluates strings as mathematical expressions, such as eval().

# Example 1:

# Input: s = "3+2*2"
# Output: 7
# Example 2:

# Input: s = " 3/2 "
# Output: 1
# Example 3:

# Input: s = " 3+5 / 2 "
# Output: 5


# Constraints:


# 1 <= s.length <= 3 * 105
# s consists of integers and operators ('+', '-', '*', '/') separated by some number of spaces.
# s represents a valid expression.
# All the integers in the expression are non-negative integers in the range [0, 231 - 1].
# The answer is guaranteed to fit in a 32-bit integer.
class Solution:
    def calculate(self, s: str) -> int:
        size = len(s)

        def get_num(i):
            while s[i] == " ":
                i += 1
            result = ""
            while i < size and s[i].isdigit():
                result += s[i]
                i += 1
            return i, int(result)

        i = 0
        l = []
        while i < size:
            if s[i].isdigit():
                i, num = get_num(i)
                if len(l) == 0:
                    l.append(["", num])
            elif s[i] == "+" or s[i] == "-":
                pi = i
                i, num = get_num(i + 1)
                l.append([s[pi], num])
            elif s[i] == "*":
                i, num = get_num(i + 1)
                l[-1][1] *= num
            elif s[i] == "/":
                i, num = get_num(i + 1)
                l[-1][1] = int(l[-1][1] / num)
            else:
                i += 1
        print(l)

        size1 = len(l)
        result = l[0][1]
        for i in range(1, size1):
            if l[i][0] == "+":
                result += l[i][1]
            else:
                result -= l[i][1]
        return int(result)
