package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestClosestCost(t *testing.T) {
	tests := []struct {
		baseCosts    []int
		toppingCosts []int
		target       int
		expect       int
	}{
		{
			baseCosts:    []int{1, 7},
			toppingCosts: []int{3, 4},
			target:       10,
			expect:       10,
		},
		{
			baseCosts:    []int{3, 10},
			toppingCosts: []int{2, 5},
			target:       9,
			expect:       8,
		},
		{
			baseCosts:    []int{52, 48, 17, 44, 33, 58},
			toppingCosts: []int{74, 28, 20, 98, 46, 9, 1, 22, 2},
			target:       93,
			expect:       93,
		},
		{
			baseCosts:    []int{52, 48, 17, 44, 33, 17, 58, 52},
			toppingCosts: []int{74, 28, 20, 98, 46, 9, 1, 1, 22, 2},
			target:       93,
			expect:       93,
		},
		{
			baseCosts:    []int{2, 3},
			toppingCosts: []int{4, 5, 100},
			target:       18,
			expect:       17,
		},
		{
			baseCosts:    []int{10},
			toppingCosts: []int{1},
			target:       1,
			expect:       10,
		},
		{
			baseCosts:    []int{4},
			toppingCosts: []int{9},
			target:       9,
			expect:       13,
		},
		{
			baseCosts:    []int{3, 2},
			toppingCosts: []int{3},
			target:       10,
			expect:       9,
		},
		{
			baseCosts:    []int{1},
			toppingCosts: []int{8, 10},
			target:       10,
			expect:       9,
		},
	}
	Convey("TestClosestCost", t, func() {
		for _, tt := range tests {
			So(closestCost(tt.baseCosts, tt.toppingCosts, tt.target), ShouldEqual, tt.expect)
		}
	})
}
