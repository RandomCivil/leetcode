inst_1031 = __import__("1031")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "nums, firstLen, secondLen, expected",
        [
            ([0, 6, 5, 2, 2, 5, 1, 9, 4], 1, 2, 20),
            ([3, 8, 1, 3, 2, 1, 8, 9, 0], 3, 2, 29),
            ([2, 1, 5, 6, 0, 9, 5, 0, 3, 8], 4, 3, 31),
        ],
    )
    def test_maxSumTwoNoOverlap(self, nums, firstLen: int, secondLen: int, expected):
        assert (
            inst_1031.Solution().maxSumTwoNoOverlap(nums, firstLen, secondLen)
            == expected
        )
