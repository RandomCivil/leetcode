inst_2115 = __import__("2115")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "recipes, ingredients, supplies,expected",
        [
            (
                ["bread", "sandwich"],
                [["yeast", "flour"], ["bread", "meat"]],
                ["yeast", "flour", "meat"],
                ["bread", "sandwich"],
            ),
            (
                ["bread", "sandwich", "burger"],
                [["yeast", "flour"], ["bread", "meat"], ["sandwich", "meat", "bread"]],
                ["yeast", "flour", "meat"],
                ["bread", "sandwich", "burger"],
            ),
            (
                ["bread", "sandwich", "burger"],
                [["yeast", "flour"], ["bread", "meat"], ["sandwich", "meat", "bread"]],
                ["yeast", "flour", "abc"],
                ["bread"],
            ),
        ],
    )
    def test_findAllRecipes(
        self,
        recipes: list[str],
        ingredients: list[list[str]],
        supplies: list[str],
        expected,
    ):
        assert (
            inst_2115.Solution().findAllRecipes(recipes, ingredients, supplies)
            == expected
        )
