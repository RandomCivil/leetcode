package leetcode

//You are given a doubly linked list which in addition to the next and previous pointers, it could have a child pointer, which may or may not point to a separate doubly linked list. These child lists may have one or more children of their own, and so on, to produce a multilevel data structure, as shown in the example below.

//Flatten the list so that all the nodes appear in a single-level, doubly linked list. You are given the head of the first level of the list.

type ChildNode struct {
	Val   int
	Prev  *ChildNode
	Next  *ChildNode
	Child *ChildNode
}

func flatten(root *ChildNode) *ChildNode {
	leaf := []*ChildNode{}
	var dfs func(cur *ChildNode)
	dfs = func(cur *ChildNode) {
		if cur == nil {
			return
		}
		tempNext := cur.Next
		if cur.Child != nil {
			dfs(cur.Child)
			//fmt.Println("cur", cur.Val)
			if len(leaf)-1 >= 0 {
				pop := leaf[len(leaf)-1]
				leaf = leaf[:len(leaf)-1]
				//fmt.Println("pop", pop.Val, cur.Val)
				if cur.Child != nil {
					if cur.Next != nil {
						pop.Next = cur.Next
						cur.Next.Prev = pop
					}

					cur.Next = cur.Child
					cur.Child.Prev = cur
					cur.Child = nil
				}
			}
		}
		if tempNext == nil {
			//fmt.Println("leaf", cur,leaf)
			leaf = append(leaf, cur)
		}
		cur = tempNext
		dfs(cur)
	}
	dfs(root)
	return root
}
