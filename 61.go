package leetcode

import "gitlab.com/RandomCivil/leetcode/common"

// Given the head of a linked list, rotate the list to the right by k places.

// Example 1:

// Input: head = [1,2,3,4,5], k = 2
// Output: [4,5,1,2,3]
// Example 2:

// Input: head = [0,1,2], k = 4
// Output: [2,0,1]

// Constraints:

// The number of nodes in the list is in the range [0, 500].
// -100 <= Node.val <= 100
// 0 <= k <= 2 * 109
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func rotateRight(head *common.ListNode, k int) *common.ListNode {
	if head == nil || head.Next == nil {
		return head
	}
	// 至少两个节点
	h := head
	var n int
	for h != nil {
		n++
		h = h.Next
	}

	var prev *common.ListNode
	cur, tail := head, head
	k = k % n
	if k == 0 {
		return head
	}
	for k > 1 {
		tail = tail.Next
		k--
	}
	for tail.Next != nil {
		prev = cur
		cur = cur.Next
		tail = tail.Next
	}
	tail.Next = head
	prev.Next = nil
	return cur
}
