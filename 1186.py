# Given an array of integers, return the maximum sum for a non-empty subarray (contiguous elements) with at most one element deletion. In other words, you want to choose a subarray and optionally delete one element from it so that there is still at least one element left and the sum of the remaining elements is maximum possible.

# Note that the subarray needs to be non-empty after deleting one element.

# Example 1:

# Input: arr = [1,-2,0,3]
# Output: 4
# Explanation: Because we can choose [1, -2, 0, 3] and drop -2, thus the subarray [1, 0, 3] becomes the maximum value.

# Example 2:

# Input: arr = [1,-2,-2,3]
# Output: 3
# Explanation: We just choose [3] and it's the maximum sum.

# Example 3:

# Input: arr = [-1,-1,-1,-1]
# Output: -1
# Explanation: The final subarray needs to be non-empty. You can't choose [-1] and delete -1 from it, then get an empty subarray to make the sum equals to 0.


class Solution:
    def maximumSum(self, arr) -> int:
        size = len(arr)
        dp = [0] * size
        reverse_dp = [0] * size

        mx = dp[0] = arr[0]
        for i in range(1, size):
            if dp[i - 1] < 0:
                dp[i] = arr[i]
            else:
                dp[i] = dp[i - 1] + arr[i]
            mx = max(mx, dp[i])

        reverse_dp[-1] = arr[size - 1]
        for i in range(size - 2, -1, -1):
            if reverse_dp[i + 1] < 0:
                reverse_dp[i] = arr[i]
            else:
                reverse_dp[i] = reverse_dp[i + 1] + arr[i]

        print(dp, reverse_dp)

        for i in range(1, size - 1):
            mx = max(mx, dp[i - 1] + reverse_dp[i + 1])
        return mx


if __name__ == '__main__':
    # 4
    arr = [1, -2, 0, 3]
    # 3
    # arr = [1, -2, -2, 3]
    # -1
    # arr = [-1, -1, -1, -1]
    # 3
    # arr = [2, 1, -2, -5, -2]
    print(Solution().maximumSum(arr))
