# You are given an array of n pairs pairs where pairs[i] = [lefti, righti] and lefti < righti.

# A pair p2 = [c, d] follows a pair p1 = [a, b] if b < c. A chain of pairs can be formed in this fashion.

# Return the length longest chain which can be formed.

# You do not need to use up all the given intervals. You can select pairs in any order.

# Example 1:

# Input: pairs = [[1,2],[2,3],[3,4]]
# Output: 2
# Explanation: The longest chain is [1,2] -> [3,4].

# Example 2:

# Input: pairs = [[1,2],[7,8],[4,5]]
# Output: 3
# Explanation: The longest chain is [1,2] -> [4,5] -> [7,8].

# Constraints:


# n == pairs.length
# 1 <= n <= 1000
# -1000 <= lefti < righti < 1000
class Solution:
    def findLongestChain(self, pairs) -> int:
        size = len(pairs)
        pairs = sorted(pairs, key=lambda x: x[0])
        print(pairs)
        dp = [0] * size
        dp[0] = 1
        for i in range(1, size):
            v = 0
            for j in range(0, i):
                si, ei = pairs[i]
                sj, ej = pairs[j]
                if ej < si:
                    v = max(v, dp[j])
            dp[i] = v+1

        print(dp)
        return dp[-1]


if __name__ == '__main__':
    # 2
    pairs = [[1, 2], [2, 3], [3, 4]]

    # 3
    pairs = [[1,2],[7,8],[4,5]]

    # 3
    pairs=[[-6,9],[1,6],[8,10],[-1,4],[-6,-2],[-9,8],[-5,3],[0,3]]
    print(Solution().findLongestChain(pairs))
