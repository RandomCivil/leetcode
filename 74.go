package leetcode

// You are given an m x n integer matrix matrix with the following two properties:

// Each row is sorted in non-decreasing order.
// The first integer of each row is greater than the last integer of the previous row.
// Given an integer target, return true if target is in matrix or false otherwise.

// You must write a solution in O(log(m * n)) time complexity.

// Example 1:

// Input: matrix = [[1,3,5,7],[10,11,16,20],[23,30,34,60]], target = 3
// Output: true
// Example 2:

// Input: matrix = [[1,3,5,7],[10,11,16,20],[23,30,34,60]], target = 13
// Output: false
func searchMatrix(matrix [][]int, target int) bool {
	var result bool
	m, n := len(matrix), len(matrix[0])
	lo, hi := 0, m*n-1
	for lo <= hi {
		midIndex := (lo + hi) / 2
		midVal := matrix[midIndex/n][midIndex%n]
		if target == midVal {
			return true
		}
		if target < midVal {
			hi = midIndex - 1
		} else {
			lo = midIndex + 1
		}
	}
	return result
}
