# A string is called happy if it does not have any of the strings 'aaa', 'bbb' or 'ccc' as a substring.

# Given three integers a, b and c, return any string s, which satisfies following conditions:

# s is happy and longest possible.
# s contains at most a occurrences of the letter 'a', at most b occurrences of the letter 'b' and at most c occurrences of the letter 'c'.
# s will only contain 'a', 'b' and 'c' letters.
# If there is no such string s return the empty string "".

# Example 1:

# Input: a = 1, b = 1, c = 7
# Output: "ccaccbcc"
# Explanation: "ccbccacc" would also be a correct answer.

# Example 2:

# Input: a = 2, b = 2, c = 1
# Output: "aabbc"

# Example 3:

# Input: a = 7, b = 1, c = 0
# Output: "aabaa"
# Explanation: It's the only correct answer in this case.

# Constraints:

# 0 <= a, b, c <= 100
# a + b + c > 0
from collections import Counter


class Solution:
    def longestDiverseString(self, a: int, b: int, c: int) -> str:
        r = ''
        c = Counter({'a': a, 'b': b, 'c': c})

        def check():
            for ch, n in c.items():
                if len(r) == 0 or (n > 0 and (len(r) > 0 and r[-1] != ch)):
                    return True

            return False

        while check():
            cm = c.most_common()
            print(cm)
            for ch, _ in cm:
                if len(r) > 0 and r[-1] == ch:
                    continue
                n = min(2, c[ch])
                r += ch * n
                c[ch] -= n
                break

        return r


if __name__ == '__main__':
    # ccaccbcc
    a, b, c = 1, 1, 7
    # aabbc
    a, b, c = 2, 2, 1
    # aabaa
    a, b, c = 7, 1, 0
    # wrong
    # "ccbccbbccbbccbbccbc"
    a, b, c = 0, 8, 11
    print(Solution().longestDiverseString(a, b, c))
