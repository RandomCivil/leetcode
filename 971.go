package leetcode

import "gitlab.com/RandomCivil/leetcode/common"

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func flipMatchVoyage(root *common.TreeNode, voyage []int) []int {
	var result []int
	var index int = -1
	var dfs func(cur *common.TreeNode) bool
	dfs = func(cur *common.TreeNode) bool {
		if cur == nil {
			return true
		}
		index++
		if cur.Val != voyage[index] {
			index--
			return false
		}
		if !dfs(cur.Left) || !dfs(cur.Right) {
			if !dfs(cur.Right) || !dfs(cur.Left) {
				return false
			}
			result = append(result, cur.Val)
			return true
		}
		return true
	}
	if !dfs(root) {
		return []int{-1}
	}
	return result
}
