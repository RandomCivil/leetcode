# Given a list of lists of integers, nums, return all elements of nums in diagonal order as shown in the below images.

# Example 1:

# Input: nums = [[1,2,3],[4,5,6],[7,8,9]]
# Output: [1,4,2,7,5,3,8,6,9]

# Example 2:

# Input: nums = [[1,2,3,4,5],[6,7],[8],[9,10,11],[12,13,14,15,16]]
# Output: [1,6,2,8,7,3,9,4,12,10,5,13,11,14,15,16]

# Example 3:

# Input: nums = [[1,2,3],[4],[5,6,7],[8],[9,10,11]]
# Output: [1,4,2,5,3,8,6,9,7,10,11]

# Example 4:

# Input: nums = [[1,2,3,4,5,6]]
# Output: [1,2,3,4,5,6]
from collections import defaultdict


class Solution:
    def findDiagonalOrder(self, nums):
        r = []
        d = defaultdict(list)
        m = len(nums)
        for i in range(m):
            for j in range(len(nums[i])):
                d[i + j].append(nums[i][j])

        print(d)
        for _, l in d.items():
            r += list(reversed(l))
        return r


if __name__ == '__main__':
    nums = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    print(Solution().findDiagonalOrder(nums))
