inst_76 = __import__("76")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "s,t,expected",
        [
            ("ADOBECODEBANC", "ABC", "BANC"),
            ("a","a","a"),
            ("a","b",""),
            ("a","aa",""),
        ],
    )
    def test_minWindow(self, s, t, expected):
        assert inst_76.Solution().minWindow(s, t) == expected
