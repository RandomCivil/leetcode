package leetcode

import (
	"fmt"

	"gitlab.com/RandomCivil/leetcode/common"
)

//There is a rectangular brick wall in front of you with n rows of bricks. The ith row has some number of bricks each of the same height (i.e., one unit) but they can be of different widths. The total width of each row is the same.

//Draw a vertical line from the top to the bottom and cross the least bricks. If your line goes through the edge of a brick, then the brick is not considered as crossed. You cannot draw a line just along one of the two vertical edges of the wall, in which case the line will obviously cross no bricks.

//Given the 2D array wall that contains the information about the wall, return the minimum number of crossed bricks after drawing such a vertical line.

//Example 1:

//Input: wall = [[1,2,2,1],[3,1,2],[1,3,2],[2,4],[3,1,2],[1,3,1,1]]
//Output: 2

//Example 2:

//Input: wall = [[1],[1],[1]]
//Output: 3
func LeastBricks(wall [][]int) int {
	var (
		r, sum int
		size   = len(wall)
		m      = make(map[int]int)
	)
	for _, layer := range wall {
		sum = 0
		for _, w := range layer {
			sum += w
			if _, ok := m[sum]; !ok {
				m[sum] = 1
			} else {
				m[sum]++
			}
		}
	}
	fmt.Println(m)

	for s, c := range m {
		if s == sum {
			continue
		}
		r = common.Max(r, c)
	}
	return size - r
}
