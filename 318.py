# Given a string array words, return the maximum value of length(word[i]) * length(word[j]) where the two words do not share common letters. If no such two words exist, return 0.

# Example 1:

# Input: words = ["abcw","baz","foo","bar","xtfn","abcdef"]
# Output: 16
# Explanation: The two words can be "abcw", "xtfn".
# Example 2:

# Input: words = ["a","ab","abc","d","cd","bcd","abcd"]
# Output: 4
# Explanation: The two words can be "ab", "cd".
# Example 3:

# Input: words = ["a","aa","aaa","aaaa"]
# Output: 0
# Explanation: No such pair of words.


# Constraints:


# 2 <= words.length <= 1000
# 1 <= words[i].length <= 1000
# words[i] consists only of lowercase English letters.
class Solution:
    def maxProduct(self, words) -> int:
        result = 0
        set_list = [set(word) for word in words]
        for i, word in enumerate(words):
            for j, st in enumerate(set_list):
                if i == j:
                    continue
                if len(set_list[i] & st) > 0:
                    continue
                result = max(result, len(word) * len(words[j]))
        return result


if __name__ == "__main__":
    # 16
    words = ["abcw", "baz", "foo", "bar", "xtfn", "abcdef"]
    # 4
    words = ["a", "ab", "abc", "d", "cd", "bcd", "abcd"]
    print(Solution().maxProduct(words))
