inst_127 = __import__("127")
inst_127_me = __import__("127_me")


import pytest


class TestClass:
    @pytest.mark.parametrize(
        "beginWord, endWord, wordList,expected",
        [
            ("hit", "cog", ["hot", "dot", "dog", "lot", "log", "cog"], 5),
            ("hit", "cog", ["hot", "dot", "dog", "lot", "log"], 0),
            (
                "leet",
                "code",
                ["lest", "leet", "lose", "code", "lode", "robe", "lost"],
                6,
            ),
        ],
    )
    def test_ladderLength(
        self, beginWord: str, endWord: str, wordList: list[str], expected
    ):
        assert (
            inst_127.Solution().ladderLength(beginWord, endWord, wordList) == expected
        )
        assert (
            inst_127_me.Solution().ladderLength(beginWord, endWord, wordList)
            == expected
        )
