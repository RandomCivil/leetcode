# Given a non-empty 2D array grid of 0's and 1's, an island is a group of 1's (representing land)
# connected 4-directionally (horizontal or vertical.) You may assume all four edges of the grid are surrounded by water.
# Find the maximum area of an island in the given 2D array. (If there is no island, the maximum area is 0.)
"""
[[0,0,1,0,0,0,0,1,0,0,0,0,0],
 [0,0,0,0,0,0,0,1,1,1,0,0,0],
 [0,1,1,0,1,0,0,0,0,0,0,0,0],
 [0,1,0,0,1,1,0,0,1,0,1,0,0],
 [0,1,0,0,1,1,0,0,1,1,1,0,0],
 [0,0,0,0,0,0,0,0,0,0,1,0,0],
 [0,0,0,0,0,0,0,1,1,1,0,0,0],
 [0,0,0,0,0,0,0,1,1,0,0,0,0]]

Given the above grid, return 6. Note the answer is not 11, because the island must be connected 4-directionally.
"""


class Solution:
    def maxAreaOfIsland(self, grid) -> int:
        m = len(grid)
        if m == 0:
            return 0
        n = len(grid[0])
        r = 0
        self.area = 0

        def dfs(i, j, path):
            if i < 0 or j < 0 or i > m - 1 or j > n - 1:
                return
            if (i, j) in path or grid[i][j] == 0:
                return
            self.area += 1
            grid[i][j] = 0
            path.add((i, j))
            dfs(i + 1, j, path)
            dfs(i, j + 1, path)
            dfs(i - 1, j, path)
            dfs(i, j - 1, path)
            path.remove((i, j))

        for i in range(m):
            for j in range(n):
                if grid[i][j] == 1:
                    self.area = 0
                    dfs(i, j, set())
                    print(self.area)
                    r = max(r, self.area)

        return r
