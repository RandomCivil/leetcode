# Given a string containing just the characters '(' and ')', return the length of the longest valid (well-formed) parentheses substring.

# Example 1:

# Input: s = "(()"
# Output: 2
# Explanation: The longest valid parentheses substring is "()".
# Example 2:

# Input: s = ")()())"
# Output: 4
# Explanation: The longest valid parentheses substring is "()()".
# Example 3:

# Input: s = ""
# Output: 0


# Constraints:


# 0 <= s.length <= 3 * 104
# s[i] is '(', or ')'.
class Solution:
    def longestValidParentheses(self, s: str) -> int:
        stack = [["", 0]]
        size = len(s)
        i = 0
        while i < size:
            if s[i] == "(":
                stack.append(["(", 0])
            else:
                if stack[-1][0] == "(":
                    _, n = stack.pop()
                    stack[-1][1] += n + 2
                else:
                    stack.append([")", 0])
            i += 1
        print(stack)
        _, mx = max(stack, key=lambda x: x[1])
        return mx
