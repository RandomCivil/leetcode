package leetcode

import "gitlab.com/RandomCivil/leetcode/common"

// Given two integer arrays inorder and postorder where inorder is the inorder traversal of a binary tree and postorder is the postorder traversal of the same tree, construct and return the binary tree.

// Example 1:

// Input: inorder = [9,3,15,20,7], postorder = [9,15,7,20,3]
// Output: [3,9,20,null,null,15,7]
// Example 2:

// Input: inorder = [-1], postorder = [-1]
// Output: [-1]

// Constraints:

// 1 <= inorder.length <= 3000
// postorder.length == inorder.length
// -3000 <= inorder[i], postorder[i] <= 3000
// inorder and postorder consist of unique values.
// Each value of postorder also appears in inorder.
// inorder is guaranteed to be the inorder traversal of the tree.
// postorder is guaranteed to be the postorder traversal of the tree.
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func buildTree1(inorder []int, postorder []int) *common.TreeNode {
	pos := make(map[int]int)
	for i, n := range inorder {
		pos[n] = i
	}

	i := len(inorder) - 1
	var build func(start, end int) *common.TreeNode
	build = func(start, end int) *common.TreeNode {
		if start > end {
			return nil
		}
		val := postorder[i]
		cur := pos[val]
		i--
		r := build(cur+1, end)
		l := build(start, cur-1)
		return &common.TreeNode{
			Val:   val,
			Left:  l,
			Right: r,
		}
	}
	return build(0, len(inorder)-1)
}
