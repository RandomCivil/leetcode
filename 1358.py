# Given a string s consisting only of characters a, b and c.

# Return the number of substrings containing at least one occurrence of all these characters a, b and c.

# Example 1:

# Input: s = "abcabc"
# Output: 10
# Explanation: The substrings containing at least one occurrence of the characters a, b and c are "abc", "abca", "abcab", "abcabc", "bca", "bcab", "bcabc", "cab", "cabc" and "abc" (again).
# Example 2:

# Input: s = "aaacb"
# Output: 3
# Explanation: The substrings containing at least one occurrence of the characters a, b and c are "aaacb", "aacb" and "acb".

# Example 3:

# Input: s = "abc"
# Output: 1
from collections import defaultdict


class Solution:
    def numberOfSubstrings(self, s: str) -> int:
        r = 0
        d = {c: 0 for c in 'abc'}
        i = 0
        size = len(s)
        for j in range(size):
            d[s[j]] += 1
            while all(d.values()):
                d[s[i]] -= 1
                i += 1
            r += i

        return r


if __name__ == '__main__':
    # 10
    s = "abcabc"
    # 3
    s = "aaacb"
    # 1
    s = "abc"
    # 11
    s = "acbbcac"
    print(Solution().numberOfSubstrings(s))
