# Given a string s. In one step you can insert any character at any index of the string.

# Return the minimum number of steps to make s palindrome.

# A Palindrome String is one that reads the same backward as well as forward.


# Example 1:

# Input: s = "zzazz"
# Output: 0
# Explanation: The string "zzazz" is already palindrome we do not need any insertions.
# Example 2:

# Input: s = "mbadm"
# Output: 2
# Explanation: String can be "mbdadbm" or "mdbabdm".
# Example 3:

# Input: s = "leetcode"
# Output: 5
# Explanation: Inserting 5 characters the string becomes "leetcodocteel".


# Constraints:


# 1 <= s.length <= 500
# s consists of lowercase English letters.
class Solution:
    def minInsertions(self, s: str) -> int:
        print(s)

        def lcs(s1, s2):
            size = len(s1)
            dp = [[0] * (size + 1) for _ in range(size + 1)]
            for i in range(1, size + 1):
                for j in range(1, size + 1):
                    if s1[i - 1] == s2[j - 1]:
                        dp[i][j] = dp[i - 1][j - 1] + 1
                    else:
                        dp[i][j] = max(dp[i - 1][j], dp[i][j - 1])
            print(s1, s2, dp)
            return dp[-1][-1]

        return len(s) - lcs("".join(reversed(s)), s)
