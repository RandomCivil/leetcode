# You are given an m x n binary matrix grid. An island is a group of 1's (representing land) connected 4-directionally (horizontal or vertical.) You may assume all four edges of the grid are surrounded by water.

# An island is considered to be the same as another if and only if one island can be translated (and not rotated or reflected) to equal the other.

# Return the number of distinct islands.

# Example 1:


# Input: grid = [[1,1,0,0,0],[1,1,0,0,0],[0,0,0,1,1],[0,0,0,1,1]]
# Output: 1
# Example 2:


# Input: grid = [[1,1,0,1,1],[1,0,0,0,0],[0,0,0,0,1],[1,1,0,1,1]]
# Output: 3


# Constraints:


# m == grid.length
# n == grid[i].length
# 1 <= m, n <= 50
# grid[i][j] is either 0 or 1.
class Solution:
    def numDistinctIslands(self, grid: list[list[int]]) -> int:
        self.result = 0
        self.s = set()
        directions = [(1, 0), (-1, 0), (0, 1), (0, -1)]
        self.min_x, self.min_y = float("inf"), float("inf")
        self.visited = set()
        m, n = len(grid), len(grid[0])

        def dfs(x, y):
            if x == -1 or x == m or y == -1 or y == n:
                return
            if grid[x][y] == 0:
                return
            if (x, y) in self.visited:
                return
            self.visited.add((x, y))
            self.min_x = min(self.min_x, x)
            self.min_y = min(self.min_y, y)
            for xx, yy in directions:
                dfs(x + xx, y + yy)

        prev_set = set()
        for i in range(m):
            for j in range(n):
                if (i, j) in self.visited:
                    continue
                dfs(i, j)
                self.save(prev_set)
                self.min_x, self.min_y = float("inf"), float("inf")
                prev_set = self.visited.copy()
        return self.result

    def save(self, prev_set):
        if len(self.visited - prev_set) == 0:
            return
        l = map(
            lambda x: (x[0] - self.min_x, x[1] - self.min_y), self.visited - prev_set
        )
        l = tuple(sorted(l))
        print(l)
        if l not in self.s:
            self.result += 1
            self.s.add(l)
