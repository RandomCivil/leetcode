package leetcode

//Given an array nums of distinct integers, return all the possible permutations. You can return the answer in any order.

//Example 1:

//Input: nums = [1,2,3]
//Output: [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]

//Example 2:

//Input: nums = [0,1]
//Output: [[0,1],[1,0]]

//Example 3:

//Input: nums = [1]
//Output: [[1]]

//Constraints:

//1 <= nums.length <= 6
//-10 <= nums[i] <= 10
//All the integers of nums are unique.
func Permute(nums []int) [][]int {
	var r [][]int
	var cp []int
	set := make(map[int]struct{})
	size := len(nums)
	var dfs func(i int, path []int)
	dfs = func(i int, path []int) {
		if i == size {
			cp = make([]int, size)
			copy(cp, path)
			r = append(r, cp)
			return
		}
		for j, n := range nums {
			if _, ok := set[j]; ok {
				continue
			}
			set[j] = struct{}{}
			path = append(path, n)
			dfs(i+1, path)
			path = path[:len(path)-1]
			delete(set, j)
		}
	}
	dfs(0, []int{})
	return r
}
