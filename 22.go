package leetcode

// Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.

// Example 1:

// Input: n = 3
// Output: ["((()))","(()())","(())()","()(())","()()()"]
// Example 2:

// Input: n = 1
// Output: ["()"]

// Constraints:

// 1 <= n <= 8

func generateParenthesis(n int) []string {
	var result []string
	var dfs func(s string, left, right int)
	dfs = func(s string, left, right int) {
		// fmt.Println(s, left, right)
		if left > n || right > n {
			return
		}
		if right > left {
			return
		}
		if left == right && left == n && s != "" {
			result = append(result, s)
			return
		}
		for _, ss := range [2]string{"(", ")"} {
			if ss == "(" {
				left++
			} else {
				right++
			}
			dfs(s+ss, left, right)
			if ss == "(" {
				left--
			} else {
				right--
			}
		}
	}
	dfs("", 0, 0)
	return result
}
