inst_624 = __import__("624")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "arrays,expected",
        [
            ([[1, 2, 3], [4, 5], [1, 2, 3]], 4),
            ([[1, 2], [2, 3]], 2),
            ([[1, 2], [1]], 1),
        ],
    )
    def test_maxDistance(self, arrays, expected):
        assert inst_624.Solution().maxDistance(arrays) == expected
