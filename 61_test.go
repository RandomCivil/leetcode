package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/RandomCivil/leetcode/common"
)

func TestRotateRight(t *testing.T) {
	tests := []struct {
		k            int
		nums, expect []int
	}{
		{
			nums:   []int{0, 1, 2},
			k:      4,
			expect: []int{2, 0, 1},
		},
		{
			nums:   []int{1, 2, 3, 4, 5},
			k:      2,
			expect: []int{4, 5, 1, 2, 3},
		},
		{
			nums:   []int{1, 2, 3, 4, 5},
			k:      3,
			expect: []int{3, 4, 5, 1, 2},
		},
	}
	Convey("TestRotateRight", t, func() {
		for _, tt := range tests {
			l := common.LinkList{Nums: tt.nums}
			So(l.Traversal(rotateRight(l.Build(), tt.k)), ShouldResemble, tt.expect)
		}
	})
}
