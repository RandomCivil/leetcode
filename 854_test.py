inst_854 = __import__("854")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "s1,s2,expected",
        [
            ("abc", "bca", 2),
            ("ab", "ba", 1),
            ("abccaacceecdeea", "bcaacceeccdeaae", 9),
            ("abccaaaa", "bcaaaaca", 3),
        ],
    )
    def test_kSimilarity(self, s1, s2, expected):
        assert inst_854.Solution().kSimilarity(s1, s2) == expected
