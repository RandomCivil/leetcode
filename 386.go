package leetcode

//Given an integer n, return 1 - n in lexicographical order.
//For example, given 13, return: [1,10,11,12,13,2,3,4,5,6,7,8,9].
//Please optimize your algorithm to use less time and space. The input size may be as large as 5,000,000.

//[1,10,100,101,102,103,104,105,106,107,108,109,11,110,111,112,113,114,115,116,117,118,119,12,120,13,14,15,16,17,18,19,2,20,21,22,23,24,25,26,27,28,29,3,30,31,32,33,34,35,36,37,38,39,4,40,41,42,43,44,45,46,47,48,49,5,50,51,52,53,54,55,56,57,58,59,6,60,61,62,63,64,65,66,67,68,69,7,70,71,72,73,74,75,76,77,78,79,8,80,81,82,83,84,85,86,87,88,89,9,90,91,92,93,94,95,96,97,98,99]

func LexicalOrder(n int) []int {
	var r []int
	var dfs func(cur int)
	dfs = func(cur int) {
		if cur > n {
			return
		}
		r = append(r, cur)
		for i := 0; i < 10; i++ {
			dfs(cur*10 + i)
		}
	}

	for i := 1; i < 10; i++ {
		dfs(i)
	}
	return r
}
