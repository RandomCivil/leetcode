# Given an array of meeting time intervals intervals where intervals[i] = [starti, endi], return the minimum number of conference rooms required.

# Example 1:

# Input: intervals = [[0,30],[5,10],[15,20]]
# Output: 2
# Example 2:

# Input: intervals = [[7,10],[2,4]]
# Output: 1


# Constraints:


# 1 <= intervals.length <= 104
# 0 <= starti < endi <= 106
import heapq


class Solution:
    def minMeetingRooms(self, intervals) -> int:
        result = 0
        intervals = sorted(intervals, key=lambda item: item[0])
        h = []
        for start, end in intervals:
            while len(h) > 0 and h[0] <= start:
                heapq.heappop(h)
            heapq.heappush(h, end)
            result = max(result, len(h))
        return result
