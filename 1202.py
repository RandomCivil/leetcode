# You are given a string s, and an array of pairs of indices in the string pairs where pairs[i] = [a, b] indicates 2 indices(0-indexed) of the string.

# You can swap the characters at any pair of indices in the given pairs any number of times.

# Return the lexicographically smallest string that s can be changed to after using the swaps.

# Example 1:

# Input: s = "dcab", pairs = [[0,3],[1,2]]
# Output: "bacd"
# Explaination:
# Swap s[0] and s[3], s = "bcad"
# Swap s[1] and s[2], s = "bacd"
# Example 2:

# Input: s = "dcab", pairs = [[0,3],[1,2],[0,2]]
# Output: "abcd"
# Explaination:
# Swap s[0] and s[3], s = "bcad"
# Swap s[0] and s[2], s = "acbd"
# Swap s[1] and s[2], s = "abcd"
# Example 3:

# Input: s = "cba", pairs = [[0,1],[1,2]]
# Output: "abc"
# Explaination:
# Swap s[0] and s[1], s = "bca"
# Swap s[1] and s[2], s = "bac"
# Swap s[0] and s[1], s = "abc"


# Constraints:

# 1 <= s.length <= 10^5
# 0 <= pairs.length <= 10^5
# 0 <= pairs[i][0], pairs[i][1] < s.length
# s only contains lower case English letters.
from collections import defaultdict
import common


class Solution:
    def smallestStringWithSwaps(self, s: str, pairs: list[list[int]]) -> str:
        pos_d, ch_d = defaultdict(list), defaultdict(list)
        size = len(s)
        uf = common.UnionFind(size)
        for start, end in pairs:
            uf.union(start, end)
        print(uf.l)

        for i, ss in enumerate(s):
            p = uf.find(i)
            pos_d[p].append(i)
            ch_d[p].append(ss)

        print("ch_d", ch_d)
        print("pos_d", pos_d)

        result = [""] * size
        for k, v in pos_d.items():
            l = sorted(ch_d[k])
            j = 0
            for i in v:
                result[i] = l[j]
                j += 1
        return "".join(result)
