# Given the head of a linked list, return the list after sorting it in ascending order.

# Example 1:

# Input: head = [4,2,1,3]
# Output: [1,2,3,4]

# Example 2:

# Input: head = [-1,5,3,4,0]
# Output: [-1,0,3,4,5]

# Example 3:

# Input: head = []
# Output: []

# Constraints:

#     The number of nodes in the list is in the range [0, 5 * 104].
#     -105 <= Node.val <= 105

# Follow up: Can you sort the linked list in O(n logn) time and O(1) memory (i.e. constant space)?
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
import common


class Solution:
    def sortList(self, head):
        def find_mid(head):
            slow, fast = head, head
            prev = None
            while fast and fast.next:
                prev = slow
                slow = slow.next
                fast = fast.next.next
            return slow, prev

        def merge(head):        # print(prev,rhead)

            if not head:
                return None
            if not head.next:
                return head

            mid, prev = find_mid(head)
            print(head.val, mid.val)
            prev.next = None
            l, r = merge(head), merge(mid)

            pre_root = common.ListNode()
            cur = pre_root
            while l and r:
                if l.val <= r.val:
                    cur.next = l
                    l = l.next
                else:
                    cur.next = r
                    r = r.next
                cur = cur.next

            if l:
                cur.next = l
            if r:
                cur.next = r
            return pre_root.next

        return merge(head)
