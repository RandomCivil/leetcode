package leetcode

import "fmt"

//You are given a sorted array consisting of only integers where every element appears exactly twice,
//except for one element which appears exactly once. Find this single element that appears only once.
//Follow up: Your solution should run in O(log n) time and O(1) space.
/*
Input: nums = [1,1,2,3,3,4,4,8,8]
Output: 2

Input: nums = [3,3,7,7,10,11,11]
Output: 10
*/
func SingleNonDuplicate(nums []int) int {
	fmt.Println(nums)
	lo := 0
	hi := len(nums) - 1
	for lo < hi {
		mid := int((lo + hi) / 2)
		//fmt.Println(mid)
		if mid%2 == 0 {
			if nums[mid] == nums[mid+1] {
				//1,1,3,3,4,4,5,8,8
				lo = mid + 2
			} else {
				//1,1,2,3,3,4,4,8,8
				hi = mid
			}
		} else {
			//1,1,2,3,3,4,4
			if nums[mid] != nums[mid-1] {
				hi = mid
			} else {
				//1,1,2,2,3,4,4
				lo = mid + 1
			}
		}
	}
	return nums[lo]
}
