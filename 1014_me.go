package leetcode

import "fmt"

//Given an array A of positive integers, A[i] represents the value of the i-th sightseeing spot, and two sightseeing spots i and j have distance j - i between them.
//The score of a pair (i < j) of sightseeing spots is (A[i] + A[j] + i - j) : the sum of the values of the sightseeing spots, minus the distance between them.
//Return the maximum score of a pair of sightseeing spots.

//Example 1:

//Input: [8,1,5,2,6]
//Output: 11
//Explanation: i = 0, j = 2, A[i] + A[j] + i - j = 8 + 5 + 0 - 2 = 11

//Note:

//2 <= A.length <= 50000
//1 <= A[i] <= 1000

func MaxScoreSightseeingPair1(A []int) int {
	fmt.Println(A)
	size := len(A)
	var r, v int
	var addtion, subtraction []int
	for i := 0; i < size; i++ {
		addtion = append(addtion, A[i]+i)
		subtraction = append(subtraction, A[i]-i)
	}
	fmt.Println(addtion, subtraction)

	mx := -200000
	for i := 0; i < size; i++ {
		if addtion[i] > mx {
			mx = addtion[i]
		}
		addtion[i] = mx
	}

	mx = -200000
	for i := size - 1; i >= 0; i-- {
		if subtraction[i] > mx {
			mx = subtraction[i]
		}
		subtraction[i] = mx
	}
	fmt.Println(addtion, subtraction)

	for i := 0; i < size-1; i++ {
		v = addtion[i] + subtraction[i+1]
		if v > r {
			r = v
		}
	}
	return r
}
