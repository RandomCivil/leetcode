# Given an integer array nums, find three numbers whose product is maximum and return the maximum product.

# Example 1:

# Input: nums = [1,2,3]
# Output: 6

# Example 2:

# Input: nums = [1,2,3,4]
# Output: 24

# Example 3:

# Input: nums = [-1,-2,-3]
# Output: -6

# Constraints:

# 3 <= nums.length <= 104
# -1000 <= nums[i] <= 1000
import math


class Solution:
    def maximumProduct(self, nums) -> int:
        print(nums)
        nums.sort()
        size = len(nums)

        return max(math.prod(nums[size - 3:]), math.prod([nums[-1],
                                                          *nums[:2]]))


if __name__ == '__main__':
    nums = [1, 2, 3]
    nums = [-1, -2, -3, 10]
    print(Solution().maximumProduct(nums))
