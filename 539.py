# Given a list of 24-hour clock time points in "HH:MM" format, return the minimum minutes difference between any two time-points in the list.

# Example 1:

# Input: timePoints = ["23:59","00:00"]
# Output: 1

# Example 2:


# Input: timePoints = ["00:00","23:59","00:00"]
# Output: 0
class Solution:
    def findMinDifference(self, timePoints) -> int:
        def convert(time):
            return int(time[:2]) * 60 + int(time[3:])

        minutes = list(map(convert, timePoints))
        print(minutes, minutes[1:] + minutes[:1])
        minutes.sort()

        return min((y - x) % (24 * 60)
                   for x, y in zip(minutes, minutes[1:] + minutes[:1]))


if __name__ == '__main__':
    timePoints = ["23:59", "00:00"]
    timePoints = ["00:00", "23:59", "00:00"]
    print(Solution().findMinDifference(timePoints))
