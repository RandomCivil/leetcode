package leetcode

import (
	"fmt"

	"gitlab.com/RandomCivil/leetcode/common"
)

//An ugly number is a positive integer whose prime factors are limited to 2, 3, and 5.

//Given an integer n, return the nth ugly number.

//Example 1:

//Input: n = 10
//Output: 12
//Explanation: [1, 2, 3, 4, 5, 6, 8, 9, 10, 12] is the sequence of the first 10 ugly numbers.

//Example 2:

//Input: n = 1
//Output: 1
//Explanation: 1 has no prime factors, therefore all of its prime factors are limited to 2, 3, and 5.

//Constraints:

//1 <= n <= 1690
func NthUglyNumber(n int) int {
	var (
		i1, i2, i3 = 0, 0, 0
		mn         int
		i          = 1
		set        = make(map[int]struct{})
	)
	arr2 := []int{1}
	arr3 := []int{1}
	arr5 := []int{1}

	for i <= n {
		mn = common.Min(arr2[i1], arr3[i2], arr5[i3])
		fmt.Println(mn)

		if _, ok := set[mn*2]; !ok {
			arr2 = append(arr2, mn*2)
		}
		if _, ok := set[mn*3]; !ok {
			arr3 = append(arr3, mn*3)
		}
		if _, ok := set[mn*5]; !ok {
			arr5 = append(arr5, mn*5)
		}

		for mn == arr2[i1] {
			i1++
		}
		for mn == arr3[i2] {
			i2++
		}
		for mn == arr5[i3] {
			i3++
		}
		set[mn] = struct{}{}
		i++
	}
	return mn
}
