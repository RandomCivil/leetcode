import pytest


inst_1584_kruskal = __import__("1584_kruskal")
inst_1584_prime = __import__("1584_prime")


class TestClass:
    @pytest.mark.parametrize(
        "points, expected",
        [
            ([[0, 0], [2, 2], [3, 10], [5, 2], [7, 0]], 20),
            ([[3, 12], [-2, 5], [-4, 1]], 18),
        ],
    )
    def test_earliestAcq(self, points, expected):
        assert inst_1584_kruskal.Solution().minCostConnectPoints(points) == expected
        assert inst_1584_prime.Solution().minCostConnectPoints(points) == expected
