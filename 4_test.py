inst_4 = __import__("4")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "nums1,nums2,expected",
        [
            ([1, 3], [2], 2),
            ([1, 2], [3, 4], 2.5),
            ([1, 3, 5, 7, 9], [2, 4, 6], 4.5),
            ([1, 2, 3, 4], [5, 6, 7, 8, 9], 5),
            ([2, 5, 6], [2], 3.5),
        ],
    )
    def test_findMedianSortedArrays(self, nums1, nums2, expected):
        assert inst_4.Solution().findMedianSortedArrays(nums1, nums2) == expected
