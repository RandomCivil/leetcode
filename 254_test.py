inst_254 = __import__("254")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "n,expected",
        [
            (12, [[2, 6], [2, 2, 3], [3, 4]]),
            (1, []),
            (37, []),
            (20, [[2, 10], [2, 2, 5], [4, 5]]),
            (8, [[2, 4], [2, 2, 2]]),
            (
                100,
                [
                    [2, 50],
                    [2, 2, 25],
                    [2, 2, 5, 5],
                    [2, 5, 10],
                    [4, 25],
                    [4, 5, 5],
                    [5, 20],
                    [10, 10],
                ],
            ),
        ],
    )
    def test_getFactors(
        self,
        n,
        expected,
    ):
        assert inst_254.Solution().getFactors(n) == expected
