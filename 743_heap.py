# There are N network nodes, labelled 1 to N.
# Given times, a list of travel times as directed edges times[i] = (u, v, w), where u is the source node, v is the target node, and w is the time it takes for a signal to travel from source to target.
# Now, we send a signal from a certain node K. How long will it take for all
# nodes to receive the signal? If it is impossible, return -1.

# Input: times = [[2,1,1],[2,3,1],[3,4,1]], N = 4, K = 2
# Output: 2

from collections import defaultdict
import heapq


class Solution:
    def networkDelayTime(self, times, n: int, k: int) -> int:
        d = defaultdict(list)
        for i, time in enumerate(times):
            src, dst, _ = time
            d[src].append(i)

        if k not in d:
            return -1

        result = [0] + [float("inf")] * (n)
        h = [(0, k)]
        heapq.heapify(h)
        while len(h) > 0:
            waste, dst = heapq.heappop(h)
            if waste < result[dst]:
                result[dst] = waste
                for i in d[dst]:
                    _, new_dst, new_waste = times[i]
                    heapq.heappush(h, (waste + new_waste, new_dst))

        print(result)
        mx = max(result)
        return mx if mx < float("inf") else -1


if __name__ == "__main__":
    times = [[2, 1, 1], [2, 3, 1], [3, 4, 1]]
    N, K = 4, 2

    # times = [[1, 2, 1]]
    # N, K = 2, 1

    # times = [[1,2,1],[2,3,2],[1,3,4]]
    # times = [[1, 2, 1], [2, 3, 2], [1, 3, 2]]
    # N, K = 3, 1
    print(Solution().networkDelayTime(times, N, K))
