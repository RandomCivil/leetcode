package leetcode

// Given a string s, return the longest
// palindromic

// substring
//  in s.

// Example 1:

// Input: s = "babad"
// Output: "bab"
// Explanation: "aba" is also a valid answer.
// Example 2:

// Input: s = "cbbd"
// Output: "bb"

// Constraints:

// 1 <= s.length <= 1000
// s consist of only digits and English letters.

// babad
func longestPalindrome(s string) string {
	size := len(s)
	if size == 1 {
		return s
	}

	cal := func(lo, hi int) (int, int) {
		for lo >= 0 && hi < size && s[lo] == s[hi] {
			lo--
			hi++
		}
		return lo + 1, hi - 1
	}

	var mx int
	var result string
	for i := 0; i < size-1; i++ {
		lo1, hi1 := cal(i, i)
		if hi1-lo1+1 > mx {
			mx = hi1 - lo1 + 1
			result = s[lo1 : hi1+1]
		}

		lo2, hi2 := cal(i, i+1)
		if hi2-lo2+1 > mx {
			mx = hi2 - lo2 + 1
			result = s[lo2 : hi2+1]
		}
	}
	return result
}
