# Given a sorted integer array arr, two integers k and x, return the k closest integers to x in the array. The result should also be sorted in ascending order.

# An integer a is closer to x than an integer b if:

# |a - x| < |b - x|, or
# |a - x| == |b - x| and a < b

# Example 1:

# Input: arr = [1,2,3,4,5], k = 4, x = 3
# Output: [1,2,3,4]

# Example 2:


# Input: arr = [1,2,3,4,5], k = 4, x = -1
# Output: [1,2,3,4]
class CustomSort:
    def __init__(self, target, val):
        self.target = target
        self.val = val

    def __lt__(x, y):
        xa = abs(x.val - x.target)
        ya = abs(y.val - y.target)
        if xa == ya:
            return x.val < y.val
        else:
            return xa < ya


class Solution:
    def findClosestElements(self, arr, k: int, x: int):
        sorted_arr = sorted([CustomSort(x, n) for n in arr])
        return sorted([s.val for s in sorted_arr[:k]])


if __name__ == '__main__':
    arr = [1, 2, 3, 4, 5]
    k = 4
    x = 3
    print(Solution().findClosestElements(arr, k, x))
