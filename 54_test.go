package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestSpiralOrder(t *testing.T) {
	tests := []struct {
		matrix [][]int
		expect []int
	}{
		{
			matrix: [][]int{
				[]int{1, 2, 3},
				[]int{4, 5, 6},
				[]int{7, 8, 9},
			},
			expect: []int{1, 2, 3, 6, 9, 8, 7, 4, 5},
		},
		{
			matrix: [][]int{
				[]int{1, 2, 3, 4},
				[]int{5, 6, 7, 8},
				[]int{9, 10, 11, 12},
			},
			expect: []int{1, 2, 3, 4, 8, 12, 11, 10, 9, 5, 6, 7},
		},
	}
	Convey("TestSpiralOrder", t, func() {
		for _, tt := range tests {
			So(spiralOrder(tt.matrix), ShouldResemble, tt.expect)
		}
	})
}
