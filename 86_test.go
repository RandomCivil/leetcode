package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/RandomCivil/leetcode/common"
)

func TestPartition1(t *testing.T) {
	tests := []struct {
		nums   []int
		x      int
		expect []int
	}{
		{
			nums:   []int{1, 4, 3, 2, 5, 2},
			x:      3,
			expect: []int{1, 2, 2, 4, 3, 5},
		},
	}
	Convey("TestPartition1", t, func() {
		for _, tt := range tests {
			l := &common.LinkList{Nums: tt.nums}
			So(l.Traversal(partition1(l.Build(), tt.x)), ShouldResemble, tt.expect)
		}
	})
}
