# Given an array of intervals where intervals[i] = [starti, endi], merge all overlapping intervals, and return an array of the non-overlapping intervals that cover all the intervals in the input.

# Example 1:

# Input: intervals = [[1,3],[2,6],[8,10],[15,18]]
# Output: [[1,6],[8,10],[15,18]]
# Explanation: Since intervals [1,3] and [2,6] overlaps, merge them into [1,6].

# Example 2:


# Input: intervals = [[1,4],[4,5]]
# Output: [[1,5]]
# Explanation: Intervals [1,4] and [4,5] are considered overlapping.
class Solution:
    def merge(self, intervals):
        intervals.sort(key=lambda x: x[0])
        r = [intervals[0]]
        size = len(intervals)
        for i in range(1, size):
            cs, ce = intervals[i]
            ls, le = r[-1]
            if le >= cs:
                r[-1][1] = max(ce, le)
            else:
                r.append(intervals[i])
        return r


if __name__ == '__main__':
    # [[1,6],[8,10],[15,18]]
    intervals = [[1, 3], [2, 6], [8, 10], [15, 18]]
    # intervals = [[1, 4], [4, 5]]
    print(Solution().merge(intervals))
