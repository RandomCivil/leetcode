package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestReverseWords(t *testing.T) {
	tests := []struct {
		s, expect []byte
	}{
		{
			s:      []byte{'t', 'h', 'e', ' ', 's', 'k', 'y', ' ', 'i', 's', ' ', 'b', 'l', 'u', 'e'},
			expect: []byte{'b', 'l', 'u', 'e', ' ', 'i', 's', ' ', 's', 'k', 'y', ' ', 't', 'h', 'e'},
		},
	}
	Convey("TestReverseWords", t, func() {
		for _, tt := range tests {
			reverseWords(tt.s)
			So(tt.s, ShouldResemble, tt.expect)
		}
	})
}
