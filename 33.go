package leetcode

import "fmt"

// There is an integer array nums sorted in ascending order (with distinct values).

// Prior to being passed to your function, nums is possibly rotated at an unknown pivot index k (1 <= k < nums.length) such that the resulting array is [nums[k], nums[k+1], ..., nums[n-1], nums[0], nums[1], ..., nums[k-1]] (0-indexed). For example, [0,1,2,4,5,6,7] might be rotated at pivot index 3 and become [4,5,6,7,0,1,2].

// Given the array nums after the possible rotation and an integer target, return the index of target if it is in nums, or -1 if it is not in nums.

// You must write an algorithm with O(log n) runtime complexity.

// Example 1:

// Input: nums = [4,5,6,7,0,1,2], target = 0
// Output: 4
// Example 2:

// Input: nums = [4,5,6,7,0,1,2], target = 3
// Output: -1
// Example 3:

// Input: nums = [1], target = 0
// Output: -1

// Constraints:

// 1 <= nums.length <= 5000
// -104 <= nums[i] <= 104
// All values of nums are unique.
// nums is an ascending array that is possibly rotated.
// -104 <= target <= 104

func search(nums []int, target int) int {
	fmt.Println(nums, target)
	size := len(nums)
	binarySearch := func(lo, hi int) int {
		for lo <= hi {
			mid := (lo + hi) / 2
			if nums[mid] == target {
				return mid
			}
			if nums[mid] > target {
				hi = mid - 1
			} else {
				lo = mid + 1
			}
		}
		return -1
	}
	if nums[0] <= nums[size-1] {
		return binarySearch(0, size-1)
	}
	pivot := findPivot(nums)
	fmt.Println("pivot", pivot)
	if target >= nums[0] {
		return binarySearch(0, pivot-1)
	}
	return binarySearch(pivot, size-1)
}

func findPivot(nums []int) int {
	size := len(nums)
	lo, hi := 0, size-1
	for lo <= hi {
		mid := (lo + hi) / 2
		fmt.Println("mid", mid)
		// nums[mid-1]>nums[mid]会panic
		if nums[mid] > nums[mid+1] {
			return mid + 1
		}
		// 不能nums[mid] > nums[0]
		if nums[mid] >= nums[0] {
			lo = mid + 1
		} else {
			hi = mid - 1
		}
	}
	return -1
}
