# There are a total of numCourses courses you have to take, labeled from 0 to numCourses - 1. You are given an array prerequisites where prerequisites[i] = [ai, bi] indicates that you must take course bi first if you want to take course ai.

# For example, the pair [0, 1], indicates that to take course 0 you have to first take course 1.
# Return true if you can finish all courses. Otherwise, return false.

# Example 1:

# Input: numCourses = 2, prerequisites = [[1,0]]
# Output: true
# Explanation: There are a total of 2 courses to take.
# To take course 1 you should have finished course 0. So it is possible.

# Example 2:

# Input: numCourses = 2, prerequisites = [[1,0],[0,1]]
# Output: false
# Explanation: There are a total of 2 courses to take.
# To take course 1 you should have finished course 0, and to take course 0 you should also have finished course 1. So it is impossible.
from collections import defaultdict, deque


class Solution:
    def canFinish(self, numCourses: int, prerequisites) -> bool:
        d = defaultdict(list)
        rs = defaultdict(set)
        indegrees = defaultdict(int)

        for end, start in prerequisites:
            d[start].append(end)
            rs[end].add(start)
            indegrees[end] += 1

        q = deque()
        for i in range(numCourses):
            if indegrees[i] == 0:
                q.append(i)

        count = 0
        while len(q) > 0:
            cur = q.popleft()
            count += 1
            for nxt in d[cur]:
                rs[nxt].remove(cur)
                if len(rs[nxt]) == 0:
                    q.append(nxt)

        return True if numCourses == count else False
