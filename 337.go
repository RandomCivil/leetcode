package leetcode

import (
	"fmt"

	"gitlab.com/RandomCivil/leetcode/common"
)

//The thief has found himself a new place for his thievery again. There is only one entrance to this area, called the "root." Besides the root, each house has one and only one parent house. After a tour, the smart thief realized that "all houses in this place forms a binary tree". It will automatically contact the police if two directly-linked houses were broken into on the same night.
//Determine the maximum amount of money the thief can rob tonight without alerting the police.

/*
Input: [3,2,3,null,3,null,1]

     3
    / \
   2   3
    \   \
     3   1

Output: 7
Explanation: Maximum amount of money the thief can rob = 3 + 3 + 1 = 7.

Input: [3,4,5,1,3,null,1]

     3
    / \
   4   5
  / \   \
 1   3   1

Output: 9
Explanation: Maximum amount of money the thief can rob = 4 + 5 = 9.
*/

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

//arr := []int{3, 2, 3, 0, 3, 0, 1}
//arr := []int{3, 4, 5, 1, 3, 0, 1}

//func rob(root *TreeNode) int {
func Rob1(root *common.TreeNode) int {
	var result int
	var dfs func(head, prev *common.TreeNode)
	dfs = func(head, prev *common.TreeNode) {
		if head == nil {
			return
		}
		dfs(head.Left, head)
		dfs(head.Right, head)
		if head == prev {
			if prev.Val > prev.Val1 {
				result = prev.Val
			} else {
				result = prev.Val1
			}
			return
		}
		fmt.Printf("head:%v,prev:%v\n", head, prev)
		prev.Val += head.Val1
		prev.Val1 += head.Val
		fmt.Printf("head1:%v,prev1:%v\n", head, prev)
	}
	dfs(root, root)
	return result
}
