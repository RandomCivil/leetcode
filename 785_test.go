package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestIsBipartite(t *testing.T) {
	tests := []struct {
		graph  [][]int
		expect bool
	}{
		{
			graph: [][]int{
				{1, 2, 3}, {0, 2}, {0, 1, 3}, {0, 2},
			},
			expect: false,
		},
		{
			graph: [][]int{
				{1, 3}, {0, 2}, {1, 3}, {0, 2},
			},
			expect: true,
		},
	}
	Convey("TestIsBipartite", t, func() {
		for _, tt := range tests {
			So(isBipartite(tt.graph), ShouldEqual, tt.expect)
		}
	})
}
