package leetcode

import (
	"fmt"
)

//Given two integer arrays A and B, return the maximum length of an subarray that appears in both arrays.

//Example 1:

//Input:
//A: [1,2,3,2,1]
//B: [3,2,1,4,7]
//Output: 3
//Explanation:
//The repeated subarray with maximum length is [3, 2, 1].

//Note:
//1 <= len(A), len(B) <= 1000
//0 <= A[i], B[i] < 100

func FindLength(A []int, B []int) int {
	fmt.Println(A, B)
	size1, size2 := len(A), len(B)
	fmt.Println(size1, size2)
	dp := make([][]int, size1+1)
	for i := 0; i <= size1; i++ {
		dp[i] = make([]int, size2+1)
	}
	fmt.Println(dp)
	r := 0

	for i := size1 - 1; i >= 0; i-- {
		for j := size2 - 1; j >= 0; j-- {
			if A[i] == B[j] {
				dp[i][j] = dp[i+1][j+1] + 1
			}
			if dp[i][j] > r {
				r = dp[i][j]
			}
		}
	}
	fmt.Println(dp)
	return r
}
