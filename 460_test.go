package leetcode

import "testing"

func TestLFUCache(t *testing.T) {
	cache := Constructor1(3)
	cache.Put(2, 2)
	cache.Put(1, 1)

	t.Log("get(2)", cache.Get(2))
	t.Log("get(1)", cache.Get(1))
	t.Log("get(2)", cache.Get(2))

	cache.Put(3, 3)
	cache.Put(4, 4)

	t.Log("get(3)", cache.Get(3)) // -1
	t.Log("get(2)", cache.Get(2)) // 2
	t.Log("get(1)", cache.Get(1))
	t.Log("get(4)", cache.Get(4))
}

func TestLFUCache1(t *testing.T) {
	cache := Constructor1(2)
	cache.Put(1, 1)
	cache.Put(2, 2)

	t.Log("get(1)", cache.Get(1))

	cache.Put(3, 3)

	t.Log("get(2)", cache.Get(2))
	t.Log("get(3)", cache.Get(3))

	cache.Put(4, 4)

	t.Log("get(1)", cache.Get(1))
	t.Log("get(3)", cache.Get(3))
	t.Log("get(4)", cache.Get(4))
}
