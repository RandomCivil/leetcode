package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestFindClosestElements(t *testing.T) {
	tests := []struct {
		arr, expect []int
		k, x        int
	}{
		// {
		// 	arr:    []int{1, 2, 3, 4, 5},
		// 	k:      4,
		// 	x:      3,
		// 	expect: []int{1, 2, 3, 4},
		// },
		// {
		// 	arr:    []int{1, 2, 3, 4, 5},
		// 	k:      4,
		// 	x:      -1,
		// 	expect: []int{1, 2, 3, 4},
		// },
		{
			arr:    []int{0, 0, 1, 2, 3, 3, 4, 7, 7, 8},
			k:      3,
			x:      5,
			expect: []int{3, 3, 4},
		},
	}
	Convey("TestFindClosestElements", t, func() {
		for _, tt := range tests {
			So(findClosestElements(tt.arr, tt.k, tt.x), ShouldResemble, tt.expect)
		}
	})
}
