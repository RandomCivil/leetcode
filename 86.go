package leetcode

import (
	"gitlab.com/RandomCivil/leetcode/common"
)

//Given the head of a linked list and a value x, partition it such that all nodes less than x come before nodes greater than or equal to x.

//You should preserve the original relative order of the nodes in each of the two partitions.

//Example 1:

//Input: head = [1,4,3,2,5,2], x = 3
//Output: [1,2,2,4,3,5]

//Example 2:

//Input: head = [2,1], x = 2
//Output: [1,2]

//Constraints:

//The number of nodes in the list is in the range [0, 200].
//-100 <= Node.val <= 100
//-200 <= x <= 200
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func partition1(head *common.ListNode, x int) *common.ListNode {
	beforeRoot, afterRoot := new(common.ListNode), new(common.ListNode)
	before, after := beforeRoot, afterRoot
	cur := head
	for cur != nil {
		if cur.Val < x {
			before.Next = cur
			before = before.Next
		} else {
			after.Next = cur
			after = after.Next
		}
		cur = cur.Next
	}
	after.Next = nil
	before.Next = afterRoot.Next

	if beforeRoot.Next == nil {
		return afterRoot.Next
	}
	return beforeRoot.Next
}
