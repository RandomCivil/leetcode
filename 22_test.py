inst_22 = __import__("22")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "n,expected",
        [(3, ["((()))", "(()())", "(())()", "()(())", "()()()"]), (1, ["()"])],
    )
    def test_generateParenthesis(self, n, expected):
        assert inst_22.Solution().generateParenthesis(n) == expected
