# You are given a list of non-negative integers, a1, a2, ..., an, and a target, S. Now you have 2 symbols + and -. For each integer, you should choose one from + and - as its new symbol.
# Find out how many ways to assign symbols to make sum of integers equal to target S.
"""
Input: nums is [1, 1, 1, 1, 1], S is 3.
Output: 5
Explanation:

-1+1+1+1+1 = 3
+1-1+1+1+1 = 3
+1+1-1+1+1 = 3
+1+1+1-1+1 = 3
+1+1+1+1-1 = 3

There are 5 ways to assign symbols to make the sum of nums be target 3.
"""


class Solution:
    def findTargetSumWays(self, nums, S: int) -> int:
        print(nums, S)
        size = len(nums)
        path = []
        self.count = 0

        def dfs(i, s):
            if i == size:
                if s == S:
                    print('hehe', path)
                    self.count += 1
                return
            for sym in (1, -1):
                path.append(sym * nums[i])
                dfs(i + 1, s + sym * nums[i])
                path.pop()

        dfs(0, 0)
        return self.count


if __name__ == '__main__':
    # 5
    nums = [1, 1, 1, 1, 1]
    S = 3

    nums = [34,16,5,38,20,20,8,43,3,46,24,12,28,19,22,28,9,46,25,36]
    S = 0
    print(Solution().findTargetSumWays(nums, S))
