package leetcode

import (
	"math"
)

//Given an array of n integers nums, a 132 pattern is a subsequence of three integers nums[i], nums[j] and nums[k] such that i < j < k and nums[i] < nums[k] < nums[j].

//Return true if there is a 132 pattern in nums, otherwise, return false.

//Follow up: The O(n^2) is trivial, could you come up with the O(n logn) or the O(n) solution?

//Example 1:

//Input: nums = [1,2,3,4]
//Output: false
//Explanation: There is no 132 pattern in the sequence.

//Example 2:

//Input: nums = [3,1,4,2]
//Output: true
//Explanation: There is a 132 pattern in the sequence: [1, 4, 2].

//Example 3:

// Input: nums = [-1,3,2,0]
// Output: true
// Explanation: There are three 132 patterns in the sequence: [-1, 3, 2], [-1, 3, 0] and [-1, 2, 0].
func Find132pattern(nums []int) bool {
	var (
		size = len(nums)
		mn   int
	)
	for j := 1; j < size-1; j++ {
		mn = math.MaxInt32
		for i := 0; i < j; i++ {
			if nums[i] < nums[j] {
				if nums[i] < mn {
					mn = nums[i]
				}
			}
		}
		for k := j + 1; k < size; k++ {
			if mn < nums[k] && nums[k] < nums[j] {
				return true
			}
		}
	}
	return false
}
