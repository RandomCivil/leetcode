# Given a string s, return the longest palindromic substring in s.

# Example 1:

# Input: s = "babad"
# Output: "bab"
# Note: "aba" is also a valid answer.

# Example 2:

# Input: s = "cbbd"
# Output: "bb"

# Example 3:

# Input: s = "a"
# Output: "a"

# Example 4:

# Input: s = "ac"
# Output: "a"

# Constraints:

# 1 <= s.length <= 1000
# s consist of only digits and English letters (lower-case and/or upper-case),


class Solution:
    def longestPalindrome(self, s: str) -> str:
        size = len(s)
        dp = [[0] * size for _ in range(size)]
        mx = 1
        result = s[0]

        for i in range(size):
            dp[i][i] = 1

        # 偶数
        for i in range(size - 1):
            if s[i] == s[i + 1]:
                mx = 2
                result = s[i:i + 2]
                dp[i][i + 1] = 2
                dis = min(i, size - 1 - i)
                for j in range(1, dis + 1):
                    if i + 1 + j == size:
                        continue
                    if s[i - j] == s[i + 1 + j]:
                        dp[i - j][i + 1 + j] = max(dp[i - j][i + 1 + j],
                                                   dp[i - j + 1][i + j] + 2)

                        if dp[i - j][i + 1 + j] > mx:
                            mx = dp[i - j][i + 1 + j]
                            result = s[i - j:i + j + 2]

        print(dp)
        # 奇数
        for i in range(size):
            dp[i][i] = 1
            if dp[i][i] > mx:
                mx = 1
                result = s[i:i + 1]

            dis = min(i, size - 1 - i)
            for j in range(1, dis + 1):
                if s[i - j] == s[i + j]:
                    if i + j == size:
                        continue
                    dp[i - j][i + j] = max(dp[i - j][i + j],
                                           dp[i - j + 1][i + j - 1] + 2)
                    if dp[i - j][i + j] > mx:
                        mx = dp[i - j][i + j]
                        result = s[i - j:i + j + 1]

        print(dp)
        return result


if __name__ == '__main__':
    # 3
    s = "babad"

    # 2
    # s = "cbbd"

    # 1
    # s = "ac"
    print(Solution().longestPalindrome(s))
