# Given an integer num, return a string of its base 7 representation.

# Example 1:

# Input: num = 100
# Output: "202"
# Example 2:

# Input: num = -7
# Output: "-10"


# Constraints:


# -107 <= num <= 107
class Solution:
    def convertToBase7(self, num: int) -> str:
        if num == 0:
            return "0"
        result = []
        neg = False
        if num < 0:
            neg = True
            num = -num
        while num > 0:
            result.append(num % 7)
            num = int(num / 7)

        result = "".join(map(str, reversed(result)))
        if neg:
            return "-" + result
        return result
