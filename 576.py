# There is an m by n grid with a ball. Given the start coordinate (i,j) of the ball, you can move the ball to adjacent cell or cross the grid boundary in four directions (up, down, left, right). However, you can at most move N times. Find out the number of paths to move the ball out of grid boundary. The answer may be very large, return it after mod 109 + 7.
"""
Input: m = 2, n = 2, N = 2, i = 0, j = 0
Output: 6

Input: m = 1, n = 3, N = 3, i = 0, j = 1
Output: 12
"""
# Once you move the ball out of boundary, you cannot move it back.
# The length and height of the grid is in range [1,50].
# N is in range [0,50].
import math


class Solution:
    def findPaths(self, m: int, n: int, N: int, i: int, j: int) -> int:
        mem = {}
        mod = math.pow(10, 9) + 7

        def dfs(N, i, j):
            if i < 0 or j < 0 or i > m - 1 or j > n - 1:
                return 1
            if N == 0:
                mem[(N, i, j)] = 0
                return 0
            if (N, i, j) in mem:
                return mem[(N, i, j)]
            v = (dfs(N - 1, i + 1, j) + dfs(N - 1, i, j + 1) +
                 dfs(N - 1, i - 1, j) + dfs(N - 1, i, j - 1)) % mod
            mem[(N, i, j)] = v
            return v

        return dfs(N, i, j)


if __name__ == '__main__':
    m, n, N, i, j = 2, 2, 2, 0, 0
    m, n, N, i, j = 1, 3, 3, 0, 1
    # 914783380
    m, n, N, i, j = 8, 50, 23, 5, 26,
    # 390153306
    # m, n, N, i, j = 36, 5, 50, 15, 3
    print(Solution().findPaths(m, n, N, i, j))
