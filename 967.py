# Return all non-negative integers of length n such that the absolute difference between every two consecutive digits is k.

# Note that every number in the answer must not have leading zeros. For example, 01 has one leading zero and is invalid.

# You may return the answer in any order.

# Example 1:

# Input: n = 3, k = 7
# Output: [181,292,707,818,929]
# Explanation: Note that 070 is not a valid number, because it has leading zeroes.

# Example 2:

# Input: n = 2, k = 1
# Output: [10,12,21,23,32,34,43,45,54,56,65,67,76,78,87,89,98]

# Example 3:

# Input: n = 2, k = 0
# Output: [11,22,33,44,55,66,77,88,99]

# Example 4:

# Input: n = 2, k = 2
# Output: [13,20,24,31,35,42,46,53,57,64,68,75,79,86,97]

# Constraints:

# 2 <= n <= 9
# 0 <= k <= 9


class Solution:
    def numsSameConsecDiff(self, n: int, k: int) -> list[int]:
        self.result = []

        def dfs(cur):
            if cur[-1] < 0 or cur[-1] > 9:
                return
            if len(cur) == n:
                self.result.append(int("".join(map(str, cur))))
                return
            if k == 0:
                l = [0]
            else:
                l = [-k, k]
            for d in l:
                cur.append(cur[-1] + d)
                dfs(cur)
                cur.pop()

        for i in range(1, 10):
            dfs([i])
        return self.result


if __name__ == "__main__":
    # [181,292,707,818,929]
    n, k = 3, 7
    print(Solution().numsSameConsecDiff(n, k))
