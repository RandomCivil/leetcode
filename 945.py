# Given an array of integers nums, a move consists of choosing any nums[i], and incrementing it by 1.

# Return the least number of moves to make every value in nums unique.

# Example 1:

# Input: nums = [1,2,2]
# Output: 1
# Explanation:  After 1 move, the array could be [1, 2, 3].

# Example 2:

# Input: nums = [3,2,1,2,1,7]
# Output: 6
# Explanation:  After 6 moves, the array could be [3, 4, 1, 2, 5, 7].
# It can be shown with 5 or less moves that it is impossible for the array to have all unique values.

# Note:

# 0 <= nums.length <= 40000
# 0 <= nums[i] < 40000
from collections import Counter, deque


class Solution:
    def minIncrementForUnique(self, nums) -> int:
        r = 0
        c = Counter(nums)
        l = []
        for k, v in c.items():
            if v > 1:
                l.extend([k] * (v - 1))
        l = sorted(l)

        dup = deque(l)
        for v in range(40001):
            if len(dup) == 0:
                break
            if v not in c and dup[0] < v:
                r += (v - dup.popleft())
        return r


if __name__ == '__main__':
    # 6
    nums = [3, 2, 1, 2, 1, 7]

    # 1
    # nums=[1,2,2]
    print(Solution().minIncrementForUnique(nums))
