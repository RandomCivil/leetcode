# Given an array A of integers, return the number of (contiguous, non-empty) subarrays that have a sum divisible by K.

# Example 1:

# Input: A = [4,5,0,-2,-3,1], K = 5
# Output: 7
# Explanation: There are 7 subarrays with a sum divisible by K = 5:
# [4, 5, 0, -2, -3, 1], [5], [5, 0], [5, 0, -2, -3], [0], [0, -2, -3], [-2, -3]

# Note:

# 1 <= A.length <= 30000
# -10000 <= A[i] <= 10000
# 2 <= K <= 10000
from collections import defaultdict


class Solution:
    def subarraysDivByK(self, nums: list[int], k: int) -> int:
        d = defaultdict(int)
        d[0] = 1
        cur = 0
        result = 0
        for n in nums:
            cur = (cur + n) % k
            if cur in d:
                result += d[cur]
            d[cur] += 1
        return result


if __name__ == "__main__":
    A = [4, 5, 0, -2, -3, 1]
    K = 5
    print(Solution().subarraysDivByK(A, K))
