package leetcode

import "fmt"

//Given a binary array, find the maximum number of consecutive 1s in this array.

//Example 1:
//Input: [1,1,0,1,1,1]
//Output: 3
//Explanation: The first two digits or the last three digits are consecutive 1s.
//The maximum number of consecutive 1s is 3.
//Note:

//The input array will only contain 0 and 1.
//The length of input array is a positive integer and will not exceed 10,000

func FindMaxConsecutiveOnes(nums []int) int {
	fmt.Println(nums)
	size := len(nums)
	start, end := 0, 0
	r := 0
	var dis int
	for i, num := range nums {
		if num == 1 {
			dis = end - start + 1
			if dis > r {
				r = dis
			}
			end++
		} else {
			if i < size-1 {
				end, start = i+1, i+1
			}
		}
	}
	return r
}
