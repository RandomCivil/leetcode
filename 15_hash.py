# Given an integer array nums, return all the triplets [nums[i], nums[j], nums[k]] such that i != j, i != k, and j != k, and nums[i] + nums[j] + nums[k] == 0.

# Notice that the solution set must not contain duplicate triplets.

# Example 1:

# Input: nums = [-1,0,1,2,-1,-4]
# Output: [[-1,-1,2],[-1,0,1]]
# Explanation:
# nums[0] + nums[1] + nums[2] = (-1) + 0 + 1 = 0.
# nums[1] + nums[2] + nums[4] = 0 + 1 + (-1) = 0.
# nums[0] + nums[3] + nums[4] = (-1) + 2 + (-1) = 0.
# The distinct triplets are [-1,0,1] and [-1,-1,2].
# Notice that the order of the output and the order of the triplets does not matter.
# Example 2:

# Input: nums = [0,1,1]
# Output: []
# Explanation: The only possible triplet does not sum up to 0.
# Example 3:

# Input: nums = [0,0,0]
# Output: [[0,0,0]]
# Explanation: The only possible triplet sums up to 0.

# Constraints:

# 3 <= nums.length <= 3000
# -105 <= nums[i] <= 105

from collections import defaultdict


class Solution:
    def threeSum(self, nums):
        nums.sort()
        d = defaultdict(list)
        s = set()
        size = len(nums)
        for i in range(size):
            for j in range(i + 1, size):
                if (nums[i], nums[j]) in s:
                    continue
                d[nums[i] + nums[j]].append((i, j))
                s.add((nums[i], nums[j]))

        print(d)

        result = set()
        for i, n in enumerate(nums):
            if -n not in d:
                continue
            for a, b in d[-n]:
                if i != a and i != b:
                    # print('match',nums[a],nums[b],n)
                    result.add(tuple(sorted([nums[a], nums[b], n])))

        # print(result)
        return list(result)


if __name__ == "__main__":
    nums = [-1, 0, 1, 2, -1, -4]
    print(Solution().threeSum(nums))
