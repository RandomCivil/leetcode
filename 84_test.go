package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestLargestRectangleArea(t *testing.T) {
	tests := []struct {
		heights []int
		expect  int
	}{
		{
			heights: []int{2, 1, 5, 6, 2, 3},
			expect:  10,
		},
		{
			heights: []int{2, 4},
			expect:  4,
		},
		{
			heights: []int{3, 1},
			expect:  3,
		},
		{
			heights: []int{2, 1, 2},
			expect:  3,
		},
	}
	Convey("TestLargestRectangleArea", t, func() {
		for _, tt := range tests {
			So(largestRectangleArea(tt.heights), ShouldEqual, tt.expect)
		}
	})
}
