package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestGenerateMatrix(t *testing.T) {
	tests := []struct {
		n      int
		expect [][]int
	}{
		{
			n: 3,
			expect: [][]int{
				[]int{1, 2, 3},
				[]int{8, 9, 4},
				[]int{7, 6, 5},
			},
		},
	}
	Convey("TestGenerateMatrix", t, func() {
		for _, tt := range tests {
			So(generateMatrix(tt.n), ShouldResemble, tt.expect)
		}
	})
}
