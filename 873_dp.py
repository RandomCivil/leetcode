# A sequence X1, X2, ..., Xn is Fibonacci-like if:

# n >= 3
# Xi + Xi+1 = Xi+2 for all i + 2 <= n
# Given a strictly increasing array arr of positive integers forming a sequence, return the length of the longest Fibonacci-like subsequence of arr. If one does not exist, return 0.

# A subsequence is derived from another sequence arr by deleting any number of elements (including none) from arr, without changing the order of the remaining elements. For example, [3, 5, 8] is a subsequence of [3, 4, 5, 6, 7, 8].

# Example 1:

# Input: arr = [1,2,3,4,5,6,7,8]
# Output: 5
# Explanation: The longest subsequence that is fibonacci-like: [1,2,3,5,8].

# Example 2:

# Input: arr = [1,3,7,11,12,14,18]
# Output: 3
# Explanation: The longest subsequence that is fibonacci-like: [1,11,12], [3,11,14] or [7,11,18].
from collections import defaultdict


class Solution:
    def lenLongestFibSubseq(self, arr: list[int]) -> int:
        dp = {}
        result = 0
        size = len(arr)
        for i in range(1, size):
            for j in range(i):
                dp[(arr[j], arr[i])] = 2

        for i in range(size):
            for j in range(i - 1, -1, -1):
                if (arr[i] - arr[j], arr[j]) in dp:
                    dp[(arr[j], arr[i])] = max(
                        dp[(arr[j], arr[i])], dp[(arr[i] - arr[j], arr[j])] + 1
                    )
                    result = max(result, dp[arr[j], arr[i]])
        print(dp)
        return result


if __name__ == "__main__":
    # 5
    arr = [1, 2, 3, 4, 5, 6, 7, 8]

    # 3
    arr = [1, 3, 7, 11, 12, 14, 18]
    print(Solution().lenLongestFibSubseq(arr))
