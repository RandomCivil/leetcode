# We are given a binary tree (with root node root), a target node, and an integer value k.

# Return a list of the values of all nodes that have a distance k from the target node.  The answer can be returned in any order.

# Example 1:

# Input: root = [3,5,1,6,2,0,8,null,null,7,4], target = 5, k = 2
# Output: [7,4,1]

# Explanation:
# The nodes that are a distance 2 from the target node (with value 5)
# have values 7, 4, and 1.

# Note that the inputs "root" and "target" are actually TreeNodes.
# The descriptions of the inputs above are just serializations of these objects.

# Note:


# The given tree is non-empty.
# Each node in the tree has unique values 0 <= node.val <= 500.
# The target node is a node in the tree.
# 0 <= k <= 1000.
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


from collections import deque


class Solution:
    def distanceK(self, root: TreeNode, target: TreeNode, k: int) -> list[int]:
        self.q = deque()
        d = {}

        def dfs(cur, parent):
            if not cur:
                return
            if cur.val == target.val:
                self.q.append((cur, 0))
            d[cur] = parent
            dfs(cur.left, cur)
            dfs(cur.right, cur)

        dfs(root, None)
        result = []
        visited = set()
        while len(self.q) > 0:
            cur, n = self.q.popleft()
            if cur.val in visited:
                continue
            visited.add(cur.val)
            if n == k:
                result.append(cur.val)
                continue
            if cur.left:
                self.q.append((cur.left, n + 1))
            if cur.right:
                self.q.append((cur.right, n + 1))
            if d[cur] is not None:
                self.q.append((d[cur], n + 1))
        return result
