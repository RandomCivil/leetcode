# Given a string s and a dictionary of strings wordDict, return true if s can be segmented into a space-separated sequence of one or more dictionary words.

# Note that the same word in the dictionary may be reused multiple times in the segmentation.

# Example 1:

# Input: s = "leetcode", wordDict = ["leet","code"]
# Output: true
# Explanation: Return true because "leetcode" can be segmented as "leet code".

# Example 2:

# Input: s = "applepenapple", wordDict = ["apple","pen"]
# Output: true
# Explanation: Return true because "applepenapple" can be segmented as "apple pen apple".
# Note that you are allowed to reuse a dictionary word.

# Example 3:

# Input: s = "catsandog", wordDict = ["cats","dog","sand","and","cat"]
# Output: false

# Constraints:


# 1 <= s.length <= 300
# 1 <= wordDict.length <= 1000
# 1 <= wordDict[i].length <= 20
# s and wordDict[i] consist of only lowercase English letters.
# All the strings of wordDict are unique.
class Solution:
    def wordBreak(self, s: str, wordDict) -> bool:
        size = len(s)
        dp = [False] * size

        for i in range(size):
            for w in wordDict:
                if i - len(w) + 1 == 0:
                    if s[:i + 1] == w:
                        dp[i] = True
                else:
                    print(i, w, s[i - len(w) + 1:i + 1])
                    if s[i - len(w) + 1:i + 1] == w:
                        dp[i] = dp[i] or dp[i - len(w)]

        print(dp)
        return dp[-1]


if __name__ == '__main__':
    s = "leetcode"
    wordDict = ["leet", "code"]

    # s = "applepenapple"
    # wordDict = ["apple", "pen"]

    # s = "catsandog"
    # wordDict = ["cats", "dog", "sand", "and", "cat"]

    # s = "dogs"
    # wordDict = ["dog", "s", "gs"]
    print(Solution().wordBreak(s, wordDict))
