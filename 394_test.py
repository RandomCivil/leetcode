import pytest

inst_394 = __import__("394")


class TestClass:
    @pytest.mark.parametrize(
        "s,expected",
        [
            ("3[a]2[bc]", "aaabcbc"),
            ("3[a2[c]]", "accaccacc"),
            ("2[abc]3[cd]ef", "abcabccdcdcdef"),
            ("2[abc]3[cd]ef", "abcabccdcdcdef"),
        ],
    )
    def test_decodeString(self, s, expected):
        assert inst_394.Solution().decodeString(s) == expected
