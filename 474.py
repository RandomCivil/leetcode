# Given an array, strs, with strings consisting of only 0s and 1s. Also two integers m and n.
# Now your task is to find the maximum number of strings that you can form with given m 0s and n 1s. Each 0 and 1 can be used at most once.
"""
Input: strs = ["10","0001","111001","1","0"], m = 5, n = 3
Output: 4
Explanation: This are totally 4 strings can be formed by the using of 5 0s and 3 1s, which are "10","0001","1","0".

Input: strs = ["10","0","1"], m = 1, n = 1
Output: 2
Explanation: You could form "10", but then you'd have nothing left. Better form "0" and "1".
"""
from collections import Counter


class Solution:
    def findMaxForm(self, strs, m: int, n: int) -> int:
        print(strs, m, n)
        size = len(strs)
        dp = [[[0] * (n + 1) for _ in range(m + 1)] for _ in range(size + 1)]
        l = []
        print(l)
        for k in range(1, size + 1):
            l.append(Counter(strs[k - 1]))
            for i in range(m + 1):
                for j in range(n + 1):
                    if i - l[k - 1]["0"] >= 0 and j - l[k - 1]["1"] >= 0:
                        dp[k][i][j] = max(
                            dp[k - 1][i - l[k - 1]["0"]][j - l[k - 1]["1"]] + 1,
                            dp[k - 1][i][j],
                        )
                    else:
                        dp[k][i][j] = dp[k - 1][i][j]

        print(dp)
        return dp[-1][-1][-1]
