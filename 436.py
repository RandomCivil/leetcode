#! /usr/bin/env python3

# Given a set of intervals, for each of the interval i, check if there exists an interval j whose start point is bigger than or equal to the end point of the interval i, which can be called that j is on the "right" of i.
# For any interval i, you need to store the minimum interval j's index, which means that the interval j has the minimum start point to build the "right" relationship for interval i. If the interval j doesn't exist, store -1 for the interval i. Finally, you need output the stored value of each interval as an array.

# You may assume the interval's end point is always bigger than its start point.
# You may assume none of these intervals have the same start point.
"""
Input: [ [1,2] ]
Output: [-1]
Explanation: There is only one interval in the collection, so it outputs -1.

Input: [ [3,4], [2,3], [1,2] ]
Output: [-1, 0, 1]
Explanation: There is no satisfied "right" interval for [3,4].
For [2,3], the interval [3,4] has minimum-"right" start point;
For [1,2], the interval [2,3] has minimum-"right" start point.

Input: [ [1,4], [2,3], [3,4] ]
Output: [-1, 2, -1]
Explanation: There is no satisfied "right" interval for [1,4] and [3,4].
For [2,3], the interval [3,4] has minimum-"right" start point.
"""

import bisect


class Solution:
    def findRightInterval(self, intervals):
        result = [-1] * len(intervals)
        size = len(intervals)
        l = [(start, i) for i, (start, _) in enumerate(intervals)]
        sorted_intervals = sorted(l, key=lambda x: x[0])
        print(sorted_intervals)

        for i, (_, end) in enumerate(intervals):
            index = bisect.bisect_left(sorted_intervals, (end, float("-inf")))
            if index == size:
                result[i] = -1
            else:
                result[i] = sorted_intervals[index][1]
        return result


if __name__ == "__main__":
    intervals = [(1, 4), (2, 3), (3, 4)]
    # intervals = [(1, 4), (2, 3), (3, 4)]
    intervals = [[3, 4], [2, 3], [1, 2]]
    r = Solution().findRightInterval(intervals)
    print(r)
