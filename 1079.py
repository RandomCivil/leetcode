# You have n  tiles, where each tile has one letter tiles[i] printed on it.

# Return the number of possible non-empty sequences of letters you can make using the letters printed on those tiles.

# Example 1:

# Input: tiles = "AAB"
# Output: 8
# Explanation: The possible sequences are "A", "B", "AA", "AB", "BA", "AAB", "ABA", "BAA".

# Example 2:

# Input: tiles = "AAABBC"
# Output: 188

# Example 3:

# Input: tiles = "V"
# Output: 1

# Constraints:

# 1 <= tiles.length <= 7
# tiles consists of uppercase English letters.
from collections import Counter


class Solution:
    def numTilePossibilities(self, tiles: str) -> int:
        print(tiles)
        self.r = 0
        size = len(tiles)
        c = Counter(tiles)
        print(c)
        nxt = list(c.keys())

        def dfs(cur, n):
            if len(cur) == n:
                print(cur, n)
                self.r += 1
                return
            for ch in nxt:
                if c[ch] == 0:
                    continue
                c[ch] -= 1
                dfs(cur + ch, n)
                c[ch] += 1

        for i in range(1, size + 1):
            dfs('', i)
        return self.r


if __name__ == '__main__':
    tiles = "AAB"
    # tiles = "AAABBC"
    print(Solution().numTilePossibilities(tiles))
