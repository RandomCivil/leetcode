import pytest


inst_1202 = __import__("1202")


class TestClass:
    @pytest.mark.parametrize(
        "s, pairs, expected",
        [
            ("dcab", [[0, 3], [1, 2]], "bacd"),
            ("dcab", [[0, 3], [1, 2], [0, 2]], "abcd"),
            ("cba", [[0, 1], [1, 2]], "abc"),
        ],
    )
    def test_smallestStringWithSwaps(self, s, pairs, expected):
        assert inst_1202.Solution().smallestStringWithSwaps(s, pairs) == expected
