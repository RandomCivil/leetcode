import pytest


inst_1996 = __import__("1996")
inst_1996_me = __import__("1996_me")


class TestClass:
    @pytest.mark.parametrize(
        "properties, expected",
        [
            ([[1, 5], [1, 7], [10, 8], [4, 6]], 3),
            ([[1, 5], [1, 7], [10, 8], [4, 6], [1, 5]], 4),
            ([[5, 5], [6, 3], [3, 6]], 0),
            ([[2, 2], [3, 3]], 1),
            ([[1, 5], [10, 4], [4, 3]], 1),
            ([[1, 1], [2, 1], [2, 2], [1, 2]], 1),
            ([[7, 9], [10, 7], [6, 9], [10, 4], [7, 5], [7, 10]], 2),
        ],
    )
    def test_numberOfWeakCharacters(self, properties, expected):
        assert inst_1996_me.Solution().numberOfWeakCharacters(properties) == expected
        assert inst_1996.Solution().numberOfWeakCharacters(properties) == expected
