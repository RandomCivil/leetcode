package leetcode

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/RandomCivil/leetcode/common"
)

func TestReverseKGroup(t *testing.T) {
	tests := []struct {
		nums, expect []int
		k            int
	}{
		{
			nums:   []int{1, 2, 3, 4, 5},
			k:      2,
			expect: []int{2, 1, 4, 3, 5},
		},
		{
			nums:   []int{1, 2, 3, 4, 5},
			k:      3,
			expect: []int{3, 2, 1, 4, 5},
		},
		{
			nums:   []int{1},
			k:      1,
			expect: []int{1},
		},
		{
			nums:   []int{1, 2, 3},
			k:      1,
			expect: []int{1, 2, 3},
		},
	}
	Convey("TestReverseKGroup", t, func() {
		for _, tt := range tests {
			l := &common.LinkList{Nums: tt.nums}
			root := l.Build()
			var result []int
			head := reverseKGroup(root, tt.k)
			for head != nil {
				result = append(result, head.Val)
				head = head.Next
			}
			So(result, ShouldResemble, tt.expect)
		}
	})
}
