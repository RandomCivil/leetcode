# Given a string s formed by digits ('0' - '9') and '#' . We want to map s to English lowercase characters as follows:

# Characters ('a' to 'i') are represented by ('1' to '9') respectively.
# Characters ('j' to 'z') are represented by ('10#' to '26#') respectively.
# Return the string formed after mapping.

# It's guaranteed that a unique mapping will always exist.

# Example 1:

# Input: s = "10#11#12"
# Output: "jkab"
# Explanation: "j" -> "10#" , "k" -> "11#" , "a" -> "1" , "b" -> "2".

# Example 2:

# Input: s = "1326#"
# Output: "acz"

# Example 3:

# Input: s = "25#"
# Output: "y"

# Example 4:

# Input: s = "12345678910#11#12#13#14#15#16#17#18#19#20#21#22#23#24#25#26#"
# Output: "abcdefghijklmnopqrstuvwxyz"

# Constraints:

# 1 <= s.length <= 1000
# s[i] only contains digits letters ('0'-'9') and '#' letter.
# s will be valid string such that mapping is always possible.


class Solution:
    def freqAlphabets(self, s: str) -> str:
        r = []
        size = len(s)
        i = size - 1
        while i >= 0:
            if s[i] != '#':
                r.append(chr(96 + int(s[i])))
                i -= 1
            else:
                i -= 3
                ss = s[i + 1:i + 3]
                r.append(chr(96 + int(ss)))

        return ''.join(list(reversed(r)))


if __name__ == '__main__':
    s = "10#11#12"
    print(Solution().freqAlphabets(s))
