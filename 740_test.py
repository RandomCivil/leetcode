inst_740 = __import__("740")

import pytest


class TestClass:
    @pytest.mark.parametrize(
        "nums,expected",
        [
            ([3, 4, 2], 6),
            ([2, 2, 3, 3, 3, 4], 9),
            ([1], 1),
            ([1, 2], 2),
        ],
    )
    def test_deleteAndEarn(self, nums, expected):
        assert inst_740.Solution().deleteAndEarn(nums) == expected
